$(document).ready(function(){

 // team_prof = $('#team_professions').html();


  $('#add_team_member').on('click',function(){
    i = $('#team-wrap').find('.team-item:last-child').data('index');
    addTeamMemberFields(++i);
  });

  $('#add_timeline_item').on('click',function(){
    i = $('#timeline-wrap').find('.timeline-item:last-child').data('index');
    addTimeLineFields(++i);
  });

  $('#add_rating_item').on('click',function(){
    i = $('#rating-wrap').find('.rating-item:last-child').data('index');
    addRatingFields(++i);
  });

  $('#add_agency_rating_item').on('click',function(){
    i = $('#agency-rating-wrap').find('.agency-rating-item:last-child').data('index');
    addAgencyRatingFields(++i);
  });


  $('#add_social_item').on('click',function(){
    i = $('#social-wrap').find('.social-item:last-child').data('index');
    addSocialFields(++i);
  });

  $('#add_link_item').on('click',function(){
    i = $('#links-wrap').find('.links-item:last-child').data('index');
    addLinkFields(++i);
  });

  $('#add_bonus_item').on('click',function(){
    i = $('#bonuses-wrap').find('.bonuses-item:last-child').data('index');
    addBonusesFields(++i);
  });

  $('#add_block_item').on('click',function(){
    i = $('#block-wrap').find('.block-item:last-child').data('index');
    addBlocksFields(++i);
  });

  $('#add_project_filter_item').on('click',function(){
    i = $('#project-filter-wrap').find('.project-filter-item:last-child').data('index');
    addProjectFilterItem(++i);
  });

  $('#add_project_filter_value_item').on('click',function(){
    i = $('#project-filter-values-wrap').find('.project-filter-value-item:last-child').data('index');
    addProjectFilterValueItem(++i,$(this).data('type'));
  });

  $('.delete-project-filter-value').on('click',function(){
    $(this).parent().parent().remove();
  });

  $('.delete-project-filter-value-set').on('click',function(){
    $(this).parent().parent().parent().remove();
  });


  $('.remove_item').on('click',function(){
    if($('.'+$(this).data('remove')).length > 1){
      $('.'+$(this).data('remove')+':last-child').remove();
    }
  });

  if($('a[data-uploader]').length){
    $('a[data-uploader]').each(function(){
      $(this).filemanager('image');
    });
  }


  if($('textarea[data-editor]').length){
    $('textarea[data-editor]').each(function(){
      $(this).ckeditor({
        filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
        filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token='+$('meta[name="csrf-token"]').attr('content'),
        filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
        filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='+$('meta[name="csrf-token"]').attr('content')
      });
    });
  }

});


function addLinkFields(i){
  $('#links-wrap').append('<div class="links-item row" data-index="'+i+'"><div class="col-lg-4"><div class="form-group"><h5>Название РУ</h5><div class="controls"><input type="text" name="links['+i+'][name_ru]" class="form-control" value=""></div></div></div><div class="col-lg-4"><div class="form-group"><h5>Название EN</h5><div class="controls"><input type="text" name="links['+i+'][name_en]" class="form-control" value=""></div></div></div><div class="col-lg-4"><div class="form-group"><h5>Ссылка</h5><div class="controls"><input type="text" name="links['+i+'][link]" class="form-control" value=""></div></div></div></div>');
}

function addBonusesFields(i){
  $('#bonuses-wrap').append('<div class="bonuses-item row" data-index="'+i+'"><div class="col-lg-4"><div class="form-group"><h5>Этап</h5><div class="controls"><select name="bonuses['+i+'][stage]" class="form-control" id="stage_list"><option value="Private sale">Private sale</option><option value="Pre-sale" >Pre-sale</option><option value="Sale">Sale</option><option value="Other">Other</option></select></div></div></div><div class="col-lg-8"><div class="form-group"><h5>Описание бонуса</h5><div class="controls"><input type="text" name="bonuses['+i+'][bonus]" class="form-control" value=""></div></div></div></div>');
}

function addTeamMemberFields(i){
  $('#team-wrap').append('<div class="team-item row" data-index="'+i+'"><div class="col-lg-2"><div class="form-group"><h5>Имя РУ</h5><div class="controls"><input type="text" name="team['+i+'][name_ru]" class="form-control"  value=""></div></div></div><div class="col-lg-2"><div class="form-group"><h5>Имя EN</h5><div class="controls"><input type="text" name="team['+i+'][name_en]" class="form-control"  value=""></div></div></div><div class="col-lg-2"><div class="form-group"><h5>Логотип</h5><div class="controls"><div class="input-group"><span class="input-group-btn"><a data-input="team_thumbnail'+i+'" data-preview="team_holder'+i+'" class="btn btn-info text-white" data-uploader="true"><i class="fa fa-picture-o"></i> Выбрать</a></span><input id="team_thumbnail'+i+'" class="form-control" type="text" name="team['+i+'][avatar]"></div></div><div id="team_holder'+i+'" class="holder"></div></div></div><div class="col-lg-2"><div class="form-group"><h5>Профессия РУ</h5><div class="controls"><input type="text" name="team['+i+'][profession_ru]" class="form-control"  value=""></div></div></div><div class="col-lg-2"><div class="form-group"><h5>Профессия EN</h5><div class="controls"><input type="text" name="team['+i+'][profession_en]" class="form-control"  value=""></div></div></div><div class="col-lg-2"><div class="form-group"><h5>Вкладка</h5><div class="controls"><select name="team['+i+'][group_id]" class="form-control">'+$('#team_group').html()+'</select></div></div></div></div>');
  $('a[data-uploader]').each(function(){
    $(this).filemanager('image');
  });
  $('select[name="team['+i+'][profession_id]"]').prop('selectedIndex', 0);
}

function addRatingFields(i){
  $('#rating-wrap').append('<div class="rating-item row" data-index="'+i+'"><div class="col-lg-6"><div class="form-group"><h5>Имя</h5><div class="controls"><select name="custom_rating['+i+'][custom_rating_id]" class="form-control">'+$('#cutsom_ratings').html()+'</select></div></div></div><div class="col-lg-6"><div class="form-group"><h5>Рейтинг</h5><div class="controls"><input type="number" name="custom_rating['+i+'][rating]" class="form-control" value=""></div></div></div></div>');
}

function addAgencyRatingFields(i){
  $('#agency-rating-wrap').append('<div class="agency-rating-item row" data-index="'+i+'"><div class="col-lg-6"><div class="form-group"><h5>Имя</h5><div class="controls"><select name="agency_rating['+i+'][agency_rating_id]" class="form-control">'+$('#agency_ratings').html()+'</select></div></div></div><div class="col-lg-6"><div class="form-group"><h5>Рейтинг</h5><div class="controls"><input type="text" name="agency_rating['+i+'][rating]" class="form-control" value=""></div></div></div></div>');
}

function addSocialFields(i){
  $('#social-wrap').append('<div class="social-item row" data-index="'+i+'"><div class="col-lg-6"><div class="form-group"><h5>Имя</h5><div class="controls"><select name="social['+i+'][social_id]" class="form-control">'+$('#social_list').html()+'</select></div></div></div><div class="col-lg-6"><div class="form-group"><h5>Ссылка</h5><div class="controls"><input type="text" name="social['+i+'][link]" class="form-control" value=""></div></div></div></div>');
}

function addTimeLineFields(i){
  var timeline='<div class="timeline-item row" data-index="'+i+'">';
  timeline=timeline+'<div class="col-lg-2"><div class="form-group"><h5>Этап</h5><div class="controls"><select name="timeline['+i+'][status_id]" class="form-control">'+$('#status_list').html()+'</select></div></div></div>';
  timeline=timeline+'<div class="col-lg-1"><div class="form-group"><h5>Подэтап</h5><div class="controls"><input type="text" name="timeline['+i+'][substatus]" class="form-control" value=""></div></div></div>';
  timeline=timeline+'<div class="col-lg-2"><div class="form-group"><h5>Начало</h5><div class="controls"><input type="date" name="timeline['+i+'][start]" class="form-control" value=""></div></div></div>';
  timeline=timeline+'<div class="col-lg-2"><div class="form-group"><h5>Конец</h5><div class="controls"><input type="date" name="timeline['+i+'][end]" class="form-control" value=""></div></div></div>';
  timeline=timeline+'<div class="col-lg-1"><div class="form-group"><h5>Час пояс </h5><div class="controls"><select name="timeline['+i+'][gmt]" class="form-control">'+$('#gmt_list').html()+'</select></div></div></div>';
  timeline=timeline+'<div class="col-lg-1"><div class="form-group"><h5>Цена</h5><div class="controls"><input type="number" step="any" name="timeline['+i+'][price]" class="form-control" value=""></div></div></div>';
  timeline=timeline+'<div class="col-lg-1"><div class="form-group"><h5>Валюта</h5><div class="controls"><select name="timeline['+i+'][price_currency_id]" class="form-control">'+$('#currency_list').html()+'</select></div></div></div>';
  timeline=timeline+'<div class="col-lg-1"><div class="form-group"><h5>Мин. взнос</h5><div class="controls"><input type="number" name="timeline['+i+'][min_invest]" class="form-control" step="any" value=""></div></div></div>';
  timeline=timeline+'<div class="col-lg-1"><div class="form-group"><h5>Валюта</h5><div class="controls"><select name="timeline['+i+'][min_invest_currency_id]" class="form-control">'+$('#currency_list').html()+'</select></div></div></div>';
  timeline=timeline+'</div>';

  $('#timeline-wrap').append(timeline);
  $('select[name="timeline['+i+'][price_currency_id]"]').prop('selectedIndex', 0);
  $('select[name="timeline['+i+'][min_invest_currency_id]"]').prop('selectedIndex', 0);
}

function addProjectFilterItem(i){
  var project_item='<div class="project-filter-item row" data-index="'+i+'">';
  project_item=project_item+'<input type="hidden" name="project_values['+i+'][id]" value="">';
  project_item=project_item+'<div class="col-lg-5"><div class="form-group"><h5>Название РУ</h5><div class="controls"><input type="text" name="project_values['+i+'][name_ru]" class="form-control" value="" required></div></div></div>';
  project_item=project_item+'<div class="col-lg-5"><div class="form-group"><h5>Название EN</h5><div class="controls"><input type="text" name="project_values['+i+'][name_en]" class="form-control" value="" required></div></div></div>';
  project_item=project_item+'<div class="col-lg-1"><div class="form-group"><h5>Удаление</h5><p class="btn btn-danger delete-project-filter-value" >Удалить элемент</p></div></div>';
  project_item=project_item+'</div>';
  $('#project-filter-wrap').append(project_item);
}

function addProjectFilterValueItem(i,type ){
  var project_value_item='<div class="project-filter-value-item row" data-index="'+i+'">';
  project_value_item=project_value_item+'<div class="col-lg-5"><div class="form-group"><h5>Параметр</h5><div class="controls"><select data-type="'+type+'" onchange="onChangeProjectFilter(this);" id="project_filter_id'+i+'" name="project_filter_value_sets['+i+'][project_filter_id]" class="form-control">'+$('#project_filters').html()+'</select></div></div></div>';
  project_value_item=project_value_item+'<div class="col-lg-5"><div class="form-group"><h5>Значение</h5><div class="controls"><select name="project_filter_value_sets['+i+'][project_filter_value_id]" class="form-control" id="project_filter_values'+i+'"><option value=""></option></select></div></div></div>';
  project_value_item=project_value_item+'<div class="col-lg-1"><div class="form-group"><h5>Удаление</h5><p class="btn btn-danger delete-project-filter-value-set" >Удалить элемент</p></div></div>';
  project_value_item=project_value_item+'</div>';
  $('#project-filter-values-wrap').append(project_value_item);
  $('.delete-project-filter-value-set').on('click',function(){
    $(this).parent().parent().parent().remove();
  });

  $('#project_filter_id'+i).prop('selectedIndex', 0);
}

$('.project_filters_select').on('change',function(){
  var index=$(this).parent().parent().parent().parent().data('index');
  var list = $('#project_filter_values'+index);
  var project_filter_id=$(this).val();
  $.ajax({
    cache: false,
    data: $(this).data('type')+'=' + project_filter_id,
    dataType: 'html',
    error: function () {
     alert('error');
    },
    success: function (data) {
     list.html(data);
    },
    type: 'GET',
    url: '/invi/ajax'
  });
  
});

function onChangeProjectFilter(el) {
  var myEl = $(el);
  var index=myEl.parent().parent().parent().parent().data('index');
  var list = $('#project_filter_values'+index);
  var project_filter_id=myEl.val();
  $.ajax({
    cache: false,
    data: myEl.data('type')+'=' + project_filter_id,
    dataType: 'html',
    error: function () {
      alert('error');
    },
    success: function (data) {
      list.html(data);
    },
    type: 'GET',
    url: '/invi/ajax'
  });
}


function addBlocksFields(i){
  var block='<div class="block-item row" data-index="'+i+'">';
  block=block+'<div class="col-lg-2"><div class="form-group"><h5>Порядок </h5><div class="controls"><input type="text" name="blocks['+i+'][priority]" class="form-control"></div></div></div>';
  block=block+'<div class="col-lg-5"><div class="form-group"><h5>Название РУ </h5><div class="controls"><input type="text" name="blocks['+i+'][caption_ru]" class="form-control"></div></div></div>';
  block=block+'<div class="col-lg-5"><div class="form-group"><h5>Название EN </h5><div class="controls"><input type="text" name="blocks['+i+'][caption_en]" class="form-control"></div></div></div>';
  block=block+'<div class="col-lg-2"></div>';
  block=block+'<div class="col-lg-5"><div class="form-group"><h5>Текст РУ </h5><div class="controls"><input type="text" name="blocks['+i+'][content_ru]" class="form-control"></div></div></div>';
  block=block+'<div class="col-lg-5"><div class="form-group"><h5>Текст EN </h5><div class="controls"><input type="text" name="blocks['+i+'][content_en]" class="form-control"></div></div></div>';
     block=block+'</div>';

  $('#block-wrap').append(block);
}

function addLinksFields(i){
  $('#social-wrap').append('<div class="social-item row" data-index="'+i+'"><div class="col-lg-6"><div class="form-group"><h5>Имя</h5><div class="controls"><select name="social['+i+'][social_id]" class="form-control">'+$('#social_list').html()+'</select></div></div></div><div class="col-lg-6"><div class="form-group"><h5>Ссылка</h5><div class="controls"><input type="text" name="social['+i+'][link]" class="form-control" value=""></div></div></div></div>');
}

