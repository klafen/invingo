(function($) {
  /* slider on home init */
  /* slickSlider */
  if ($('#home-slider').length){
    $('#home-slider').slick({
      centerMode: true,
      centerPadding: 0,
      arrows: true,
      nextArrow: '<span class="slick-arrow ar-right"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="assets/img/sprite.svg#arrow-right"></use></svg></span>',
      prevArrow: '<span class="slick-arrow ar-left"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="assets/img/sprite.svg#arrow-left"></use></svg></span>',
      dots: true,
      dotsClass: 'number-page',
      customPaging: function (slider, i) {
        return ('0'+(i+1)).slice(-2);
      }
    });
  }

  /* calendar init */
  /* jQueryUI */
  var dateFormat = "yy-mm-dd",
  from = $( "#from" )
    .datepicker({
      showOtherMonths: true,
      dateFormat: 'yy-mm-dd',
      })
    .on( "change", function() {
      to.datepicker( "option", "minDate", getDate( this ) );
    }),
  to = $( "#to" ).datepicker({
    showOtherMonths: true,
    dateFormat: 'yy-mm-dd',
  })

  .on( "change", function() {
    from.datepicker( "option", "maxDate", getDate( this ) );
  });

  function getDate( element ) {
    var date;
    try {
      date = $.datepicker.parseDate( dateFormat, element.value );
    } catch( error ) {
      date = null;
    }
    return date;
  }

  /* slider range init */
  /* jQueryUI
  if ( $('.custom-range').length || $('#range-slider').length ) {
    $("#range-slider").slider({
      range: true,
      min: 0,
      max: 100,
      values: [20, 80],
      slide: function (event, ui) {
        $("#range-start").val(ui.values[0]);
        $("#range-end").val(ui.values[1]);
      }
    });
    $("#range-start").val($("#range-slider").slider("values", 0));
    $("#range-end").val($("#range-slider").slider("values", 1));
  }*/

  /* tooltip init */
  $('[data-toggle="tooltip"]').tooltip();


  /* search init */
  var openCtrl = document.getElementById('btn-search'),
    closeCtrl = document.getElementById('btn-search-close'),
    searchContainer = document.querySelector('.search'),
    inputSearch = searchContainer.querySelector('.search__input');

  function init() {
    initEvents();
  }

  function initEvents() {
    openCtrl.addEventListener('click', openSearch);
    closeCtrl.addEventListener('click', closeSearch);
    document.addEventListener('keyup', function (ev) {
      // escape key.
      if (ev.keyCode == 27) {
        closeSearch();
      }
    });
  }

  function openSearch() {
    searchContainer.classList.add('search--open');
    inputSearch.focus();
  }

  function closeSearch() {
    searchContainer.classList.remove('search--open');
    inputSearch.blur();
    inputSearch.value = '';
  }
  init();

  $(".hamburger").on('click', function(){
    $(this).toggleClass('is-active');
    $('header .nav').toggleClass('mobile-menu');
  });

  $('#search-icon').on('click', function(){
    $('#ico-form .search__input').toggleClass('active');
  });


  function open_filter() {
    $('.filter__header').on('click', function(){
      $(this).toggleClass('open');
      if ($window.width() < 768) {
        $apply_filter = $('#apply_filter');
        $btn_filter_block=$('#btn_filter');
        $apply_filter.addClass('fixed-apply-button');
        $apply_filter.css('width',$btn_filter_block.innerWidth()+'px');
      }
      else {
        $apply_filter.removeClass('fixed-apply-button');
      }
    });
  }
  open_filter();


  $('.ar-right').on("click", function(e){
    e.preventDefault();
    $('.table-scroller').animate( { scrollLeft: '+=70' }, 500);
  });

  $('.ar-left').on("click", function(e){
    e.preventDefault();
    $('.table-scroller').animate( { scrollLeft: '-=70' }, 500);
  });

  // $('.card-header').on('click', function() {
  //   $(this).find('.plus-minus-toggle').toggleClass('collapsed');
  // });
  
	$('[data-toggle="popover"]').popover({
		placement: 'right',
		trigger: 'focus',
		template: '<div class="popover" role="tooltip"><div class="arrow"></div><div class="popover-body"></div></div>',	
	});
	
	$(window).scroll(function () {
		var sc = $(window).scrollTop()
		var wd = $(window).width()
		if (sc > 20) {
			$("header > div").addClass("menu-fixed")
		} else {
			$("header > div").removeClass("menu-fixed")
		}
	});

})(jQuery);

//search
var search_input = $('#search__input');
search_input.on('input',(function() {
  s=search_input.val();   
  if (s !='') {
    $.ajax({
      cache: false,
      data: 'mode=ajax&s=' + search_input.val(),
      dataType: 'html',
      error: function () {
       
      },
      success: function (data) {
        $('#results_ajax').html(data);
      },
      type: 'GET',
      url: '/search_ajax'
    });
  }
  else {
    $('#results_ajax').html('');
  }
}));
