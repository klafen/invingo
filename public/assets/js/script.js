$(document).ready(function(){
  $.getJSON('https://min-api.cryptocompare.com/data/pricemultifull?fsyms=BTC&tsyms=USD', function(data) {
    $('#btc-curs').html(data.RAW.BTC.USD.PRICE.toFixed(0));
    $('#btc-percent').html(data.RAW.BTC.USD.CHANGEPCT24HOUR.toFixed(1));
  });
  $.getJSON('https://min-api.cryptocompare.com/data/pricemultifull?fsyms=ETH&tsyms=USD', function(data) {
    $('#eth-curs').html(data.RAW.ETH.USD.PRICE.toFixed(0));
    $('#eth-percent').html(data.RAW.ETH.USD.CHANGEPCT24HOUR.toFixed(1));
  });


  if($('.adverti').length){
    $('.btn__card').on('click',function(){
      $('#modal-form').find('input[name="contract_name"]').val($(this).data('contract-name'));
      $('#modal-form').find('.modal-form-title').html($(this).data('contract-name'));
      $('#modal-form').modal();
    });
    
  }



});
