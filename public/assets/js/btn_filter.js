$(document).ready(function($) {
  $window = $(window);
  $window.resize(function() {
    $apply_filter = $('#apply_filter');
    $btn_filter_block=$('#btn_filter');
    $('.select-checkboxes').css('width',$btn_filter_block.innerWidth()+'px')
    if ($window.width() < 768) {
      $apply_filter.addClass('fixed-apply-button');
      $window.scroll(function() {
        $apply_filter.css('width',$btn_filter_block.innerWidth()+'px');
        $h = $btn_filter_block.offset().top - $window.scrollTop()-$window.height();
        if ($h>0 && $window.width() < 768) {
          $apply_filter.addClass('fixed-apply-button');
        } else {
          $apply_filter.removeClass('fixed-apply-button');
        }
      });
    }
    else {
      $apply_filter.removeClass('fixed-apply-button');
    }
  });
  $window.resize();
});

$('.selectBox').on('click', function(e) {
  var selector=$(this).data('expand');
  var checkboxes = $('#'+selector);
  if (checkboxes.data('expanded')) {
    checkboxes.hide();
    checkboxes.data('expanded',false);
  }
  else {
    hideAllCheckboxes();
    checkboxes.show();
    checkboxes.data('expanded',true);
    e.stopPropagation(); // This is the preferred method.
    return false;
  }
});

function hideAllCheckboxes(){
  var select_checkboxes=$('.select-checkboxes');
  select_checkboxes.hide();
  select_checkboxes.data('expanded',false);
}

$('.checkbox-select-input').on('click', function(e) {
  var $k=$(this).parent().parent().data('count');
  if($(this).prop('checked')) {
    $k++;
  }
  else {
    $k--;
  }
  $($(this).parent().data('for')+' option:selected').text('Выбрано: '+($k) );
  $(this).parent().parent().data('count',$k);
});

$('.clearmultiselect').on('click', function(e) {
  var div_checkboxes = $('#'+$(this).data('clear'));
  div_checkboxes.data('count',0);
  $($('#'+$(this).data('clear')+' :first-child').data('for')+' option:selected').text('Выбрано: 0');
  div_checkboxes.children().each(function(){
    $(this).children().prop('checked', false);
  });


});



$(document).click(function() {
  hideAllCheckboxes();
});