<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectExtra extends Model
{
  protected $table = 'project_extra';

  protected $fillable = [
      'project_id', 'extra_id'
  ];

  public $timestamps = false;
}
