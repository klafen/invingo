<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InviLinks extends Model
{
  protected $table = 'invi_links';

  protected $fillable = [
      'name_ru', 'name_en', 'text_ru', 'text_en', 'is_visible', 'url', 'priority'
  ];

  public $timestamps = false;
}
