<?php

namespace App\Models;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use App;
/**
 * App\Models\Projects.
 *
 * @property int $id
 * @property string $name
 * @property string $logo
 * @property string $token
 * @property int $rating
 * @property int $category_id
 * @property int $platform_id
 * @property int $status_id
 * @property int $country_id
 * @property int $substatus_id
 * @property boolean $is_slider
 * @property boolean $is_adv
 * @property boolean $is_whitelist
 * @property boolean $is_kyc
 * @property boolean $is_kyt
 * @property boolean $is_smartcontract
 * @property boolean $is_visible
 * @property boolean $is_bonus
 * @property boolean $is_airdrop
 * @property boolean $is_mvp
 * @property boolean $is_escrow
 * @property boolean $is_tranding
 * @property boolean $is_bounty
 * @property boolean $is_etherscancheck
 * @property boolean $is_howey_test
 * @property string $roi
 * @property string $all_currency
 * @property string $road_map
 * @property int $type_token_id
 * @property int $jurisdiction
 * @property int $priority
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProjectPrices[] $prices
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProjectLifetime[] $lifetimes
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProjectFilterValueSet[] $project_filter_value_sets
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProjectTeam[] $teams
 * @property-read \App\Models\ProjectLifetime $currentlifetime

 */
class Projects extends Model
{
  protected $table = 'projects';

  protected $fillable = [
      'name', 'logo', 'token', 'rating', 'category_id', 'platform_id', 'status_id', 'country_id', 'substatus_id', 'is_slider', 'is_adv',
      'is_whitelist', 'is_kyc', 'is_kyt', 'is_smartcontract','is_visible', 'is_bonus', 'is_airdrop',  'is_mvp', 'is_escrow', 'roi', 'is_tranding', 'all_currency',
       'road_map','type_token_id', 'is_bounty', 'is_etherscancheck', 'is_howey_test', 'priority', 'jurisdiction',
  ];

  /**
   * @return \Illuminate\Database\Eloquent\Relations\HasOne
   */
  public function prices()
  {
    return $this->hasOne(ProjectPrices::class, 'project_id');
  }

  /**
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function lifetimes()
  {
    return $this->hasMany(ProjectLifetime::class, 'project_id','id');
  }

  /**
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function teams()
  {
    return $this->hasMany(ProjectTeam::class, 'project_id','id')->orderBy('group_id');
  }

  /**
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function project_filter_value_sets()
  {
    return $this->hasMany(ProjectFilterValueSet::class, 'project_id','id');
  }

  /**
   * @return array
   */
  public function currentlifetime()
  {
    $current_lifetime=null;
    $now=new \DateTime('now', new \DateTimeZone('UTC') );
    foreach ($this->lifetimes as $lifetime) {
      //если обе даты указаны
        if ($lifetime->start and $lifetime->end) {
          $start_date=new \DateTime($lifetime->start, new \DateTimeZone('UTC'));
          $end_date=new \DateTime($lifetime->end.' 23:59:59', new \DateTimeZone('UTC'));
          if ($lifetime->gmt) {
            $start_date->modify(-1*$lifetime->gmt.' hours');
            $end_date->modify('+1 days');
            $end_date->modify(-1*$lifetime->gmt.' hours');
          }

          //если еще не началось
          if ($start_date>$now) {
            $datestring=date('Y, m-1, d',strtotime($lifetime->start));
            $status='upcoming';
            $substatus=4;
            $current_lifetime=array('project_lifetime'=>$lifetime, 'name'=>$lifetime->status->name(), 'status'=>$status, 'date_string'=>$datestring, 'substatus'=>$substatus, 'gmt'=>$lifetime->gmt, 'stage'=>$lifetime->status_id);
            break;
          }
          elseif ($end_date>=$now) {
            $datestring=date('Y, m-1, d+1',strtotime($lifetime->end));
            $status='active';
            $substatus=3;
            $current_lifetime=array('project_lifetime'=>$lifetime, 'name'=>$lifetime->status->name(), 'status'=>$status, 'date_string'=>$datestring, 'substatus'=>$substatus, 'gmt'=>$lifetime->gmt, 'stage'=>$lifetime->status_id);
            break;
          }
          else {
            $datestring='';
            $status='ended';
            $substatus=6;
            $current_lifetime=array('project_lifetime'=>$lifetime, 'name'=>$lifetime->status->name(), 'status'=>$status, 'date_string'=>$datestring, 'substatus'=>$substatus, 'gmt'=>$lifetime->gmt, 'stage'=>$lifetime->status_id);
          }
        }
        //если дата начала
        elseif ($lifetime->start) {
          $start_date=new \DateTime($lifetime->start, new \DateTimeZone('UTC'));
          if ($lifetime->gmt) {
            $start_date->modify(-1*$lifetime->gmt.' hours');
          }
          //если  дата начала уже прошла, то выводим TBA active
          if ($start_date<=$now) {
            $status='active';
            $substatus=3;
          }
          else { //иначе TBA upcoming
            $status='upcoming';
            $substatus=4;
          }
          $datestring='';
          $current_lifetime=array('project_lifetime'=>$lifetime, 'name'=>$lifetime->status->name(), 'status'=>$status, 'date_string'=>$datestring, 'substatus'=>$substatus, 'gmt'=>$lifetime->gmt, 'stage'=>$lifetime->status_id );
          break;
        }
        //если же обе не указаны, то будем выводить TBA upcoming
        else {
          $datestring='';
          $status='upcoming';
          $substatus=4;
          $current_lifetime=array('project_lifetime'=>$lifetime, 'name'=>$lifetime->status->name(), 'status'=>$status, 'date_string'=>$datestring, 'substatus'=>$substatus, 'gmt'=>$lifetime->gmt, 'stage'=>$lifetime->status_id );
          break;
        }
    }
    return $current_lifetime;
  }

  /**
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function blocks()
  {
    return $this->hasMany(ProjectBlock::class, 'project_id','id')->orderBy('priority', 'DESC');
  }
}
