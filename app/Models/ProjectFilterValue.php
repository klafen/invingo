<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App;
/**
 * App\Models\ProjectFilterValue.
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name_en 
 * @property int $priority
 * @property int $project_filter_id
 *
 * @property-read \App\Models\ProjectFilter $project_filter
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProjectFilterValueSet[] $project_filter_value_sets

 */
class ProjectFilterValue extends Model
{
  protected $table = 'project_filter_value';

  protected $fillable = [
      'name_ru', 'name_en', 'project_filter_id', 'priority'
  ];

  public $timestamps = false;

  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function project_filter()
  {
    return $this->belongsTo(ProjectFilter::class, 'project_filter_id');
  }

  /**
   * @return string
   */
  public function name()
  {
    $locale = App::getLocale('locale');
    if ($locale=='ru') {return $this->name_ru;} else {return $this->name_en;}
  }

  /**
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function project_filter_value_sets()
  {
    return $this->hasMany(ProjectFilterValueSet::class, 'project_filter_value_id','id');
  }

  public function delete() {
    foreach ($this->project_filter_value_sets as $value_set) {
      $value_set->delete();
    }
    parent::delete();
  }
  
}
