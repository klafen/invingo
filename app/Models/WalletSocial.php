<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WalletSocial extends Model
{
  protected $table = 'wallet_social';

  protected $fillable = [
      'wallet_id', 'social_id', 'link'
  ];

  public $timestamps = false;
}
