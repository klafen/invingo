<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomRating extends Model
{
  protected $table = 'custom_rating';

  protected $fillable = [
      'name_ru', 'name_en'
  ];

  public $timestamps = false;
}
