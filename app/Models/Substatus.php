<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App;
/**
 * App\Models\Substatus.
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name_en
 *
 */

class Substatus extends Model
{
  protected $table = 'substatus';

  protected $fillable = [
      'name_ru', 'name_en'
  ];

  public $timestamps = false;

  /**
   * @return string
   */
  public function name()
  {
    $locale = App::getLocale('locale');
    if ($locale=='ru') {return $this->name_ru;} else {return $this->name_en;}
  }
}
