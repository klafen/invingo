<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App;
/**
 * App\Models\WalletFilterValueSet
 *
 * @property int $id
 * @property int $project_id
 * @property int $project_filter_value_id
 *
 * @property-read \App\Models\WalletFilterValue $filter_value
 * @property-read \App\Models\Wallets $wallet

 */
class WalletFilterValueSet extends Model
{
  protected $table = 'wallet_filter_value_set';

  protected $fillable = [
      'wallet_id', 'wallet_filter_value_id'
  ];

  public $timestamps = false;

  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function filter_value()
  {
    return $this->belongsTo(WalletFilterValue::class, 'wallet_filter_value_id');
  }

  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function wallet()
  {
    return $this->belongsTo(Wallets::class, 'wallet_id');
  }
  
}
