<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectLinks extends Model
{
  protected $table = 'project_links';

  protected $fillable = [
      'project_id', 'name_ru', 'name_en', 'link'
  ];

  public $timestamps = false;
}
