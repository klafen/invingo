<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WalletOs extends Model
{
  protected $table = 'wallet_os';

  protected $fillable = [
      'wallet_id', 'os_id'
  ];

  public $timestamps = false;
}
