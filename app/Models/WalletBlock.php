<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App;
/**
 * App\Models\WalletBlock.
 *
 * @property int $id
 * @property int $wallet_id
 * @property string caption_ru
 * @property string caption_en
 * @property string content_ru
 * @property string content_en
 */
class WalletBlock extends Model
{
  protected $table = 'wallet_blocks';

  protected $fillable = [
      'caption_ru', 'caption_en', 'content_ru', 'content_en', 'wallet_id', 'priority'
  ];

  public $timestamps = false;

  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function wallet()
  {
    return $this->belongsTo(Wallets::class, 'wallet_id');
  }

  /**
   * @return string
   */
  public function name()
  {
    $locale = App::getLocale('locale');
    if ($locale=='ru') {return $this->caption_ru;} else {return $this->caption_en;}
  }

  /**
   * @return string
   */
  public function content()
  {
    $locale = App::getLocale('locale');
    if ($locale=='ru') {return $this->content_ru;} else {return $this->content_en;}
  }
}
