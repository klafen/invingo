<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App;
/**
 * App\Models\ProjectTeam
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name_en
 * @property string $avatar
 * @property int $project_id
 * @property int $profession_id
 * @property int $group_id
 * @property string $profession_ru
 * @property string $profession_en
 * 
 * @property-read \App\Models\Projects $project
 * @property-read \App\Models\Professions $profession
 * @property-read \App\Models\TeamGroup $group
 **/
class ProjectTeam extends Model
{
  protected $table = 'project_team';

  protected $fillable = [
      'project_id', 'name_ru', 'name_en', 'profession_id', 'avatar', 'group_id', 'profession_ru', 'profession_en'
  ];

  public $timestamps = false;

  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function project()
  {
    return $this->belongsTo(Projects::class, 'project_id');
  }
  
  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function profession()
  {
    return $this->belongsTo(Professions::class, 'profession_id');
  }


  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function group()
  {
    return $this->belongsTo(TeamGroup::class, 'group_id');
  }

  /**
   * @return string
   */
  public function profession_name()
  {
    $locale = App::getLocale('locale');
    if ($locale=='ru') {return $this->profession_ru;} else {return $this->profession_en;}
  }

  /**
   * @return string
   */
  public function name()
  {
    $locale = App::getLocale('locale');
    if ($locale=='ru') {return $this->name_ru;} else {return $this->name_en;}
  }
  

  /**
   * @return integer
   */
  public function count_projects()
  {
    return Projects::select('projects.id as id', 'projects.name as name')
        ->leftjoin('project_team','project_team.project_id', '=', 'projects.id')
        ->where('is_visible',1)
        ->whereraw('(project_team.name_ru = \''.addcslashes($this->name_ru,"'").'\' OR project_team.name_en = \''.addcslashes($this->name_en,"'").'\') ')->distinct()->get()->count();
  }

  /**
   * @return integer
   */
  public function count_wallets()
  {
    return Wallets::select('wallets.id as id', 'wallets.name as name')
        ->leftjoin('wallet_team','wallet_team.wallet_id', '=', 'wallets.id')
        ->where('is_visible',1)
        ->whereraw('(wallet_team.name_ru = \''.addcslashes($this->name_ru,"'").'\' OR wallet_team.name_en = \''.addcslashes($this->name_en,"'").'\') ')->distinct()->get()->count();
  }

  /**
   * @return integer
   */
  public function count_works()
  {
    return $this->count_projects()+$this->count_wallets();
  }
}
