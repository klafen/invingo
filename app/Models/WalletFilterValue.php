<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App;
/**
 * App\Models\WalletFilterValue.
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name_en 
 * @property int $priority
 * @property int $wallet_filter_id
 *
 * @property-read \App\Models\WalletFilter $filter
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\WalletFilterValueSet[] $filter_value_sets

 */
class WalletFilterValue extends Model
{
  protected $table = 'wallet_filter_value';

  protected $fillable = [
      'name_ru', 'name_en', 'wallet_filter_id', 'priority'
  ];

  public $timestamps = false;

  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function filter()
  {
    return $this->belongsTo(WalletFilter::class, 'wallet_filter_id');
  }

  /**
   * @return string
   */
  public function name()
  {
    $locale = App::getLocale('locale');
    if ($locale=='ru') {return $this->name_ru;} else {return $this->name_en;}
  }

  /**
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function filter_value_sets()
  {
    return $this->hasMany(WalletFilterValueSet::class, 'wallet_filter_value_id','id');
  }

  public function delete() {
    foreach ($this->filter_value_sets as $value_set) {
      $value_set->delete();
    }
    parent::delete();
  }
  
}
