<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeToken extends Model
{
  protected $table = 'type_token';

  protected $fillable = [
      'name'
  ];

  public $timestamps = false;
}
