<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OS extends Model
{
  protected $table = 'os';

  protected $fillable = [
      'name'
  ];

  public $timestamps = false;
}
