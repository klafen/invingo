<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Validation extends Model
{
  protected $table = 'validation';

  protected $fillable = [
      'name_ru', 'name_en'
  ];

  public $timestamps = false;
}
