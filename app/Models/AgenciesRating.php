<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgenciesRating extends Model
{
  protected $table = 'agencies_rating';

  protected $fillable = [
      'name', 'is_main', 'url'
  ];

  public $timestamps = false;
}
