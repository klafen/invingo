<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WalletDescription extends Model
{
  protected $table = 'wallet_description';

  protected $fillable = [
      'wallet_id', 'info_ru', 'info_en', 'other_ru', 'other_en',
  ];

  public $timestamps = false;
}
