<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectTrading extends Model
{
  protected $table = 'project_trading';

  protected $fillable = [
      'project_id', 'trading_id'
  ];

  public $timestamps = false;
}
