<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WalletSupport extends Model
{
  protected $table = 'wallet_support';

  protected $fillable = [
      'wallet_id', 'cryptocoin_id'
  ];

  public $timestamps = false;
}
