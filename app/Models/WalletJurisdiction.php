<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WalletJurisdiction extends Model
{
  protected $table = 'wallet_jurisdiction';

  protected $fillable = [
      'wallet_id', 'company_name', 'phone', 'email', 'address'
  ];

  public $timestamps = false;
}
