<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WalletType extends Model
{
  protected $table = 'wallet_type';

  protected $fillable = [
      'name_ru', 'name_en'
  ];

  public $timestamps = false;
}
