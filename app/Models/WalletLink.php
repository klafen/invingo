<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WalletLink extends Model
{
  protected $table = 'wallet_links';

  protected $fillable = [
      'wallet_id','name_ru', 'name_en', 'link'
  ];

  public $timestamps = false;
}
