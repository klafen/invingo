<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdvRequest extends Model
{
  protected $table = 'adv_requests';

  protected $fillable = [
      'name', 'phone', 'email', 'message', 'contract_name'
  ];
}
