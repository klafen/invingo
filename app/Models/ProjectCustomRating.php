<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectCustomRating extends Model
{
  protected $table = 'project_custom_rating';

  protected $fillable = [
      'project_id', 'custom_rating_id', 'rating'
  ];

  public $timestamps = false;
}
