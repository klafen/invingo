<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App;
/**
 * App\Models\ProjectBlock.
 *
 * @property int $id
 * @property int $project_id
 * @property string caption_ru
 * @property string caption_en
 * @property string content_ru
 * @property string content_en
 */
class ProjectBlock extends Model
{
  protected $table = 'project_blocks';

  protected $fillable = [
      'caption_ru', 'caption_en', 'content_ru', 'content_en', 'project_id', 'priority'
  ];

  public $timestamps = false;

  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function project()
  {
    return $this->belongsTo(Wallets::class, 'project_id');
  }

  /**
   * @return string
   */
  public function name()
  {
    $locale = App::getLocale('locale');
    if ($locale=='ru') {return $this->caption_ru;} else {return $this->caption_en;}
  }

  /**
   * @return string
   */
  public function content()
  {
    $locale = App::getLocale('locale');
    if ($locale=='ru') {return $this->content_ru;} else {return $this->content_en;}
  }
}
