<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WalletLanguage extends Model
{
  protected $table = 'wallet_language';

  protected $fillable = [
      'wallet_id', 'language_id'
  ];

  public $timestamps = false;
}
