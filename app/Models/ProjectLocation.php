<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectLocation extends Model
{
  protected $table = 'project_location';

  protected $fillable = [
      'project_id', 'coordinates'
  ];

  public $timestamps = false;
}
