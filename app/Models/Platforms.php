<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Platforms extends Model
{
  protected $table = 'platforms';

  protected $fillable = [
      'name'
  ];

  public $timestamps = false;
}
