<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WalletExtra extends Model
{
  protected $table = 'wallet_extra';

  protected $fillable = [
      'wallet_id', 'extra_id'
  ];

  public $timestamps = false;
}
