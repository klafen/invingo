<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InviSocial extends Model
{
  protected $table = 'invi_socials';

  protected $fillable = [
      'followers', 'social_id', 'link'
  ];

  public $timestamps = false;
}
