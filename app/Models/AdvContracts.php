<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdvContracts extends Model
{
  protected $table = 'adv_contracts';

  protected $fillable = [
      'name_ru', 'text_ru', 'name_en', 'text_en', 'is_discussing', 'is_visible'
  ];

}
