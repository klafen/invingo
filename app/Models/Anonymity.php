<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Anonymity extends Model
{
  protected $table = 'anonymity';

  protected $fillable = [
      'name_ru', 'name_en'
  ];

  public $timestamps = false;
}
