<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/**
 * App\Models\ProjectPrices.
 *
 * @property int $id
 * @property int $project_id
 * @property \DateTime $start
 * @property \DateTime $end
 * @property int $status_id
 * @property int $substatus_id
 * @property float $price
 * @property float $min_invest
 * @property int $price_currency_id
 * @property boolean $is_tba
 * @property int $min_invest_currency_id
 * @property string $substatus
 * @property-read \App\Models\Projects $project
 * @property-read \App\Models\Cryptocoins $price_currency
 * @property-read \App\Models\Cryptocoins $min_invest_currency
 * @property-read \App\Models\Status $status
 */
class ProjectLifetime extends Model
{
  protected $table = 'project_lifetime';

  protected $fillable = [
      'project_id', 'start', 'end', 'status_id', 'substatus_id', 'price', 'min_invest',
      'price_currency_id', 'min_invest_currency_id', 'gmt', 'is_tba', 'substatus'
  ];

  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function project()
  {
    return $this->belongsTo(Projects::class, 'project_id');
  }

  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function status()
  {
    return $this->belongsTo(Status::class, 'status_id');
  }
  

  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function price_currency()
  {
    return $this->belongsTo(Cryptocoins::class, 'price_currency_id', 'id');
  }
  
  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function min_invest_currency()
  {
    return $this->belongsTo(Cryptocoins::class, 'min_invest_currency_id', 'id');
  }

  /**
   * @return string
   */
  public function price_currency_string()
  {
    $currency= $this->price_currency;
    if ($currency) {return $currency->code;}
    elseif($this->project->all_currency) {return $this->project->all_currency;}
    else {return '$';}
  }

  /**
   * @return string
   */
  public function min_invest_currency_string()
  {
    $currency= $this->min_invest_currency;
    if ($currency) {return $currency->code;}
    elseif($this->project->all_currency) {return $this->project->all_currency;}
    else {return '$';}
  }

  /**
   * @return string
   */
  public function min_invest_string()
  {
    if ($this->min_invest) {return rtrim(rtrim(number_format($this->min_invest,10, ',',' '),0),',');} else {return null;}
  }
}
