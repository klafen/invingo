<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectDescription extends Model
{
  protected $table = 'project_description';

  protected $fillable = [
      'project_id', 'info_ru', 'about_us_ru', 'other_ru', 'info_en', 'about_us_en', 'other_en'
  ];

  public $timestamps = false;
}
