<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InviSet extends Model
{
  protected $table = 'invi_sets';

  protected $fillable = [
      'email_form', 'email_contact', 'projects_by_page', 'wallets_by_page', 'namecaption_site', 'slider_max_projects'
  ];

  public $timestamps = false;
}
