<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Magister extends Authenticatable
{
  use Notifiable;
  protected $table = 'magisters';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'login', 'name', 'email', 'password', 'api', 'rank', 'remember_token'
  ];


}
