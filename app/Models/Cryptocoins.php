<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cryptocoins extends Model
{
  protected $table = 'cryptocoins';

  protected $fillable = [
      'name', 'code'
  ];

  public $timestamps = false;
}
