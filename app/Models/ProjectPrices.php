<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/**
 * App\Models\ProjectPrices.
 *
 * @property int $id
 * @property int $project_id
 * @property int $min_cap
 * @property int $soft_cap
 * @property int $hard_cap
 * @property int $collected
 * @property int $for_sale
 * @property int $soft_cap_currency_id
 * @property int $hard_cap_currency_id
 * @property int $collected_currency_id
 * @property int $for_sale_currency_id
 *
 * @property-read \App\Models\Projects $project
 * @property-read \App\Models\Cryptocoins $soft_cap_currency
 * @property-read \App\Models\Cryptocoins $hard_cap_currency
 * @property-read \App\Models\Cryptocoins $collected_currency
 * @property-read \App\Models\Cryptocoins $for_sale_currency


 */
class ProjectPrices extends Model
{
  protected $table = 'project_prices';

  protected $fillable = [
      'project_id', 'min_cap', 'soft_cap', 'hard_cap', 'collected', 'for_sale',
      'soft_cap_currency_id', 'hard_cap_currency_id', 'collected_currency_id', 'for_sale_currency_id'
  ];

  public $timestamps = false;

  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function project()
  {
    return $this->belongsTo(Projects::class, 'project_id', 'id');
  }

  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function soft_cap_currency()
  {
    return $this->belongsTo(Cryptocoins::class, 'soft_cap_currency_id', 'id');
  }

  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function hard_cap_currency()
  {
    return $this->belongsTo(Cryptocoins::class, 'hard_cap_currency_id', 'id');
  }
  
  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function collected_currency()
  {
    return $this->belongsTo(Cryptocoins::class, 'collected_currency_id', 'id');
  }
  
  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function for_sale_currency()
  {
    return $this->belongsTo(Cryptocoins::class, 'for_sale_currency_id', 'id');
  }
  
  /**
   * @return string
   */
  public function soft_cap_currency_string()
  {
    /** @var Cryptocoins $currency */
    $currency=$this->soft_cap_currency;
    if ($currency) {
      return $currency->code;
    }
    elseif ($this->project->all_currency){
     return $this->project->all_currency;
    }
    else {
      return '$';
    }
  }
  
  /**
   * @return string
  */
  public function hard_cap_currency_string()
  {
    /** @var Cryptocoins $currency */
    $currency=$this->hard_cap_currency;
    if ($currency) {
      return $currency->code;
    }
    elseif ($this->project->all_currency){
      return $this->project->all_currency;
    }
    else {
     return '$'; 
    }
  }

  /**
   * @return string
   */
  public function collected_currency_string()
  {
    /** @var Cryptocoins $currency */
    $currency=$this->collected_currency;
    if ($currency) {
      return $currency->code;
    }
    elseif ($this->project->all_currency){
      return $this->project->all_currency;
    }
    else {
      return '$';
    }
  }

  /**
   * @return string
   */
  public function for_sale_currency_string()
  {
    /** @var Cryptocoins $currency */
    $currency=$this->for_sale_currency;
    if ($currency) {
      return $currency->code;
    }
    else {
      return '';
    }
  }
 
  
}
