<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectAgenciesRating extends Model
{
  protected $table = 'project_agencies_rating';

  protected $fillable = [
      'project_id', 'agency_rating_id', 'rating'
  ];

  public $timestamps = false;
}
