<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App;
/**
 * App\Models\WalletFilter.
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name_en
 * @property string $system_name
 * @property int $priority
 * @property string $type
 * @property boolean $is_system
 * @property boolean $is_visible
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\WalletFilterValue[] $values

 */

class WalletFilter extends Model
{
  protected $table = 'wallet_filter';

  protected $fillable = [
      'name_ru', 'name_en', 'system_name', 'priority', 'type', 'is_system', 'is_visible', 'is_visible_name'
  ];

  public $timestamps = false;

  /**
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function values()
  {
    return $this->hasMany(WalletFilterValue::class, 'wallet_filter_id','id');
  }

  /**
   * @return string
   */
  public function name()
  {
    $locale = App::getLocale('locale');
    if ($locale=='ru') {return $this->name_ru;} else {return $this->name_en;}
  }
  
  
  public function delete() {
    foreach ($this->values as $value) {
      $value->delete();
    }
    parent::delete();
  }
}
