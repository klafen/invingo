<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectRestrictedCoutries extends Model
{
  protected $table = 'project_restricted_countries';

  protected $fillable = [
      'project_id', 'country_id'
  ];

  public $timestamps = false;
}
