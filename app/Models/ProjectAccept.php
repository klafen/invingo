<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectAccept extends Model
{
  protected $table = 'project_accept';

  protected $fillable = [
      'project_id', 'cryptocoin_id'
  ];

  public $timestamps = false;
}
