<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Extra extends Model
{
  protected $table = 'extra';

  protected $fillable = [
      'name_ru', 'name_en'
  ];

  public $timestamps = false;
}
