<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App;
/**
 * App\Models\ProjectFilterValueSet
 *
 * @property int $id
 * @property int $project_id
 * @property int $project_filter_value_id
 *
 * @property-read \App\Models\ProjectFilterValue $project_filter_value
 * @property-read \App\Models\Projects $project

 */
class ProjectFilterValueSet extends Model
{
  protected $table = 'project_filter_value_set';

  protected $fillable = [
      'project_id', 'project_filter_value_id'
  ];

  public $timestamps = false;

  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function project_filter_value()
  {
    return $this->belongsTo(ProjectFilterValue::class, 'project_filter_value_id');
  }

  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function project()
  {
    return $this->belongsTo(Projects::class, 'project_id');
  }
  
}
