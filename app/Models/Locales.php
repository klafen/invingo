<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Locales extends Model
{
  protected $table = 'locales';

  protected $fillable = [
      'name', 'code'
  ];

  public $timestamps = false;
}
