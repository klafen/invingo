<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContentSettings extends Model
{
  protected $table = 'content_settings';

  protected $fillable = [
      'name', 'caption', 'text_ru', 'text_en', 'is_visible'
  ];

  public $timestamps = false;
}
