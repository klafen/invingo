<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrivateKeyControl extends Model
{
  protected $table = 'private_key_control';

  protected $fillable = [
      'name_ru', 'name_en'
  ];

  public $timestamps = false;
}
