<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectBonuses extends Model
{
  protected $table = 'project_bonuses';

  protected $fillable = [
      'project_id', 'stage', 'bonus'
  ];

  public $timestamps = false;
}
