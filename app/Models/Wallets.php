<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/**
 * App\Models\Wallets.
 *
 * @property int $id
 * @property string $name
 * @property string $logo 
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\WalletBlock[] $blocks
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\WalletFilterValueSet[] $filter_value_sets
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\WalletTeam[] $teams

 */
class Wallets extends Model
{
  protected $table = 'wallets';

  protected $fillable = [
      'name', 'logo', 'rating', 'wallet_type_id', 'private_key_control_id', 'validation_id', 'anonymity_id', 'country_id','is_sponsored', 'is_visible',
      'is_hardware', 'is_multisignature', 'is_reserve_copy', 'is_seed_phrase', 'is_smart_contract',
      'is_2FA', 'is_exchange', 'is_open_source','security', 'is_qrcode', 'is_touch_id', 'is_fiat', 'road_map', 'priority', 'is_adapt'
  ];

  /**
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function blocks()
  {
    return $this->hasMany(WalletBlock::class, 'wallet_id','id')->orderBy('priority', 'DESC');
  }

  /**
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function filter_value_sets()
  {
    return $this->hasMany(WalletFilterValueSet::class, 'wallet_id','id');
  }

  /**
   * @return \Illuminate\Database\Eloquent\Relations\HasMany
   */
  public function teams()
  {
    return $this->hasMany(WalletTeam::class, 'wallet_id','id')->orderBy('group_id');
  }

}
