<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Countries extends Model
{
  protected $table = 'countries';

  protected $fillable = [
      'name_ru', 'name_en'
  ];

  public $timestamps = false;
}
