<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Trading extends Model
{
  protected $table = 'trading';

  protected $fillable = [
      'name'
  ];

  public $timestamps = false;
}
