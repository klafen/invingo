<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App;
/**
 * App\Models\WalletTeam
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name_en
 * @property string $avatar
 * @property int $wallet_id
 * @property int $profession_id
 * @property int $group_id
 * @property string $profession_ru
 * @property string $profession_en
 * @property-read \App\Models\Wallets $wallet
 * @property-read \App\Models\Professions $profession
 * @property-read \App\Models\TeamGroup $group
 **/
class WalletTeam extends Model
{
  protected $table = 'wallet_team';

  protected $fillable = [
      'wallet_id', 'name_ru', 'name_en', 'avatar', 'profession_id', 'group_id', 'profession_ru', 'profession_en'
  ];

  public $timestamps = false;


  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function wallet()
  {
    return $this->belongsTo(Wallets::class, 'wallet_id');
  }

  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function profession()
  {
    return $this->belongsTo(Professions::class, 'profession_id');
  }

  /**
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  public function group()
  {
    return $this->belongsTo(TeamGroup::class, 'group_id');
  }

  /**
   * @return string
   */
  public function name()
  {
    $locale = App::getLocale('locale');
    if ($locale=='ru') {return $this->name_ru;} else {return $this->name_en;}
  }

  /**
   * @return string
   */
  public function profession_name()
  {
    $locale = App::getLocale('locale');
    if ($locale=='ru') {return $this->profession_ru;} else {return $this->profession_en;}
  }

  /**
   * @return integer
   */
  public function count_projects()
  {
    return Projects::select('projects.id as id', 'projects.name as name')
        ->leftjoin('project_team','project_team.project_id', '=', 'projects.id')
        ->where('is_visible',1)
        ->whereraw('(project_team.name_ru like \'%'.$this->name_ru.'%\' OR project_team.name_en like \'%'.$this->name_en.'%\') ')->distinct()->get()->count();
  }

  /**
   * @return integer
   */
  public function count_wallets()
  {
    return Wallets::select('wallets.id as id', 'wallets.name as name')
        ->leftjoin('wallet_team','wallet_team.wallet_id', '=', 'wallets.id')
        ->where('is_visible',1)
        ->whereraw('(wallet_team.name_ru like \'%'.$this->name_ru.'%\' OR wallet_team.name_en like \'%'.$this->name_en.'%\') ')->distinct()->get()->count();
  }
  
  /**
   * @return integer
   */
  public function count_works()
  {
    return $this->count_projects()+$this->count_wallets();
  }
}
