<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectSocial extends Model
{
  protected $table = 'project_social';

  protected $fillable = [
      'project_id', 'social_id', 'link'
  ];

  public $timestamps = false;
}
