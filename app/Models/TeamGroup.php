<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App;
/**
 * App\Models\TeamGroup
 *
 * @property int $id
 * @property string $name_ru
 * @property string $name_en
 **/
class TeamGroup extends Model
{
  protected $table = 'team_group';

  protected $fillable = [
      'name_ru', 'name_en'
  ];

  public $timestamps = false;

  /**
   * @return string
   */
  public function name()
  {
    $locale = App::getLocale('locale');
    if ($locale=='ru') {return $this->name_ru;} else {return $this->name_en;}
  }
}
