<?php

namespace App\Http\Controllers;


use App\Models\Projects;
use App\Models\Wallets;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{

  public function show()
  {
    $locale = \App::getLocale();
    $wallets=array();
    $projects=array();
    
    if (isset($_GET['s']) && $_GET['s']!='') {
      $mode='';
      if (isset($_GET['mode']) && $_GET['mode']!='') {
        $mode=$_GET['mode'];
      }
      $s=$_GET['s'];
      $walletsraw=Wallets::select('wallets.id','wallets.name','wallets.logo','wallets.rating','wallet_type.name_'.$locale.' as wallet_type', 'wallets.priority',
          'wallets.private_key_control_id','wallets.is_sponsored','wallet_oses.os', 'wallet_support.code as currency', 'private_key_control.name_'.$locale.' as private_key_control_name' )
          ->leftjoin('wallet_type', 'wallets.wallet_type_id', '=', 'wallet_type.id')
          ->leftjoin(DB::raw("(SELECT
                  wallet_os.wallet_id,
                  GROUP_CONCAT(os.name SEPARATOR ', ') as os
                  FROM wallet_os
                  JOIN os ON wallet_os.os_id = os.id
                  GROUP BY wallet_os.wallet_id
                  ) as wallet_oses"),function($join){
            $join->on("wallets.id","=","wallet_oses.wallet_id");
          })
          ->leftjoin(DB::raw("(SELECT
                  wallet_support.wallet_id,
                  GROUP_CONCAT(cryptocoins.code SEPARATOR ', ') as code
                  FROM wallet_support
                  JOIN cryptocoins ON wallet_support.cryptocoin_id = cryptocoins.id
                  GROUP BY wallet_support.wallet_id
                  ) as wallet_support"),function($join){
            $join->on("wallets.id","=","wallet_support.wallet_id");

          })
          ->leftjoin('private_key_control','wallets.private_key_control_id', '=', 'private_key_control.id')
          ->leftjoin('wallet_description','wallet_description.wallet_id', '=', 'wallets.id')
          ->leftjoin('wallet_team','wallet_team.wallet_id', '=', 'wallets.id')
          ->where('wallets.is_visible',1);


      $projectsraw=Projects::select('projects.id as id', 'projects.name as name', 'logo', 'hard_cap', 'token', 'rating', 'collected', 'categories.name_'.$locale.' as category', 'platforms.name as platform',   'is_adv', 'all_currency', 'projects.priority')
          ->leftjoin('categories', 'categories.id', '=', 'projects.category_id')
          ->leftjoin('project_prices', 'projects.id', '=', 'project_prices.project_id')
          ->leftjoin('platforms', 'platforms.id', '=', 'projects.platform_id')
          ->leftjoin('project_description','project_description.project_id', '=', 'projects.id')
          ->leftjoin('project_team','project_team.project_id', '=', 'projects.id')
          ->join('project_lifetime', 'project_lifetime.project_id', '=', 'projects.id')
          ->where('is_visible',1);

      //если это переход не с ссылки члена команды

      if ($mode=='team') {
        $walletsraw
            ->whereraw('(wallet_team.name_ru = \''.$s.'\' OR wallet_team.name_en = \''.$s.'\') ');

        $projectsraw
            ->whereraw('(project_team.name_ru = \''.$s.'\' OR project_team.name_en = \''.$s.'\') ');
      }
      else{
        $sqlstring='';
        $sqlstring.=' wallet_team.name_ru like \'%'.$s.'\'';
        $sqlstring.=' OR wallet_team.name_en like \'%'.$s.'\'';
        $sqlstring.=' OR wallet_description.info_ru like \'%'.$s.'\'';
        $sqlstring.=' OR wallet_description.info_en like \'%'.$s.'\'';
        $sqlstring.=' OR wallets.name like \'%'.$s.'\'';
        $walletsraw->whereraw($sqlstring);

        $sqlstring='';
        $sqlstring.=' project_team.name_ru like \'%'.$s.'\'';
        $sqlstring.=' OR project_team.name_en like \'%'.$s.'\'';
        $sqlstring.=' OR project_description.info_ru like \'%'.$s.'\'';
        $sqlstring.=' OR project_description.info_en like \'%'.$s.'\'';
        $sqlstring.=' OR projects.token like \'%'.$s.'\'';
        $sqlstring.=' OR projects.name like \'%'.$s.'\'';
        $projectsraw->whereraw($sqlstring);
      }



      $walletsraw->orderby('wallets.is_sponsored', 'DESC')->orderby('wallets.priority', 'DESC')->orderby('id', 'DESC')->distinct();
      $projectsraw->orderby('projects.is_adv', 'DESC')->orderby('projects.priority', 'DESC')->orderby('projects.id','DESC' )->distinct();

      $walletsraw=$walletsraw->get();
      $projectsraw=$projectsraw->get();;

      foreach ($walletsraw as $wallet) {
        array_push($wallets, $wallet);
      }

      //определяем для каждого проекта текущий статус и пихаем все это в массив
      foreach ($projectsraw as $project) {
        array_push($projects,array('project'=>$project, 'current_lifetime'=>$project->currentlifetime()));
      }
    }
    else {
      $s='';
    }

      return view('search',[
          's'=>$s,
          'projects'=>$projects,
          'wallets' => $wallets,
          'settings'=>$this->settings(),
      ]);
    
  }

  public function search_ajax()
  {
    $locale = \App::getLocale();
    $wallets=array();
    $projects=array();

    if (isset($_GET['s']) && $_GET['s']!='' && (isset($_GET['mode']) && $_GET['mode']=='ajax')) {
      $s=$_GET['s'];
      $walletsraw=Wallets::select('wallets.id','wallets.name','wallets.logo','wallets.rating','wallet_type.name_'.$locale.' as wallet_type', 'wallets.priority',
          'wallets.private_key_control_id','wallets.is_sponsored','wallet_oses.os', 'wallet_support.code as currency', 'private_key_control.name_'.$locale.' as private_key_control_name' )
          ->leftjoin('wallet_type', 'wallets.wallet_type_id', '=', 'wallet_type.id')
          ->leftjoin(DB::raw("(SELECT
                  wallet_os.wallet_id,
                  GROUP_CONCAT(os.name SEPARATOR ', ') as os
                  FROM wallet_os
                  JOIN os ON wallet_os.os_id = os.id
                  GROUP BY wallet_os.wallet_id
                  ) as wallet_oses"),function($join){
            $join->on("wallets.id","=","wallet_oses.wallet_id");
          })
          ->leftjoin(DB::raw("(SELECT
                  wallet_support.wallet_id,
                  GROUP_CONCAT(cryptocoins.code SEPARATOR ', ') as code
                  FROM wallet_support
                  JOIN cryptocoins ON wallet_support.cryptocoin_id = cryptocoins.id
                  GROUP BY wallet_support.wallet_id
                  ) as wallet_support"),function($join){
            $join->on("wallets.id","=","wallet_support.wallet_id");
          })
          ->leftjoin('private_key_control','wallets.private_key_control_id', '=', 'private_key_control.id')
          ->where('wallets.name', 'like', '%'.$s.'%')
          ->where('wallets.is_visible',1)->orderby('wallets.is_sponsored', 'DESC')->orderby('wallets.priority', 'DESC')->orderby('id', 'DESC');


      $projectsraw=Projects::select('projects.id as id', 'projects.name as name', 'logo', 'hard_cap', 'token', 'rating', 'collected', 'categories.name_'.$locale.' as category', 'platforms.name as platform',   'is_adv', 'all_currency', 'projects.priority')
          ->leftjoin('categories', 'categories.id', '=', 'projects.category_id')
          ->leftjoin('project_prices', 'projects.id', '=', 'project_prices.project_id')
          ->leftjoin('platforms', 'platforms.id', '=', 'projects.platform_id')
          ->join('project_lifetime', 'project_lifetime.project_id', '=', 'projects.id')
          ->where('is_visible',1)
          ->whereraw('(projects.name like \'%'.$s.'%\' ) ')
          ->orderby('projects.is_adv', 'DESC')->orderby('projects.priority', 'DESC')->orderby('projects.id','DESC' )->distinct();
      //если запрос - ajax, то ограничеваем вывод
      if (isset($_GET['mode']) && $_GET['mode']=='ajax') {
        $walletsraw->limit(5);
        $projectsraw->limit(5);
      }
      $walletsraw=$walletsraw->get();
      $projectsraw=$projectsraw->get();;

      foreach ($walletsraw as $wallet) {
        array_push($wallets, $wallet);
      }

      //определяем для каждого проекта текущий статус и пихаем все это в массив
      foreach ($projectsraw as $project) {
        array_push($projects,array('project'=>$project, 'current_lifetime'=>$project->currentlifetime()));
      }
      //если запрос - ajax
      if (isset($_GET['mode']) && $_GET['mode']=='ajax')  {
        //ограничением количество выводимых элементов
        return view('search_ajax',[
            's'=>$s,
            'projects'=>$projects,
            'wallets' => $wallets,
            'settings'=>$this->settings(),
        ]);
      }
    }

    return null;

  }

}
