<?php

namespace App\Http\Controllers;

use App\Models\ContentSettings;
use Illuminate\Http\Request;
use App\Models\AdvContracts;

class AdvertController extends Controller
{


  public function index()
  {
    $locale = \App::getLocale();
    return view('advert',[
      'contracts' => AdvContracts::select('id','name_'.$locale.' as name', 'text_'.$locale.' as text', 'is_discussing')->where('is_visible',1)->get(),
      'adv_preheader'=>ContentSettings::select('text_'.$locale.' AS text')->where('name','=','adv_preheader')->where('is_visible',1)->first(),
      'adv_offer'=>ContentSettings::select('text_'.$locale.' AS text')->where('name','=','adv_offer')->where('is_visible',1)->first(),
      'settings'=>$this->settings()
    ]);
  }


}
