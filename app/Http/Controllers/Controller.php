<?php

namespace App\Http\Controllers;

use App\Models\ContentSettings;
use App\Models\InviLinks;
use App\Models\InviSet;
use App\Models\InviSocial;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function settings()
    {
        return array(
            'invisocials'=>$this->getinvisocials(),
            'cryptoadressfooter'=> $this->cryptoadressfooter(),
            'links'=>$this->links(),
            'footer_disk'=>$this->footer_disk(),
            'parameters'=>$this->parameters(),
        );
    }
    
    public function getinvisocials()
    {
        return InviSocial::select('socials.icon as icon', 'socials.name as name', 'link', 'followers')->
        leftjoin('socials', 'invi_socials.social_id', '=', 'socials.id')->get();
    }

    public function cryptoadressfooter()
    {
        $locale = \App::getLocale();
        return ContentSettings::select('text_'.$locale.' AS text')->where('name','=','cryptoadressfooter')->where('is_visible',1)->first();
    }

    public function links()
    {
        $locale = \App::getLocale();
        return InviLinks::select('text_'.$locale.' AS text', 'name_'.$locale.' AS name', 'url')->where('is_visible',1)->orderby('priority','desc')->get();
    }
    
    public function footer_disk () {
        $locale = \App::getLocale();
        return ContentSettings::select('text_'.$locale.' AS text')->where('name','=','footer_disk')->where('is_visible',1)->first();
    }
    
    public function parameters () {
        return InviSet::where('id','=',1)->first();
    }

    public function or_sql ($input_array, $joincolumn ) {
        $sql='(';
        foreach ($input_array as $key => $value) {
            $sql.=$joincolumn.' = '.$key.' OR ';
        }
        $sql=substr($sql,0,-3 );
        $sql.=')';
        return $sql;
    }

}
