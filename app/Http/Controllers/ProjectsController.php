<?php

namespace App\Http\Controllers;



use App\Models\Categories;
use App\Models\Countries;
use App\Models\Cryptocoins;
use App\Models\Platforms;
use App\Models\Extra;
use App\Models\ProjectBonuses;
use App\Models\Projects;
use App\Models\ProjectAgenciesRating;
use App\Models\ProjectCustomRating;
use App\Models\ProjectLifetime;
use App\Models\ProjectLinks;
use App\Models\Trading;
use App\Models\ProjectRestrictedCoutries;
use App\Models\ProjectSocial;
use App\Models\ProjectTeam;
use App\Models\ContentSettings;
use App\Models\Substatus;
use App\Models\TypeToken;
use App\Models\Status;
use DB;
use App;

class ProjectsController extends Controller
{
  public function index()
  {
    $settings=$this->settings();
    //По сколько выводить
    $pagination=$settings['parameters']->projects_by_page;
    //$pagination=4;
    //локаль
    $locale = App::getLocale('locale');

    $projectsquery=Projects::select('projects.id as id', 'projects.name as name', 'logo', 'hard_cap', 'token', 'rating', 'collected',
        'categories.name_'.$locale.' as category', 'platforms.name as platform',   'is_adv', 'all_currency', 'projects.priority')
        ->selectraw('GET_SORT1(projects.id) AS sort1, GET_STATUS(projects.id) AS status, GET_STAGE(projects.id) AS stage ')
        ->leftjoin('categories', 'categories.id', '=', 'projects.category_id')
        ->leftjoin('project_prices', 'projects.id', '=', 'project_prices.project_id')
        ->leftjoin('platforms', 'platforms.id', '=', 'projects.platform_id')
        ->join('project_lifetime', 'project_lifetime.project_id', '=', 'projects.id')
        ->where('is_visible',1);
//платформа
    /*if(isset($_GET['platform']) && $_GET['platform']!=0 ) {
      $projectsquery->where('projects.platform_id', $_GET['platform']);
    }*/

    if(isset($_GET['platforms'])) {
      $sql=$this->or_sql($_GET['platforms'], 'projects.platform_id' );
      $projectsquery->whereraw($sql);
    }
//категория
    /*
    if(isset($_GET['category']) && $_GET['category']!=0 ) {
      $projectsquery->where('projects.category_id', $_GET['category']);
    }
*/
    if(isset($_GET['categorys'])) {
      $sql=$this->or_sql($_GET['categorys'], 'projects.category_id' );
      $projectsquery->whereraw($sql);
    }

//тип токена
    /*
    if(isset($_GET['token_type']) && $_GET['token_type']!=0 ) {
      $projectsquery->where('projects.type_token_id', $_GET['token_type']);
    }
    */

    if(isset($_GET['token_types'])) {
      $sql=$this->or_sql($_GET['token_types'], 'projects.type_token_id' );
      $projectsquery->whereraw($sql);
    }

//страна проекта
    /*
    if(isset($_GET['project_country']) && $_GET['project_country']!=0 ) {
      $projectsquery->where('projects.country_id', $_GET['project_country']);
    }    */

    if(isset($_GET['countrys'])) {
      $sql=$this->or_sql($_GET['countrys'], 'projects.country_id' );
      $projectsquery->whereraw($sql);
    }

//рейтинг
    if(isset($_GET['range-start']) && isset($_GET['range-end']) ) {
      $projectsquery->wherebetween('projects.rating', array($_GET['range-start'],$_GET['range-end']));
    }

    //юрисдикция
    if(isset($_GET['jurisdiction']) && $_GET['jurisdiction']=='on' ) {
      $projectsquery->where('projects.jurisdiction', 1);
    }

    if(isset($_GET['jurisdiction']) && $_GET['jurisdiction']=='off' ) {
      $projectsquery->where('projects.jurisdiction', 0);
    }

    $now=date("Y-m-d");
    $current_lifetime=null;
    /* закомментил старую версию
        if(isset($_GET['substatuss'])) {
          $sql='(';
          foreach ($_GET['substatuss'] as $key => $substatus) {
            //статус - активен
            if ($key==3) {
              $sql.='(project_lifetime.start <= \''.$now.'\' AND  (project_lifetime.end >= \''.$now.'\''.'  OR project_lifetime.is_tba=1) ) OR ';
            }
            //предстоящие - текущая дата меньше даты старта какого-либо этапа
            if ($key==4) {
              $sql.='(project_lifetime.start > \''.$now.'\' OR  project_lifetime.is_tba=1)  OR ';
            }
            //оконченные - текущая дата больше даты окончания
            if ($key==6) {
              $sql.='(project_lifetime.end <= \''.$now.'\')  OR ';
            }
          }
          $sql=substr($sql,0,-3 );//обрезаем последний OR
          $sql.=')';
          $projectsquery->whereraw($sql); //добавляем к запросу
        }


        //подстатус: Это пока не фильтрация, пока просто условием отсекаются явно не подходящие проекты
        if(isset($_GET['substatus']) && $_GET['substatus']!=0 ) {
          //активные - текущая дата между началом и окончанием какого-либо этапа
          if ($_GET['substatus']==1) {
            $projectsquery->where('project_lifetime.start','<=', $now)->where('project_lifetime.end','>=', $now);
          }
          //предстоящие - текущая дата меньше даты старта какого-либо этапа
          if ($_GET['substatus']==2) {
            $projectsquery->whereraw('project_lifetime.start > '.$now.' OR project_lifetime.start IS NULL');
          }
          //оконченные - текущая дата больше даты окончания
          if ($_GET['substatus']==3) {
            $projectsquery->where('project_lifetime.end','<=', $now);
          }
        }
    */
    //текущий этап
    $sql='(';
    $needor=false;
    if(isset($_GET['private']) ) {
      $sql.=' GET_STAGE(projects.id)=2 ';
      $needor=true;
    }
    if(isset($_GET['presale']) ) {
      if ($needor) {
        $sql.=' OR ';
      }
      $sql.=' GET_STAGE(projects.id)=3 ';
      $needor=true;
    }

    if(isset($_GET['sale']) ) {
      if ($needor) {
        $sql.=' OR ';
      }
      $sql.=' GET_STAGE(projects.id)=4 ';
    }

    if ($sql!='(') {
      $sql.=')';
      $projectsquery->whereraw($sql);
    }


    //статус этапа
    if(isset($_GET['substatuss'])) {
      $sql='(';
      foreach ($_GET['substatuss'] as $key => $substatus) {
        //статус - активен
        if ($key==3) {
          $sql.='( GET_STATUS(projects.id)=1 ) OR ';
        }
        //предстоящие - текущая дата меньше даты старта какого-либо этапа
        if ($key==4) {
          $sql.='( GET_STATUS(projects.id)=2 )  OR ';
        }
        //оконченные - текущая дата больше даты окончания
        if ($key==6) {
          $sql.='(GET_STATUS(projects.id)=3)  OR ';
        }
      }
      $sql=substr($sql,0,-3 );//обрезаем последний OR
      $sql.=')';
      $projectsquery->whereraw($sql); //добавляем к запросу
    }


    if(isset($_GET['accepteds'])) {
      $projectsquery->leftjoin('project_accept','projects.id', '=', 'project_accept.project_id');
      $sql=$this->or_sql($_GET['accepteds'], 'project_accept.cryptocoin_id' );
      $projectsquery->whereraw($sql);
    }


    if(isset($_GET['extras'])) {
      $projectsquery->leftjoin('project_extra','projects.id', '=', 'project_extra.project_id');
      $sql=$this->or_sql($_GET['extras'], 'project_extra.extra_id' );
      $projectsquery->whereraw($sql);
    }


    if(isset($_GET['restricteds'])) {
      $projectsquery->leftjoin('project_restricted_countries','projects.id', '=', 'project_restricted_countries.project_id');
      $sql=$this->or_sql($_GET['restricteds'], 'project_restricted_countries.country_id' );
      $projectsquery->whereraw($sql);
    }

//торговля

    if(isset($_GET['trading_ons'])) {
      $projectsquery->leftjoin('project_trading','projects.id', '=', 'project_trading.project_id');
      $sql=$this->or_sql($_GET['trading_ons'], 'project_trading.trading_id' );
      $projectsquery->whereraw($sql);
    }

//регистрация
    if(isset($_GET['kys']) ) {
      $projectsquery->where('projects.is_kyc', 1);
    }
    if(isset($_GET['kyt']) ) {
      $projectsquery->where('projects.is_kyt', 1);
    }
    if(isset($_GET['white_list']) ) {
      $projectsquery->where('projects.is_whitelist', 1);
    }
//поехали стандартные чекбоксы
    
//MVP
    if(isset($_GET['mvp']) && $_GET['mvp']=='on' ) {
      $projectsquery->where('projects.is_mvp', 1);
    }

//airdrop
    if(isset($_GET['airdrop']) && $_GET['airdrop']=='on' ) {
      $projectsquery->where('projects.is_airdrop', 1);
    }
    //bonus
    if(isset($_GET['bonus']) && $_GET['bonus']=='on' ) {
      $projectsquery->where('projects.is_bonus', 1);
    }
    //escrow
    if(isset($_GET['escrow']) && $_GET['escrow']=='on' ) {
      $projectsquery->where('projects.is_escrow', 1);
    }
    //bounty
    if(isset($_GET['bounty']) && $_GET['bounty']=='on' ) {
      $projectsquery->where('projects.is_bounty', 1);
    }
    //smart_contract
    if(isset($_GET['smart_contract']) && $_GET['smart_contract']=='on' ) {
      $projectsquery->where('projects.is_smartcontract', 1);
    }
    //etherscancheck
    if(isset($_GET['etherscancheck']) && $_GET['etherscancheck']=='on' ) {
      $projectsquery->where('projects.is_etherscancheck', 1);
    }
    //howey_test
    if(isset($_GET['howey_test']) && $_GET['howey_test']=='on' ) {
      $projectsquery->where('projects.is_howey_test', 1);
    }


//даты
    $need_tba_one=true; //флаг необходимости проверки TBA галочки отдельно (если даты указаны, то проверять не будем)
      if(isset($_GET['from']) && ($_GET['from']!='') ) {
        $need_tba_one=false;
        //если галочка TBA стоит, то будем проверять и TBA
        if (isset($_GET['tba'])) {
          $projectsquery->whereraw('project_lifetime.start >= \''.$_GET['from'].'\' OR project_lifetime.is_tba=1');
        }
        else {
          $projectsquery->where('project_lifetime.start','>=', $_GET['from']);
        }
      }

      if(isset($_GET['to']) && ($_GET['to']!='') ) {
        $need_tba_one=false;
        if (isset($_GET['tba'])) {
          $projectsquery->whereraw('project_lifetime.start <= \''.$_GET['to'].'\' OR project_lifetime.is_tba=1');
        }
        else {
          $projectsquery->where('project_lifetime.start','<=', $_GET['to']);
        }
      }

    if(isset($_GET['tba']) && $need_tba_one) {
      $projectsquery->where('project_lifetime.is_tba','=', 1);
    }

    $data_filter=array(
        'platform' => Platforms::select('platforms.id as id','platforms.name as name')
            ->rightjoin('projects', 'platforms.id', '=', 'projects.platform_id')->distinct()
            ->get(),
        'category' => Categories::select('categories.id as id','categories.name_'.$locale.' as name')
            ->rightjoin('projects', 'categories.id', '=', 'projects.category_id')->distinct()
            ->get(),
        'token_type'=>TypeToken::select('type_token.id as id','type_token.name as name')->join('projects', 'type_token.id', '=', 'projects.type_token_id')->distinct()
            ->get(),
        'extra' => Extra::select('extra.id as id','extra.name_'.$locale.' as name')
            ->join('project_extra', 'extra.id', '=', 'project_extra.extra_id')->distinct()
            ->get(),
        'restricted' => Countries::select('countries.id as id','countries.name_'.$locale.' as name')
            ->join('project_restricted_countries', 'countries.id', '=', 'project_restricted_countries.country_id')->distinct()
            ->get(),
        'substatus'=>Substatus::select('id','name_'.$locale.' AS name')->distinct()
            ->get(),
        'accepted'=>Cryptocoins::select('cryptocoins.id as id','cryptocoins.code as name')
            ->join('project_accept', 'cryptocoins.id', '=', 'project_accept.cryptocoin_id')->distinct()
            ->get(),
        'trading_on'=>Trading::select('trading.id as id','trading.name as name')
            ->join('project_trading', 'trading.id', '=', 'project_trading.trading_id')->distinct()
            ->get(),
        'country' => Countries::select('countries.id as id','countries.name_'.$locale.' as name')
            ->rightjoin('projects', 'countries.id', '=', 'projects.country_id')->distinct()
            ->get(),
    );

    $project_filters=App\Models\ProjectFilter::select('project_filter.id as id','project_filter.name_'.$locale.' as name',
        'project_filter.system_name','project_filter.priority','project_filter.type','project_filter.is_system', 'project_filter.is_visible_name'
    )->where('is_visible',1)->orderby('priority', 'DESC')->get();

    foreach ($project_filters as $project_filter) {
      //если не системный, то добавляем фильтрацию по его значению и запрашиваем значения для формирования фильтра
      if(!($project_filter->is_system)) {
        //если отображение - мультиселект
        if ($project_filter->type =='multiselect') {
          $getname=$project_filter['system_name'].'s';
          //пользовательские параметры
          if(isset($_GET[$getname])) {
            $projectsquery->leftjoin('project_filter_value_set as '.$getname,'projects.id', '=', $getname.'.project_id');
            $sql=$this->or_sql($_GET[$getname], $getname.'.project_filter_value_id' );
            $projectsquery->whereraw($sql);
          }
          $values=App\Models\ProjectFilterValue::select('project_filter_value.id as id','project_filter_value.name_'.$locale.' as name', 'project_filter_value.priority as priority')
              ->where('project_filter_value.project_filter_id', '=',$project_filter->id )
              ->orderby('priority','DESC')
              ->get();
          $data_filter[$project_filter['system_name']]=$values;
        }
        //если отображение - чекбокс
        elseif ($project_filter->type =='onecheckbox') {
          $getname=$project_filter['system_name'];
          //пользовательские параметры
          if(isset($_GET[$getname]) && $_GET[$getname]=='on') {
            $projectsquery->leftjoin('project_filter_value_set as '.$getname,'projects.id', '=', $getname.'.project_id')
                ->join('project_filter_value as value_'.$getname,$getname.'.project_filter_value_id' ,'=','value_'.$getname.'.id')
                ->where('value_'.$getname.'.project_filter_id','=',$project_filter->id);
          }
        }

      }
    }

    $projectsquery->orderby('sort1')->orderby('projects.priority', 'DESC')->orderby('status')->orderby('stage')->orderby('id')->distinct('projects.id');
    //считаем сколько всего проектов к выводу
    $projects_count=$projectsquery->count('projects.id');
    //ограничеваем вывод пагинацией
    $projectsquery->limit($pagination);

    //обработка пагинации страниц
    if (isset($_GET['page'])) {$page=$_GET['page'];} else {$page=1;}

    //задаем смещение
    $projectsquery->offset(($page - 1) * $pagination);
    //получаем проекты
    $projects=$projectsquery->get();
    //определяем текущие статусы и подстатусы всех полученных проектов
    //определяем для каждого проекта текущий статус
    $all_projects=array();
    foreach ($projects as $project) {
      $current_lifetime=$project->currentlifetime();
      array_push($all_projects,array('project'=>$project, 'current_lifetime'=>$current_lifetime));
      /*
      //если статус и субстатус в фильтре установлены и они совпадают с текущими, то помещаем его в результаты выдачи
      if (isset($_GET['substatuss']) && (isset($_GET['private']) || isset($_GET['presale']) || isset($_GET['sale'])) ) {
        $need_add=false;
        if (array_key_exists($current_lifetime['substatus'],$_GET['substatuss']))  {
          if (isset($_GET['private']) && $current_lifetime['stage'] == 3) {
            $need_add = true;
          };
          if (isset($_GET['presale']) && $current_lifetime['stage'] == 2) {
            $need_add = true;
          };
          if (isset($_GET['sale']) && $current_lifetime['stage'] == 4) {
            $need_add = true;
          };
        }
        if ($need_add) {array_push($all_projects,array('project'=>$project, 'current_lifetime'=>$current_lifetime));}
      }
      //иначе - если установлен только субстатус и он сопадает, то включаем
      elseif ((isset($_GET['substatuss']))) {
        if (array_key_exists($current_lifetime['substatus'],$_GET['substatuss'])) {array_push($all_projects,array('project'=>$project, 'current_lifetime'=>$current_lifetime));}
      }
      //иначе - если устанлен только один из статусов
      elseif (isset($_GET['private']) || isset($_GET['presale']) || isset($_GET['sale'])) {
        $need_add=false;
        if (isset($_GET['private']) && $current_lifetime['stage']==3 ) {$need_add=true;};
        if (isset($_GET['presale']) && $current_lifetime['stage']==2 ) {$need_add=true;};
        if (isset($_GET['sale']) && $current_lifetime['stage']==4 ) {$need_add=true;};
        //если надо добавлять
        if ($need_add) {
          array_push($all_projects,array('project'=>$project, 'current_lifetime'=>$current_lifetime));
        }
      }
      //иначе (если в фильтре не установлены статус и субстатус, то будем помещать все, с учетом состояния текущего жизненного цикла)
      else {
        //если TBA active, то в массив перед законченными upcoming
        if ($current_lifetime['substatus']==3 && $current_lifetime['date_string']=='') {
          array_push($all_projects_active_tba,array('project'=>$project, 'current_lifetime'=>$current_lifetime));
        }
        //если active
        elseif ($current_lifetime['substatus']==3) {
          array_push($all_projects_active,array('project'=>$project, 'current_lifetime'=>$current_lifetime));
        }
        //если TBA upcoming, то в массив перед законченными проектами
        elseif ($current_lifetime['substatus']==4 && $current_lifetime['date_string']=='') {
          array_push($all_projects_upcoming_tba,array('project'=>$project, 'current_lifetime'=>$current_lifetime));
        }
        //если upcoming
        elseif ($current_lifetime['substatus']==4) {
          array_push($all_projects_upcoming,array('project'=>$project, 'current_lifetime'=>$current_lifetime));
        }
        //если закончен, то во временный массив, потом вконце его прилепим к основному
        elseif ($current_lifetime['substatus']==6) {
          array_push($all_projects_ended,array('project'=>$project, 'current_lifetime'=>$current_lifetime));
        }

        else {
          array_push($all_projects,array('project'=>$project, 'current_lifetime'=>$current_lifetime));
        }
      }*/
    }
    //соединяем все массивы
    //$all_projects=array_merge($all_projects,$all_projects_active,$all_projects_active_tba,$all_projects_upcoming,$all_projects_upcoming_tba,$all_projects_ended);
    //считаем сколько получилось
    $count = count($all_projects);
    //обработка пагинации страниц
    if (isset($_GET['page'])) {
      return view('projects_ajax',[
          'projects' => $all_projects,
          'count' => $projects_count,
      ]);
    }
    
    return view('projects',[
      'projects' => $all_projects,

      'substatuses' => Substatus::select('substatus.id as id','substatus.name_'.$locale.' as name')
          ->leftjoin('projects', 'substatus.id', '=', 'projects.substatus_id')->distinct()
          ->get(),

      'count' => $projects_count,
      'page'=>$page,
      'pagination'=>$pagination,
      'main_metodology'=>ContentSettings::select('text_'.$locale.' AS text')->where('name','=','home_metodology')->where('is_visible',1)->first(),
      'project_filters'=>$project_filters,
      'data_filter'=>$data_filter,
      'settings'=>$settings,

    ]);

  }

  public function show($id)
  {
    $locale = App::getLocale('locale');

    $project=Projects::select('projects.id as id', 'projects.status_id', 'projects.jurisdiction',  'projects.is_mvp', 'projects.is_escrow', 'type_token.name as type_token','is_smartcontract', 'is_whitelist', 'is_kyc', 'countries.name_'.$locale.' as country',
        'projects.roi', 'project_prices.for_sale', 'projects.name as name', 'logo', 'token', 'rating', 'coordinates', 'categories.name_'.$locale.' as category',
        'platforms.name as platform', 'status.name_'.$locale.' as status', 'substatus.name_'.$locale.' as substatus',
        'is_adv', 'info_'.$locale.' as info', 'about_us_'.$locale.' as about_us', 'other_'.$locale.' as other',
        'min_cap', 'soft_cap', 'hard_cap', 'token_price','pre_token_price','collected', 'project_accept.code as currency', 'project_trading.name as trading', 'project_extra.name as extra',
        'project_restricted_countries.name as restricted', 'is_bonus', 'is_airdrop', 'all_currency', 'road_map', 'is_bounty', 'is_kyt', 'is_howey_test', 'is_etherscancheck', 'is_adv')
        ->leftjoin('categories', 'categories.id', '=', 'projects.category_id')
        ->leftjoin('platforms', 'platforms.id', '=', 'projects.platform_id')
        ->leftjoin('status', 'status.id', '=', 'projects.status_id')
        ->leftjoin('substatus', 'substatus.id', '=', 'projects.substatus_id')
        ->leftjoin('type_token', 'type_token.id', '=', 'projects.type_token_id')
        ->leftjoin('project_description', 'project_description.project_id', '=', 'projects.id')
        ->leftjoin('countries', 'countries.id', '=', 'projects.country_id')
        ->leftjoin('project_prices', 'project_prices.project_id', '=', 'projects.id')
        ->leftjoin(DB::raw("(SELECT
          project_accept.project_id,
          GROUP_CONCAT(cryptocoins.code SEPARATOR ', ') as code
          FROM project_accept
          JOIN cryptocoins ON project_accept.cryptocoin_id = cryptocoins.id
          GROUP BY project_accept.project_id
          ) as project_accept"),function($join){
          $join->on("projects.id","=","project_accept.project_id");
        })
        ->leftjoin(DB::raw("(SELECT
          project_trading.project_id,
          GROUP_CONCAT(trading.name SEPARATOR ', ') as name
          FROM project_trading
          JOIN trading ON project_trading.trading_id = trading.id
          GROUP BY project_trading.project_id
          ) as project_trading"),function($join){
          $join->on("projects.id","=","project_trading.project_id");
        })
        ->leftjoin(DB::raw("(SELECT
          project_extra.project_id,
          GROUP_CONCAT(extra.name_".$locale." SEPARATOR ', ') as name
          FROM project_extra
          JOIN extra ON project_extra.extra_id = extra.id
          GROUP BY project_extra.project_id
          ) as project_extra"),function($join){
          $join->on("projects.id","=","project_extra.project_id");
        })
        ->leftjoin(DB::raw("(SELECT
          project_restricted_countries.project_id,
          GROUP_CONCAT(countries.name_".$locale." SEPARATOR ', ') as name
          FROM project_restricted_countries
          JOIN countries ON project_restricted_countries.country_id = countries.id
          GROUP BY project_restricted_countries.project_id
          ) as project_restricted_countries"),function($join){
          $join->on("projects.id","=","project_restricted_countries.project_id");
        })
        ->where('projects.id',$id)->where('projects.is_visible',1)->first();


    if (!($project)) {
        return abort(404);
    }

    //этапы жизненного цикла
    $project_lifetimes=ProjectLifetime::select('project_lifetime.id as id', 'status.name_'.$locale.' as name', 'project_lifetime.start', 'project_lifetime.end', 'project_lifetime.substatus', 
        'project_lifetime.price', 'project_lifetime.min_invest', 'project_lifetime.price_currency_id', 'project_lifetime.min_invest_currency_id', 'project_lifetime.project_id')
        ->leftjoin('status','project_lifetime.status_id', '=', 'status.id')
        ->where('project_lifetime.project_id',$id)->orderby('start','asc')->get();

    
    $current_lifetime=$project->currentlifetime();

    $next_lifetime=ProjectLifetime::select('project_lifetime.id as id', 'status.name_'.$locale.' as name', 'project_lifetime.start', 'project_lifetime.end', 'project_lifetime.substatus',
        'project_lifetime.price', 'project_lifetime.min_invest', 'project_lifetime.price_currency_id', 'project_lifetime.min_invest_currency_id', 'project_lifetime.project_id')
        ->leftjoin('status','project_lifetime.status_id', '=', 'status.id')
        ->where('project_lifetime.project_id',$id)
        ->where('project_lifetime.id', '>',$current_lifetime['project_lifetime']->id)
        ->first();
    
    /*
        foreach ($project_lifetimes as $project_lifetime) {
          //если еще не началось
          if ($project_lifetime['start']>$now) {
            $datestring=date('Y, m-1, d',strtotime($project_lifetime['start']));
            if($locale=='ru') { $status='Предстоящий'; }else {$status='Upcoming';}
            $current_lifetime=array('project_lifetime'=>$project_lifetime, 'status'=>$status, 'date_string'=>$datestring);
          }
          elseif ($project_lifetime['end']>=$now) {
            $datestring=date('Y, m-1, d+1',strtotime($project_lifetime['end']));
            if($locale=='ru') { $status='Активен'; }else {$status='Active';}
            $current_lifetime=array('project_lifetime'=>$project_lifetime, 'status'=>$status, 'date_string'=>$datestring);       
          }
          else {
            $datestring='';
            if($locale=='ru') { $status='Закончен'; }else {$status='Ended';}
            $current_lifetime=array('project_lifetime'=>$project_lifetime, 'status'=>$status, 'date_string'=>$datestring);
          }
        }*/
    $all_more_projects=array();
    $all_add_more_projects=array();
    $now=date("Y-m-d");
    //если проплачен
    if ($project->is_adv) {
      $all_more_projects=array();
    }
    else {
      $more_projects=Projects::select('projects.id as id', 'projects.name as name', 'logo', 'hard_cap', 'collected', 'rating', 'categories.name_'.$locale.' as category',  'is_adv', 'all_currency')
          ->selectraw('GET_SORT1(projects.id) AS sort1, GET_STATUS(projects.id) AS status ')
          ->leftjoin('categories', 'categories.id', '=', 'projects.category_id')
          ->leftjoin('project_prices', 'projects.id', '=', 'project_prices.project_id')
          ->leftjoin('project_lifetime', 'project_lifetime.project_id', '=', 'projects.id')
          ->where('is_visible',1)
          ->where('projects.id', '<>', $id)
          ->orderby('sort1') //сначала рекламные и незавершенные
          ->orderby('status')
          ->inRandomOrder()->distinct()->limit(3)->get();

      //определяем для каждого проекта текущий статус
      foreach ($more_projects as $more_project) {
        /*//этапы жизненного цикла
        $more_project_lifetimes=ProjectLifetime::select('project_lifetime.id as id', 'status.name_'.$locale.' as name', 'project_lifetime.start', 'project_lifetime.end', 'project_lifetime.price', 'project_lifetime.min_invest')
            ->leftjoin('status','project_lifetime.status_id', '=', 'status.id')
            ->where('project_lifetime.project_id',$more_project->id)->orderby('start','asc')->get();

        $now=date("Y-m-d");
        $more_current_lifetime=null;
        foreach ($more_project_lifetimes as $project_lifetime) {
          //если еще не началось
          if ($project_lifetime['start']>$now) {
            if($locale=='ru') { $status='Предстоящий'; }else {$status='Upcoming';}
          }
          elseif ($project_lifetime['end']>=$now) {
            if($locale=='ru') { $status='Активен'; }else {$status='Active';}
          }
          else {
            if($locale=='ru') { $status='Закончен'; }else {$status='Ended';}
          }
          $more_current_lifetime=array('project_lifetime'=>$project_lifetime, 'status'=>$status);
        }*/
        array_push($all_more_projects,array('project'=>$more_project, 'current_lifetime'=>$more_project->currentlifetime()));
      }
    }

    
    

    return view('project',[
      'project' => $project,
      /*'project_teams' => ProjectTeam::select('team_group.name_'.$locale.' as team_group_name', 'team_group.id as team_group_id',  'professions.name_'.$locale.' as profession', 'project_team.id as team_id', 'project_team.name_'.$locale.' as name', 'avatar')
          ->leftjoin('professions', 'professions.id', '=', 'project_team.profession_id')
          ->leftjoin('team_group', 'team_group.id', '=', 'project_team.group_id')
          ->where('project_team.project_id',$id)
          ->orderby('team_group_id')
          ->get(),*/
      'project_socials' => ProjectSocial::select('socials.icon', 'socials.name', 'link')->leftjoin('socials', 'socials.id', '=', 'project_social.social_id')->where('project_social.project_id',$id)->get(),
      'project_restricted_countries' => ProjectRestrictedCoutries::where('project_id',$id)->select(DB::raw('GROUP_CONCAT(`country_id`) as country_id'))->first(),
      'project_links' => ProjectLinks::select( 'name_'.$locale.' as name', 'link')->where('project_id',$id)->get(),
      'project_bonuses' => ProjectBonuses::select( 'stage as stage', 'bonus')->where('project_id',$id)->get(),
      'project_custom_rating'=> ProjectCustomRating::select('custom_rating.name_'.$locale.' as name', 'rating')->leftjoin('custom_rating', 'custom_rating.id', '=', 'project_custom_rating.custom_rating_id')->where('project_custom_rating.project_id',$id)->get(),
      'project_agencies_rating' => ProjectAgenciesRating::where('project_id',$id)->get(),
      'project_lifetimes'=>$project_lifetimes,
      'current_lifetime'=>$current_lifetime,
      'next_lifetime'=>$next_lifetime,
      'more_projects'=>$all_more_projects,
      'settings'=>$this->settings(),
      'main_metodology'=>ContentSettings::select('text_'.$locale.' AS text')->where('name','=','home_metodology')->where('is_visible',1)->first(),
    ]);
  }

}
