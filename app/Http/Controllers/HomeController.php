<?php

namespace App\Http\Controllers;

use App\Models\ContentSettings;
use App\Models\ProjectLifetime;
use Illuminate\Http\Request;
use App\Models\Projects;
use App\Models\ProjectAgenciesRating;
use App\Models\AgenciesRating;

class HomeController extends Controller
{


    public function index()
    {
      $locale = \App::getLocale();
      $settings = $this->settings();
      if ($settings['parameters']->slider_max_projects) {$max_sliders=$settings['parameters']->slider_max_projects;} else {$max_sliders=5;}
      //для рейтинга
      $projects_temp = Projects::select('projects.id as id','name','logo','rating')->join('project_lifetime', 'project_lifetime.project_id', '=', 'projects.id')
          ->where('is_visible',1)->orderBy('projects.created_at','desc')->offset(0)->limit(10)->get()->toArray();
      $projects = [];
      foreach ($projects_temp as $project) {
        $projects[$project['id']]['id'] = $project['id'];
        $projects[$project['id']]['name'] = $project['name'];
        $projects[$project['id']]['logo'] = $project['logo'];
        $projects[$project['id']]['rating'] = $project['rating'];
        $project_rating = ProjectAgenciesRating::select('agency_rating_id','rating')
                                          ->where('project_id',$project['id'])
                                          ->get()->toArray();
        foreach ($project_rating as $value) {
          $projects[$project['id']]['agency'][$value['agency_rating_id']] = $value['rating'];
        }
      };
      //слайдер
      $sliders_temp=Projects::select('projects.id','projects.name','projects.logo','categories.name_'.$locale.' as category','projects.created_at',
          'project_prices.collected', 'project_prices.hard_cap', 'slider_'.$locale.' as slider','all_currency')
          ->leftjoin('categories', 'categories.id', '=', 'projects.category_id')
          ->leftjoin('project_prices', 'project_prices.project_id', '=', 'projects.id')
          ->leftjoin('project_description', 'project_description.project_id', '=', 'projects.id' )
          ->join('project_lifetime', 'project_lifetime.project_id', '=', 'projects.id')
          ->where('is_visible',1)
          ->where('is_slider',1)
          ->orderBy('projects.created_at','desc')
          ->offset(0)->limit($max_sliders)->distinct()
          ->get();

      //определяем дату окончания продаж
      $now=date("Y-m-d");
      $sliders=array();
      foreach ($sliders_temp as $slider) {
        //текущий этап жизненного цикла проекта
        $current_lifetime=$slider->currentlifetime();

        /*$project_lifetime=ProjectLifetime::select('project_lifetime.id as id', 'status.name_'.$locale.' as name', 'project_lifetime.start', 'project_lifetime.end', 'project_lifetime.price', 'project_lifetime.min_invest')
            ->leftjoin('status','project_lifetime.status_id', '=', 'status.id')
            ->where('project_lifetime.project_id',$slider['id'])->orderby('end','desc')->first();*/
          //если идет
          if ($current_lifetime['substatus']==1) {
            //считаем сколько до окончания
            if ($current_lifetime['date_string']=='') {
              if($locale=='ru') { $sale_ended=$current_lifetime['name'].': анонсирован'; }else {$sale_ended=$current_lifetime['name'].': TBA';}
            }
            else {
              $days_before_end=round(((strtotime($current_lifetime['project_lifetime']['end']) - strtotime($now))/60/60/24));
              if($locale=='ru') { $sale_ended=$current_lifetime['name'].' закончится через: '.$days_before_end.' <span>ДНЕЙ</span>'; }else {$sale_ended=$current_lifetime['name'].' ends in: '.$days_before_end.' <span>DAYS</span>';}

            }
          }
          //если upcoming
          elseif ($current_lifetime['substatus']==2) {
            //если TBA
            if ($current_lifetime['date_string']=='') {
              if($locale=='ru') { $sale_ended=$current_lifetime['name'].': анонсирован'; }else {$sale_ended=$current_lifetime['name'].': TBA';}
            }
            //иначе -
            else {
              //считаем сколько до окончания
              $days_before_begin=round(((strtotime($current_lifetime['project_lifetime']['start'])-strtotime($now))/60/60/24));
              if($locale=='ru') { $sale_ended=$current_lifetime['name'].' начнется через: '.$days_before_begin.' <span>ДНЕЙ</span>'; }else {$sale_ended=$current_lifetime['name'].' start in: '.$days_before_begin.' <span>DAYS</span>';}
            }
          }
          //иначе - будем выводить дату окончания
          else {
            if($locale=='ru') { $sale_ended='Продажа токенов закончена: <span>'.$current_lifetime['project_lifetime']['end'].'</span>'; }else {$sale_ended='Token Sale ends at: <span>'.$current_lifetime['project_lifetime']['end'].'</span>';}
          }
        array_push($sliders,array('project'=>$slider, 'sale_ended'=>$sale_ended));
      }


      //$now="'".$now."'";
      //считаем количество проектов по статусам
      //активные проекты - есть этап, который не кончился (или TBA), но уже начался
      $count_active=Projects::select('projects.id')
          ->join('project_lifetime','project_lifetime.project_id', '=', 'projects.id')
          ->where('is_visible',1)
          ->whereraw('(project_lifetime.end >=\''.$now.'\' OR project_lifetime.is_tba=1) ')
          ->where('project_lifetime.start', '<=', $now)
          ->distinct()->get()->toArray();
      
      //все проекты
      $count_all=Projects::select('projects.id')
          ->join('project_lifetime','project_lifetime.project_id', '=', 'projects.id')
          ->where('is_visible',1)
          ->distinct()->get()->toArray();
      //проекты, у которы есть незаконченные этапы или TBA
      $count_not_ended=Projects::select('projects.id')
          ->join('project_lifetime','project_lifetime.project_id', '=', 'projects.id')
          ->where('is_visible',1)
          ->whereraw('(project_lifetime.end >= \''.$now.'\' OR project_lifetime.is_tba=1)')
          ->distinct()->get()->toArray();
      //завершенные проекты: все проекты минус те проекты, у которых есть назаконченные этапы
      $count_ended=count($count_all)-count($count_not_ended);
      //предстоящие проекты: всепроекты, минус завершенные и минус активные
      $count_upcoming=count($count_all)-$count_ended-count($count_active);
      return view('home',[
        'projects' => $projects,
        'sliders' => $sliders,
        'agencies' => AgenciesRating::where('is_main',1)->get(),
          'count_active' =>count($count_active),
          'count_upcoming' =>$count_upcoming,
          'count_ended' =>$count_ended,
          'settings'=>$settings,
          'main_metodology'=>ContentSettings::select('text_'.$locale.' AS text')->where('name','=','home_metodology')->where('is_visible',1)->first(),

      ]);
    }
  
}
