<?php

namespace App\Http\Controllers;
use Cookie;
use Illuminate\Http\Request;

class LocaleController extends Controller
{
  public function locale($locale)
  {
      if (in_array($locale, \Config::get('app.locales'))) {
          Cookie::queue(Cookie::make('locale', $locale, '43200'));
      }
      return redirect()->back();
  }
}
