<?php

namespace App\Http\Controllers\invi;


use App\Models\AgenciesRating;
use App\Models\Categories;
use App\Models\Countries;
use App\Models\Cryptocoins;
use App\Models\CustomRating;
use App\Models\Extra;
use App\Models\Locales;
use App\Models\Platforms;
use App\Models\Professions;
use App\Models\ProjectBlock;
use App\Models\ProjectBonuses;
use App\Models\ProjectExtra;
use App\Models\ProjectFilterValueSet;
use App\Models\Projects;
use App\Models\ProjectAccept;
use App\Models\ProjectAgenciesRating;
use App\Models\ProjectCustomRating;
use App\Models\ProjectDescription;
use App\Models\ProjectLifetime;
use App\Models\ProjectLinks;
use App\Models\ProjectLocation;
use App\Models\ProjectPrices;
use App\Models\ProjectRestrictedCoutries;
use App\Models\ProjectSocial;
use App\Models\ProjectTeam;
use App\Models\ProjectTrading;
use App\Models\Socials;
use App\Models\Status;
use App\Models\Substatus;
use App\Models\TeamGroup;
use App\Models\Trading;
use App\Models\TypeToken;
use App\Models\ProjectFilter;
use DB;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('invi.projects',[
        'projects' => Projects::select()->get()
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('invi.project_create',[
        'agencies_ratings' => AgenciesRating::select()->orderBy('name','asc')->get(),
        'categories' => Categories::select()->orderBy('name_ru','asc')->get(),
        'countries' => Countries::select()->orderBy('name_ru','asc')->get(),
        'cryptocoins' => Cryptocoins::select()->orderBy('name','asc')->get(),
        'customratings' => CustomRating::select()->orderBy('name_ru','asc')->get(),
        'platforms' => Platforms::select()->orderBy('name','asc')->get(),
        'socials' => Socials::select()->orderBy('name','asc')->get(),
        'statuses' => Status::select()->orderBy('name_ru','asc')->get(),
        'substatuses' => Substatus::select()->orderBy('name_ru','asc')->get(),
        'type_tokens' => TypeToken::select()->orderBy('name','asc')->get(),
        'tradings' => Trading::select()->orderBy('name','asc')->get(),
        'extras' => Extra::select()->orderBy('name_ru','asc')->get(),
        'team_groups' => TeamGroup::select()->orderBy('id','asc')->get(),
      ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->all();

        $project = Projects::create([
            'name' => $data['name'],
            'logo' => $data['logo'],
            'road_map' => $data['road_map'],
            'token' => $data['token'],
            'rating' => $data['rating'],
            'category_id' => $data['category_id'],
            'country_id' => $data['country_id'],
            'platform_id' => $data['platform_id'],
            'jurisdiction' => $data['jurisdiction'],
            'is_slider' => $data['is_slider'],
            'is_adv' => $data['is_adv'],
            'is_whitelist' => $data['is_whitelist'],
            'is_kyc' => $data['is_kyc'],
            'is_smartcontract' => $data['is_smartcontract'],
            'is_visible' => $data['is_visible'],
            'is_airdrop' => $data['is_airdrop'],
            'is_bonus' => $data['is_bonus'],
            'roi'=>$data['roi'],
            'is_mvp'=>$data['is_mvp'],
            'is_escrow'=>$data['is_escrow'],
            'all_currency'=>$data['all_currency'],
            'type_token_id'=>$data['type_token_id'],
            'is_kyt'=>$data['is_kyt'],
            'is_bounty'=>$data['is_bounty'],
            'is_etherscancheck'=>$data['is_etherscancheck'],
            'is_howey_test'=>$data['is_howey_test'],
            'priority' => $data['priority'],
        ]);

        if(isset($data['accept'])){
          foreach ($data['accept'] as $accept) {
            ProjectAccept::create([
              'project_id' => $project->id,
              'cryptocoin_id' => $accept
            ]);
          };
        }

        if(isset($data['restricted_countries'])){
          foreach ($data['restricted_countries'] as $restricted_country) {
            ProjectRestrictedCoutries::create([
              'project_id' => $project->id,
              'country_id' => $restricted_country
            ]);
          }
        }

        if(isset($data['project_tradings'])){
            foreach ($data['project_tradings'] as $project_trading) {
                ProjectTrading::create([
                    'project_id' => $project->id,
                    'trading_id' => $project_trading
                ]);
            }
        }

        if(isset($data['project_extra'])){
            foreach ($data['project_extra'] as $extra) {
                ProjectExtra::create([
                    'project_id' => $project->id,
                    'extra_id' => $extra
                ]);
            };
        }

        ProjectPrices::create([
            'project_id' => $project->id,
        ]);


        ProjectDescription::create([
          'project_id' => $project->id
        ]);

        return redirect()->route('projects.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Projects  $projects
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

      return view('invi.project',[
        'project' => Projects::where('id',$id)->first(),
        'project_team' => ProjectTeam::where('project_id',$id)->get(),
        'project_social' => ProjectSocial::where('project_id',$id)->get(),
        'project_restricted_countries' => ProjectRestrictedCoutries::where('project_id',$id)->select(DB::raw('GROUP_CONCAT(`country_id`) as country_id'))->first(),
        'project_prices' => ProjectPrices::where('project_id',$id)->first(),
        'project_links' => ProjectLinks::where('project_id',$id)->get(),
        'project_bonuses' => ProjectBonuses::where('project_id',$id)->get(),
        'project_blocks' => ProjectBlock::where('project_id',$id)->orderby('priority', 'DESC')->get(),
        'project_lifetime' => ProjectLifetime::where('project_id',$id)->get(),
        'project_description' => ProjectDescription::where('project_id',$id)->first(),
        'project_custom_rating'=> ProjectCustomRating::where('project_id',$id)->get(),
        'project_agencies_rating' => ProjectAgenciesRating::where('project_id',$id)->get(),
        'project_accept' => ProjectAccept::where('project_id',$id)->select(DB::raw('GROUP_CONCAT(`cryptocoin_id`) as cryptocoin_id'))->first(),
        'project_tradings' => ProjectTrading::where('project_id',$id)->select(DB::raw('GROUP_CONCAT(`trading_id`) as trading_id'))->first(),
        'project_extra' => ProjectExtra::where('project_id',$id)->select(DB::raw('GROUP_CONCAT(`extra_id`) as extra_id'))->first(),
        'agencies_ratings' => AgenciesRating::select()->orderBy('name','asc')->get(),
        'categories' => Categories::select()->orderBy('name_ru','asc')->get(),
        'countries' => Countries::select()->orderBy('name_ru','asc')->get(),
        'cryptocoins' => Cryptocoins::select()->orderBy('name','asc')->get(),
        'customratings' => CustomRating::select()->orderBy('name_ru','asc')->get(),
        'platforms' => Platforms::select()->orderBy('name','asc')->get(),
        'socials' => Socials::select()->orderBy('name','asc')->get(),
        'statuses' => Status::select()->orderBy('name_ru','asc')->get(),
        'substatuses' => Substatus::select()->orderBy('name_ru','asc')->get(),
        'type_tokens' => TypeToken::select()->orderBy('name','asc')->get(),
        'tradings' => Trading::select()->orderBy('name','asc')->get(),
        'extras' => Extra::select()->orderBy('name_ru','asc')->get(),
        'team_groups' => TeamGroup::select()->orderBy('id','asc')->get(),
        'project_filters' => ProjectFilter::select()->where('is_system',0)->orderBy('name_ru','asc')->get(),
      ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Projects  $projects
     * @return \Illuminate\Http\Response
     */
    public function edit(Projects $projects)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Projects  $projects
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

              $data = $request->all();

              $project = Projects::where('id',$id)->update([
                'name' => $data['name'],
                'logo' => $data['logo'],
                'road_map' => $data['road_map'],
                'token' => $data['token'],
                'rating' => $data['rating'],
                'category_id' => $data['category_id'],
                'platform_id' => $data['platform_id'],
                'country_id' => $data['country_id'],
                'jurisdiction' => $data['jurisdiction'],
                'is_slider' => $data['is_slider'],
                'is_adv' => $data['is_adv'],
                'is_whitelist' => $data['is_whitelist'],
                'is_kyc' => $data['is_kyc'],
                'is_smartcontract' => $data['is_smartcontract'],
                'is_visible' => $data['is_visible'],
                'is_airdrop' => $data['is_airdrop'],
                'is_bonus' => $data['is_bonus'],
                'roi'=>$data['roi'],
                'is_mvp'=>$data['is_mvp'],
                'is_escrow'=>$data['is_escrow'],
                'all_currency'=>$data['all_currency'],
                'type_token_id'=>$data['type_token_id'],
                'is_kyt'=>$data['is_kyt'],
                'is_bounty'=>$data['is_bounty'],
                'is_etherscancheck'=>$data['is_etherscancheck'],
                'is_howey_test'=>$data['is_howey_test'],
                'priority' => $data['priority'],
              ]);

              if(isset($data['accept'])){
                ProjectAccept::where('project_id',$id)->delete();
                foreach ($data['accept'] as $accept) {
                  ProjectAccept::create([
                    'project_id' => $id,
                    'cryptocoin_id' => $accept
                  ]);
                };
              }

              if(isset($data['restricted_countries'])){
                ProjectRestrictedCoutries::where('project_id',$id)->delete();
                foreach ($data['restricted_countries'] as $restricted_country) {
                  ProjectRestrictedCoutries::create([
                    'project_id' => $id,
                    'country_id' => $restricted_country
                  ]);
                }
              }
        
            if(isset($data['project_tradings'])){
                ProjectTrading::where('project_id',$id)->delete();
                foreach ($data['project_tradings'] as $project_trading) {
                    ProjectTrading::create([
                        'project_id' => $id,
                        'trading_id' => $project_trading
                    ]);
                }
            }

            if(isset($data['project_extra'])){
                ProjectExtra::where('project_id',$id)->delete();
                foreach ($data['project_extra'] as $extra) {
                    ProjectExtra::create([
                        'project_id' => $id,
                        'extra_id' => $extra
                    ]);
                };
            }
        

              ProjectPrices::where('project_id',$id)->update([
                'collected' => $data['collected'],
                'soft_cap' => $data['soft_cap'],
                'hard_cap' => $data['hard_cap'],
                'for_sale' => $data['for_sale'],
                'soft_cap_currency_id' => $data['soft_cap_currency_id'],
                'hard_cap_currency_id' => $data['hard_cap_currency_id'],
                'collected_currency_id' => $data['collected_currency_id'],
                'for_sale_currency_id' => $data['for_sale_currency_id'],
              ]);

              if(isset($data['social'])){
                ProjectSocial::where('project_id',$id)->delete();
                foreach ($data['social'] as $social) {
                  if($social['link'] != NULL){
                    ProjectSocial::create([
                      'project_id' => $id,
                      'social_id' => $social['social_id'],
                      'link' => $social['link']
                    ]);
                  }
                };
              }

              ProjectDescription::where('project_id',$id)->update([
                'info_ru' => $data['info_ru'],
                'about_us_ru' => $data['about_us_ru'],
                'slider_ru' => $data['slider_ru'],
                'info_en' => $data['info_en'],
                'about_us_en' => $data['about_us_en'],
                'slider_en' => $data['slider_en']
              ]);

              if(isset($data['team'])){
                ProjectTeam::where('project_id',$id)->delete();
                foreach ($data['team'] as $i => $team) {
                  if($team['name_ru'] != NULL && $team['name_en'] != NULL){
                    ProjectTeam::create([
                      'project_id' => $id,
                      'name_ru' => $team['name_ru'],
                      'name_en' => $team['name_en'],
                      'avatar' => $team['avatar'],
                      'profession_ru' => $team['profession_ru'], 
                      'profession_en' => $team['profession_en'],
                      'group_id' => $team['group_id']
                    ]);
                  }
                };
              }


              if(isset($data['agency_rating'])){
                ProjectAgenciesRating::where('project_id',$id)->delete();
                foreach ($data['agency_rating'] as $agency_rating) {
                  if($agency_rating['rating'] != NULL){
                    ProjectAgenciesRating::create([
                      'project_id' => $id,
                      'agency_rating_id' => $agency_rating['agency_rating_id'],
                      'rating' => $agency_rating['rating']
                    ]);
                  }
                };
              }


              if(isset($data['custom_rating'])){
                ProjectCustomRating::where('project_id',$id)->delete();
                foreach ($data['custom_rating'] as $custom_rating) {
                  if($custom_rating['rating'] != NULL){
                    ProjectCustomRating::create([
                      'project_id' => $id,
                      'custom_rating_id' => $custom_rating['custom_rating_id'],
                      'rating' => $custom_rating['rating']
                    ]);
                  }
                };
              }

              if(isset($data['timeline'])){
                ProjectLifetime::where('project_id',$id)->delete();
                foreach ($data['timeline'] as $timeline) {
                  if($timeline['status_id'] != NULL){
                     if ($timeline['start'] != NULL && $timeline['end'] != NULL)  {$tba=0;} else {$tba=1;}

                    ProjectLifetime::create([
                      'project_id' => $id,
                      'start' => $timeline['start'],
                      'end' => $timeline['end'],
                      'gmt' => $timeline['gmt'],
                      'status_id' => $timeline['status_id'],
                      'substatus' => $timeline['substatus'],
                      'price' => $timeline['price'],
                      'min_invest' => $timeline['min_invest'],
                      'price_currency_id' => $timeline['price_currency_id'],
                      'min_invest_currency_id' => $timeline['min_invest_currency_id'],
                      'is_tba' => $tba,
                    ]);
                  }
                };
              }

              if(isset($data['links'])){
                ProjectLinks::where('project_id',$id)->delete();
                foreach ($data['links'] as $links) {
                  if($links['name_ru'] != NULL && $links['name_en'] != NULL && $links['link'] != NULL){
                    ProjectLinks::create([
                      'project_id' => $id,
                      'name_ru' => $links['name_ru'],
                      'name_en' => $links['name_en'],
                      'link' => $links['link']
                    ]);
                  }
                };
              }

            if(isset($data['bonuses'])){
                ProjectBonuses::where('project_id',$id)->delete();
                foreach ($data['bonuses'] as $bonus) {
                    if($bonus['stage'] != NULL && $bonus['bonus'] != NULL ){
                        ProjectBonuses::create([
                            'project_id' => $id,
                            'stage' => $bonus['stage'],
                            'bonus' => $bonus['bonus']
                        ]);
                    }
                };
            }

            if(isset($data['blocks'])){
                ProjectBlock::where('project_id',$id)->delete();
                foreach ($data['blocks'] as $block) {
                    if($block['caption_ru'] != NULL && $block['caption_en'] != NULL){
                        ProjectBlock::create([
                            'project_id' => $id,
                            'caption_ru' => $block['caption_ru'],
                            'caption_en' => $block['caption_en'],
                            'content_ru' => $block['content_ru'],
                            'content_en' => $block['content_en'],
                            'priority' => $block['priority']
                        ]);
                    }
                };
            }

            if(isset($data['project_filter_value_sets'])){
                ProjectFilterValueSet::where('project_id',$id)->delete();
                foreach ($data['project_filter_value_sets'] as $value_set) {
                    if($value_set['project_filter_value_id'] != NULL){
                        ProjectFilterValueSet::create([
                            'project_id' => $id,
                            'project_filter_value_id' => $value_set['project_filter_value_id']
                        ]);
                    }
                };
            }
            else {
                ProjectFilterValueSet::where('project_id',$id)->delete();
            }


              return redirect()->route('projects.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Projects  $projects
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Projects::where('id', $id)->delete();
      ProjectAccept::where('project_id',$id)->delete();
      ProjectAgenciesRating::where('project_id',$id)->delete();
      ProjectCustomRating::where('project_id',$id)->delete();
      ProjectDescription::where('project_id',$id)->delete();
      ProjectLifetime::where('project_id',$id)->delete();
      ProjectLinks::where('project_id',$id)->delete();
      ProjectBonuses::where('project_id',$id)->delete();
      ProjectLocation::where('project_id',$id)->delete();
      ProjectPrices::where('project_id',$id)->delete();
      ProjectRestrictedCoutries::where('project_id',$id)->delete();
      ProjectSocial::where('project_id',$id)->delete();
      ProjectTeam::where('project_id',$id)->delete();
      ProjectTrading::where('project_id',$id)->delete();
      ProjectExtra::where('project_id',$id)->delete();
      ProjectBlock::where('project_id',$id)->delete();
      return redirect()->route('projects.index');

    }
}
