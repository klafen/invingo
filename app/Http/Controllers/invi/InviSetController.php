<?php

namespace App\Http\Controllers\invi;

use App\Models\InviSet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InviSetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('invi.inviset',[
            'inviset' => InviSet::where('id',1)->first()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        InviSet::where('id', $request->id)->update($request->except(['_method', '_token', 'id']));
      return redirect()->route('settings.index');
    }
    
}
