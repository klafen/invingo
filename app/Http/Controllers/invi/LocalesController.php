<?php

namespace App\Http\Controllers\invi;

use App\Models\Locales;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LocalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('invi.locales',[
        'locales' => Locales::select()->get()
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $result = Locales::create($request->except(['_token']));
      return redirect()->route('locales.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Locales  $locales
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      return view('invi.locale',[
        'locale' => Locales::where('id',$id)->first()
      ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Locales  $locales
     * @return \Illuminate\Http\Response
     */
    public function edit(Locales $locales)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Locales  $locales
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Locales $locales)
    {
      Locales::where('id', $request->id)->update($request->except(['_method', '_token', 'id']));
      return redirect()->route('locales.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Locales  $locales
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Locales::where('id', $id)->delete();
      return redirect()->route('locales.index');
    }
}
