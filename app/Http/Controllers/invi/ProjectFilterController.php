<?php

namespace App\Http\Controllers\invi;

use App\Models\InviSet;
use App\Models\ProjectFilter;
use App\Models\ProjectFilterValue;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProjectFilterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('invi.project_filter_index',[
            'project_filters' => ProjectFilter::orderby('is_visible', 'DESC')->orderby('priority', 'DESC')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $result = ProjectFilter::create(
            [
                'name_ru' => $data['name_ru'],
                'name_en' => $data['name_en'],
                'priority' => $data['priority'],
                'is_system' => 0,
                'is_visible' => 0,
                'is_visible_name' => 1,
                'type' => 'multiselect'
            ]);
        if ($result) {
            ProjectFilter::where('id', $result->id)->update(['system_name'=>'userparameter'.$result->id]);
        }
        return redirect()->route('project-filter.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProjectFilter  $projectfilter
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('invi.project_filter',[
            'project_filter' => ProjectFilter::where('id',$id)->first()
        ]);
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->all();
        $project_filter=ProjectFilter::where('id', $request->id)->first();
        if (isset($data['type'])) {$type=$data['type'];} else {$type=$project_filter->type;}
        $project_filter->update([
                'name_ru' => $data['name_ru'],
                'name_en' => $data['name_en'],
                'priority' => $data['priority'],
                'is_visible' => $data['is_visible'],
                'is_visible_name' => $data['is_visible_name'],
                'type' => $type
            ]
        );
        //если установлены варианты, то
        if(isset($data['project_values'])){
            $ids=array();
            //формируем список айдиников, пришедших от формы
            foreach ($data['project_values'] as $poject_value) {
                if($poject_value['id']){
                    $ids[$poject_value['id']]=$poject_value['id'];
                }
            }
            
            //проверяем наличие старых вариантов, если его нет в пришедших, то удаляем его из БД
            foreach ($project_filter->values as $poject_value) {
                if (!(array_key_exists($poject_value->id,$ids))) {
                    $poject_value->delete();
                }
            }
            //пробегаем по всем пришедшим
            foreach ($data['project_values'] as $poject_value) {
                if($poject_value['name_ru'] != NULL && $poject_value['name_en'] != NULL ) {
                    //если айди задан, то обновляем
                    if($poject_value['id']){
                        ProjectFilterValue::where('id', $poject_value['id'])->update([
                            'name_ru' => $poject_value['name_ru'],
                            'name_en' => $poject_value['name_en']
                        ]);
                    }
                    //иначе - создаем новый
                    else {
                        ProjectFilterValue::create([
                            'name_ru' => $poject_value['name_ru'],
                            'name_en' => $poject_value['name_en'],
                            'project_filter_id' => $project_filter->id
                        ]);
                    }
                }
            };
        }
        else {
            foreach ($project_filter->values as $poject_value) {
                    $poject_value->delete();
            }
        }
      return redirect()->route('project-filter.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProjectFilter  $project_filter
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /** @var ProjectFilter $project_filter */
        $project_filter=ProjectFilter::where('id', $id)->first();
        $project_filter->delete();
        return redirect()->route('project-filter.index');

    }
    
}
