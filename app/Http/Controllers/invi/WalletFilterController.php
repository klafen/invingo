<?php

namespace App\Http\Controllers\invi;

use App\Models\InviSet;
use App\Models\ProjectFilter;
use App\Models\ProjectFilterValue;
use App\Models\WalletFilter;
use App\Models\WalletFilterValue;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WalletFilterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('invi.wallet_filter_index',[
            'filters' => WalletFilter::orderby('is_visible', 'DESC')->orderby('priority', 'DESC')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $result = WalletFilter::create(
            [
                'name_ru' => $data['name_ru'],
                'name_en' => $data['name_en'],
                'priority' => $data['priority'],
                'is_system' => 0,
                'is_visible' => 0,
                'is_visible_name' => 1,
                'type' => 'multiselect'
            ]);
        if ($result) {
            WalletFilter::where('id', $result->id)->update(['system_name'=>'userparameter'.$result->id]);
        }
        return redirect()->route('wallet-filter.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProjectFilter  $projectfilter
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('invi.wallet_filter',[
            'filter' => WalletFilter::where('id',$id)->first()
        ]);
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->all();
        $wallet_filter=WalletFilter::where('id', $request->id)->first();
        if (isset($data['type'])) {$type=$data['type'];} else {$type=$wallet_filter->type;}
        $wallet_filter->update([
                'name_ru' => $data['name_ru'],
                'name_en' => $data['name_en'],
                'priority' => $data['priority'],
                'is_visible' => $data['is_visible'],
                'is_visible_name' => $data['is_visible_name'],
                'type' => $type
            ]
        );
        //если установлены варианты, то
        if(isset($data['project_values'])){
            $ids=array();
            //формируем список айдиников, пришедших от формы
            foreach ($data['project_values'] as $value) {
                if($value['id']){
                    $ids[$value['id']]=$value['id'];
                }
            }
            
            //проверяем наличие старых вариантов, если его нет в пришедших, то удаляем его из БД
            foreach ($wallet_filter->values as $value) {
                if (!(array_key_exists($value->id,$ids))) {
                    $value->delete();
                }
            }
            //пробегаем по всем пришедшим
            foreach ($data['project_values'] as $value) {
                if($value['name_ru'] != NULL && $value['name_en'] != NULL ) {
                    //если айди задан, то обновляем
                    if($value['id']){
                        WalletFilterValue::where('id', $value['id'])->update([
                            'name_ru' => $value['name_ru'],
                            'name_en' => $value['name_en']
                        ]);
                    }
                    //иначе - создаем новый
                    else {
                        WalletFilterValue::create([
                            'name_ru' => $value['name_ru'],
                            'name_en' => $value['name_en'],
                            'wallet_filter_id' => $wallet_filter->id
                        ]);
                    }
                }
            };
        }
        else {
            foreach ($wallet_filter->values as $value) {
                $value->delete();
            }
        }
      return redirect()->route('wallet-filter.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProjectFilter  $project_filter
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /** @var ProjectFilter $project_filter */
        $project_filter=ProjectFilter::where('id', $id)->first();
        $project_filter->delete();
        return redirect()->route('project-filter.index');

    }
    
}
