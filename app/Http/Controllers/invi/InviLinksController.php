<?php

namespace App\Http\Controllers\invi;

use App\Models\ContentSettings;
use App\Models\InviLinks;
use App\Models\Language;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InviLinksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('invi.invilinks',[
        'invilinks' => InviLinks::select()->get()
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $result = InviLinks::create($request->except(['_token']));
      return redirect()->route('invilinks.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      return view('invi.invilink',[
        'invilink' => InviLinks::where('id',$id)->first()
      ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function edit(Language $language)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Language $language)
    {
        InviLinks::where('id', $request->id)->update($request->except(['_method', '_token', 'id']));
      return redirect()->route('invilinks.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      InviLinks::where('id', $id)->delete();
      return redirect()->route('invilinks.index');
    }
}
