<?php

namespace App\Http\Controllers\invi;

use App\Models\WalletType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WalletTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('invi.wallet_types',[
        'types' => WalletType::select()->get()
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $result = WalletType::create($request->except(['_token']));
      return redirect()->route('wallettype.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WalletType  $walletType
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      return view('invi.wallet_type',[
        'type' => WalletType::where('id',$id)->first()
      ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WalletType  $walletType
     * @return \Illuminate\Http\Response
     */
    public function edit(WalletType $walletType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WalletType  $walletType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WalletType $walletType)
    {
      WalletType::where('id', $request->id)->update($request->except(['_method', '_token', 'id']));
      return redirect()->route('wallettype.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WalletType  $walletType
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      WalletType::where('id', $id)->delete();
      return redirect()->route('wallettype.index');
    }
}
