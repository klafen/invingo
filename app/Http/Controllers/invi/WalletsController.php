<?php

namespace App\Http\Controllers\invi;

use App\Models\Anonymity;
use App\Models\Countries;
use App\Models\Extra;
use App\Models\Language;
use App\Models\PrivateKeyControl;
use App\Models\TeamGroup;
use App\Models\Validation;
use App\Models\WalletBlock;
use App\Models\WalletExtra;
use App\Models\WalletFilter;
use App\Models\WalletFilterValueSet;
use App\Models\WalletJurisdiction;
use App\Models\WalletLanguage;
use DB;
use App\Models\Wallets;
use App\Models\WalletTeam;
use App\Models\WalletSupport;
use App\Models\WalletSocial;
use App\Models\WalletDescription;
use App\Models\WalletType;
use App\Models\WalletOs;
use App\Models\WalletLink;
use App\Models\Cryptocoins;
use App\Models\Professions;
use App\Models\Socials;
use App\Models\OS;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WalletsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('invi.wallets',[
        'wallets' => Wallets::select('id','name','rating','is_visible')->get()
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('invi.wallet_create',[
        'cryptocoins' => Cryptocoins::select('id','name')->orderBy('name','asc')->get(),
        'countries' => Countries::select()->orderBy('name_ru','asc')->get(),
        'socials' => Socials::select('id','name', 'icon')->orderBy('name','asc')->get(),
        'wallet_types' => WalletType::select('id','name_ru', 'name_en')->orderBy('name_ru','asc')->get(),
        'oses' => OS::select('id','name')->orderBy('name','asc')->get(),
        'languages' => Language::select('id','name_ru as name')->orderBy('name_ru','asc')->get(),
        'extras' => Extra::select('id','name_ru as name')->orderBy('name_ru','asc')->get(),
        'private_key_controls' => PrivateKeyControl::select('id','name_ru', 'name_en')->orderBy('name_ru','asc')->get(),
        'validations'=>Validation::select('id','name_ru', 'name_en')->orderBy('name_ru','asc')->get(),
        'anonymities'=>Anonymity::select('id','name_ru', 'name_en')->orderBy('id','asc')->get(),
        'team_groups' => TeamGroup::select()->orderBy('id','asc')->get(),
      ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $data = $request->all();
      $wallet = Wallets::create([
        'name' => $data['name'],
        'logo' => $data['logo'], 
        'road_map' => $data['road_map'],
        'rating' => $data['rating'],
        'wallet_type_id' => $data['wallet_type_id'],
        'private_key_control_id' => $data['private_key_control_id'],
        'validation_id' => $data['validation_id'],
        'anonymity_id' => $data['anonymity_id'],
        'country_id' => $data['country_id'],
        'is_sponsored' => $data['is_sponsored'],
        'is_visible' => $data['is_visible'],
        'is_hardware' => $data['is_hardware'],
        'is_2FA' => $data['is_2FA'],
        'is_multisignature' => $data['is_multisignature'],
        'is_reserve_copy' => $data['is_reserve_copy'],
        'is_seed_phrase' => $data['is_seed_phrase'],
        'is_smart_contract' => $data['is_smart_contract'],
        'is_exchange' => $data['is_exchange'],
        'is_open_source' => $data['is_open_source'],
        'is_qrcode' => $data['is_qrcode'],
        'is_touch_id' => $data['is_touch_id'],
        'is_fiat' => $data['is_fiat'],
        'is_adapt' => $data['is_adapt'],
        'priority' => $data['priority'],
      ]);
      if(isset($data['accept'])){
        foreach ($data['accept'] as $accept) {
          WalletSupport::create([
            'wallet_id' => $wallet->id,
            'cryptocoin_id' => $accept
          ]);
        };
      }

      if(isset($data['wallet_os'])){
        foreach ($data['wallet_os'] as $wallet_os) {
          WalletOs::create([
              'wallet_id' => $wallet->id,
              'os_id' => $wallet_os
          ]);
        };
      }

      if(isset($data['wallet_extra'])){
        foreach ($data['wallet_extra'] as $wallet_extra) {
          WalletExtra::create([
              'wallet_id' => $wallet->id,
              'extra_id' => $wallet_extra
          ]);
        };
      }
      if(isset($data['wallet_language'])){
        foreach ($data['wallet_language'] as $wallet_language) {
          WalletLanguage::create([
              'wallet_id' => $wallet->id,
              'language_id' => $wallet_language
          ]);
        };
      }

      WalletDescription::create([
          'wallet_id' => $wallet->id
      ]);

      return redirect()->route('wallets.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Wallets  $wallets
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      return view('invi.wallet',[
        'wallet_os' => WalletOs::select(DB::raw('GROUP_CONCAT(`os_id`) as os_id'))->where('wallet_id',$id)->first()->toArray(),
        'wallet_language' => WalletLanguage::select(DB::raw('GROUP_CONCAT(`language_id`) as language_id'))->where('wallet_id',$id)->first()->toArray(),
        'wallet_extra' => WalletExtra::select(DB::raw('GROUP_CONCAT(`extra_id`) as extra_id'))->where('wallet_id',$id)->first()->toArray(),
        'wallet_supports' => WalletSupport::select(DB::raw('GROUP_CONCAT(`cryptocoin_id`) as cryptocoin_id'))->where('wallet_id',$id)->first()->toArray(),
        'wallet' => Wallets::select('wallets.id as id','wallet_description.other_ru','wallet_description.other_en','wallet_description.info_ru','wallet_description.info_en','wallets.*')
              ->leftjoin('wallet_description','wallet_description.wallet_id','=','wallets.id')             
              ->where('wallets.id',$id)->first(),
        'wallet_blocks'=>WalletBlock::where('wallet_id', $id)->orderby('priority', 'DESC')->get(),
        'wallet_team' => WalletTeam::where('wallet_id',$id)->get(),
        'wallet_social' => WalletSocial::where('wallet_id',$id)->get(),
        'wallet_links' => WalletLink::where('wallet_id',$id)->get(),
        'cryptocoins' => Cryptocoins::select('id','name')->orderBy('name','asc')->get(),
        'socials' => Socials::select('id','name', 'icon')->orderBy('name','asc')->get(),
        'wallet_types' => WalletType::select('id','name_ru', 'name_en')->orderBy('name_ru','asc')->get(),
        'oses' => OS::select('id','name')->orderBy('name','asc')->get(),
        'languages' => Language::select('id','name_ru as name')->orderBy('name_ru','asc')->get(),
        'extras' => Extra::select('id','name_ru as name')->orderBy('name_ru','asc')->get(),
        'private_key_controls' => PrivateKeyControl::select('id','name_ru', 'name_en')->orderBy('name_ru','asc')->get(),
        'validations' => Validation::select('id','name_ru', 'name_en')->orderBy('name_ru','asc')->get(),
        'anonymities'=>Anonymity::select('id','name_ru', 'name_en')->orderBy('id','asc')->get(),
        'countries' => Countries::select()->orderBy('name_ru','asc')->get(),
        'team_groups' => TeamGroup::select()->orderBy('id','asc')->get(),
        'filters'=>WalletFilter::select()->where('is_system',0)->orderBy('name_ru','asc')->get()
      ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Wallets  $projects
     * @return \Illuminate\Http\Response
     */
    public function edit(Wallets $wallet)
    {
        return false;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Wallets  $projects
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Wallets $wallet)
    {
      $data = $request->all();
      Wallets::where('id',$wallet->id)->update([
        'name' => $data['name'],
        'logo' => $data['logo'],
        'road_map' => $data['road_map'],
        'rating' => $data['rating'],
        'wallet_type_id' => $data['wallet_type_id'],
        'private_key_control_id' => $data['private_key_control_id'],
        'validation_id' => $data['validation_id'],
        'anonymity_id' => $data['anonymity_id'],
        'country_id' => $data['country_id'],
        'is_sponsored' => $data['is_sponsored'],
        'is_visible' => $data['is_visible'],
        'is_hardware' => $data['is_hardware'],
        'is_2FA' => $data['is_2FA'],
        'is_multisignature' => $data['is_multisignature'],
        'is_reserve_copy' => $data['is_reserve_copy'],
        'is_seed_phrase' => $data['is_seed_phrase'],
        'is_smart_contract' => $data['is_smart_contract'],
        'is_exchange' => $data['is_exchange'],
        'is_open_source' => $data['is_open_source'],
        'is_qrcode' => $data['is_qrcode'],
        'is_touch_id' => $data['is_touch_id'],
        'is_fiat' => $data['is_fiat'],
        'is_adapt' => $data['is_adapt'],
        'priority' => $data['priority'],
      ]);
      if(isset($data['accept'])){
        WalletSupport::where('wallet_id',$wallet->id)->delete();
        foreach ($data['accept'] as $accept) {
          WalletSupport::create([
            'wallet_id' => $wallet->id,
            'cryptocoin_id' => $accept
          ]);
        };
      }
      if(isset($data['social'])){
        WalletSocial::where('wallet_id',$wallet->id)->delete();
        foreach ($data['social'] as $social) {
          if($social['link'] != NULL){
            WalletSocial::create([
              'wallet_id' => $wallet->id,
              'social_id' => $social['social_id'],
              'link' => $social['link']
            ]);
          }
        };
      }
      WalletDescription::where('wallet_id',$wallet->id)->update([
        'wallet_id' => $wallet->id,
        'info_ru' => $data['info_ru'],
        'info_en' => $data['info_en'],
        'other_ru' => $data['other_ru'],
        'other_en' => $data['other_en']
      ]);

      if(isset($data['blocks'])){
        WalletBlock::where('wallet_id',$wallet->id)->delete();
        foreach ($data['blocks'] as $block) {
          if($block['caption_ru'] != NULL && $block['caption_en'] != NULL){
            WalletBlock::create([
                'wallet_id' => $wallet->id,
                'caption_ru' => $block['caption_ru'],
                'caption_en' => $block['caption_en'],
                'content_ru' => $block['content_ru'],
                'content_en' => $block['content_en'],
                'priority' => $block['priority']
            ]);
          }
        };
      }

      if(isset($data['team'])){
        WalletTeam::where('wallet_id',$wallet->id)->delete();
        foreach ($data['team'] as $team) {
          if($team['name_ru'] != NULL && $team['name_en'] != NULL){
            WalletTeam::create([
              'wallet_id' => $wallet->id,
              'name_ru' => $team['name_ru'],
              'name_en' => $team['name_en'],
              'avatar' => $team['avatar'], 
              'profession_ru' => $team['profession_ru'],
              'profession_en' => $team['profession_en'],
              'group_id' => $team['group_id']
            ]);
          }
        };
      }
      if(isset($data['links'])){
        WalletLink::where('wallet_id',$wallet->id)->delete();
        foreach ($data['links'] as $link) {
          if($link['name_ru'] != NULL && $link['name_en'] != NULL && $link['link'] != NULL){
            WalletLink::create([
              'wallet_id' => $wallet->id,
              'name_ru' => $link['name_ru'],
              'name_en' => $link['name_en'],
              'link' => $link['link']
            ]);
          }
        };
      }

      if(isset($data['wallet_os'])){
        WalletOs::where('wallet_id',$wallet->id)->delete();
        foreach ($data['wallet_os'] as $wallet_os) {
          WalletOs::create([
              'wallet_id' => $wallet->id,
              'os_id' => $wallet_os
          ]);
        };
      }

      if(isset($data['wallet_language'])){
        WalletLanguage::where('wallet_id',$wallet->id)->delete();
        foreach ($data['wallet_language'] as $wallet_language) {
          WalletLanguage::create([
              'wallet_id' => $wallet->id,
              'language_id' => $wallet_language
          ]);
        };
      }

      if(isset($data['wallet_extra'])){
        WalletExtra::where('wallet_id',$wallet->id)->delete();
        foreach ($data['wallet_extra'] as $wallet_extra) {
          WalletExtra::create([
              'wallet_id' => $wallet->id,
              'extra_id' => $wallet_extra
          ]);
        };
      }

      if(isset($data['project_filter_value_sets'])){
        WalletFilterValueSet::where('wallet_id',$wallet->id)->delete();
        foreach ($data['project_filter_value_sets'] as $value_set) {
          if($value_set['project_filter_value_id'] != NULL){
            WalletFilterValueSet::create([
                'wallet_id' => $wallet->id,
                'wallet_filter_value_id' => $value_set['project_filter_value_id']
            ]);
          }
        };
      }
      
      return redirect()->route('wallets.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Wallets  $projects
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Wallets::where('id', $id)->delete();
      WalletTeam::where('wallet_id',$id)->delete();
      WalletBlock::where('wallet_id',$id)->delete();
      WalletSupport::where('wallet_id',$id)->delete();
      WalletSocial::where('wallet_id',$id)->delete();
      WalletDescription::where('wallet_id',$id)->delete();
      WalletOs::where('wallet_id',$id)->delete();
      WalletLanguage::where('wallet_id',$id)->delete();
      WalletExtra::where('wallet_id',$id)->delete();
      WalletLink::where('wallet_id',$id)->delete();
      return redirect()->route('wallets.index');
    }
}
