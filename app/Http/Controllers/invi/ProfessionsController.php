<?php

namespace App\Http\Controllers\invi;

use App\Models\Professions;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfessionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('invi.professions',[
        'professions' => Professions::select()->get()
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $result = Professions::create($request->except(['_token']));
      return redirect()->route('professions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Professions  $Professions
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      return view('invi.profession',[
        'profession' => Professions::where('id',$id)->first()
      ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Professions  $Professions
     * @return \Illuminate\Http\Response
     */
    public function edit(Professions $Professions)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Professions  $Professions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Professions $Professions)
    {
      Professions::where('id', $request->id)->update($request->except(['_method', '_token', 'id']));
      return redirect()->route('professions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Professions  $Professions
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Professions::where('id', $id)->delete();
      return redirect()->route('professions.index');
    }
}
