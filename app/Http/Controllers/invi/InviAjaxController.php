<?php

namespace App\Http\Controllers\invi;

use App\Models\InviSet;
use App\Models\ProjectFilter;
use App\Models\WalletFilter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InviAjaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $result=null;
        $data=$request->all();
        if (isset($data['project_filter'])){
            $result='<option value=""></option>';
            $project_filter=ProjectFilter::where('id',$data['project_filter'])->first();
            foreach ($project_filter->values as $value) {
                $result.='<option value="'.$value->id.'" >'.$value->name_ru.' ('.$value->name_en.')</option>';
            }
        }
        elseif (isset($data['wallet_filter'])){
            $result='<option value=""></option>';
            $wallet_filter=WalletFilter::where('id',$data['wallet_filter'])->first();
            foreach ($wallet_filter->values as $value) {
                $result.='<option value="'.$value->id.'" >'.$value->name_ru.' ('.$value->name_en.')</option>';
            }
        }

        return $result;

    }


    
}
