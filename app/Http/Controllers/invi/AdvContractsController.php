<?php

namespace App\Http\Controllers\invi;

use App\Models\AdvContracts;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class AdvContractsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('invi.advert',[
        'contracts' => AdvContracts::select()->get()
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('invi.advert_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $result = AdvContracts::create($request->except(['_token', '_wysihtml5_mode']));
      return redirect()->route('advert.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AdvContracts  $advContracts
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('invi.advert_show',[
          'contract' => AdvContracts::where('id',$id)->first()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AdvContracts  $advContracts
     * @return \Illuminate\Http\Response
     */
    public function edit(AdvContracts $advContracts)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AdvContracts  $advContracts
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AdvContracts $advContracts)
    {
      AdvContracts::where('id', $request->id)->update($request->except(['_method', '_token', '_wysihtml5_mode']));
      return redirect()->route('advert.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AdvContracts  $advContracts
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      AdvContracts::where('id', $id)->delete();
      return redirect()->route('advert.index');
    }
}
