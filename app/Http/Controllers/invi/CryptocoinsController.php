<?php

namespace App\Http\Controllers\invi;

use App\Models\Cryptocoins;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CryptocoinsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('invi.cryptocoins',[
        'cryptocoins' => Cryptocoins::select()->get()
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $result = Cryptocoins::create($request->except(['_token']));
      return redirect()->route('cryptocoins.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cryptocoins  $cryptocoins
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      return view('invi.cryptocoin',[
        'cryptocoin' => Cryptocoins::where('id',$id)->first()
      ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cryptocoins  $cryptocoins
     * @return \Illuminate\Http\Response
     */
    public function edit(Cryptocoins $cryptocoins)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cryptocoins  $cryptocoins
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cryptocoins $cryptocoins)
    {
      Cryptocoins::where('id', $request->id)->update($request->except(['_method', '_token', 'id']));
      return redirect()->route('cryptocoins.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cryptocoins  $cryptocoins
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Cryptocoins::where('id', $id)->delete();
      return redirect()->route('cryptocoins.index');
    }
}
