<?php

namespace App\Http\Controllers\invi;

use App\Models\OS;
use App\Models\Trading;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TradingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('invi.tradings',[
        'tradings' => Trading::select()->get()
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $result = Trading::create($request->except(['_token']));
      return redirect()->route('tradings.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OS  $oS
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      return view('invi.trading',[
        'trading' => Trading::where('id',$id)->first()
      ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OS  $oS
     * @return \Illuminate\Http\Response
     */
    public function edit(OS $oS)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OS  $oS
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Trading $trading)
    {
      Trading::where('id', $request->id)->update($request->except(['_method', '_token', 'id']));
      return redirect()->route('tradings.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OS  $oS
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Trading::where('id', $id)->delete();
      return redirect()->route('tradings.index');
    }
}
