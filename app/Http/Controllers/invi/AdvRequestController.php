<?php

namespace App\Http\Controllers\invi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AdvRequest;

class AdvRequestController extends Controller
{
  public function index()
  {
    return view('invi.advrequests',[
      'advrequests' => AdvRequest::select()->where('is_archive','<>',1)->get()
    ]);
  }

  public function archive()
  {
    return view('invi.advrequests',[
      'advrequests' => AdvRequest::select()->where('is_archive',1)->get(),
      'archive' => true
    ]);
  }


  public function show($id)
  {
    return view('invi.advrequest',[
      'advrequest' => AdvRequest::where('id',$id)->first()
    ]);
  }

  public function update(Request $request, AdvRequest $advrequest)
  {
    AdvRequest::where('id', $request->id)->update($request->except(['_method', '_token']));
    return redirect()->route('advrequest.index');
  }


  public function destroy($id)
  {
    AdvRequest::where('id', $id)->delete();
    return redirect()->route('advrequest.index');
  }


}
