<?php

namespace App\Http\Controllers\invi;

use App\Models\Socials;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SocialsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('invi.socials',[
        'socials' => Socials::select()->get()
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $result = Socials::create($request->except(['_token']));
      return redirect()->route('socials.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Socials  $socials
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      return view('invi.social',[
        'social' => Socials::where('id',$id)->first()
      ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Socials  $socials
     * @return \Illuminate\Http\Response
     */
    public function edit(Socials $socials)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Socials  $socials
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Socials $socials)
    {
      Socials::where('id', $request->id)->update($request->except(['_method', '_token', 'id']));
      return redirect()->route('socials.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Socials  $socials
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Socials::where('id', $id)->delete();
      return redirect()->route('socials.index');
    }
}
