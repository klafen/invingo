<?php

namespace App\Http\Controllers\invi;

use App\Models\AgenciesRating;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AgenciesRatingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('invi.agencies-rating',[
        'agencies' => AgenciesRating::select()->get()
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $result = AgenciesRating::create($request->except(['_token']));
      return redirect()->route('agencies-rating.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AgenciesRating  $agenciesRating
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      return view('invi.agency-rating',[
        'agency' => AgenciesRating::where('id',$id)->first()
      ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AgenciesRating  $agenciesRating
     * @return \Illuminate\Http\Response
     */
    public function edit(AgenciesRating $agenciesRating)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AgenciesRating  $agenciesRating
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AgenciesRating $agenciesRating)
    {
      AgenciesRating::where('id', $request->id)->update($request->except(['_method', '_token', 'id']));
      return redirect()->route('agencies-rating.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AgenciesRating  $agenciesRating
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      AgenciesRating::where('id', $id)->delete();
      return redirect()->route('agencies-rating.index');
    }
}
