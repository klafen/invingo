<?php

namespace App\Http\Controllers\invi;

use App\Models\Extra;
use App\Models\InviSocial;
use App\Models\Language;
use App\Models\Socials;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InviSocialsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('invi.invisocials',[
        'invisocials' => InviSocial::select('invi_socials.id as id','socials.name as name', 'link', 'followers')
            ->leftjoin('socials', 'invi_socials.social_id', '=', 'socials.id')->get(),
          'socials'=>Socials::select()->get(),
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $result = InviSocial::create($request->except(['_token']));
      return redirect()->route('invisocials.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InviSocial  $invisocial
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      return view('invi.invisocial',[
        'invisocial' => InviSocial::where('id',$id)->first(), 
         'socials'=>Socials::select()->get(),
      ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function edit(Language $language)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InviSocial $language)
    {
        InviSocial::where('id', $request->id)->update($request->except(['_method', '_token', 'id']));
      return redirect()->route('invisocials.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      InviSocial::where('id', $id)->delete();
      return redirect()->route('invisocials.index');
    }
}
