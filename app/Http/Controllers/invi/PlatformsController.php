<?php

namespace App\Http\Controllers\invi;

use App\Models\Platforms;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PlatformsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('invi.platforms',[
        'platforms' => Platforms::select()->get()
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $result = Platforms::create($request->except(['_token']));
      return redirect()->route('platforms.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Platforms  $platforms
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      return view('invi.platform',[
        'platform' => Platforms::where('id',$id)->first()
      ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Platforms  $platforms
     * @return \Illuminate\Http\Response
     */
    public function edit(Platforms $platforms)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Platforms  $platforms
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Platforms $platforms)
    {
      Platforms::where('id', $request->id)->update($request->except(['_method', '_token', 'id']));
      return redirect()->route('platforms.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Platforms  $platforms
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Platforms::where('id', $id)->delete();
      return redirect()->route('platforms.index');
    }
}
