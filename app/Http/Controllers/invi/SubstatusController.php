<?php

namespace App\Http\Controllers\invi;

use App\Models\Substatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubstatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('invi.substatuses',[
        'substatuses' => Substatus::select()->get()
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $result = Substatus::create($request->except(['_token']));
      return redirect()->route('substatus.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Substatus  $substatus
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      return view('invi.substatus',[
        'substatus' => Substatus::where('id',$id)->first()
      ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Substatus  $substatus
     * @return \Illuminate\Http\Response
     */
    public function edit(Substatus $substatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Substatus  $substatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Substatus $substatus)
    {
      Substatus::where('id', $request->id)->update($request->except(['_method', '_token', 'id']));
      return redirect()->route('substatus.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Substatus  $substatus
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Substatus::where('id', $id)->delete();
      return redirect()->route('substatus.index');
    }
}
