<?php

namespace App\Http\Controllers;


use App\Models\ContentSettings;

class MetodologyController extends Controller
{

  public function show()
  {
    $locale = \App::getLocale();
    
      return view('metodology',[
          'main_metodology'=>ContentSettings::select('text_'.$locale.' AS text')->where('name','=','home_metodology')->where('is_visible',1)->first(),
          'settings'=>$this->settings()
      ]);
    
  }

}
