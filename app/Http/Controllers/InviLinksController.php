<?php

namespace App\Http\Controllers;


use App\Models\InviLinks;

class InviLinksController extends Controller
{

  public function show($url)
  {
    $locale = \App::getLocale();

    $invilink=InviLinks::select('url', 'name_'.$locale.' as name', 'text_'.$locale.' as text' )->where('url',$url)->where('is_visible',1)->first();

    if ($invilink) {
      return view('invilink',[
          'invilink'=>$invilink,
          'settings'=>$this->settings()
      ]);
    }
    else {
      return abort(404);
    }
    
    


  }

}
