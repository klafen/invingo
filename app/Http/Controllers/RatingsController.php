<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Projects;
use App\Models\ProjectAgenciesRating;
use App\Models\AgenciesRating;


class RatingsController extends Controller
{
  public function index()
  {
    $projects_temp = Projects::select('projects.id as id','name','logo','rating')
        ->selectraw('GET_STATUS(projects.id) AS status, GET_STAGE(projects.id) AS stage ')
        ->join('project_lifetime', 'project_lifetime.project_id', '=', 'projects.id')
        ->where('is_visible',1)
        ->orderby('status')
        ->orderby('name')
        ->get()->toArray();
    $projects = array();
    foreach ($projects_temp as $project) {
      $projects[$project['id']]['id'] = $project['id'];
      $projects[$project['id']]['name'] = $project['name'];
      $projects[$project['id']]['logo'] = $project['logo'];
      $projects[$project['id']]['rating'] = $project['rating'];
      $project_rating = ProjectAgenciesRating::select('agency_rating_id','rating')
                                        ->where('project_id',$project['id'])
                                        ->get()->toArray();
      foreach ($project_rating as $value) {
        $projects[$project['id']]['agency'][$value['agency_rating_id']] = $value['rating'];
      }
    };
    return view('ratings',[
      'projects' => $projects,
      'agencies' => AgenciesRating::select('agencies_rating.id as id', 'agencies_rating.name as name', 'agencies_rating.url as url')
          ->join('project_agencies_rating','project_agencies_rating.agency_rating_id', '=', 'agencies_rating.id')->distinct()->get(),
      'settings'=>$this->settings()
    ]);
  }

}
