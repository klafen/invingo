<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AdvRequest;
use Illuminate\Support\Facades\Mail;

class AjaxController extends Controller
{


  public function saveContracts(Request $request)
  {
        $user = [];
        parse_str($request->user, $user);
        $result = AdvRequest::create($user);



        if($result) {
            $parameters=$this->parameters();
            $to = $parameters->email_form;
            $subject = 'Новая заявка на рекламу';
            $message = view('emails.new_adv',[
                'name' => $result->name,
                'phone' => $result->phone,
                'email' => $result->email,
                'message' => $result->message,
                'contract_name' => $result->contract_name,]);

            $headers[] = 'MIME-Version: 1.0';
            $headers[] = 'Content-type: text/html; charset=utf8';
            //$headers[] = 'To: '.$parameters->namecaption_site.' <'.$parameters->email_form.'>';
            $headers[] = 'From: '.$parameters->namecaption_site.' <'.$parameters->email_contact.'.>';

            mail($to, $subject, $message, implode("\r\n", $headers));

            return response()->json([
                'status' => 'done'
            ]);
        }
        else{
          return response()->json([
              'status' => 'error'
          ]);
        }
  }


}
