<?php

namespace App\Http\Controllers;

use App\Models\Anonymity;
use App\Models\Countries;
use App\Models\Extra;
use App\Models\Language;
use App\Models\OS;
use App\Models\PrivateKeyControl;
use App\Models\Validation;
use App\Models\WalletFilter;
use App\Models\WalletFilterValue;
use App\Models\WalletLink;
use App\Models\WalletSocial;
use App\Models\WalletTeam;
use App\Models\WalletType;
use DB;
use Illuminate\Http\Request;
use App\Models\Wallets;

class WalletsController extends Controller
{
  public function index()
  {

  $settings=$this->settings();
  //По сколько выводить
  $pagination=$settings['parameters']->wallets_by_page;

    $locale = \App::getLocale();

      $walletsraw=Wallets::select('wallets.id','wallets.name','wallets.logo','wallets.rating','wallet_type.name_'.$locale.' as wallet_type', 'wallets.priority',
          'wallets.private_key_control_id','wallets.is_sponsored','wallet_oses.os', 'wallet_support.code as currency', 'private_key_control.name_'.$locale.' as private_key_control_name' )
          ->leftjoin('wallet_type', 'wallets.wallet_type_id', '=', 'wallet_type.id')
          ->leftjoin(DB::raw("(SELECT
                  wallet_os.wallet_id,
                  GROUP_CONCAT(os.name SEPARATOR ', ') as os
                  FROM wallet_os
                  JOIN os ON wallet_os.os_id = os.id
                  GROUP BY wallet_os.wallet_id
                  ) as wallet_oses"),function($join){
              $join->on("wallets.id","=","wallet_oses.wallet_id");
          })
          ->leftjoin(DB::raw("(SELECT
                  wallet_support.wallet_id,
                  GROUP_CONCAT(cryptocoins.code SEPARATOR ', ') as code
                  FROM wallet_support
                  JOIN cryptocoins ON wallet_support.cryptocoin_id = cryptocoins.id
                  GROUP BY wallet_support.wallet_id
                  ) as wallet_support"),function($join){
              $join->on("wallets.id","=","wallet_support.wallet_id");
          })
          ->leftjoin('private_key_control','wallets.private_key_control_id', '=', 'private_key_control.id');
      /*
      if(isset($_GET['type']) && $_GET['type']!=0 ) {
          $walletsraw->where('wallets.wallet_type_id', $_GET['type']);
      }
*/
      if(isset($_GET['types'])) {
          $sql=$this->or_sql($_GET['types'], 'wallets.wallet_type_id' );
          $walletsraw->whereraw($sql);
      }

      //привате контрол
      /*
      if(isset($_GET['private_key_control']) && $_GET['private_key_control']!=0 ) {
          $walletsraw->where('private_key_control_id', $_GET['private_key_control']);
      }
      */
      if(isset($_GET['private_key_controls'])) {
          $sql=$this->or_sql($_GET['private_key_controls'], 'wallets.private_key_control_id' );
          $walletsraw->whereraw($sql);
      }
//валидация
      /*
      if(isset($_GET['validation']) && $_GET['validation']!=0 ) {
          $walletsraw->where('validation_id', $_GET['validation']);
      }
      */
      if(isset($_GET['validations'])) {
          $sql=$this->or_sql($_GET['validations'], 'wallets.validation_id' );
          $walletsraw->whereraw($sql);
      }

//Анонимность
      /*
      if(isset($_GET['anonymity']) && $_GET['anonymity']!=0 ) {
          $walletsraw->where('anonymity_id', $_GET['anonymity']);
      }*/

      if(isset($_GET['anonymitys'])) {
          $sql=$this->or_sql($_GET['anonymitys'], 'wallets.anonymity_id' );
          $walletsraw->whereraw($sql);
      }

//Страна
      /*
      if(isset($_GET['wallet_country']) && $_GET['wallet_country']!=0 ) {
          $walletsraw->where('country_id', $_GET['wallet_country']);
      }
      */
      if(isset($_GET['countrys'])) {
          $sql=$this->or_sql($_GET['countrys'], 'wallets.country_id' );
          $walletsraw->whereraw($sql);
      }



      /*
      if(isset($_GET['language']) && $_GET['language']!=0 ) {
          $walletsraw->leftjoin('wallet_language', 'wallet_language.wallet_id', '=', 'wallets.id')->where('wallet_language.language_id', $_GET['language']);
      }
      */
      if(isset($_GET['languages'])) {
          $walletsraw->leftjoin('wallet_language','wallet_language.wallet_id', '=', 'wallets.id');
          $sql=$this->or_sql($_GET['languages'], 'wallet_language.language_id' );
          $walletsraw->whereraw($sql);
      }

      /*
      if(isset($_GET['os']) && $_GET['os']!=0 ) {
          $walletsraw->leftjoin('wallet_os', 'wallet_os.wallet_id', '=', 'wallets.id')->where('wallet_os.os_id', $_GET['os']);
      }
      */
      if(isset($_GET['platforms'])) {
          $walletsraw->leftjoin('wallet_os','wallet_os.wallet_id', '=', 'wallets.id');
          $sql=$this->or_sql($_GET['platforms'], 'wallet_os.os_id' );
          $walletsraw->whereraw($sql);
      }
/*
      if(isset($_GET['extra']) && $_GET['extra']!=0 ) {
          $walletsraw->join('wallet_extra', 'wallet_extra.wallet_id', '=', 'wallets.id')->where('wallet_extra.extra_id',$_GET['extra']);
      }*/

      if(isset($_GET['extras'])) {
          $walletsraw->leftjoin('wallet_extra','wallet_extra.wallet_id', '=', 'wallets.id');
          $sql=$this->or_sql($_GET['extras'], 'wallet_extra.extra_id' );
          $walletsraw->whereraw($sql);
      }

      if(isset($_GET['range-start']) && isset($_GET['range-end']) ) {
          $walletsraw->wherebetween('wallets.rating', array($_GET['range-start'],$_GET['range-end']));
      }       


//поехали стандартные чекбоксы

      if(isset($_GET['multisignature']) && $_GET['multisignature']=='on' ) {
          $walletsraw->where('wallets.is_multisignature', 1);
      }

      if(isset($_GET['2FA']) && $_GET['2FA']=='on' ) {
          $walletsraw->where('wallets.is_2FA', 1);
      }

      if(isset($_GET['reserve_copy']) && $_GET['reserve_copy']=='on' ) {
          $walletsraw->where('wallets.is_reserve_copy', 1);
      }


      if(isset($_GET['seed_phrase']) && $_GET['seed_phrase']=='on' ) {
          $walletsraw->where('wallets.is_seed_phrase', 1);
      }

      if(isset($_GET['qr_code']) && $_GET['qr_code']=='on' ) {
          $walletsraw->where('wallets.is_qrcode', 1);
      }

      if(isset($_GET['touch_id']) && $_GET['touch_id']=='on' ) {
          $walletsraw->where('wallets.is_touch_id', 1);
      }

      if(isset($_GET['fiat']) && $_GET['fiat']=='on' ) {
          $walletsraw->where('wallets.is_fiat', 1);
      }

      if(isset($_GET['smart_contracts_support']) && $_GET['smart_contracts_support']=='on' ) {
          $walletsraw->where('wallets.is_smart_contract', 1);
      }

      if(isset($_GET['hardware']) && $_GET['hardware']=='on' ) {
          $walletsraw->where('wallets.is_hardware', 1);
      }

      if(isset($_GET['open_source']) && $_GET['open_source']=='on' ) {
          $walletsraw->where('wallets.is_open_source', 1);
      }

      if(isset($_GET['exchange']) && $_GET['exchange']=='on' ) {
          $walletsraw->where('wallets.is_exchange', 1);
      }
     



      $data_filter=array(
          'platform' => OS::select('os.id as id','os.name as name')
              ->join('wallet_os', 'os.id', '=', 'wallet_os.os_id')->distinct()->get(),
          'language' => Language::select('language.id as id','language.name_'.$locale.' as name')
              ->join('wallet_language', 'language.id', '=', 'wallet_language.language_id')->distinct()->get(),
          'type' => WalletType::select('wallet_type.id as id','wallet_type.name_'.$locale.' as name')
              ->join('wallets', 'wallets.wallet_type_id', '=', 'wallet_type.id')->distinct()->get(),
          'country' => Countries::select('countries.id as id','countries.name_'.$locale.' as name')
              ->rightjoin('wallets', 'countries.id', '=', 'wallets.country_id')->distinct()
              ->get(),
          'extra'=>Extra::select('extra.id as id','extra.name_'.$locale.' as name')
              ->join('wallet_extra', 'wallet_extra.extra_id', '=', 'extra.id')->distinct()->get(),
          'private_key_control'=>PrivateKeyControl::select('private_key_control.id as id','private_key_control.name_'.$locale.' as name')
              ->join('wallets', 'wallets.private_key_control_id', '=', 'private_key_control.id')->distinct()->get(),
          'validation'=>Validation::select('validation.id as id','validation.name_'.$locale.' as name')
              ->join('wallets', 'wallets.validation_id', '=', 'validation.id')->distinct()->get(),
          'anonymity'=>Anonymity::select('anonymity.id as id','anonymity.name_'.$locale.' as name')
              ->join('wallets', 'wallets.anonymity_id', '=', 'anonymity.id')->distinct()->get(),
      );

      $filters=WalletFilter::select('wallet_filter.id as id','wallet_filter.name_'.$locale.' as name',
          'wallet_filter.system_name','wallet_filter.priority','wallet_filter.type','wallet_filter.is_system', 'wallet_filter.is_visible_name'
      )->where('is_visible',1)->orderby('priority', 'DESC')->get();

      foreach ($filters as $filter) {
          //если не системный, то добавляем фильтрацию по его значению и запрашиваем значения для формирования фильтра
          if(!($filter->is_system)) {
              //если отображение - мультиселект
              if ($filter->type =='multiselect') {
                  $getname=$filter['system_name'].'s';
                  //пользовательские параметры
                  if(isset($_GET[$getname])) {
                      $walletsraw->leftjoin('wallet_filter_value_set as '.$getname,'wallets.id', '=', $getname.'.wallet_id');
                      $sql=$this->or_sql($_GET[$getname], $getname.'.wallet_filter_value_id' );
                      $walletsraw->whereraw($sql);
                  }
                  $values=WalletFilterValue::select('wallet_filter_value.id as id','wallet_filter_value.name_'.$locale.' as name', 'wallet_filter_value.priority as priority')
                      ->where('wallet_filter_value.wallet_filter_id', '=',$filter->id )
                      ->orderby('priority','DESC')
                      ->get();
                  $data_filter[$filter['system_name']]=$values;
              }
              //если отображение - чекбокс
              elseif ($filter->type =='onecheckbox') {
                  $getname=$filter['system_name'];
                  //пользовательские параметры
                  if(isset($_GET[$getname]) && $_GET[$getname]=='on') {
                      $walletsraw->leftjoin('wallet_filter_value_set as '.$getname,'wallets.id', '=', $getname.'.wallet_id')
                          ->join('wallet_filter_value as value_'.$getname,$getname.'.wallet_filter_value_id' ,'=','value_'.$getname.'.id')
                          ->where('value_'.$getname.'.wallet_filter_id','=',$filter->id);
                  }
              }

          }
      }


      $walletsraw=$walletsraw->where('wallets.is_visible',1)->orderby('wallets.is_sponsored', 'DESC')->orderby('wallets.priority', 'DESC')->orderby('wallets.name')->get();
    $wallets=array();
    foreach ($walletsraw as $wallet) {
        array_push($wallets, $wallet);
    }
    $count=count($wallets);
      
      if (isset($_GET['page'])) {
          $page=$_GET['page'];
          $wallets=array_slice($wallets, ($page - 1) * $pagination, $pagination);
          return view('wallets_ajax',[
              'wallets' => $wallets,
              'count' => $count,
          ]);
      }
      else {
          $page=1;
          $wallets=array_slice($wallets, ($page - 1) * $pagination, $pagination);
      }

    return view('wallets',[
      'wallets' => $wallets,
        'oses' => OS::select('os.id as id','os.name as name')
            ->join('wallet_os', 'os.id', '=', 'wallet_os.os_id')->distinct()->get(),
        'languages' => Language::select('language.id as id','language.name_'.$locale.' as name')
            ->join('wallet_language', 'language.id', '=', 'wallet_language.language_id')->distinct()->get(),
        'wallettypes' => WalletType::select('wallet_type.id as id','wallet_type.name_'.$locale.' as name')
            ->join('wallets', 'wallets.wallet_type_id', '=', 'wallet_type.id')->distinct()->get(),
        'wallet_countries' => Countries::select('countries.id as id','countries.name_'.$locale.' as name')
            ->rightjoin('wallets', 'countries.id', '=', 'wallets.country_id')->distinct()
            ->get(),
        'extras'=>Extra::select('extra.id as id','extra.name_'.$locale.' as name')
            ->join('wallet_extra', 'wallet_extra.extra_id', '=', 'extra.id')->distinct()->get(),
        'private_key_controls'=>PrivateKeyControl::select('private_key_control.id as id','private_key_control.name_'.$locale.' as name')
            ->join('wallets', 'wallets.private_key_control_id', '=', 'private_key_control.id')->distinct()->get(),
        'validations'=>Validation::select('validation.id as id','validation.name_'.$locale.' as name')
            ->join('wallets', 'wallets.validation_id', '=', 'validation.id')->distinct()->get(),
        'anonymities'=>Anonymity::select('anonymity.id as id','anonymity.name_'.$locale.' as name')
            ->join('wallets', 'wallets.anonymity_id', '=', 'anonymity.id')->distinct()->get(),
        'count'=>$count,
        'pagination'=>$pagination,
        'page'=>$page,
        'settings'=>$settings,
        'data_filter'=>$data_filter,
        'filters'=>$filters,
    ]);
  }

  public function show($id)
  {
    $locale = \App::getLocale();

    $wallet=Wallets::select('wallets.id','wallets.name','wallets.logo','wallets.rating','wallets.road_map','wallet_type.name_'.$locale.' as wallet_type', 'private_key_control_id',  'countries.name_'.$locale.' as country',
        'private_key_control.name_'.$locale.' as private_key_control_name','wallets.is_sponsored','wallet_os.os', 'wallet_language.language', 'wallet_extra.extra', 'wallet_support.code as currency',
        'wallet_description.info_'.$locale.' as info','wallet_description.other_'.$locale.' as other', 'validation.name_'.$locale.' as validation_name','anonymity.name_'.$locale.' as anonymity_name',
        'is_hardware', 'is_2FA', 'is_multisignature', 'is_reserve_copy', 'is_seed_phrase', 'is_smart_contract',
        'is_2FA', 'is_exchange', 'is_open_source','security', 'is_qrcode', 'is_touch_id', 'is_fiat', 'is_adapt')
        ->leftjoin('wallet_type', 'wallets.wallet_type_id', '=', 'wallet_type.id')
        ->leftjoin('wallet_description', 'wallets.id', '=', 'wallet_description.wallet_id')
        ->leftjoin('countries', 'countries.id', '=', 'wallets.country_id')
        ->leftjoin(DB::raw("(SELECT
                  wallet_os.wallet_id,
                  GROUP_CONCAT(os.name SEPARATOR ', ') as os
                  FROM wallet_os
                  JOIN os ON wallet_os.os_id = os.id
                  GROUP BY wallet_os.wallet_id
                  ) as wallet_os"),function($join){
            $join->on("wallets.id","=","wallet_os.wallet_id");
        })
        ->leftjoin(DB::raw("(SELECT
                      wallet_language.wallet_id,
                      GROUP_CONCAT(language.name_".$locale." SEPARATOR ', ') as language
                      FROM wallet_language
                      JOIN language ON wallet_language.language_id = language.id
                      GROUP BY wallet_language.wallet_id
                      ) as wallet_language"),function($join){
            $join->on("wallets.id","=","wallet_language.wallet_id");
        })
        ->leftjoin(DB::raw("(SELECT
                          wallet_extra.wallet_id,
                          GROUP_CONCAT(extra.name_".$locale." SEPARATOR ', ') as extra
                          FROM wallet_extra
                          JOIN extra ON wallet_extra.extra_id = extra.id
                          GROUP BY wallet_extra.wallet_id
                          ) as wallet_extra"),function($join){
            $join->on("wallets.id","=","wallet_extra.wallet_id");
        })
        ->leftjoin(DB::raw("(SELECT
                  wallet_support.wallet_id,
                  GROUP_CONCAT(cryptocoins.code SEPARATOR ', ') as code
                  FROM wallet_support
                  JOIN cryptocoins ON wallet_support.cryptocoin_id = cryptocoins.id
                  GROUP BY wallet_support.wallet_id
                  ) as wallet_support"),function($join){
            $join->on("wallets.id","=","wallet_support.wallet_id");
        })
        ->leftjoin('private_key_control','wallets.private_key_control_id','=','private_key_control.id')
        ->leftjoin('validation','wallets.validation_id','=','validation.id')
        ->leftjoin('anonymity','wallets.anonymity_id','=','anonymity.id')
        ->where('wallets.id',$id)->first();


      if (!($wallet)) {
          return abort(404);
      }

    return view('wallet',[
      'wallet' => $wallet,
        /*'wallet_teams' => WalletTeam::select( 'wallet_team.id as id', 'team_group.name_'.$locale.' as team_group_name', 'team_group.id as team_group_id',  'professions.name_'.$locale.' as profession', 'wallet_team.name_'.$locale.' as name', 'avatar')
            ->leftjoin('professions', 'professions.id', '=', 'wallet_team.profession_id')
            ->leftjoin('team_group', 'team_group.id', '=', 'wallet_team.group_id')
            ->where('wallet_team.wallet_id',$id)
            ->orderby('team_group_id')->get(),*/
        'wallet_socials' => WalletSocial::select('socials.icon', 'socials.name', 'link')->leftjoin('socials', 'socials.id', '=', 'wallet_social.social_id')->where('wallet_social.wallet_id',$id)->get(),
        'wallet_links' => WalletLink::select('wallet_links.id', 'wallet_links.link', 'wallet_links.name_'.$locale.' as name')->where('wallet_id',$id)->get(),
        'wallets' => Wallets::select('wallets.id','wallets.name','wallets.logo','wallets.rating','wallet_type.name_'.$locale.' as wallet_type','wallets.private_key_control_id','wallets.is_sponsored','wallet_os.os', 'wallet_support.code as currency', 'private_key_control.name_'.$locale.' as private_key_control_name')
                ->leftjoin('wallet_type', 'wallets.wallet_type_id', '=', 'wallet_type.id')
                ->leftjoin(DB::raw("(SELECT
                  wallet_os.wallet_id,
                  GROUP_CONCAT(os.name SEPARATOR ', ') as os
                  FROM wallet_os
                  JOIN os ON wallet_os.os_id = os.id
                  GROUP BY wallet_os.wallet_id
                  ) as wallet_os"),function($join){
                    $join->on("wallets.id","=","wallet_os.wallet_id");
                })
                ->leftjoin(DB::raw("(SELECT
                  wallet_support.wallet_id,
                  GROUP_CONCAT(cryptocoins.code SEPARATOR ', ') as code
                  FROM wallet_support
                  JOIN cryptocoins ON wallet_support.cryptocoin_id = cryptocoins.id
                  GROUP BY wallet_support.wallet_id
                  ) as wallet_support"),function($join){
                    $join->on("wallets.id","=","wallet_support.wallet_id");
                })
            ->leftjoin('private_key_control','wallets.private_key_control_id', '=', 'private_key_control.id')
            ->where('wallets.id','<>',$id)->orderby('wallets.is_sponsored','DESC')->inRandomOrder()->distinct()->limit(3)->get(),
        'settings'=>$this->settings()
    ]);
  }
}
