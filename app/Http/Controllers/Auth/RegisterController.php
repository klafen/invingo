<?php

namespace App\Http\Controllers\Auth;

use App\Models\Magister;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/invi/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function register(Request $request)
    {


        $request->validate([
          'email' => 'required|string|email|max:255|unique:magisters',
          'password' => 'required|string|min:6|confirmed',
          'secret' => 'required|string|min:6',
        ]);


        if($request->secret == config('app.invi_key')){
          $user =  Magister::create([
              'login' => '',
              'name' => '',
              'email' => $request->email,
              'password' => Hash::make($request->password),
              'api' => md5($request->email.str_random(40)),
              'rank' => '10',
          ]);

          if($user->id) {
              Auth::loginUsingId($user->id);
              return redirect('/invi/dashboard');
          }
        }
        else{
          return redirect('/invireg');
        }

    }
}
