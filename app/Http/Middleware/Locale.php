<?php

namespace App\Http\Middleware;

use App;
use Config;
use Closure;
use Cookie;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $default_lang = Config::get('app.locale');
      $locale = Cookie::get('locale') ?? substr($request->server('HTTP_ACCEPT_LANGUAGE'), 0, 2) ?? $default_lang;
      $locale = in_array($locale, Config::get('app.locales')) ? $locale : $default_lang;
      App::setLocale($locale);
      return $next($request);
    }
}
