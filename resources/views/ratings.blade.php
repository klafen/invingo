@extends('layouts.app')
@section('title',__('ratings.ico_rating_map')." - ")
@section('content')


<div class="section ico__ration">
  <div class="container">
    <h1 class="page__title">{{__('ratings.ico_rating_map')}}</h1>
    <div class="row">
      <div class="col">
        <div class="table-responsive">
          <div class="ico__table-nav">
            <span class="slick-arrow ar-left">
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <use xlink:href="{{asset('assets/img/sprite.svg#arrow-left')}}"></use>
              </svg>
            </span>
            <span class="slick-arrow ar-right">
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <use xlink:href="{{asset('assets/img/sprite.svg#arrow-right')}}"></use>
              </svg>
            </span>
          </div>
          <div class="table-wrapper">
            <div class="table-scroller">
              <table class="rating-table mask" id="rating_table">
                <thead>
                  <tr>
                    <th class="d-flex justify-content-between sticky-col">
                      <span>{{__('ratings.ICO_projects')}}</span>
                      <span class="search__ico">
                        <form  id="ico-form">
                          <input type="text" name="search" id="search" class="search__input" placeholder="{{__('ratings.search')}}...">
                          <span class="svg-icon" id="search-icon">
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                              <use xlink:href="{{asset('assets/img/sprite.svg#search')}}"></use>
                            </svg>
                          </span>
                        </form>
                      </span>
                    </th>
                    <th><span>ICO Gravity</span></th>
                    @foreach($agencies as $agency)
                    <th><span>@if($agency->url)<a href="{{$agency->url}}" target="_blank" title="{{__('ratings.opensite')}}">{{$agency->name}}</a>@else{{$agency->name}}@endif</span></th>
                    @endforeach
                  </tr>
                </thead>
                <tbody>
                  @foreach($projects as $project)
                  <tr>
                    <td class="sticky-col">
                      <a href="{{route('ico-project',$project['id'])}}">
                        <span class="project__icon">
                          <img src="{{$project['logo']}}" alt="logo">
                        </span>
                        {{$project['name']}}
                      </a>
                    </td>
                    <td>{{$project['rating']}}</td>
                    @foreach($agencies as $agency)
                    <td> @if(isset($project['agency'][$agency->id])) {{$project['agency'][$agency->id]}} @else  @endif</td>
                    @endforeach
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function(){
    $("#search").keyup(function(){
      _this = this;

      $.each($("#rating_table tbody tr"), function() {
        if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1) {
          $(this).hide();
        } else {
          $(this).show();
        }
      });
    });
  });
</script>

@endsection
