@extends('layouts.app')
@section('title',$invilink->name." - ")
@section('content')


<div class="section page">
  <div class="container">
    <h1 class="page__title">{{$invilink->name}}</h1>
    <div class="row">
      <div class="col-12 col-md-12">
        {!! $invilink->text !!}
      </div>
      <div class="col">

      </div>
    </div>
  </div>
</div>



@endsection
