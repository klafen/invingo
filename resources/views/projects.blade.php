@extends('layouts.app')
@section('title',__('projects.ico_projects')." - ")
@section('content')

<div class="section arhive__projects">
  <div class="container">
    <h1 class="page__title">{{__('projects.ico_projects')}}</h1>
    <div class="row">
      <div class="col-12 col-md-4 col-lg-3 p-2">
        <div class="filter__col">
          <div class="filter__header">
            <div>
              <h3>{{__('projects.filter')}}</h3>
            </div>
            <div>
              <span class="filter__icon">
                <span class="svg-icon">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <use xlink:href="{{asset('assets/img/sprite.svg#filter-icon')}}"></use>
                  </svg>
                </span>
              </span>
              <a href="javascript:void(0)" class="filter__clear" id="clearfilter">
                <span>{{__('projects.clear')}}</span>
                <span class="svg-icon">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <use xlink:href="{{asset('assets/img/sprite.svg#refresh')}}"></use>
                  </svg>
                </span>
              </a>
            </div>
          </div>
          <div class="filter__body d-none d-md-block">
            <form action="" method="get" name="filter_form">

              @include('project_filters.index',['filters'=>$project_filters])

              <div class="d-block" id="btn_filter" >
                <button type="submit" id="apply_filter" class="btn btn-submit">{{__('projects.apply_filter')}}</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="col">
        <div class="projects__table" id="projects__table">
          @include('projects_ajax')

        </div>
        @if ($page<(floor($count/$pagination) + ($count % $pagination > 0 ? 1 : 0)))
          <div class="project__pagination text-center">
            <a href="javascript:void(0)" class="btn" id="more">
              {{__('projects.load_more')}}
              <span class="eye-icon">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                  <use xlink:href="assets/img/sprite.svg#arrow-down"></use>
                </svg>
              </span>
            </a>
          </div>
        @endif
      </div>
    </div>
  </div>
</div>


@include('project_filters.js_scripts')




@endsection
