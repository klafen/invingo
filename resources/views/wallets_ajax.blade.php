
          <div class="row" >
            <!-- Items -->
            @forelse($wallets as $wallet)
            <div class="@if(isset($class_item)){{$class_item}}@else col-12 col-sm-6 col-lg-4 p-2 @endif">
              <a href="{{route('crypto-wallet',$wallet->id)}}">
                <div class="project__content d-flex">
                  <div class="content__top">
                    <div class="project__logo">
                      <img src="{{$wallet->logo}}" alt="img">
                    </div>
                    <div>
                      <span class="project__name">{{$wallet->name}}</span>
                      <span class="project__category">{{$wallet->wallet_type}}</span>
                    </div>
                    @if($wallet->is_sponsored ==1)
                    <div>
                      <span class="sponsor" data-toggle="tooltip" data-placement="bottom" title="{{__('wallets.sponsored')}}">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                          <use xlink:href="{{asset('assets/img/sprite.svg#hand-shake')}}"></use>
                        </svg>
                      </span>
                    </div>
                    @endif
                  </div>
                  <div class="content__middle">
                    <div>
                      <span class="project__support">{{__('wallets.supported_coins')}}</span>
                      <span class="project__money">{{str_replace(","," / ",$wallet->currency)}}</span>
                      <span class="project__key">{{__('wallets.private_key_control')}}</span>
                      <span class="project__stat">@if($wallet->private_key_control_id == 1) {{__('wallets.available')}} @elseif($wallet->private_key_control_id == 2) {{__('wallets.unavailable')}} @else {{$wallet->private_key_control_name}} @endif</span>
                    </div>
                    <div>
                      <span class="project__count">{{$wallet->rating}}</span>
                    </div>
                  </div>
                  <div class="content__bottom ">
                    <div>
                      @forelse(explode(',',$wallet->os) as $os)
                      <span class="project__state">{{$os}}</span>
                      @empty
                      @endforelse
                    </div>
                  </div>
                </div>
              </a>
            </div>
            @empty
            NO DATA
            @endforelse

          </div>
