<div class="select__title">
  <div>{{$caption}}</div>
  <div>
    <a id="clear{{$name}}" class="clearmultiselect" data-clear="{{$name}}-checkboxes" href="javascript:void(0)">
      {{__('projects.clear')}}
    </a>
  </div>
</div>
<div class="selectBox" data-expand="{{$name}}-checkboxes">
  <select class="custom-select" name="{{$name}}" id="{{$name}}" >
    <option selected value="0">{{__('projects.selected')}}: @if(isset($_GET[$name.'s']) && count($_GET[$name.'s'])>0){{count($_GET[$name.'s'])}}@else 0 @endif</option>
  </select>
  <div class="overSelect"></div>
</div>
<div id="{{$name}}-checkboxes" data-count="@if(isset($_GET[$name.'s']) && count($_GET[$name.'s'])>0){{count($_GET[$name.'s'])}}@else 0 @endif" class="select-checkboxes" data-expanded="false">
  @foreach($items as $item)
    @if($item->id)
    <div class="custom-control custom-checkbox checkbox-select" data-for="#{{$name}}" >
      <input type="checkbox" class="custom-control-input checkbox-select-input"
             name="{{$name}}s[{{$item->id}}]" id="{{$name}}{{$item->id}}"
             @if(isset($_GET[$name.'s']) && array_key_exists($item->id,$_GET[$name.'s'])  ) checked="checked" @endif />
      <label class="custom-control-label checkbox-select-label" for="{{$name}}{{$item->id}}">{{$item->name}}</label>
    </div>
    @endif
  @endforeach
</div>



