<div class="select__title">
  <div>{{__('projects.jurisdiction')}}</div>
  <div><a id="clearjurisdiction" href="javascript:void(0)">
      {{__('projects.clear')}}
    </a></div>
</div>
<select class="custom-select" name="jurisdiction" id="jurisdiction">
  <option value="0">{{__('projects.not_select')}}</option>
  <option value="off" @if(isset($_GET['jurisdiction']) && $_GET['jurisdiction']=='off') selected="selected" @endif>{{__('projects.unverify')}}</option>
  <option value="on" @if(isset($_GET['jurisdiction']) && $_GET['jurisdiction']=='on') selected="selected" @endif >{{__('projects.verify')}}</option>
</select>