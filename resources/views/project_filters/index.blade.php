@foreach($filters as $filter)
  @php if($filter->is_visible_name) {$caption=$filter->name;} else {$caption=null;} @endphp
  @if($filter->type=='multiselect')
    @include('project_filters.multiselect',['items'=>$data_filter[$filter->system_name],'name'=>$filter->system_name,'caption'=>$caption])
  @endif
  @if($filter->type=='onecheckbox')
    @include('project_filters.onecheckbox',['name'=>$filter->system_name,'caption'=>$caption])
  @endif
  @if($filter->type=='custom')
    @include('project_filters.'.$filter->system_name,['caption'=>$caption])
  @endif
@endforeach