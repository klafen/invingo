<script>

  /* slider range init */
  /* jQueryUI */
  if ( $('.custom-range').length || $('#range-slider').length ) {
    $("#range-slider").slider({
      range: true,
      min: 0,
      max: 100,
      values: [@if(isset($_GET['range-start']) ){{$_GET['range-start']}}@else 0 @endif, @if(isset($_GET['range-end']) ){{$_GET['range-end']}}@else 100 @endif],
      slide: function (event, ui) {
        $("#range-start").val(ui.values[0]);
        $("#range-end").val(ui.values[1]);
      }
    });
    $("#range-start").val($("#range-slider").slider("values", 0));
    $("#range-end").val($("#range-slider").slider("values", 1));
  }


  $('#clearrating').click(function() {
    $("#range-slider").slider({
      range: true,
      min: 0,
      max: 100,
      values: [@if(isset($_GET['range-start']) ){{$_GET['range-start']}}@else 0 @endif, @if(isset($_GET['range-end']) ){{$_GET['range-end']}}@else 100 @endif],
      slide: function (event, ui) {
        $("#range-start").val(ui.values[0]);
        $("#range-end").val(ui.values[1]);
      }
    });
    $("#range-start").val($("#range-slider").slider("values", 0));
    $("#range-end").val($("#range-slider").slider("values", 1));
  });

  $('#clearsubstatus').click(function() {
    $('#substatus').prop('selectedIndex', 0);
    $('#ch1').prop('checked', false);
    $('#ch2').prop('checked', false);
    $('#ch3').prop('checked', false);
  });

  $('#clearregistration').click(function() {
    $('#kyc').prop('checked', false);
    $('#kyt').prop('checked', false);
    $('#white_list').prop('checked', false);
  });


  $('#clearjurisdiction').click(function() {
    $('#jurisdiction').prop('selectedIndex', 0);
  });

  $('#cleardate').click(function() {
    $('#from').prop('value', '');
    $('#to').prop('value', '');
    $('#ch_tba').prop('checked', false);
  });

  $('#clearfilter').click(function() {
    $('#clearrating').click();
    $('#clearregistration').click();
    $('#clearjurisdiction').click();
    $('#clearcountry').click();
    $('#mvp').prop('checked', false);
    $('#airdrop').prop('checked', false);
    $('#bonus').prop('checked', false);
    $('#escrow').prop('checked', false);
    $('#bounty').prop('checked', false);
    $('#smart_contract').prop('checked', false);
    $('#etherscancheck').prop('checked', false);
    $('#howey_test').prop('checked', false);
    $('#cleardate').click();
    $('.clearmultiselect').click();
    $('.custom__switch-input').prop('checked', false)
  });

  var page = 1;
  var pageCount = {{ floor($count / $pagination) + ($count % $pagination > 0 ? 1 : 0) }};
  var more = $('#more');
  more.click(function() {
    more.prop('disabled', true);
    $('#projects-notice').remove();
    ++page;
    if (page <= pageCount) {
      var separator = $('<h5><span>{{__('pagination.page')}} ' + page + ' {{__('pagination.of')}} ' + pageCount + '</span></h5>');
      var list = $('#projects__table');
      list.append(separator).append('<p id="projects-notice" style="text-align:center;">{{__('pagination.loading')}}...</p>');
      $.ajax({
        cache: false,
        data: $('form[name="filter_form"]').serialize() + '&page=' + page,
        dataType: 'html',
        error: function () {
          $('#projects-notice').remove();
          list.find('h5').last().remove();
          list.append('<p id="projects-notice" style="margin-top:20px;text-align:center;">{{__('pagination.loading_failed')}}</p>');
          --page;
          more.prop('disabled', false);
        },
        success: function (data) {
          $('#projects-notice').remove();
          list.append(data);
          if (page == pageCount) more.parent().hide();
          more.prop('disabled', false);
        },
        type: 'GET',
        url: $(location).attr('href')
      });
    }
    else more.parent().hide();
  });


  $(document).ready(function($) {
    $window = $(window);
    $window.resize(function() {
      $apply_filter = $('#apply_filter');
      $btn_filter_block=$('#btn_filter');
      $('.select-checkboxes').css('width',$btn_filter_block.innerWidth()+'px')
      if ($window.width() < 768) {
        $apply_filter.addClass('fixed-apply-button');
        $window.scroll(function() {
          $apply_filter.css('width',$btn_filter_block.innerWidth()+'px');
          $h = $btn_filter_block.offset().top - $window.scrollTop()-$window.height();
          if ($h>0 && $window.width() < 768) {
            $apply_filter.addClass('fixed-apply-button');
          } else {
            $apply_filter.removeClass('fixed-apply-button');
          }
        });
      }
      else {
        $apply_filter.removeClass('fixed-apply-button');
      }
    });
    $window.resize();
  });

  $('.selectBox').on('click', function(e) {
    var selector=$(this).data('expand');
    var checkboxes = $('#'+selector);
    if (checkboxes.data('expanded')) {
      checkboxes.hide();
      checkboxes.data('expanded',false);
    }
    else {
      hideAllCheckboxes();
      checkboxes.show();
      checkboxes.data('expanded',true);
      e.stopPropagation(); // This is the preferred method.
      return false;
    }
  });

  function hideAllCheckboxes(){
    var select_checkboxes=$('.select-checkboxes');
    select_checkboxes.hide();
    select_checkboxes.data('expanded',false);
  }

  $('.checkbox-select-input').on('click', function(e) {
    var $k=$(this).parent().parent().data('count');
    if($(this).prop('checked')) {
      $k++;
    }
    else {
      $k--;
    }
    $($(this).parent().data('for')+' option:selected').text('{{__('projects.selected')}}: '+($k) );
    $(this).parent().parent().data('count',$k);
  });

  $('.clearmultiselect').on('click', function(e) {
    var div_checkboxes = $('#'+$(this).data('clear'));
    div_checkboxes.data('count',0);
    $($('#'+$(this).data('clear')+' :first-child').data('for')+' option:selected').text('{{__('projects.selected')}}: 0');
    div_checkboxes.children().each(function(){
      $(this).children().prop('checked', false);
    });


  });



  $(document).click(function() {
    hideAllCheckboxes();
  });
</script>