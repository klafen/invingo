<!-- Registration -->
<div class="select__title">
  <div>{{__('projects.registration')}}</div>
  <div>
    <a id="clearregistration" href="javascript:void(0)">
      {{__('projects.clear')}}
    </a>
  </div>
</div>
<div class="custom-control custom-checkbox">
  <input class="custom-control-input" type="checkbox" id="kyc" name="kyc"  @if(isset($_GET['kyc']) && $_GET['kyc']=='on') checked="checked" @endif >
  <label class="custom-control-label" for="kyc">{{__('projects.kyc')}}</label>
</div>
<div class="custom-control custom-checkbox">
  <input class="custom-control-input" type="checkbox" id="kyt" name="kyt" @if(isset($_GET['kyt']) && $_GET['kyt']=='on') checked="checked" @endif>
  <label class="custom-control-label" for="kyt">{{__('projects.kyt')}}</label>
</div>
<div class="custom-control custom-checkbox">
  <input class="custom-control-input" type="checkbox" id="white_list" name="white_list" @if(isset($_GET['white_list']) && $_GET['white_list']=='on') checked="checked" @endif>
  <label class="custom-control-label" for="white_list">{{__('projects.white_list')}}</label>
</div>