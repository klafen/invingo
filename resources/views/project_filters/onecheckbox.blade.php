<div class="select__title">
  <div>{{$caption}}</div>
  <div>
    <span class="custom-switch">
      <input type="checkbox" class="custom__switch-input" id="{{$name}}" name="{{$name}}" @if(isset($_GET[$name]) && $_GET[$name]=='on') checked="checked" @endif>
      <label class="custom__switch-label" for="{{$name}}">
        <span class="custom__switch-point"></span>
      </label>
    </span>
  </div>
</div>