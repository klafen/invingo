<!-- Rating -->
<div class="select__title">
  <div>{{__('projects.rating')}}</div>
  <div>
    <a id="clearrating" href="javascript:void(0)">
      {{__('projects.clear')}}
    </a>
  </div>
</div>
<div class="custom-range">
  <div class="d-flex justify-content-between">
    <input id="range-start" name="range-start" type="text" value="" readonly>
    <input id="range-end" name="range-end" type="text" value="" readonly>
  </div>
  <div id="range-slider" class="custom__range-slider"></div>
</div>