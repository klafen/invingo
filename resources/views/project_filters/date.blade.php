<!-- date -->
<div class="select__title">
  <div>{{__('projects.publication_date')}}</div>
  <div>
    <a href="javascript:void(0)" id="cleardate">
      {{__('projects.clear')}}
    </a>
  </div>
</div>
<div id="date-container" class="custom-datepicker">
  <div class="input-daterange input-group input-group-sm">
    <input type="text" class="form-control custom-calendar" id="from" name="from" @if(isset($_GET['from']) && $_GET['from']!='') value="{{$_GET['from']}}" @endif  placeholder="2018-01-01">
    <input type="text" class="form-control custom-calendar" id="to" name="to" @if(isset($_GET['to']) && $_GET['to']!='') value="{{$_GET['to']}}" @endif  placeholder="2018-12-31">
  </div>
</div>
<div class="custom-control custom-checkbox">
  <input class="custom-control-input" type="checkbox" id="ch_tba" name="tba"  @if(isset($_GET['tba']) && $_GET['tba']=='on') checked="checked" @endif >
  <label class="custom-control-label" for="ch_tba">{{__('projects.TBA')}}</label>
</div>