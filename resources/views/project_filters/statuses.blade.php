<div class="custom-control custom-checkbox">
  <input class="custom-control-input" type="checkbox" id="ch1" name="private"  @if(isset($_GET['private']) && $_GET['private']=='on') checked="checked" @endif >
  <label class="custom-control-label" for="ch1">{{__('projects.private_sale')}}</label>
</div>
<div class="custom-control custom-checkbox">
  <input class="custom-control-input" type="checkbox" id="ch2" name="presale" @if(isset($_GET['presale']) && $_GET['presale']=='on') checked="checked" @endif>
  <label class="custom-control-label" for="ch2">{{__('projects.pre_sale')}}</label>
</div>
<div class="custom-control custom-checkbox">
  <input class="custom-control-input" type="checkbox" id="ch3" name="sale" @if(isset($_GET['sale']) && $_GET['sale']=='on') checked="checked" @endif>
  <label class="custom-control-label" for="ch3">{{__('projects.sale')}}</label>
</div>