@extends('layouts.app')
@section('title',__('home.metodology')." - ")
@section('content')


<div class="section page">
  <div class="container">
    <h1 class="page__title">{{__('home.metodology')}}</h1>
    <div class="row">
      <div class="col-12 col-md-12">
        {!! $main_metodology->text !!}
      </div>
      <div class="col">

      </div>
    </div>
  </div>
</div>



@endsection
