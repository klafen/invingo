@if (count($teams)>0)
  @php
    $team_group_name=$teams[0]->team_group_name;
    $groups_count=1;
    foreach($teams as $team) {
      if ($team->team_group_name != $team_group_name) {
        $groups_count++;
        }
    }
  @endphp
  @if ($groups_count>1)
    <div class="detailed_about">
      <div class="detailed_about-title">
        <ul class="d-flex justify-content-between nav" role="tablist">
          @php $team_group_name=$teams[0]->team_group_name; @endphp
          <li>
            <a class="active" href="#{{$teams[0]->team_group_name}}" data-toggle="tab" role="tab" aria-controls="{{$teams[0]->team_group_name}}" id="{{$teams[0]->team_group_name}}-tab" aria-selected="true">{{$teams[0]->team_group_name}}</a>
          </li>
          @foreach($teams as $team)
            @if ($team->team_group_name != $team_group_name)
              <li>
                <a href="#{{$team->team_group_name}}" data-toggle="tab" role="tab" aria-controls="{{$team->team_group_name}}" id="{{$team->team_group_name}}-tab">{{$team->team_group_name}}</a>
              </li>
              @php $team_group_name=$team->team_group_name @endphp
            @endif
          @endforeach
        </ul>
      </div>
      <div class="tab-content">
        @php $team_group_name=$teams[0]->team_group_name; @endphp
        <div class="detailed_about-content tab-pane fade show active" id="{{$teams[0]->team_group_name}}" role="tabpanel" aria-labelledby="{{$teams[0]->team_group_name}}-tab">
          @foreach($teams as $team)
            @if ($team->team_group_name != $team_group_name)
        </div>
        <div class="detailed_about-content tab-pane fade" id="{{$team->team_group_name}}" role="tabpanel" aria-labelledby="{{$team->team_group_name}}-tab">
              @php $team_group_name=$team->team_group_name @endphp
            @endif
          @include('team_item', ['team'=>$team])
          @endforeach
        </div>
      </div>
    </div>
  @else
    @foreach($teams as $team)
      @include('team_item', ['team'=>$team])
    @endforeach
  @endif
@endif