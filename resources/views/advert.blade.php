@extends('layouts.app')
@section('content')

<div class="section adverti">
  <div class="container">
    <h1 class="page__title">{{__('advert.advertisign_contacts')}}</h1>
    <div class="row">
      <div class="col">
        @if ($adv_preheader)
        <div class="adverti__full-content backgroud big-text">
            {!! $adv_preheader->text !!}
        </div>
        @endif
        <div class="adverti__full-content">
          @if ($adv_offer)
            {!! $adv_offer->text !!}
          @endif
        </div>
        <div class="adverti__card-content">
          <div class="row">
            @forelse($contracts as $contract)
            <div class="col-12 col-sm-6 col-lg-4 p-1">
              <div class="card card__content">
                <div class="card-header">
                  <h2>{{$contract->name}}</h2>
                  <div class="divider"></div>
                </div>
                <div class="card-body">
                  {!!$contract->text!!}
                </div>
                <div class="card-footer">
                  @if($contract->is_discussing == 1)
                  <div class="card__option">
                    <span>{{__('advert.discussing_option')}}</span>
                  </div>
                  @endif
                  <a href="javascript:void(0)" class="btn btn__card card-footer" data-contract-name="{{$contract->name}}">{{__('advert.submit')}}</a>
                </div>
              </div>
            </div>
            @empty
            NO DATA
            @endforelse
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="modal-form" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-form-title"></h2>
        <a href="#" class="close btn__modal-close" data-dismiss="modal" aria-label="Close">
          <span class="svg-icon">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
              <use xlink:href="assets/img/sprite.svg#close"></use>
            </svg>
          </span>
        </a>
      </div>
      <form action="" id="modal_form">
      <div class="modal-body">
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="inputName">{{__('advert.your_name')}}</label>
            <input type="text" class="form-control" name="name" id="inputName" required>
          </div>
          <div class="form-group col-md-6">
            <label for="inputPhone">{{__('advert.phone')}}</label>
            <input type="tel" class="form-control" name="phone" id="inputPhone" required>
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="inputEmail">{{__('advert.email')}}</label>
            <input type="email" class="form-control" name="email" id="inputEmail" required>
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-12">
            <label for="inputMessage">{{__('advert.message')}}</label>
            <textarea class="form-control textarea" name="message" id="inputMessage"></textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <input type="hidden" name="contract_name" value="">
        <button type="submit" class="btn btn__modal-submit">{{__('advert.submit_application')}}</button>
      </div>
      </form>
    </div>
  </div>
</div>

  <script>
    $('#modal_form').on('submit',function(e){
      e.preventDefault();
      $(this).attr('disabled',true);
      var _data = {
        user : $(this).serialize(),
        _token: $('meta[name="csrf-token"]').attr('content')
      };
      $.ajax({
        type: "POST",
        url: "/ajax/savecontracts",
        data: _data
      }).done(function( result ) {
        if(result.status == 'done'){
          $('#modal_form').html('<div style="text-align: center; padding: 40px">{{__('advert.success')}}</div>');
        }
      });
    });
  </script>

@endsection
