@if (count($teams)>0)
  @php
    $team_group_name=$teams[0]->group->name();
    $groups_count=1;
    foreach($teams as $team) {
      if ($team->group->name() != $team_group_name) {
        $groups_count++;
        }
    }
  @endphp
  @if ($groups_count>1)
    <div class="detailed_about">
      <div class="detailed_about-title">
        <ul class="d-flex justify-content-between nav" role="tablist">
          @php $team_group_name=$teams[0]->group->name(); @endphp
          <li>
            <a class="active" href="#{{$team_group_name}}" data-toggle="tab" role="tab" aria-controls="{{$team_group_name}}" id="{{$team_group_name}}-tab" aria-selected="true">{{$team_group_name}}</a>
          </li>
          @foreach($teams as $team)
            @if ($team->group->name() != $team_group_name)
              <li>
                <a href="#{{$team->group->name()}}" data-toggle="tab" role="tab" aria-controls="{{$team->group->name()}}" id="{{$team->group->name()}}-tab">{{$team->group->name()}}</a>
              </li>
              @php $team_group_name=$team->group->name() @endphp
            @endif
          @endforeach
        </ul>
      </div>
      <div class="tab-content">
        @php $team_group_name=$teams[0]->group->name(); @endphp
        <div class="detailed_about-content tab-pane fade show active" id="{{$team_group_name}}" role="tabpanel" aria-labelledby="{{$team_group_name}}-tab">
          @foreach($teams as $team)
            @if ($team->group->name() != $team_group_name)
        </div>
        <div class="detailed_about-content tab-pane fade" id="{{$team->group->name()}}" role="tabpanel" aria-labelledby="{{$team->group->name()}}-tab">
              @php $team_group_name=$team->group->name() @endphp
            @endif
          @include('teamlist_item', ['team'=>$team])
          @endforeach
        </div>
      </div>
    </div>
  @else
    @foreach($teams as $team)
      @include('teamlist_item', ['team'=>$team])
    @endforeach
  @endif
@endif