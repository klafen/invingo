@if($settings['footer_disk'])
  <div class="section metodology">
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-3">
          <h2 class="section__title">{{__('footer.disclamer')}}</h2>
        </div>
        <div class="col-12 col-lg-9">
          {!! $settings['footer_disk']->text !!}
        </div>
      </div>
    </div>
  </div>
@endif


<div class="top-bg"></div>
<div class="middle-bg"></div>
<div class="bottom-bg"></div>

<footer>
  <div class="container">
    <div class="row">
      <div class="col-12 col-lg-3 col-md-6 order-md-0">
        <h3>{{__('footer.links')}}</h3>
        <nav class="nav flex-column footer__menu">
          <a class="nav-link" href="{{route('advertise')}}">{{__('menu.advertisign_contacts')}}</a>
          @foreach($settings['links'] as $link)
          <a class="nav-link" href="{{route('invilink',$link->url)}}">{{$link->name}}</a>
          @endforeach
          @if ($settings['cryptoadressfooter']) {!! $settings['cryptoadressfooter']->text  !!}@endif
        </nav>
      </div>
      <div class="col-12 col-lg-6 order-md-2 order-lg-1">

      </div>
      <div class="col-12 col-lg-3 col-md-6 order-md-1">
        @if (count($settings['invisocials'])>0)
        <h3>{{__('footer.follow_us')}}</h3>
        <ul class="footer__social">
          @foreach($settings['invisocials'] as $invisocial)
              <li>
                <a href="{{$invisocial->link}}" target="_blank">
                  <span class="svg-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                      <use xlink:href="{{asset('assets/img/sprite.svg#'.$invisocial->name)}}"></use>
                    </svg>
                  </span>
                </a>
              </li>
           @endforeach
        </ul>
          @endif
      </div>
    </div>
    <div class="row footer__bottom">
      <div class="order-1 order-lg-0 col-12 col-lg-3">
        <span>©2018 ICO Gravity</span>
      </div>
      <div class="order-0 col-12 col-lg-9">
        <span>{{__('footer.contact_email')}}
          <a href="mailto:{{$settings['parameters']->email_contact}}">{{$settings['parameters']->email_contact}}</a>
        </span>
      </div>
    </div>
  </div>
</footer>

<!-- search -->
<div class="search">
  <a id="btn-search-close" class="btn--search-close" aria-label="Close search form">
    <span class="svg-icon">
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <use xlink:href="{{asset('assets/img/sprite.svg#close')}}"></use>
      </svg>
    </span>
  </a>
  <form class="search__form" action="{{url('search')}}" method="get">
    <input name="s" id="search__input" class="search__input" type="search" placeholder="{{__('footer.search')}}..." autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" />
    <span class="search__info">{{__('footer.tooltip_search')}}</span>
    <div id="results_ajax"></div>
  </form>
</div><!-- /search -->

<!--[if lt IE 9]>
  <script src="libs/html5shiv/es5-shim.min.js"></script>
  <script src="libs/html5shiv/html5shiv.min.js"></script>
  <script src="libs/html5shiv/html5shiv-printshiv.min.js"></script>
  <script src="libs/respond/respond.min.js"></script>
  <![endif]-->
<script src="{{asset('assets/libs/bootstrap/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('assets/libs/slick/slick.min.js')}}"></script>
<script src="{{asset('assets/libs/simplebar/simplebar.js')}}"></script>
<script src="{{asset('assets/js/common.js')}}"></script>
<script src="{{asset('assets/js/script.js')}}"></script>
