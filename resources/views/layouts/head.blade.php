<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>@yield('title') {{$settings['parameters']->namecaption_site}}</title>
  <meta name="description" content="">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/img/favicon/apple-touch-icon.png')}}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{asset('assets/img/favicon/favicon-32x32.png')}}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/img/favicon/favicon-16x16.png')}}">
  <link rel="manifest" href="{{asset('assets/img/favicon/site.webmanifest')}}">
  <link rel="mask-icon" href="{{asset('assets/img/favicon/safari-pinned-tab.svg')}}" color="#5bbad5">
  <link rel="shortcut icon" href="{{asset('assets/img/favicon/favicon.ico')}}">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-config" content="{{asset('assets/img/favicon/browserconfig.xml')}}">
  <meta name="theme-color" content="#ffffff">
  <link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}" />
  <link rel="stylesheet" href="{{asset('assets/libs/simplebar/simplebar.css')}}" />
  <link rel="stylesheet" href="{{asset('assets/css/main.min.css')}}" />
  <link rel="stylesheet" href="{{asset('assets/css/fix.min.css')}}" />
  <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}" />
  <script src="{{asset('assets/libs/modernizr/modernizr.min.js')}}"></script>
  <script src="{{asset('assets/libs/jquery/jquery-3.3.1.min.js')}}"></script>
  <script src="{{asset('assets/libs/jquery-ui/jquery-ui.min.js')}}"></script>
  <script src="{{asset('assets/libs/countdown/js/jquery.mb-comingsoon.min.js')}}"></script>
</head>
