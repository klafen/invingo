<ul class="nav d-none d-md-flex justify-content-between">
  <li class="nav-item"> <!--active-->
    <a class="nav-link" href="{{route('ico-projects')}}">
      <span>{{__('menu.ico_projects')}}</span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{route('crypto-wallets')}}">
      <span>{{__('menu.rypto_wallets_for_ICO')}}</span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{route('ico-ratings')}}">
      <span>{{__('menu.ICO rating_map')}}</span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{route('metodology')}}">
      <span>{{__('home.metodology')}}</span>
    </a>
  </li>
</ul>
<div class="site__search">
  <a href="javascript:void(0);" id="btn-search">
    <span class="svg-icon">
      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <use xlink:href="{{asset('assets/img/sprite.svg#search')}}"></use>
      </svg>
    </span>
  </a>
</div>
<div class="hamburger hamburger--squeeze d-md-none">
  <div class="hamburger-box">
    <div class="hamburger-inner"></div>
  </div>
</div>
