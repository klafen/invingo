<header>
<div class="">
  <div class="container">
    <div class="row">
      <div class="col-6 col-md-3">
        <div class="logo__wrap">
          <a href="/">
            <img src="{{asset('assets/img/logo.svg')}}" alt="logo" class="figure-img">
          </a>
        </div>
      </div>
      <div class="col-6 col-md-9 pl-md-0">
        <div class="row">
          <div class="d-none d-md-block col-6">
            <div class="header__social">
              <ul>
                @foreach($settings['invisocials'] as $invisocial)
                  <li>
                    <a href="{{$invisocial->link}}" target="_blank">
                      <span class="svg-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                          <use xlink:href="{{asset('assets/img/sprite.svg#'.$invisocial->name)}}"></use>
                        </svg>
                      </span>
                      @if ($invisocial->followers) +{{$invisocial->followers}} @endif
                    </a>
                  </li>
                @endforeach
              </ul>
            </div>
          </div>
          <div class="d-md-block col-6">
            <div class="header__course">
              <ul>
                <li>BTC $<span id="btc-curs"></span> <span class="percent" id="btc-percent"></span>%</li>
                <li>ETH $<span id="eth-curs"></span> <span class="percent" id="eth-percent"></span>%</li>
              </ul>
            </div>
            <div class="site__lang">
              <a href="{{route('setlocale', \App::getLocale('locale') == 'ru' ? 'en' : 'ru' )}}">
                <span>{{\App::getLocale('locale') == 'ru' ? 'En' : 'Ру'}}</span>
              </a>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col">
            @include('layouts.menu')
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</header>
