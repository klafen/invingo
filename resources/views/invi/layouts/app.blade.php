@include('invi.layouts.head')
<body class="hold-transition skin-blue sidebar-mini fixed">
@include('invi.layouts.menu')
@yield('content')
@include('invi.layouts.footer')
</body>
</html>
