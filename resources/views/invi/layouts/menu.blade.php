<div class="wrapper">
  <header class="main-header">
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
  </header>
  <aside class="main-sidebar">
    <section class="sidebar">
      <div class="user-panel">
        <div class="ulogo">
          <a href="{{route('invi_dashboard')}}">
            <img src="{{ asset('invi/img/logo.svg') }}" alt="">
          </a>
        </div>
        <div class="info">
          <p>{{ Auth::user()->email }}</p>
          <a href="{{ route('logout') }}" class="link" data-toggle="tooltip" title=""  onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fa fa-power-off"></i></a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
          </form>
        </div>
      </div>
      <ul class="sidebar-menu" data-widget="tree">
        <li class="nav-devider"></li>
        <li class="header nav-small-cap">МЕНЮ</li>
        <li class="{{ request()->is('/invi/dashboard') ? 'active' : '' }}">
          <a href="{{route('invi_dashboard')}}">
          <i class="fa fa-dashboard"></i> <span>Рабочий стол</span>
        </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-cogs" aria-hidden="true"></i>
            <span>Справочники</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('categories.index')}}">Категории</a></li>
            <li><a href="{{route('platforms.index')}}">Платформы</a></li>
            <li><a href="{{route('cryptocoins.index')}}">Криптовалюты</a></li>
            <li><a href="{{route('countries.index')}}">Страны</a></li>
            <li><a href="{{route('status.index')}}">Статусы</a></li>
            <li><a href="{{route('language.index')}}">Языки</a></li>
            <li><a href="{{route('extra.index')}}">Extra</a></li>
            <li><a href="{{route('agencies-rating.index')}}">Рейтинговые агенты</a></li>
            <li><a href="{{route('custom-rating.index')}}">Доп рейтинги</a></li>
            <li><a href="{{route('professions.index')}}">Профессии</a></li>
            <li><a href="{{route('os.index')}}">ОС</a></li>
            <li><a href="{{route('wallettype.index')}}">Типы кошельков</a></li>
            <li><a href="{{route('socials.index')}}">Соц сети</a></li>
            <li><a href="{{route('type_tokens.index')}}">Типы токенов</a></li>
            <li><a href="{{route('tradings.index')}}">Биржи</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-cogs" aria-hidden="true"></i>
            <span>Настройки сайта</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('invisocials.index')}}">Соц сети в шапке и футере</a></li>
            <li><a href="{{route('contentsettings.index')}}">Контентные блоки</a></li>
            <li><a href="{{route('invilinks.index')}}">Службные страницы</a></li>
            <li><a href="{{route('settings.index')}}">Параметры</a></li>
            <li><a href="{{route('project-filter.index')}}">Фильтр проектов</a></li>
            <li><a href="{{route('wallet-filter.index')}}">Фильтр кошельков</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-tags" aria-hidden="true"></i>
            <span>Реклама</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{route('advert.index')}}">Реклама</a></li>
            <li><a href="{{route('advrequest.index')}}">Заявки</a></li>
          </ul>
        </li>
        <li class="{{ request()->is('/invi/wallets') ? 'active' : '' }}">
          <a href="{{route('wallets.index')}}">
          <i class="fa fa-money" aria-hidden="true"></i> <span>Кошельки</span>
        </a>
        </li>
        <li class="{{ request()->is('/invi/projects') ? 'active' : '' }}">
          <a href="{{route('projects.index')}}">
          <i class="fa fa-folder-open" aria-hidden="true"></i> <span>Проекты</span>
        </a>
        </li>
      </ul>
    </section>
  </aside>
