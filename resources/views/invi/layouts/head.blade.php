<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="@yield('description')">
    <meta name="author" content="invi">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ asset('invi/favicon.ico') }}">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{ asset('invi/css/bootstrap.css') }}?{{ time() }}">
    <link rel="stylesheet" href="{{ asset('invi/css/export.css') }}?{{ time() }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('invi/css/bootstrap-extend.css') }}?{{ time() }}">
    <link rel="stylesheet" href="{{ asset('invi/css/admin_style.css') }}?{{ time() }}">
    <meta name="robots" content="noindex, nofollow"/>
    <script src="{{ asset('invi/js/jquery.js') }}?{{ time() }}"></script>
    <script src="{{ asset('invi/js/popper.min.js') }}?{{ time() }}"></script>
    <script src="{{ asset('invi/js/bootstrap.min.js') }}?{{ time() }}"></script>
    <script src="{{ asset('invi/js/jquery.slimscroll.js') }}?{{ time() }}"></script>
    <script src="{{ asset('invi/js/fastclick.js') }}?{{ time() }}"></script>
    <script src="{{ asset('invi/js/jquery.dataTables.min.js') }}?{{ time() }}"></script>
    <script src="{{ asset('invi/js/template.js') }}?{{ time() }}"></script>
    <script src="{{ asset('invi/js/script.js') }}?{{ time() }}"></script>
  </head>
