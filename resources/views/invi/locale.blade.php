@extends('invi.layouts.app')
@section('title','inviadmin - '.$locale->name)
@section('description','inviadmin - Языковая зона '.$locale->name)
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title') {{$locale->name}} @endslot
    @slot('parent') Языковые зоны @endslot
    @slot('parent_link') {{route('locales.index')}} @endslot
    @slot('active') {{$locale->name}} @endslot
  @endcomponent
  <section class="content">
      <div class="row">
        <div class="col-lg-6 col-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Языковая зона</h3>
            </div>
            <div class="box-body">
              <form method="post" action="{{route('locales.update', $locale)}}">
                <input type="hidden" name="_method" value="put">
                <input type="hidden" name="id" value="{{$locale->id}}">
                {{csrf_field()}}
                <div class="form-group">
                  <h5>Название <span class="text-danger">*</span></h5>
                  <div class="controls">
                    <input type="text" name="name" class="form-control" required=""  value="{{$locale->name}}">
                  </div>
                </div>
                <div class="form-group">
                  <h5>Код <span class="text-danger">*</span></h5>
                  <div class="controls">
                    <input type="text" name="code" class="form-control" required=""  value="{{$locale->code}}">
                  </div>
                </div>
                <button type="submit" class="btn btn-success">Сохранить</button>
                <button type="button" class="btn btn-danger" onclick="event.preventDefault();document.getElementById('delete').submit();">Удалить</button>
              </form>
            </div>
          </div>
        </div>
    </div>
  </section>
</div>



<form id="delete" action="{{ route('locales.destroy',$locale->id) }}" method="POST" style="display: none;">
  <input type="hidden" name="_method" value="delete">
  @csrf
</form>

@endsection
