@extends('invi.layouts.app')
@section('title','inviadmin - Проекты')
@section('description','inviadmin - Проекты')
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title') Проекты @endslot
    @slot('active') Проекты @endslot
  @endcomponent
  <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="box box-solid bg-dark">
            <div class="box-header with-border">
              <div class="row">
                <div class="col-lg-12">
                  <h3 class="box-title">Полный список проектов</h3>
                  <h6 class="box-subtitle">Данные можно экспортировать в CSV, Excel, PDF.</h6>
                  <div class="add-client-btn">
                    <a href="{{route('projects.create')}}" class="btn btn-block btn-success" >Добавить проект</a>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

        <div class="table-responsive">
          <table id="projects" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
          <thead>
            <tr>
              <th>ID</th>
              <th>Название</th>
              <th>Токен</th>
              <th>Рейтинг</th>
              <th>Статус</th>
            </tr>
          </thead>
          <tbody>
            @forelse($projects as $project)
              <tr>
                <td>{{$project->id}}</td>
                <td><a href="{{route('projects.show',$project->id)}}">{{$project->name}}</a></td>
                <td>{{$project->token}}</td>
                <td>{{$project->rating}}</td>
                <td>@if($project->is_visible == 1) <span class="label label-success">Виден</span> @else <span class="label label-danger">Не виден</span> @endif</td>
              </tr>
            @empty
              <tr>
                <td colspan="5">Нет данных</td>
              </tr>
            @endforelse
          </tbody>
          <tfoot>
            <tr>
              <th>ID</th>
              <th>Название</th>
              <th>Токен</th>
              <th>Рейтинг</th>
              <th>Статус</th>
            </tr>
          </tfoot>
        </table>
        </div>
            </div>
            <!-- /.box-body -->
          </div>


        </div>
      </div>
  </section>
@if(isset($projects[0]))
  @component('invi.components.table_excel')
    @slot('table_id') projects @endslot
  @endcomponent
@endif
</div>
@endsection
