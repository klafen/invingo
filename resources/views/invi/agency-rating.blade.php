@extends('invi.layouts.app')
@section('title','inviadmin - '.$agency->name)
@section('description','inviadmin - Агенты '.$agency->name)
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title') {{$agency->name}} @endslot
    @slot('parent') Агенты @endslot
    @slot('parent_link') {{route('agencies-rating.index')}} @endslot
    @slot('active') {{$agency->name}} @endslot
  @endcomponent
  <section class="content">
      <div class="row">
        <div class="col-lg-6 col-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Агенты</h3>
            </div>
            <div class="box-body">
              <form method="post" action="{{route('agencies-rating.update', $agency)}}">
                <input type="hidden" name="_method" value="put">
                <input type="hidden" name="id" value="{{$agency->id}}">
                {{csrf_field()}}
                <div class="form-group">
                  <h5>Название <span class="text-danger">*</span></h5>
                  <div class="controls">
                    <input type="text" name="name" class="form-control" required=""  value="{{$agency->name}}">
                  </div>
                </div>
                <div class="form-group">
                  <h5>Ссылка <span class="text-danger">*</span></h5>
                  <div class="controls">
                    <input type="url" name="url" class="form-control" required=""  value="{{$agency->url}}">
                  </div>
                </div>
                <div class="form-group">
                  <h5>Основной?</h5>
                  <div class="controls">
                    <input type="radio" id="is_main1" name="is_main" value="1" @if($agency->is_main == 1) checked @endif>
                    <label for="is_main1">Да</label>
                    <input type="radio" id="is_main0" name="is_main" value="0" @if($agency->is_main == 0) checked @endif>
                    <label for="is_main0">Нет</label>
                  </div>
                </div>
                <button type="submit" class="btn btn-success">Сохранить</button>
                <button type="button" class="btn btn-danger" onclick="event.preventDefault();document.getElementById('delete').submit();">Удалить</button>
              </form>
            </div>
          </div>
        </div>
    </div>
  </section>
</div>



<form id="delete" action="{{ route('agencies-rating.destroy',$agency->id) }}" method="POST" style="display: none;">
  <input type="hidden" name="_method" value="delete">
  @csrf
</form>

@endsection
