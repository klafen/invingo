@extends('invi.layouts.app')
@section('title','inviadmin - Контентный блок - '.$contentsetting->name_ru)
@section('description','inviadmin - Контентный блок - '.$contentsetting->name_ru)
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title') Контентный блок - {{$contentsetting->name_ru}} @endslot
    @slot('parent') Контентный блок @endslot
    @slot('parent_link') {{route('advert.index')}} @endslot
    @slot('active') Контентный блок - {{$contentsetting->name_ru}} @endslot
  @endcomponent
  <section class="content">
    <form method="post" action="{{route('contentsettings.update',$contentsetting)}}">
      <input type="hidden" name="_method" value="put">
      <input type="hidden" name="id" value="{{$contentsetting->id}}">
      {{csrf_field()}}
      <div class="row">
        <div class="col-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Данные Контентного блока</h3>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-6">
                  <div class="form-group">
                    <h5>Название</h5>
                    <div class="controls">
                      <input type="text" name="caption" class="form-control" required=""  value="{{ $contentsetting->caption }}">
                    </div>
                  </div>
                  <div class="form-group">
                    <h5>Переменная</h5>
                    <div class="controls">
                      <input type="text" name="name" class="form-control" required=""  value="{{ $contentsetting->name }}">
                    </div>
                  </div>
                  <div class="form-group">
                    <h5>Видимость</h5>
                    <div class="controls">
                      <input type="radio" id="is_visible1" name="is_visible" value="1" @if($contentsetting->is_visible == 1) checked @endif>
                      <label for="is_visible1">Да</label>
                      <input type="radio" id="is_visible0" name="is_visible" value="0" @if($contentsetting->is_visible == 0) checked @endif>
                      <label for="is_visible0">Нет</label>
                    </div>
                  </div>
                </div>
                <div class="col-6">
                  <div class="form-group">
                    <h5>Текст РУ</h5>
                    <div class="controls">
                      <textarea name="text_ru" class="form-control" rows="6" data-editor="true">{!! $contentsetting->text_ru??'' !!}</textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <h5>Основной текст EN</h5>
                    <div class="controls">
                      <textarea name="text_en" class="form-control" rows="6" data-editor="true">{!! $contentsetting->text_en??'' !!}</textarea>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-success">Обновить контетный блок</button>
                </div>
                </div>
              </div>
            </div>
          </div>
        </div>

    </form>
  </section>
</div>


<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
<link rel="stylesheet" href="{{ asset('invi/css/bootstrap3-wysihtml5.min.css') }}?{{ time() }}">
<script src="{{ asset('invi/js/bootstrap3-wysihtml5.all.js') }}?{{ time() }}"></script>
<script src="{{ asset('invi/js/pages/project_create.js') }}?{{ time() }}"></script>


@endsection
