@extends('invi.layouts.app')
@section('title','inviadmin - Заявка - '.$advrequest->name)
@section('description','inviadmin - Заявка - '.$advrequest->name)
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title') Заявка - {{$advrequest->name}} @endslot
    @slot('parent') Заявки @endslot
    @slot('parent_link') {{route('advrequest.index')}} @endslot
    @slot('active') Заявка - {{$advrequest->name}} @endslot
  @endcomponent
  <section class="content">
    <form method="post" action="{{route('advrequest.update',$advrequest)}}">
      <input type="hidden" name="_method" value="put">
      <input type="hidden" name="id" value="{{$advrequest->id}}">
      {{csrf_field()}}
      <div class="row">
        <div class="col-6">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Данные заявки</h3>
            </div>
            <div class="box-body">
              <div class="form-group">
                <h5>Имя</h5>
                <div class="controls">
                  <span>{{ $advrequest->name }}</span>
                </div>
              </div>
              <div class="form-group">
                <h5>Телефон</h5>
                <div class="controls">
                  <span>{{ $advrequest->phone }}</span>
                </div>
              </div>
              <div class="form-group">
                <h5>E-mail</h5>
                <div class="controls">
                  <span>{{ $advrequest->email }}</span>
                </div>
              </div>
              <div class="form-group">
                <h5>Сообщение</h5>
                <div class="controls">
                  <span>{{ $advrequest->message }}</span>
                </div>
              </div>
              <div class="form-group">
                <h5>Добавить комментарий</h5>
                <div class="controls">
                  <textarea name="comment" class="form-control" rows="6">{{ $advrequest->comment }}</textarea>
                </div>
              </div>
              <div class="form-group">
                <h5>Архивировать?</h5>
                <div class="controls">
                  <input type="radio" id="is_archive1" name="is_archive" value="1" @if($advrequest->is_archive == 1) checked @endif>
                  <label for="is_archive1">Да</label>
                  <input type="radio" id="is_archive0" name="is_archive" value="0" @if($advrequest->is_archive == 0) checked @endif>
                  <label for="is_archive0">Нет</label>
                </div>
              </div>
              <button type="submit" class="btn btn-success">Обновить заявку</button>
              <button type="button" class="btn btn-danger" onclick="event.preventDefault();document.getElementById('delete').submit();">Удалить</button>
            </div>
          </div>
        </div>
      </div>
    </form>
  </section>
</div>
<form id="delete" action="{{ route('advrequest.destroy',$advrequest->id) }}" method="POST" style="display: none;">
  <input type="hidden" name="_method" value="delete">
  @csrf
</form>


@endsection
