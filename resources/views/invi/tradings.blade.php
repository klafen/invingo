@extends('invi.layouts.app')
@section('title','inviadmin - Биржи')
@section('description','inviadmin - Биржи')
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title') ОС @endslot
    @slot('active') ОС @endslot
  @endcomponent
  <section class="content">
      <div class="row">
        <div class="col-12 col-lg-6">
          <div class="box box-solid bg-dark">
            <div class="box-header with-border">
              <div class="row">
                <div class="col-lg-12">
                  <h3 class="box-title">Полный список бирж</h3>
                  <h6 class="box-subtitle">Данные можно экспортировать в CSV, Excel, PDF.</h6>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

            <div class="table-responsive">
              <table id="os" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Название</th>
                </tr>
              </thead>

              <tbody>
                @forelse($tradings as $trading)
                  <tr>
                    <td>{{$trading->id}}</td>
                    <td><a href="{{route('tradings.show',$trading->id)}}">{{$trading->name}}</a></td>
                  </tr>
                @empty
                  <tr>
                    <td colspan="2">Нет данных</td>
                  </tr>
                @endforelse
              </tbody>
              <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Название</th>
                </tr>
              </tfoot>
            </table>
            </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Добавить биржу</h3>
            </div>
            <div class="box-body">
              <form method="post" action="{{route('tradings.store')}}">
                {{csrf_field()}}
                <div class="form-group">
                  <h5>Название <span class="text-danger">*</span></h5>
                  <div class="controls">
                    <input type="text" name="name" class="form-control" required=""  value="">
                  </div>
                </div>
                <button type="submit" class="btn btn-success float-left">Добавить биржу</button>
              </form>
            </div>
          </div>
        </div>
    </div>
  </section>
</div>


@if(isset($tradings[0]))
  @component('invi.components.table_excel')
    @slot('table_id') os @endslot
  @endcomponent
@endif

@endsection
