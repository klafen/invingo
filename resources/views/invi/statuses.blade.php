@extends('invi.layouts.app')
@section('title','inviadmin - Статусы')
@section('description','inviadmin - Статусы')
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title') Статусы @endslot
    @slot('active') Статусы @endslot
  @endcomponent
  <section class="content">
      <div class="row">
        <div class="col-12 col-lg-6">
          <div class="box box-solid bg-dark">
            <div class="box-header with-border">
              <div class="row">
                <div class="col-lg-12">
                  <h3 class="box-title">Полный список статусов</h3>
                  <h6 class="box-subtitle">Данные можно экспортировать в CSV, Excel, PDF.</h6>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

            <div class="table-responsive">
              <table id="status" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Название РУ</th>
                  <th>Название EN</th>
                </tr>
              </thead>

              <tbody>
                @forelse($statuses as $status)
                  <tr>
                    <td>{{$status->id}}</td>
                    <td><a href="{{route('status.show',$status->id)}}">{{$status->name_ru}}</a></td>
                    <td><a href="{{route('status.show',$status->id)}}">{{$status->name_en}}</a></td>
                  </tr>
                @empty
                  <tr>
                    <td colspan="3">Нет данных</td>
                  </tr>
                @endforelse
              </tbody>
              <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Название РУ</th>
                  <th>Название EN</th>
                </tr>
              </tfoot>
            </table>
            </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Добавить статус</h3>
            </div>
            <div class="box-body">
              <form method="post" action="{{route('status.store')}}">
                {{csrf_field()}}
                <div class="form-group">
                  <h5>Название РУ<span class="text-danger">*</span></h5>
                  <div class="controls">
                    <input type="text" name="name_ru" class="form-control" required=""  value="">
                  </div>
                </div>
                <div class="form-group">
                  <h5>Название EN<span class="text-danger">*</span></h5>
                  <div class="controls">
                    <input type="text" name="name_en" class="form-control" required=""  value="">
                  </div>
                </div>
                <button type="submit" class="btn btn-success float-left">Добавить статус</button>
              </form>
            </div>
          </div>
        </div>
    </div>
  </section>
</div>


@if(isset($statuses[0]))
  @component('invi.components.table_excel')
    @slot('table_id') status @endslot
  @endcomponent
@endif

@endsection
