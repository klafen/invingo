@extends('invi.layouts.app')
@section('title','inviadmin - Соц сети')
@section('description','inviadmin - Соц сети')
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title') Соц сети @endslot
    @slot('active') Соц сети @endslot
  @endcomponent
  <section class="content">
      <div class="row">
        <div class="col-12 col-lg-6">
          <div class="box box-solid bg-dark">
            <div class="box-header with-border">
              <div class="row">
                <div class="col-lg-12">
                  <h3 class="box-title">Полный список соц сетей</h3>
                  <h6 class="box-subtitle">Данные можно экспортировать в CSV, Excel, PDF.</h6>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

            <div class="table-responsive">
              <table id="socials" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Название</th>
                  <th>Иконка</th>
                </tr>
              </thead>

              <tbody>
                @forelse($socials as $social)
                  <tr>
                    <td>{{$social->id}}</td>
                    <td><a href="{{route('socials.show',$social->id)}}">{{$social->name}}</a></td>
                    <td>{!! $social->icon !!}</td>
                  </tr>
                @empty
                  <tr>
                    <td colspan="3">Нет данных</td>
                  </tr>
                @endforelse
              </tbody>
              <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Название</th>
                  <th>Иконка</th>
                </tr>
              </tfoot>
            </table>
            </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Добавить соц сеть</h3>
            </div>
            <div class="box-body">
              <form method="post" action="{{route('socials.store')}}">
                {{csrf_field()}}
                <div class="form-group">
                  <h5>Название <span class="text-danger">*</span></h5>
                  <div class="controls">
                    <input type="text" name="name" class="form-control" required=""  value="">
                  </div>
                </div>
                <div class="form-group">
                  <h5>Иконка <span class="text-danger">*</span></h5>
                  <div class="controls">
                    <button type="button" class="form-control-button" id="select-icon">Выбрать</button>
                    <input type="text" name="icon" class="form-control" required=""  value="">
                  </div>
                </div>
                <button type="submit" class="btn btn-success float-left">Добавить соц сеть</button>
              </form>
            </div>
          </div>
        </div>
    </div>
  </section>
</div>

<div class="popup select-icon">
  <div class="popup-wrap">
    <div class="box">
  			<div class="social-icon-list row">
               <div class="social-icon-item"><i class="fa fa-facebook-square" aria-hidden="true"></i></div>
               <div class="social-icon-item"><i class="fa fa-facebook" aria-hidden="true"></i></div>
               <div class="social-icon-item"><i class="fa fa-facebook-official" aria-hidden="true"></i></div>
               <div class="social-icon-item"><i class="fa fa-github" aria-hidden="true"></i></div>
               <div class="social-icon-item"><i class="fa fa-google" aria-hidden="true"></i></div>
               <div class="social-icon-item"><i class="fa fa-google-plus-official" aria-hidden="true"></i></div>
               <div class="social-icon-item"><i class="fa fa-google-plus" aria-hidden="true"></i></div>
               <div class="social-icon-item"><i class="fa fa-instagram" aria-hidden="true"></i></div>
               <div class="social-icon-item"><i class="fa fa-linkedin" aria-hidden="true"></i></div>
               <div class="social-icon-item"><i class="fa fa-linkedin-square" aria-hidden="true"></i></div>
               <div class="social-icon-item"><i class="fa fa-odnoklassniki-square" aria-hidden="true"></i></div>
               <div class="social-icon-item"><i class="fa fa-odnoklassniki" aria-hidden="true"></i></div>
               <div class="social-icon-item"><i class="fa fa-reddit-square" aria-hidden="true"></i></div>
               <div class="social-icon-item"><i class="fa fa-reddit" aria-hidden="true"></i></div>
               <div class="social-icon-item"><i class="fa fa-reddit-alien" aria-hidden="true"></i></div>
               <div class="social-icon-item"><i class="fa fa-skype" aria-hidden="true"></i></div>
               <div class="social-icon-item"><i class="fa fa-steam-square" aria-hidden="true"></i></div>
               <div class="social-icon-item"><i class="fa fa-steam" aria-hidden="true"></i></div>
               <div class="social-icon-item"><i class="fa fa-telegram" aria-hidden="true"></i></div>
               <div class="social-icon-item"><i class="fa fa-twitch" aria-hidden="true"></i></div>
               <div class="social-icon-item"><i class="fa fa-twitter" aria-hidden="true"></i></div>
               <div class="social-icon-item"><i class="fa fa-twitter-square" aria-hidden="true"></i></div>
               <div class="social-icon-item"><i class="fa fa-vimeo" aria-hidden="true"></i></div>
               <div class="social-icon-item"><i class="fa fa-weixin" aria-hidden="true"></i></div>
               <div class="social-icon-item"><i class="fa fa-vk" aria-hidden="true"></i></div>
               <div class="social-icon-item"><i class="fa fa-weibo" aria-hidden="true"></i></div>
               <div class="social-icon-item"><i class="fa fa-whatsapp" aria-hidden="true"></i></div>
               <div class="social-icon-item"><i class="fa fa-youtube-square" aria-hidden="true"></i></div>
               <div class="social-icon-item"><i class="fa fa-youtube-play" aria-hidden="true"></i></div>
               <div class="social-icon-item"><i class="fa fa-youtube" aria-hidden="true"></i></div>
               <div class="social-icon-item"><i class="fa fa-gitlab" aria-hidden="true"></i></div>
                <div class="social-icon-item"><i class="fa fa-medium" aria-hidden="true"></i></div>
                <div class="social-icon-item"><i class="fa fa-tumblr" aria-hidden="true"></i></div>
                <div class="social-icon-item"><i class="fa fa-flickr" aria-hidden="true"></i></div>
                <div class="social-icon-item"><i class="fa fa-slack" aria-hidden="true"></i></div>
        </div>
  	</div>
  </div>
</div>
@if(isset($socials[0]))
  @component('invi.components.table_excel')
    @slot('table_id') socials @endslot
  @endcomponent
@endif

<script src="{{ asset('invi/js/pages/socials.js') }}"></script>


@endsection
