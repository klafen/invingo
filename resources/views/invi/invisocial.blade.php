@extends('invi.layouts.app')
@section('title','inviadmin - '.$invisocial->name)
@section('description','inviadmin - Соц сеть Invi '.$invisocial->name)
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title') {{$invisocial->name}} @endslot
    @slot('parent') Соц сети Invi@endslot
    @slot('parent_link') {{route('invisocials.index')}} @endslot
    @slot('active') {{$invisocial->name}} @endslot
  @endcomponent
  <section class="content">
      <div class="row">
        <div class="col-lg-6 col-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Соц сеть INVI</h3>
            </div>
            <div class="box-body">
              <form method="post" action="{{route('invisocials.update', $invisocial)}}">
                <input type="hidden" name="_method" value="put">
                <input type="hidden" name="id" value="{{$invisocial->id}}">
                {{csrf_field()}}
                  <div class="form-group">
                      <h5>Соц Сеть <span class="text-danger">*</span></h5>
                      <div class="controls">
                          <select name="social_id" class="form-control" id="social_id">
                              @foreach($socials as $social)
                                  <option value="{{$social->id}}" @if($social->id==$invisocial->social_id) selected="selected" @endif>{{$social->name}}</option>
                              @endforeach
                          </select>
                      </div>
                  </div>
                <div class="form-group">
                  <h5>Ссылка <span class="text-danger">*</span></h5>
                  <div class="controls">
                    <input type="url" name="link" class="form-control" required=""  value="{{$invisocial->link}}">
                  </div>
                </div>
                <div class="form-group">
                  <h5>Подписчиков</h5>
                  <div class="controls">
                    <input type="text" name="followers" class="form-control" value="{{$invisocial->followers}}">
                  </div>
                </div>
                <button type="submit" class="btn btn-success">Сохранить</button>
                <button type="button" class="btn btn-danger" onclick="event.preventDefault();document.getElementById('delete').submit();">Удалить</button>
              </form>
            </div>
          </div>
        </div>
    </div>
  </section>
</div>



<form id="delete" action="{{ route('invisocials.destroy',$invisocial->id) }}" method="POST" style="display: none;">
  <input type="hidden" name="_method" value="delete">
  @csrf
</form>



@endsection
