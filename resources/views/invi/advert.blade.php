@extends('invi.layouts.app')
@section('title','inviadmin - Реклама')
@section('description','inviadmin - Реклама')
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title') Реклама @endslot
    @slot('active') Реклама @endslot
  @endcomponent
  <section class="content">
      <div class="row">
        <div class="col-12">

          <div class="box box-solid bg-dark">
            <div class="box-header with-border">
              <div class="row">
                <div class="col-lg-12">
                  <h3 class="box-title">Полный список рекламы</h3>
                  <h6 class="box-subtitle">Данные можно экспортировать в CSV, Excel, PDF.</h6>
                  <div class="add-client-btn">
                    <a href="{{route('advert.create')}}" class="btn btn-block btn-success" >Добавить рекламу</a>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

        <div class="table-responsive">
          <table id="contracts" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
          <thead>
            <tr>
              <th>ID</th>
              <th>Название</th>
              <th>Статус</th>
            </tr>
          </thead>
          <tbody>
            @forelse($contracts as $contract)
              <tr>
                <td>{{$contract->id}}</td>
                <td><a href="{{route('advert.show',$contract->id)}}">{{$contract->name_ru}} ({{$contract->name_en}})</a></td>
                <td>
                  @if($contract->is_visible == 1)<span class="label label-success">Виден</span>@else<span class="label label-warning">Не виден</span>@endif
                </td>
              </tr>
            @empty
              <tr>
                <td colspan="3">Нет данных</td>
              </tr>
            @endforelse
          </tbody>
          <tfoot>
            <tr>
              <th>ID</th>
              <th>Название</th>
              <th>Статус</th>
            </tr>
          </tfoot>
        </table>
        </div>
            </div>
            <!-- /.box-body -->
          </div>


        </div>
      </div>
  </section>
@if(isset($contracts[0]))
  @component('invi.components.table_excel')
    @slot('table_id') contracts @endslot
  @endcomponent
@endif
</div>
@endsection
