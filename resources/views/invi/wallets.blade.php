@extends('invi.layouts.app')
@section('title','inviadmin - Кошельки')
@section('description','inviadmin - Кошельки')
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title') Кошельки @endslot
    @slot('active') Кошельки @endslot
  @endcomponent
  <section class="content">
      <div class="row">
        <div class="col-12">

          <div class="box box-solid bg-dark">
            <div class="box-header with-border">
              <div class="row">
                <div class="col-lg-12">
                  <h3 class="box-title">Полный список кошельков</h3>
                  <h6 class="box-subtitle">Данные можно экспортировать в CSV, Excel, PDF.</h6>
                  <div class="add-client-btn">
                    <a href="{{route('wallets.create')}}" class="btn btn-block btn-success" >Добавить кошелёк</a>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

        <div class="table-responsive">
          <table id="wallets" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
          <thead>
            <tr>
              <th>ID</th>
              <th>Название</th>
              <th>Рейтинг</th>
              <th>Статус</th>
            </tr>
          </thead>
          <tbody>
            @forelse($wallets as $wallet)
              <tr>
                <td>{{$wallet->id}}</td>
                <td><a href="{{route('wallets.show',$wallet->id)}}">{{$wallet->name}}</a></td>
                <td>{{$wallet->rating}}</td>
                <td>@if($wallet->is_visible == 1)<span class="label label-success">Виден</span> @else <span class="label label-danger">Скрыт</span> @endif</td>
              </tr>
            @empty
              <tr>
                <td colspan="5">Нет данных</td>
              </tr>
            @endforelse
          </tbody>
          <tfoot>
            <tr>
              <th>ID</th>
              <th>Название</th>
              <th>Рейтинг</th>
              <th>Статус</th>
            </tr>
          </tfoot>
        </table>
        </div>
            </div>
            <!-- /.box-body -->
          </div>


        </div>
      </div>
  </section>
@if(isset($wallets[0]))
  @component('invi.components.table_excel')
    @slot('table_id') wallets @endslot
  @endcomponent
@endif
</div>
@endsection
