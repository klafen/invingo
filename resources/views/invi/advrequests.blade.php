@extends('invi.layouts.app')
@section('title','inviadmin - Заявки')
@section('description','inviadmin - Заявки')
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title') Заявки @endslot
    @slot('active') Заявки @endslot
  @endcomponent
  <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="box box-solid bg-dark">
            <div class="box-header with-border">
              <div class="row">
                <div class="col-lg-12">
                  <h3 class="box-title">Полный список заявок</h3>
                  <h6 class="box-subtitle">Данные можно экспортировать в CSV, Excel, PDF.</h6>
                  <div class="add-client-btn">
                    @if(isset($archive))
                    <a href="{{route('advrequest.index')}}" class="btn btn-block btn-success" >Основные заявки</a>
                    @else
                    <a href="{{route('advrequest-archive')}}" class="btn btn-block btn-warning" >Архиф</a>
                    @endif
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

            <div class="table-responsive">
              <table id="advrequests" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Имя</th>
                  <th>Телефон</th>
                  <th>email</th>
                  <th>Пакет</th>
                </tr>
              </thead>
              <tbody>
                @forelse($advrequests as $advrequest)
                  <tr>
                    <td>{{$advrequest->id}}</td>
                    <td><a href="{{route('advrequest.show',$advrequest->id)}}">{{$advrequest->name}}</a></td>
                    <td><a href="{{route('advrequest.show',$advrequest->id)}}">{{$advrequest->phone}}</a></td>
                    <td><a href="{{route('advrequest.show',$advrequest->id)}}">{{$advrequest->email}}</a></td>
                    <td>{{$advrequest->contract_name}}</td>
                  </tr>
                @empty
                  <tr>
                    <td colspan="5">Нет данных</td>
                  </tr>
                @endforelse
              </tbody>
              <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Имя</th>
                  <th>Телефон</th>
                  <th>email</th>
                  <th>Пакет</th>
                </tr>
              </tfoot>
            </table>
            </div>
            </div>
          </div>
        </div>
    </div>
  </section>
</div>


@if(isset($advrequests[0]))
  @component('invi.components.table_excel')
    @slot('table_id') advrequests @endslot
  @endcomponent
@endif

@endsection
