@extends('invi.layouts.app')
@section('title','inviadmin - Агенты')
@section('description','inviadmin - Агенты')
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title') Агенты @endslot
    @slot('active') Агенты @endslot
  @endcomponent
  <section class="content">
      <div class="row">
        <div class="col-12 col-lg-6">
          <div class="box box-solid bg-dark">
            <div class="box-header with-border">
              <div class="row">
                <div class="col-lg-12">
                  <h3 class="box-title">Полный список агентов</h3>
                  <h6 class="box-subtitle">Данные можно экспортировать в CSV, Excel, PDF.</h6>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

            <div class="table-responsive">
              <table id="agencies-rating" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Название</th>
                  <th>Статус</th>
                </tr>
              </thead>
              <tbody>
                @forelse($agencies as $agency)
                  <tr>
                    <td>{{$agency->id}}</td>
                    <td><a href="{{route('agencies-rating.show',$agency->id)}}">{{$agency->name}}</a></td>
                    <td>@if($agency->is_main == 1)<span class="label label-info">Основной</span>@endif</td>
                  </tr>
                @empty
                  <tr>
                    <td colspan="3">Нет данных</td>
                  </tr>
                @endforelse
              </tbody>
              <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Название</th>
                  <th>Статус</th>
                </tr>
              </tfoot>
            </table>
            </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Добавить агента</h3>
            </div>
            <div class="box-body">
              <form method="post" action="{{route('agencies-rating.store')}}">
                {{csrf_field()}}
                <div class="form-group">
                  <h5>Название <span class="text-danger">*</span></h5>
                  <div class="controls">
                    <input type="text" name="name" class="form-control" required=""  value="">
                  </div>
                </div>
                <div class="form-group">
                  <h5>Ссылка <span class="text-danger">*</span></h5>
                  <div class="controls">
                    <input type="url" name="url" class="form-control" required=""  value="">
                  </div>
                </div>
                <div class="form-group">
                  <h5>Основной?</h5>
                  <div class="controls">
                    <input type="radio" id="is_main1" name="is_main" value="1">
                    <label for="is_main1">Да</label>
                    <input type="radio" id="is_main0" name="is_main" value="0" checked>
                    <label for="is_main0">Нет</label>
                  </div>
                </div>
                <button type="submit" class="btn btn-success float-left">Добавить агента</button>
              </form>
            </div>
          </div>
        </div>
    </div>
  </section>
</div>


@if(isset($agencies[0]))
  @component('invi.components.table_excel')
    @slot('table_id') agencies-rating @endslot
  @endcomponent
@endif

@endsection
