@extends('invi.layouts.app')
@section('title','inviadmin - '.$trading->name)
@section('description','inviadmin - Брижа '.$trading->name)
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title') {{$trading->name}} @endslot
    @slot('parent') ОС @endslot
    @slot('parent_link') {{route('tradings.index')}} @endslot
    @slot('active') {{$trading->name}} @endslot
  @endcomponent
  <section class="content">
      <div class="row">
        <div class="col-lg-6 col-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Биржа</h3>
            </div>
            <div class="box-body">
              <form method="post" action="{{route('tradings.update', $trading)}}">
                <input type="hidden" name="_method" value="put">
                <input type="hidden" name="id" value="{{$trading->id}}">
                {{csrf_field()}}
                <div class="form-group">
                  <h5>Название <span class="text-danger">*</span></h5>
                  <div class="controls">
                    <input type="text" name="name" class="form-control" required=""  value="{{$trading->name}}">
                  </div>
                </div>
                <button type="submit" class="btn btn-success">Сохранить</button>
                <button type="button" class="btn btn-danger" onclick="event.preventDefault();document.getElementById('delete').submit();">Удалить</button>
              </form>
            </div>
          </div>
        </div>
    </div>
  </section>
</div>



<form id="delete" action="{{ route('tradings.destroy',$trading->id) }}" method="POST" style="display: none;">
  <input type="hidden" name="_method" value="delete">
  @csrf
</form>

@endsection
