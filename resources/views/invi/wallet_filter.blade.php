@extends('invi.layouts.app')
@section('title','inviadmin - '.$filter->name_ru)
@section('description','inviadmin - Extra '.$filter->name_ru)
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title') {{$filter->name_ru}} @endslot
    @slot('parent') Параметры кошелька @endslot
    @slot('parent_link') {{route('wallet-filter.index')}} @endslot
    @slot('active') {{$filter->name_ru}} @endslot
  @endcomponent
  <section class="content">
      <div class="row">
        <div class="col-lg-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Параметр фильтра кошелька</h3>
            </div>
            <div class="box-body">
              <form method="post" action="{{route('wallet-filter.update', $filter)}}">
                <input type="hidden" name="_method" value="put">
                <input type="hidden" name="id" value="{{$filter->id}}">
                {{csrf_field()}}
                <div class="row">
                  <div class="col-lg-3">
                    <div class="form-group">
                      <h5>Название РУ<span class="text-danger">*</span></h5>
                      <div class="controls">
                        <input type="text" name="name_ru" class="form-control" required=""  value="{{$filter->name_ru}}">
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3">
                    <div class="form-group">
                      <h5>Название EN<span class="text-danger">*</span></h5>
                      <div class="controls">
                        <input type="text" name="name_en" class="form-control" required=""  value="{{$filter->name_en}}">
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3">
                    <div class="form-group">
                      <h5>Приоритет</h5>
                      <div class="controls">
                        <input type="text" name="priority" class="form-control" value="{{$filter->priority}}">
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3">
                    <div class="form-group">
                      <h5>Отображать как</h5>
                      <div class="controls">
                        <select name="type" class="form-control" id="type_list" @if($filter->is_system) disabled @endif>
                          <option value="multiselect" @if($filter->type == 'multiselect') selected @endif>Список</option>
                          <option value="onecheckbox" @if($filter->type == 'onecheckbox') selected @endif>Галочка</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row with-border">
                  <div class="col-lg-3">
                    <div class="form-group">
                      <h5>Отображать в фильтре?</h5>
                      <div class="controls">
                        <input type="radio" id="is_visible1" name="is_visible" value="1" @if($filter->is_visible == 1) checked @endif>
                        <label for="is_visible1">Да</label>
                        <input type="radio" id="is_visible0" name="is_visible" value="0" @if($filter->is_visible == 0) checked @endif>
                        <label for="is_visible0">Нет</label>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3">
                    <div class="form-group">
                      <h5>Отображать название a фильтре?</h5>
                      <div class="controls">
                        <input type="radio" id="is_visible_name1" name="is_visible_name" value="1" @if($filter->is_visible_name == 1) checked @endif>
                        <label for="is_visible_name1">Да</label>
                        <input type="radio" id="is_visible_name0" name="is_visible_name" value="0" @if($filter->is_visible_name == 0) checked @endif>
                        <label for="is_visible_name0">Нет</label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="box-header with-border">
                  <h3 class="box-title">Список значений</h3>
                </div>
                <div id="project-filter-wrap">
                @if (!($filter->is_system))
                    @forelse($filter->values as $i=>$filter_value)
                      <div class="project-filter-item row" data-index="{{$i}}">
                        <input type="hidden" name="project_values[{{$i}}][id]" value="{{$filter_value->id}}">
                        <div class="col-lg-5">
                          <div class="form-group">
                            <h5>Название РУ<span class="text-danger">*</span></h5>
                            <div class="controls">
                              <input type="text" name="project_values[{{$i}}][name_ru]" class="form-control" required=""  value="{{$filter_value->name_ru}}">
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-5">
                          <div class="form-group">
                            <h5>Название EN<span class="text-danger">*</span></h5>
                            <div class="controls">
                              <input type="text" name="project_values[{{$i}}][name_en]" class="form-control" required=""  value="{{$filter_value->name_en}}">
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-1">
                          <h5>Удаление</h5>
                          <button type="button" class="btn btn-danger delete-project-filter-value" id="remove_project_filter_value">Удалить элемент</button>
                        </div>
                      </div>
                    @empty
                      <div class="project-filter-item row" data-index="0">
                        <input type="hidden" name="project_values[0][id]" value="">
                        <div class="col-lg-5">
                          <div class="form-group">
                            <h5>Название РУ<span class="text-danger">*</span></h5>
                            <div class="controls">
                              <input type="text" name="project_values[0][name_ru]" class="form-control" required=""  value="">
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-5">
                          <div class="form-group">
                            <h5>Название EN<span class="text-danger">*</span></h5>
                            <div class="controls">
                              <input type="text" name="project_values[0][name_en]" class="form-control" required=""  value="">
                            </div>
                          </div>
                        </div>
                      </div>
                    @endforelse
                 </div>
                <div class="row">
                  <div class="col-lg-1"><button type="button" class="btn btn-info float-left" id="add_project_filter_item"><i class="fa fa-plus"></i></button></div>
                </div>
                  <hr/>
                 @endif
                <div class="row">
                  <div class="col-lg-12">
                  <button type="submit" class="btn btn-success">Сохранить</button>
                  @if(!($filter->is_system))
                  <button type="button" class="btn btn-danger" onclick="dropElement();">Удалить параметр</button>
                  @endif
                    </div>
                </div>
              </form>
            </div>
          </div>
        </div>
    </div>
  </section>
</div>



<form id="delete" action="{{ route('wallet-filter.destroy',$filter->id) }}" method="POST" style="display: none;">
  <input type="hidden" name="_method" value="delete">
  @csrf
</form>
<script src="{{ asset('invi/js/pages/project_create.js') }}?{{ time() }}"></script>
@endsection
