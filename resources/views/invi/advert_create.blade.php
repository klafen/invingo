@extends('invi.layouts.app')
@section('title','inviadmin - Добавить рекламу')
@section('description','inviadmin - Добавить рекламу')
@section('content')
  <div class="content-wrapper">
    @component('invi.components.breadcrumb')
    @slot('title') Добавить рекламу @endslot
    @slot('parent') Реклама @endslot
    @slot('parent_link') {{route('advert.index')}} @endslot
    @slot('active') Добавить рекламу @endslot
    @endcomponent
    <section class="content">
      <form method="post" action="{{route('advert.store')}}">
        {{csrf_field()}}
        <div class="row">
          <div class="col-12">
            <div class="box box-solid">
              <div class="box-header with-border">
                <h3 class="box-title">Данные рекламы</h3>
              </div>
              <div class="box-body">
                <div class="row">
                  <div class="col-6">
                    <div class="form-group">
                      <h5>Название РУ</h5>
                      <div class="controls">
                        <input type="text" name="name_ru" class="form-control" required=""  value="{{ old('name_ru') }}">
                      </div>
                    </div>
                    <div class="form-group">
                      <h5>Основной текст РУ</h5>
                      <div class="controls">
                        <textarea name="text_ru" id="text_ru" class="form-control" rows="6" data-editor="true"></textarea>
                      </div>
                    </div>
                    <div class="form-group">
                      <h5>Обсуждаются условия?</h5>
                      <div class="controls">
                        <input type="radio" id="is_discussing1" name="is_discussing" value="1">
                        <label for="is_discussing1">Да</label>
                        <input type="radio" id="is_discussing0" name="is_discussing" value="0" checked>
                        <label for="is_discussing0">Нет</label>
                      </div>
                    </div>
                    <div class="form-group">
                      <h5>Видимость</h5>
                      <div class="controls">
                        <input type="radio" id="is_visible1" name="is_visible" value="1">
                        <label for="is_visible1">Да</label>
                        <input type="radio" id="is_visible0" name="is_visible" value="0" checked>
                        <label for="is_visible0">Нет</label>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-success float-left">Добавить рекламу</button>
                  </div>
                  <div class="col-6">
                    <div class="form-group">
                      <h5>Название EN</h5>
                      <div class="controls">
                        <input type="text" name="name_en" class="form-control" required=""  value="{{ old('name_en') }}">
                      </div>
                    </div>
                    <div class="form-group">
                      <h5>Основной текст EN</h5>
                      <div class="controls">
                        <textarea name="text_en" id="text_en" class="form-control" rows="6" data-editor="true"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </section>
  </div>

  <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
  <script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
  <script src="{{ asset('invi/js/pages/project_create.js') }}?{{ time() }}"></script>



@endsection
