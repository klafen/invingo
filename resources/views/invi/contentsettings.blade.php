@extends('invi.layouts.app')
@section('title','inviadmin - Контентные блоки')
@section('description','inviadmin - Контентные блоки')
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title') Контентные блоки @endslot
    @slot('active') Контентные блоки @endslot
  @endcomponent
  <section class="content">
      <div class="row">
        <div class="col-12 col-lg-6">
          <div class="box box-solid bg-dark">
            <div class="box-header with-border">
              <div class="row">
                <div class="col-lg-12">
                  <h3 class="box-title">Полный список контентных блоков</h3>
                  <h6 class="box-subtitle">Данные можно экспортировать в CSV, Excel, PDF.</h6>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

            <div class="table-responsive">
              <table id="socials" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Название</th>
                  <th>Переменная</th>
                  <th>Состояние</th>
                </tr>
              </thead>

              <tbody>
                @forelse($contentsettings as $contentsetting)
                  <tr>
                    <td>{{$contentsetting->id}}</td>
                    <td><a href="{{route('contentsettings.show',$contentsetting->id)}}">{{$contentsetting->caption}}</a></td>
                    <td>{{$contentsetting->name}}</td>
                    <td>@if($contentsetting->is_visible == 1) <span class="label label-success">Виден</span> @else <span class="label label-danger">Скрыт</span> @endif</td>
                  </tr>
                @empty
                  <tr>
                    <td colspan="4">Нет данных</td>
                  </tr>
                @endforelse
              </tbody>
              <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Название</th>
                  <th>Переменная</th>
                  <th>Состояние</th>
                </tr>
              </tfoot>
            </table>
            </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Добавить контетный блок</h3>
            </div>
            <div class="box-body">
              <form method="post" action="{{route('contentsettings.store')}}">
                {{csrf_field()}}
                  <div class="form-group">
                    <h5>Название (видно только вам)<span class="text-danger">*</span></h5>
                    <div class="controls">
                      <input type="text" name="caption" class="form-control" required=""  value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <h5>Переменная (нужна для обработки кодом)<span class="text-danger">*</span></h5>
                    <div class="controls">
                      <input type="text" name="name" class="form-control" required=""  value="">
                    </div>
                  </div>
                <input type="hidden" name="is_visible" value="0">
                <button type="submit" class="btn btn-success float-left">Добавить контентный блок</button>
              </form>
            </div>
          </div>
        </div>
    </div>
  </section>
</div>


@if(isset($socials[0]))
  @component('invi.components.table_excel')
    @slot('table_id') socials @endslot
  @endcomponent
@endif


@endsection
