@extends('invi.layouts.app')
@section('title','inviadmin - '.$type_token->name)
@section('description','inviadmin - Тип токена '.$type_token->name)
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title') {{$type_token->name}} @endslot
    @slot('parent') Тип токена @endslot
    @slot('parent_link') {{route('type_tokens.index')}} @endslot
    @slot('active') {{$type_token->name}} @endslot
  @endcomponent
  <section class="content">
      <div class="row">
        <div class="col-lg-6 col-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Тип токена</h3>
            </div>
            <div class="box-body">
              <form method="post" action="{{route('type_tokens.update', $type_token)}}">
                <input type="hidden" name="_method" value="put">
                <input type="hidden" name="id" value="{{$type_token->id}}">
                {{csrf_field()}}
                <div class="form-group">
                  <h5>Название <span class="text-danger">*</span></h5>
                  <div class="controls">
                    <input type="text" name="name" class="form-control" required=""  value="{{$type_token->name}}">
                  </div>
                </div>
                <button type="submit" class="btn btn-success">Сохранить</button>
                <button type="button" class="btn btn-danger" onclick="event.preventDefault();document.getElementById('delete').submit();">Удалить</button>
              </form>
            </div>
          </div>
        </div>
    </div>
  </section>
</div>



<form id="delete" action="{{ route('type_tokens.destroy',$type_token->id) }}" method="POST" style="display: none;">
  <input type="hidden" name="_method" value="delete">
  @csrf
</form>

@endsection
