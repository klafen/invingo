@extends('invi.layouts.app')
@section('title','inviadmin - '.$platform->name)
@section('description','inviadmin - Платформа '.$platform->name)
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title') {{$platform->name}} @endslot
    @slot('parent') Платформы @endslot
    @slot('parent_link') {{route('platforms.index')}} @endslot
    @slot('active') {{$platform->name}} @endslot
  @endcomponent
  <section class="content">
      <div class="row">
        <div class="col-lg-6 col-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Платформы</h3>
            </div>
            <div class="box-body">
              <form method="post" action="{{route('platforms.update', $platform)}}">
                <input type="hidden" name="_method" value="put">
                <input type="hidden" name="id" value="{{$platform->id}}">
                {{csrf_field()}}
                <div class="form-group">
                  <h5>Название <span class="text-danger">*</span></h5>
                  <div class="controls">
                    <input type="text" name="name" class="form-control" required=""  value="{{$platform->name}}">
                  </div>
                </div>
                <button type="submit" class="btn btn-success">Сохранить</button>
                <button type="button" class="btn btn-danger" onclick="event.preventDefault();document.getElementById('delete').submit();">Удалить</button>
              </form>
            </div>
          </div>
        </div>
    </div>
  </section>
</div>



<form id="delete" action="{{ route('platforms.destroy',$platform->id) }}" method="POST" style="display: none;">
  <input type="hidden" name="_method" value="delete">
  @csrf
</form>

@endsection
