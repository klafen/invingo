@extends('invi.layouts.app')
@section('title','inviadmin - Файлы')
@section('description','inviadmin - Файлы')
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title') Файлы @endslot
    @slot('active') Файлы @endslot
  @endcomponent
  <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="box box-solid bg-dark">
            <div class="box-header with-border">
              <div class="row">
                <div class="col-lg-12">
                  <h3 class="box-title">Файлы</h3>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

            </div>
          </div>
        </div>
    </div>
  </section>
</div>



@endsection
