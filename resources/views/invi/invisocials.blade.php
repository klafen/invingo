@extends('invi.layouts.app')
@section('title','inviadmin - Соц сети Invingo')
@section('description','inviadmin - Соц сети Invingo')
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title') Соц сети @endslot
    @slot('active') Соц сети @endslot
  @endcomponent
  <section class="content">
      <div class="row">
        <div class="col-12 col-lg-6">
          <div class="box box-solid bg-dark">
            <div class="box-header with-border">
              <div class="row">
                <div class="col-lg-12">
                  <h3 class="box-title">Полный список соц сетей</h3>
                  <h6 class="box-subtitle">Данные можно экспортировать в CSV, Excel, PDF.</h6>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

            <div class="table-responsive">
              <table id="socials" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Название</th>
                  <th>Ссылка</th>
                  <th>Подписчиков</th>
                </tr>
              </thead>

              <tbody>
                @forelse($invisocials as $invisocial)
                  <tr>
                    <td>{{$invisocial->id}}</td>
                    <td><a href="{{route('invisocials.show',$invisocial->id)}}">{{$invisocial->name}}</a></td>
                    <td><a href="{{$invisocial->link}}" target="_blank">{{$invisocial->link}}</a></td>
                    <td>{{$invisocial->followers}}</td>
                  </tr>
                @empty
                  <tr>
                    <td colspan="4">Нет данных</td>
                  </tr>
                @endforelse
              </tbody>
              <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Название</th>
                  <th>Иконка</th>
                  <th>Подписчиков</th>
                </tr>
              </tfoot>
            </table>
            </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Добавить соц сеть Invi</h3>
            </div>
            <div class="box-body">
              <form method="post" action="{{route('invisocials.store')}}">
                {{csrf_field()}}
                  <div class="form-group">
                      <h5>Соц Сеть <span class="text-danger">*</span></h5>
                      <div class="controls">
                          <select name="social_id" class="form-control" id="social_id">
                              @foreach($socials as $social)
                                  <option value="{{$social->id}}">{{$social->name}}</option>
                              @endforeach
                          </select>
                      </div>
                  </div>
                <div class="form-group">
                  <h5>Ссылка <span class="text-danger">*</span></h5>
                  <div class="controls">
                    <input type="url" name="link" class="form-control" required=""  value="">
                  </div>
                </div>
                <div class="form-group">
                  <h5>Подписчиков</h5>
                  <div class="controls">
                    <input type="text" name="followers" class="form-control"  value="">
                  </div>
                </div>
                <button type="submit" class="btn btn-success float-left">Добавить соц сеть Invi</button>
              </form>
            </div>
          </div>
        </div>
    </div>
  </section>
</div>


@if(isset($socials[0]))
  @component('invi.components.table_excel')
    @slot('table_id') socials @endslot
  @endcomponent
@endif


@endsection
