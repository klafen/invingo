@extends('invi.layouts.app')
@section('title','inviadmin - Добавить проект')
@section('description','inviadmin - Добавить проект')
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title') Добавить проект @endslot
    @slot('parent') Проекты @endslot
    @slot('parent_link') {{route('projects.index')}} @endslot
    @slot('active') Добавить проект @endslot
  @endcomponent
  <section class="content">
    <form method="post" action="{{route('projects.store')}}" enctype="multipart/form-data">
      {{csrf_field()}}
      <div class="row">
        <div class="col-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Данные проекта</h3>
            </div>
            <ul class="nav nav-tabs customtab" role="tablist">
              <li class="nav-item"> <a class="nav-link active show" data-toggle="tab" href="#all-container" role="tab" aria-selected="true"><span class="hidden-sm-up"><i class="ion-home"></i></span> <span class="hidden-xs-down">Основные</span></a> </li>
              <li class="nav-item"> <a class="nav-link disabled" data-toggle="tab" href="#money-container" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ion-person"></i></span> <span class="hidden-xs-down">Финансы</span></a> </li>
              <li class="nav-item"> <a class="nav-link disabled" data-toggle="tab" href="#info-container" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ion-person"></i></span> <span class="hidden-xs-down">Описание</span></a> </li>
              <li class="nav-item"> <a class="nav-link disabled" data-toggle="tab" href="#team-container" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ion-email"></i></span> <span class="hidden-xs-down">Команда</span></a> </li>
              <li class="nav-item"> <a class="nav-link disabled" data-toggle="tab" href="#road_map-container" role="tab" aria-selected="false"><span class="hidden-xs-down">Дорожная карта</span></a> </li>
              <li class="nav-item"> <a class="nav-link disabled" data-toggle="tab" href="#agency-rating-container" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ion-email"></i></span> <span class="hidden-xs-down">Рейтинги агенств</span></a> </li>
              <li class="nav-item"> <a class="nav-link disabled" data-toggle="tab" href="#rating-container" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ion-email"></i></span> <span class="hidden-xs-down">Доп рейтинги</span></a> </li>
              <li class="nav-item"> <a class="nav-link disabled" data-toggle="tab" href="#social-container" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ion-email"></i></span> <span class="hidden-xs-down">Социальные сети</span></a> </li>
              <li class="nav-item"> <a class="nav-link disabled" data-toggle="tab" href="#links-container" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ion-email"></i></span> <span class="hidden-xs-down">Другие ссылки</span></a> </li>
    				</ul>
            <div class="box-body">
              <div class="tab-content">
      					<div class="tab-pane active show" id="all-container" role="tabpanel">
                  <div class="row">
                    <div class="col-lg-6 col-12">
                      <div class="form-group">
                        <h5>Название </h5>
                        <div class="controls">
                          <input type="text" name="name" class="form-control" required=""  value="{{ old('name') }}">
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Логотип </h5>
                        <div class="controls">
                          <div class="input-group">
                            <span class="input-group-btn">
                              <a data-uploader="true" data-input="thumbnail" data-preview="holder" class="btn btn-info text-white">
                                <i class="fa fa-picture-o"></i> Выбрать
                              </a>
                            </span>
                            <input id="thumbnail" class="form-control" type="text" name="logo" required>
                          </div>
                        </div>
                        <div id="holder" class="holder"></div>
                      </div>
                      <div class="form-group">
                        <h5>Токен </h5>
                        <div class="controls">
                          <input type="text" name="token" class="form-control" required=""  value="{{ old('token') }}">
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Рейтинг </h5>
                        <div class="controls">
                          <input type="number" name="rating" class="form-control" required="" value="{{ old('rating') }}">
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Приоритет </h5>
                        <div class="controls">
                          <input type="number" name="priority" class="form-control"  value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Главная валюта </h5>
                        <div class="controls">
                          <input type="text" name="all_currency" class="form-control"  value="{{ old('all_currency')??'' }}">
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>ROI</h5>
                        <div class="controls">
                          <input type="text" name="roi" class="form-control"   value="{{old('roi')??''}}" required="">
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Категория </h5>
                        <div class="controls">
                          <select name="category_id" required="" class="form-control">
                            @foreach($categories as $category)
                              <option value="{{$category->id}}">{{$category->name_ru}} ({{$category->name_en}})</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Страна</h5>
                        <div class="controls">
                          <select name="country_id" class="form-control">
                            <option value=""></option>
                            @foreach($countries as $country)
                              <option value="{{$country->id}}">{{$country->name_ru}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Платформа </h5>
                        <div class="controls">
                          <select name="platform_id" required="" class="form-control">
                            @foreach($platforms as $platform)
                              <option value="{{$platform->id}}">{{$platform->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Тип токена </h5>
                        <div class="controls">
                          <select name="type_token_id" required="" class="form-control">
                            @foreach($type_tokens as $type_token)
                              <option value="{{$type_token->id}}">{{$type_token->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Валюты: <small>Зажмите ctrl</small> </h5>
                        <div class="controls">
                          <select name="accept[]" class="form-control" multiple>
                            @foreach($cryptocoins as $cryptocoin)
                              <option value="{{$cryptocoin->id}}">{{$cryptocoin->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Торговля на биржах: <small>Зажмите ctrl</small> </h5>
                        <div class="controls">
                          <select name="project_tradings[]" class="form-control" multiple>
                            @foreach($tradings as $trading)
                              <option value="{{$trading->id}}">{{$trading->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-6 col-12">
                      <div class="form-group">
                        <h5>Видимость</h5>
                        <div class="controls">
                          <input type="radio" id="is_visible1" name="is_visible" value="1">
                          <label for="is_visible1">Да</label>
                          <input type="radio" id="is_visible0" name="is_visible" value="0" checked>
                          <label for="is_visible0">Нет</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Отображать на слайдере?</h5>
                        <div class="controls">
                          <input type="radio" id="is_slider1" name="is_slider" value="1">
                          <label for="is_slider1">Да</label>
                          <input type="radio" id="is_slider0" name="is_slider" value="0" checked>
                          <label for="is_slider0">Нет</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Рекламный проект?</h5>
                        <div class="controls">
                          <input type="radio" id="is_adv1" name="is_adv" value="1">
                          <label for="is_adv1">Да</label>
                          <input type="radio" id="is_adv0" name="is_adv" value="0" checked>
                          <label for="is_adv0">Нет</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Белый лист (whitelist)?</h5>
                        <div class="controls">
                          <input type="radio" id="is_whitelist1" name="is_whitelist" value="1">
                          <label for="is_whitelist1">Да</label>
                          <input type="radio" id="is_whitelist0" name="is_whitelist" value="0" checked>
                          <label for="is_whitelist0">Нет</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>KYC?</h5>
                        <div class="controls">
                          <input type="radio" id="is_kyc1" name="is_kyc" value="1">
                          <label for="is_kyc1">Да</label>
                          <input type="radio" id="is_kyc0" name="is_kyc" value="0" checked>
                          <label for="is_kyc0">Нет</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>KYT?</h5>
                        <div class="controls">
                          <input type="radio" id="is_kyt1" name="is_kyt" value="1">
                          <label for="is_kyt1">Да</label>
                          <input type="radio" id="is_kyt0" name="is_kyt" value="0" checked>
                          <label for="is_kyt0">Нет</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Юрисдикция </h5>
                        <div class="controls">
                          <input type="radio" id="jurisdiction1" name="jurisdiction" value="1" >
                          <label for="jurisdiction1">Verify</label>
                          <input type="radio" id="jurisdiction0" name="jurisdiction" value="0" checked >
                          <label for="jurisdiction0">Unverify</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Смарт контракт?</h5>
                        <div class="controls">
                          <input type="radio" id="is_smart1" name="is_smartcontract" value="1">
                          <label for="is_smart1">Да</label>
                          <input type="radio" id="is_smart0" name="is_smartcontract" value="0" checked>
                          <label for="is_smart0">Нет</label>
                        </div>
                      </div>
                      <h5>Air drop?</h5>
                      <div class="controls">
                        <input type="radio" id="is_airdrop1" name="is_airdrop" value="1" >
                        <label for="is_airdrop1">Да</label>
                        <input type="radio" id="is_airdrop0" name="is_airdrop" value="0"  checked >
                        <label for="is_airdrop0">Нет</label>
                      </div>
                      <h5>Бонусы?</h5>
                      <div class="controls">
                        <input type="radio" id="is_bonus1" name="is_bonus" value="1" >
                        <label for="is_bonus1">Да</label>
                        <input type="radio" id="is_bonus0" name="is_bonus" value="0" checked >
                        <label for="is_bonus0">Нет</label>
                      </div>
                      <div class="form-group">
                      <h5>Bounty?</h5>
                        <div class="controls">
                          <input type="radio" id="is_bounty1" name="is_bounty" value="1">
                          <label for="is_bounty1">Да</label>
                          <input type="radio" id="is_bounty0" name="is_bounty" value="0" checked>
                          <label for="is_bounty0">Нет</label>
                        </div>
                      </div>
                      <h5>MVP?</h5>
                      <div class="controls">
                        <input type="radio" id="is_mvp1" name="is_mvp" value="1" >
                        <label for="is_mvp1">Да</label>
                        <input type="radio" id="is_mvp0" name="is_mvp" value="0"  checked >
                        <label for="is_mvp0">Нет</label>
                      </div>
                      <h5>Etherscancheck?</h5>
                      <div class="controls">
                        <input type="radio" id="is_etherscancheck1" name="is_etherscancheck" value="1" >
                        <label for="is_etherscancheck1">Да</label>
                        <input type="radio" id="is_etherscancheck0" name="is_etherscancheck" value="0"  checked >
                        <label for="is_etherscancheck0">Нет</label>
                      </div>

                      <h5>Howey Test?</h5>
                      <div class="controls">
                        <input type="radio" id="is_howey_test1" name="is_howey_test" value="1" >
                        <label for="is_howey_test1">Да</label>
                        <input type="radio" id="is_howey_test0" name="is_howey_test" value="0"  checked >
                        <label for="is_howey_test0">Нет</label>
                      </div>
                      <h5>Escrow?</h5>
                      <div class="controls">
                        <input type="radio" id="is_escrow1" name="is_escrow" value="1" >
                        <label for="is_escrow1">Да</label>
                        <input type="radio" id="is_escrow0" name="is_escrow" value="0" checked >
                        <label for="is_escrow0">Нет</label>
                      </div>
                      <div class="form-group">
                        <h5>Запрет для стран: <small>Зажмите ctrl</small> </h5>
                        <div class="controls">
                          <select name="restricted_countries[]" class="form-control" multiple>
                            @foreach($countries as $country)
                              <option value="{{$country->id}}">{{$country->name_ru}} ({{$country->name_en}})</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Extra: <small>Зажмите ctrl</small> </h5>
                        <div class="controls">
                          <select name="project_extra[]" class="form-control" multiple>
                            @foreach($extras as $extra)
                              <option value="{{$extra->id}}" >{{$extra->name_ru}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
      					</div>
                <div class="tab-pane pad" id="money-container" role="tabpanel">
                  <div class="row">
                    <div class="col-lg-3">
                      <div class="form-group">
                        <h5>Финансы</h5>
                      </div>
                      <div class="form-group">
                        <h5>Soft cap</h5>
                        <div class="controls">
                          <input type="number" name="soft_cap" class="form-control" value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Hard cap</h5>
                        <div class="controls">
                          <input type="number" name="hard_cap" class="form-control" value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Собрано</h5>
                        <div class="controls">
                          <input type="number" name="collected" class="form-control"   value="">
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Токенов на продажу</h5>
                        <div class="controls">
                          <input type="number" name="for_sale" class="form-control"   value="">
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-9">
                      <div class="form-group">
                        <h5>Жизненный цикл</h5>
                      </div>
                      <div id="timeline-wrap">
                        <div class="timeline-item row" data-index="0">
                          <div class="col-lg-4">
                            <div class="form-group">
                              <h5>Статус </h5>
                              <div class="controls">
                                <select name="timeline[0][status_id]" class="form-control" id="status_list">
                                  <option value=""></option>
                                  @foreach($statuses as $status)
                                    <option value="{{$status->id}}">{{$status->name_ru}} ({{$status->name_en}})</option>
                                  @endforeach
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-2">
                            <div class="form-group">
                              <h5>Начало</h5>
                              <div class="controls">
                                <input type="date" name="timeline[0][start]" class="form-control" value="">
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-2">
                            <div class="form-group">
                              <h5>Конец</h5>
                              <div class="controls">
                                <input type="date" name="timeline[0][end]" class="form-control" value="">
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-2">
                            <div class="form-group">
                              <h5>Цена</h5>
                              <div class="controls">
                                <input type="number" name="timeline[0][price]" step="any" class="form-control" value="">
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-2">
                            <div class="form-group">
                              <h5>Мин Инвест</h5>
                              <div class="controls">
                                <input type="number" name="timeline[0][min_invest]" step="any" class="form-control" value="">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <button type="button" class="btn btn-info float-left" id="add_timeline_item"><i class="fa fa-plus"></i></button>
                      <button type="button" class="btn btn-info float-left remove_item" data-remove="timeline-item" style="margin-left: 5px"><i class="fa fa-minus"></i></button>
                    </div>
                  </div>
                </div>
      			<div class="tab-pane pad" id="info-container" role="tabpanel">
                  <div class="row">
                    <div class="col-6">
                      <div class="form-group">
                        <h5>Основной текст РУ</h5>
                        <div class="controls">
                          <textarea name="info_ru" class="form-control" rows="6" data-editor="true"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Про компанию РУ</h5>
                        <div class="controls">
                          <textarea name="about_us_ru" class="form-control" rows="6" data-editor="true"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Другое РУ</h5>
                        <div class="controls">
                          <textarea name="other_ru" class="form-control" rows="6" data-editor="true"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Короткий текст для слайдера RU</h5>
                        <div class="controls">
                          <input type="text" name="slider_ru" class="form-control"  value="">
                        </div>
                      </div>
                    </div>
                    <div class="col-6">
                      <div class="form-group">
                        <h5>Основной текст EN</h5>
                        <div class="controls">
                          <textarea name="info_en" class="form-control" rows="6" data-editor="true"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Про компанию EN</h5>
                        <div class="controls">
                          <textarea name="about_us_en" class="form-control" rows="6" data-editor="true"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Другое EN</h5>
                        <div class="controls">
                          <textarea name="other_en" class="form-control" rows="6" data-editor="true"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Короткий текст для слайдера EN</h5>
                        <div class="controls">
                          <input type="text" name="slider_en" class="form-control"   value="">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane pad" id="team-container" role="tabpanel">
                  <div class="form-group">
                    <h5>Команда</h5>
                  </div>
                  <div id="team-wrap">
                    <div class="team-item row" data-index="0">
                      <div class="col-lg-3">
                        <div class="form-group">
                          <h5>Имя РУ</h5>
                          <div class="controls">
                            <input type="text" name="team[0][name_ru]" class="form-control"  value="">
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <h5>Имя EN</h5>
                          <div class="controls">
                            <input type="text" name="team[0][name_en]" class="form-control"  value="">
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-2">
                        <div class="form-group">
                          <h5>Картинка</h5>
                          <div class="controls">
                            <div class="input-group">
                              <span class="input-group-btn">
                                <a data-input="team_thumbnail" data-preview="team_holder" class="btn btn-info text-white" data-uploader="true">
                                  <i class="fa fa-picture-o"></i> Выбрать
                                </a>
                              </span>
                              <input id="team_thumbnail" class="form-control" type="text" name="team[0][avatar]">
                            </div>
                          </div>
                          <div id="team_holder" class="holder"></div>
                        </div>
                      </div>
                      <div class="col-lg-2">
                        <div class="form-group">
                          <h5>Профессия</h5>
                          <div class="controls">
                            <select name="team[0][profession_id]" class="form-control" id="team_professions">
                              <option value="">Без указания профессии</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-2">
                        <div class="form-group">
                          <h5>Вкладка</h5>
                          <div class="controls">
                            <select name="team[0][group_id]" class="form-control" id="team_group">
                              @foreach($team_groups as $group)
                                <option value="{{$group->id}}">{{$group->name_ru}} ({{$group->name_en}})</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <button type="button" class="btn btn-info float-left" id="add_team_member"><i class="fa fa-plus"></i></button>
                  <button type="button" class="btn btn-info float-left remove_item" data-remove="team-item" style="margin-left: 5px"><i class="fa fa-minus"></i></button>
                </div>
                <div class="tab-pane pad" id="road_map-container" role="tabpanel">
                  <div class="row">
                    <div class="col-6">
                      <div class="form-group">
                        <h5>Дорожная карта </h5>
                        <div class="controls">
                          <div class="input-group">
                            <span class="input-group-btn">
                              <a data-uploader="true" data-input="road_map_thumbnail" data-preview="road_map_holder" class="btn btn-info text-white">
                                <i class="fa fa-picture-o"></i> Выбрать
                              </a>
                            </span>
                            <input id="road_map_thumbnail" class="form-control" type="text" name="road_map" value="">
                          </div>
                        </div>
                        <div id="road_map_holder" class="holder" style="max-width:none;">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane pad" id="agency-rating-container" role="tabpanel">
                  <div class="form-group">
                    <h5>Рейтинги агенств</h5>
                  </div>
                  <div id="agency-rating-wrap">
                    <div class="agency-rating-item row" data-index="0">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <h5>Имя</h5>
                          <div class="controls">
                            <select name="agency_rating[0][agency_rating_id]" class="form-control" id="agency_ratings">
                              @foreach($agencies_ratings as $agencies_rating)
                                <option value="{{$agencies_rating->id}}">{{$agencies_rating->name}}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <h5>Рейтинг</h5>
                          <div class="controls">
                            <input type="text" name="agency_rating[0][rating]" class="form-control" value="">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <button type="button" class="btn btn-info float-left" id="add_agency_rating_item"><i class="fa fa-plus"></i></button>
                  <button type="button" class="btn btn-info float-left remove_item" data-remove="agency-rating-item" style="margin-left: 5px"><i class="fa fa-minus"></i></button>
                </div>
                <div class="tab-pane pad" id="rating-container" role="tabpanel">
                  <div class="form-group">
                    <h5>Доп рейтинги</h5>
                  </div>
                  <div id="rating-wrap">
                    <div class="rating-item row" data-index="0">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <h5>Имя</h5>
                          <div class="controls">
                            <select name="custom_rating[0][custom_rating_id]" class="form-control" id="cutsom_ratings">
                              @foreach($customratings as $customrating)
                                <option value="{{$customrating->id}}">{{$customrating->name_ru}} ({{$customrating->name_en}})</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <h5>Рейтинг</h5>
                          <div class="controls">
                            <input type="number" name="custom_rating[0][rating]" class="form-control" value="">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <button type="button" class="btn btn-info float-left" id="add_rating_item"><i class="fa fa-plus"></i></button>
                  <button type="button" class="btn btn-info float-left remove_item" data-remove="rating-item" style="margin-left: 5px"><i class="fa fa-minus"></i></button>
                </div>
                <div class="tab-pane pad" id="social-container" role="tabpanel">
                  <div class="form-group">
                    <h5>Социальные сети</h5>
                  </div>
                  <div id="social-wrap">
                    <div class="social-item row" data-index="0">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <h5>Имя</h5>
                          <div class="controls">
                            <select name="social[0][social_id]" class="form-control" id="social_list">
                              @foreach($socials as $social)
                                <option value="{{$social->id}}">{{$social->name}}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <h5>Ссылка</h5>
                          <div class="controls">
                            <input type="text" name="social[0][link]" class="form-control" value="">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <button type="button" class="btn btn-info float-left" id="add_social_item"><i class="fa fa-plus"></i></button>
                  <button type="button" class="btn btn-info float-left remove_item" data-remove="social-item" style="margin-left: 5px"><i class="fa fa-minus"></i></button>
                </div>
                <div class="tab-pane pad" id="links-container" role="tabpanel">
                  <div class="form-group">
                    <h5>Другие ссылки</h5>
                  </div>
                  <div id="links-wrap">
                    <div class="links-item row" data-index="0">
                      <div class="col-lg-4">
                        <div class="form-group">
                          <h5>Название РУ</h5>
                          <div class="controls">
                            <input type="text" name="links[0][name_ru]" class="form-control" value="">
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group">
                          <h5>Название EN</h5>
                          <div class="controls">
                            <input type="text" name="links[0][name_en]" class="form-control" value="">
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group">
                          <h5>Ссылка</h5>
                          <div class="controls">
                            <input type="text" name="links[0][link]" class="form-control" value="">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <button type="button" class="btn btn-info float-left" id="add_link_item"><i class="fa fa-plus"></i></button>
                  <button type="button" class="btn btn-info float-left remove_item" data-remove="links-item" style="margin-left: 5px"><i class="fa fa-minus"></i></button>
                </div>
      				</div>
              <hr>
              <button type="submit" class="btn btn-success float-left">Добавить проект</button>
            </div>
          </div>
        </div>
      </div>
    </form>
  </section>
</div>

<script src="{{asset('/vendor/laravel-filemanager/js/stand-alone-button.js')}}"></script>
<script src="{{asset('/vendor/ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('/vendor/unisharp/laravel-ckeditor/adapters/jquery.js')}}"></script>
<script src="{{ asset('invi/js/pages/project_create.js') }}?{{ time() }}"></script>


@endsection
