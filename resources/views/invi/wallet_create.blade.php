@extends('invi.layouts.app')
@section('title','inviadmin - Добавить кошелёк')
@section('description','inviadmin - Добавить кошелёк')
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title') Добавить кошелёк @endslot
    @slot('parent') Кошельки @endslot
    @slot('parent_link') {{route('wallets.index')}} @endslot
    @slot('active') Добавить кошелёк @endslot
  @endcomponent
  <section class="content">
    <form method="post" action="{{route('wallets.store')}}">
      {{csrf_field()}}
      <div class="row">
        <div class="col-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Данные кошелёка</h3>
            </div>
            <ul class="nav nav-tabs customtab" role="tablist">
    		  <li class="nav-item"> <a class="nav-link active show" data-toggle="tab" href="#all-container" role="tab" aria-selected="true"><span class="hidden-xs-down">Основные</span></a> </li>
              <li class="nav-item"> <a class="nav-link disabled" data-toggle="tab" href="#info-container" role="tab" aria-selected="false"><span class="hidden-xs-down">Описание</span></a> </li>
    		  <li class="nav-item"> <a class="nav-link disabled" data-toggle="tab" href="#team-container" role="tab" aria-selected="false"><span class="hidden-xs-down">Команда</span></a> </li>
              <li class="nav-item"> <a class="nav-link disabled" data-toggle="tab" href="#jurisdiction-container" role="tab" aria-selected="false"><span class="hidden-xs-down">Юридическая инфа</span></a> </li>
              <li class="nav-item"> <a class="nav-link disabled" data-toggle="tab" href="#road_map-container" role="tab" aria-selected="false"><span class="hidden-xs-down">Дорожная карта</span></a> </li>
              <li class="nav-item"> <a class="nav-link disabled" data-toggle="tab" href="#social-container" role="tab" aria-selected="false"><span class="hidden-xs-down">Социальные сети</span></a> </li>
              <li class="nav-item"> <a class="nav-link disabled" data-toggle="tab" href="#links-container" role="tab" aria-selected="false"><span class="hidden-xs-down">Ссылки</span></a> </li>
    				</ul>
            <div class="box-body">
              <div class="tab-content">
      					<div class="tab-pane active show" id="all-container" role="tabpanel">
                  <div class="row">
                    <div class="col-lg-6 col-12">
                      <div class="form-group">
                        <h5>Название </h5>
                        <div class="controls">
                          <input type="text" name="name" class="form-control" required=""  value="{{ old('name') }}">
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Логотип </h5>
                        <div class="controls">
                          <div class="input-group">
                            <span class="input-group-btn">
                              <a data-uploader="true" data-input="thumbnail" data-preview="holder" class="btn btn-info text-white">
                                <i class="fa fa-picture-o"></i> Выбрать
                              </a>
                            </span>
                            <input id="thumbnail" class="form-control" type="text" name="logo" required>
                          </div>
                        </div>
                        <div id="holder" class="holder"></div>
                      </div>
                      <div class="form-group">
                        <h5>Рейтинг </h5>
                        <div class="controls">
                          <input type="number" name="rating" class="form-control" required=""  value="{{ old('rating') }}">
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Приоритет </h5>
                        <div class="controls">
                          <input type="number" name="priority" class="form-control"  value="{{ old('priority') }}">
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Тип</h5>
                        <div class="controls">
                          <select name="wallet_type_id" required="" class="form-control">
                            @foreach($wallet_types as $wallet_type)
                              <option value="{{$wallet_type->id}}">{{$wallet_type->name_ru}} ({{$wallet_type->name_en}})</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Страна</h5>
                        <div class="controls">
                          <select name="country_id" class="form-control">
                            <option value=""></option>
                            @foreach($countries as $country)
                              <option value="{{$country->id}}">{{$country->name_ru}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Private key control</h5>
                        <div class="controls">
                          <select name="private_key_control_id" required="" class="form-control">
                            <option value=""></option>
                            @foreach($private_key_controls as $private_key_control)
                              <option value="{{$private_key_control->id}}">{{$private_key_control->name_ru}} ({{$private_key_control->name_en}})</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Validation</h5>
                        <div class="controls">
                          <select name="validation_id"  class="form-control">
                            @foreach($validations as $validation)
                              <option value="{{$validation->id}}">{{$validation->name_ru}} ({{$validation->name_en}})</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Anonymity</h5>
                        <div class="controls">
                          <select name="anonymity_id"  class="form-control">
                            @foreach($anonymities as $anonymity)
                              <option value="{{$anonymity->id}}">{{$anonymity->name_ru}} ({{$anonymity->name_en}})</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Тип</h5>
                        <div class="controls">
                          <select name="wallet_type_id" required="" class="form-control">
                            @foreach($wallet_types as $wallet_type)
                              <option value="{{$wallet_type->id}}">{{$wallet_type->name_ru}} ({{$wallet_type->name_en}})</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>ОС: <small>Зажмите ctrl</small> </h5>
                        <div class="controls">
                          <select name="wallet_os[]" class="form-control" multiple>
                            @foreach($oses as $os)
                              <option value="{{$os->id}}">{{$os->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Валюты: <small>Зажмите ctrl</small> </h5>
                        <div class="controls">
                          <select name="accept[]" class="form-control" multiple>
                            @foreach($cryptocoins as $cryptocoin)
                              <option value="{{$cryptocoin->id}}">{{$cryptocoin->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Языки: <small>Зажмите ctrl</small> </h5>
                        <div class="controls">
                          <select name="wallet_language[]" class="form-control" multiple>
                            @foreach($languages as $language)
                              <option value="{{$language->id}}" >{{$language->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Exra: <small>Зажмите ctrl</small> </h5>
                        <div class="controls">
                          <select name="extra[]" class="form-control" multiple>
                            @foreach($extras as $extra)
                              <option value="{{$extra->id}}">{{$extra->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-6 col-12">
                      <div class="form-group">
                        <h5>Видимость</h5>
                        <div class="controls">
                          <input type="radio" id="is_visible1" name="is_visible" value="1">
                          <label for="is_visible1">Да</label>
                          <input type="radio" id="is_visible0" name="is_visible" value="0" checked>
                          <label for="is_visible0">Нет</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Рекламный кошелёк?</h5>
                        <div class="controls">
                          <input type="radio" id="is_sponsored1" name="is_sponsored" value="1">
                          <label for="is_sponsored1">Да</label>
                          <input type="radio" id="is_sponsored0" name="is_sponsored" value="0" checked>
                          <label for="is_sponsored0">Нет</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Adapt for ICO?</h5>
                        <div class="controls">
                          <input type="radio" id="is_adapt1" name="is_adapt" value="1">
                          <label for="is_adapt1">Да</label>
                          <input type="radio" id="is_adapt0" name="is_adapt" value="0" checked>
                          <label for="is_adapt0">Нет</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>HardWare?</h5>
                        <div class="controls">
                          <input type="radio" id="is_hardware1" name="is_hardware" value="1">
                          <label for="is_hardware1">Да</label>
                          <input type="radio" id="is_hardware0" name="is_hardware" value="0" checked>
                          <label for="is_hardware0">Нет</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>2FA?</h5>
                        <div class="controls">
                          <input type="radio" id="is_2FA1" name="is_2FA" value="1">
                          <label for="is_2FA1">Да</label>
                          <input type="radio" id="is_2FA0" name="is_2FA" value="0" checked>
                          <label for="is_2FA0">Нет</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Multisignature?</h5>
                        <div class="controls">
                          <input type="radio" id="is_multisignature1" name="is_multisignature" value="1">
                          <label for="is_multisignature1">Да</label>
                          <input type="radio" id="is_multisignature0" name="is_multisignature" value="0" checked>
                          <label for="is_multisignature0">Нет</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Резервное копирование?</h5>
                        <div class="controls">
                          <input type="radio" id="is_reserve_copy1" name="is_reserve_copy" value="1">
                          <label for="is_reserve_copy1">Да</label>
                          <input type="radio" id="is_reserve_copy0" name="is_reserve_copy" value="0" checked>
                          <label for="is_reserve_copy0">Нет</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Seed Phrase?</h5>
                        <div class="controls">
                          <input type="radio" id="is_seed_phrase1" name="is_seed_phrase" value="1">
                          <label for="is_seed_phrase1">Да</label>
                          <input type="radio" id="is_seed_phrase0" name="is_seed_phrase" value="0" checked>
                          <label for="is_seed_phrase0">Нет</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Смарт-контракты?</h5>
                        <div class="controls">
                          <input type="radio" id="is_smart_contract1" name="is_smart_contract" value="1">
                          <label for="is_smart_contract1">Да</label>
                          <input type="radio" id="is_smart_contract0" name="is_smart_contract" value="0" checked>
                          <label for="is_smart_contract0">Нет</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Обменник?</h5>
                        <div class="controls">
                          <input type="radio" id="is_exchange1" name="is_exchange" value="1">
                          <label for="is_exchange1">Да</label>
                          <input type="radio" id="is_exchange0" name="is_exchange" value="0" checked>
                          <label for="is_exchange0">Нет</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Open Source?</h5>
                        <div class="controls">
                          <input type="radio" id="is_open_source1" name="is_open_source" value="1">
                          <label for="is_open_source1">Да</label>
                          <input type="radio" id="is_open_source0" name="is_open_source" value="0" checked>
                          <label for="is_open_source0">Нет</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>QR-коды?</h5>
                        <div class="controls">
                          <input type="radio" id="is_qrcode1" name="is_qrcode" value="1">
                          <label for="is_qrcode1">Да</label>
                          <input type="radio" id="is_qrcode0" name="is_qrcode" value="0" checked>
                          <label for="is_qrcode0">Нет</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Touch_ID?</h5>
                        <div class="controls">
                          <input type="radio" id="is_touch_id1" name="is_touch_id" value="1">
                          <label for="is_touch_id1">Да</label>
                          <input type="radio" id="is_touch_id0" name="is_touch_id" value="0" checked>
                          <label for="is_touch_id0">Нет</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Поддержка фиата?</h5>
                        <div class="controls">
                          <input type="radio" id="is_fiat1" name="is_fiat" value="1">
                          <label for="is_fiat1">Да</label>
                          <input type="radio" id="is_fiat0" name="is_fiat" value="0" checked>
                          <label for="is_fiat0">Нет</label>
                        </div>
                      </div>
                    </div>
                  </div>
      					</div>
      					<div class="tab-pane pad" id="info-container" role="tabpanel">
                  <div class="row">
                    <div class="col-6">
                      <div class="form-group">
                        <h5>Основной текст РУ</h5>
                        <div class="controls">
                          <textarea name="info_ru" class="form-control" rows="6" data-editor="true"></textarea>
                        </div>
                      </div>
                    </div>
                    <div class="col-6">
                      <div class="form-group">
                        <h5>Основной текст EN</h5>
                        <div class="controls">
                          <textarea name="info_en" class="form-control" rows="6" data-editor="true"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-6">
                      <div class="form-group">
                        <h5>Дополнительный текст РУ</h5>
                        <div class="controls">
                          <textarea name="other_ru" class="form-control" rows="6" data-editor="true"></textarea>
                        </div>
                      </div>
                    </div>
                    <div class="col-6">
                      <div class="form-group">
                        <h5>Дополнительный текст EN</h5>
                        <div class="controls">
                          <textarea name="other_en" class="form-control" rows="6" data-editor="true"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
      					<div class="tab-pane pad" id="team-container" role="tabpanel">
                  <div class="form-group">
                    <h5>Команда</h5>
                  </div>
                  <div id="team-wrap">
                    <div class="team-item row" data-index="0">
                      <div class="col-lg-3">
                        <div class="form-group">
                          <h5>Имя РУ</h5>
                          <div class="controls">
                            <input type="text" name="team[0][name_ru]" class="form-control"  value="">
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-3">
                        <div class="form-group">
                          <h5>Имя EN</h5>
                          <div class="controls">
                            <input type="text" name="team[0][name_en]" class="form-control"  value="">
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-2">
                        <div class="form-group">
                          <h5>Картинка</h5>
                          <div class="controls">
                            <div class="input-group">
                              <span class="input-group-btn">
                                <a data-input="team_thumbnail" data-preview="team_holder" class="btn btn-info text-white" data-uploader="true">
                                  <i class="fa fa-picture-o"></i> Выбрать
                                </a>
                              </span>
                              <input id="team_thumbnail" class="form-control" type="text" name="team[0][avatar]">
                            </div>
                          </div>
                          <div id="team_holder" class="holder"></div>
                        </div>
                      </div>
                      <div class="col-lg-2">
                        <div class="form-group">
                          <h5>Профессия</h5>
                          <div class="controls">
                            <select name="team[0][profession_id]" class="form-control" id="team_professions">
                              <option value="">Без указания профессии</option>
                              @foreach($professions as $profession)
                                <option value="{{$profession->id}}">{{$profession->name_ru}} ({{$profession->name_en}})</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-2">
                        <div class="form-group">
                          <h5>Вкладка</h5>
                          <div class="controls">
                            <select name="team[0][group_id]" class="form-control" id="team_group">
                              @foreach($team_groups as $group)
                                <option value="{{$group->id}}" >{{$group->name_ru}} ({{$group->name_en}})</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <button type="button" class="btn btn-info float-left" id="add_team_member"><i class="fa fa-plus"></i></button>
                  <button type="button" class="btn btn-info float-left remove_item" data-remove="team-item" style="margin-left: 5px"><i class="fa fa-minus"></i></button>
                </div>
                <div class="tab-pane pad" id="jurisdiction-container" role="tabpanel">
                  <div class="form-group">
                    <h5>Блоки данных</h5>
                  </div>
                  <div id="block-wrap">
                      <div class="block-item row" data-index="0">
                        <div class="col-lg-2">
                          <div class="form-group">
                            <h5>Порядок</h5>
                            <div class="controls">
                              <input type="number" name="blocks[0][priority]" class="form-control" value="">
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-5">
                          <div class="form-group">
                            <h5>Название РУ</h5>
                            <div class="controls">
                              <input type="text" name="blocks[0][caption_ru]" class="form-control" value="">
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-5">
                          <div class="form-group">
                            <h5>Текст РУ</h5>
                            <div class="controls">
                              <input type="text" name="blocks[0][content_ru]" class="form-control" value="">
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-2">
                        </div>
                        <div class="col-lg-5">
                          <div class="form-group">
                            <h5>Название EN</h5>
                            <div class="controls">
                              <input type="text" name="blocks[0][caption_en]" class="form-control" value="">
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-5">
                          <div class="form-group">
                            <h5>Название EN</h5>
                            <div class="controls">
                              <input type="text" name="blocks[0][content_en]" class="form-control" value="">
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
                  <button type="button" class="btn btn-info float-left" id="add_block_item"><i class="fa fa-plus"></i></button>
                  <button type="button" class="btn btn-info float-left remove_item" data-remove="social-item" style="margin-left: 5px"><i class="fa fa-minus"></i></button>
                </div>
                <div class="tab-pane pad" id="road_map-container" role="tabpanel">
                  <div class="row">
                    <div class="col-6">
                      <div class="form-group">
                        <h5>Дорожная карта </h5>
                        <div class="controls">
                          <div class="input-group">
                            <span class="input-group-btn">
                              <a data-uploader="true" data-input="road_map_thumbnail" data-preview="road_map_holder" class="btn btn-info text-white">
                                <i class="fa fa-picture-o"></i> Выбрать
                              </a>
                            </span>
                            <input id="road_map_thumbnail" class="form-control" type="text" name="road_map" value="">
                          </div>
                        </div>
                        <div id="road_map_holder" class="holder" style="max-width:none;">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane pad" id="social-container" role="tabpanel">
                  <div class="form-group">
                    <h5>Социальные сети</h5>
                  </div>
                  <div id="social-wrap">
                    <div class="social-item row" data-index="0">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <h5>Имя</h5>
                          <div class="controls">
                            <select name="social[0][social_id]" class="form-control" id="social_list">
                              @foreach($socials as $social)
                                <option value="{{$social->id}}">{{$social->name}}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <h5>Ссылка</h5>
                          <div class="controls">
                            <input type="text" name="social[0][link]" class="form-control" value="">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <button type="button" class="btn btn-info float-left" id="add_social_item"><i class="fa fa-plus"></i></button>
                  <button type="button" class="btn btn-info float-left remove_item" data-remove="social-item" style="margin-left: 5px"><i class="fa fa-minus"></i></button>
                </div>
                <div class="tab-pane pad" id="links-container" role="tabpanel">
                  <div class="form-group">
                    <h5>Другие ссылки</h5>
                  </div>
                  <div id="links-wrap">
                    <div class="links-item row" data-index="0">
                      <div class="col-lg-4">
                        <div class="form-group">
                          <h5>Название РУ</h5>
                          <div class="controls">
                            <input type="text" name="links[0][name_ru]" class="form-control" value="">
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group">
                          <h5>Название EN</h5>
                          <div class="controls">
                            <input type="text" name="links[0][name_en]" class="form-control" value="">
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group">
                          <h5>Ссылка</h5>
                          <div class="controls">
                            <input type="text" name="links[0][link]" class="form-control" value="">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <button type="button" class="btn btn-info float-left" id="add_link_item"><i class="fa fa-plus"></i></button>
                  <button type="button" class="btn btn-info float-left remove_item" data-remove="links-item" style="margin-left: 5px"><i class="fa fa-minus"></i></button>
                </div>
      				</div>
              <hr>
              <button type="submit" class="btn btn-success float-left">Добавить кошелёк</button>
            </div>
          </div>
        </div>
      </div>
    </form>
  </section>
</div>

<script src="{{asset('/vendor/laravel-filemanager/js/stand-alone-button.js')}}"></script>
<script src="{{asset('/vendor/ckeditor/ckeditor.js')}}"></script>
<script src="{{asset('/vendor/unisharp/laravel-ckeditor/adapters/jquery.js')}}"></script>
<script src="{{asset('invi/js/pages/project_create.js') }}?{{ time() }}"></script>

@endsection
