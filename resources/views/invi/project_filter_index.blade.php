@extends('invi.layouts.app')
@section('title','inviadmin - Настройки фильтра проектов')
@section('description','inviadmin - Настройки фильтра проектов')
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title') Настройки фильтра проектов @endslot
    @slot('active')  Настройки фильтра проектов @endslot
  @endcomponent
  <section class="content">
      <div class="row">
        <div class="col-12 col-lg-6">
          <div class="box box-solid bg-dark">
            <div class="box-header with-border">
              <div class="row">
                <div class="col-lg-12">
                  <h3 class="box-title">Полный список параметров фильтра проектов</h3>
                  <h6 class="box-subtitle">Данные можно экспортировать в CSV, Excel, PDF.</h6>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

            <div class="table-responsive">
              <table id="project-filter" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Название</th>
                  <th>Статус</th>
                  <th>Приоритет</th>
                </tr>
              </thead>

              <tbody>
                @forelse($project_filters as $project_filter)
                  <tr>
                    <td>{{$project_filter->id}}</td>
                    <td><a href="{{route('project-filter.show',['id'=>$project_filter->id])}}">{{$project_filter->name_ru}}({{$project_filter->name_en}})</a></td>
                    <td>@if($project_filter->is_visible == 1) <span class="label label-success">Виден</span> @else <span class="label label-danger">Не виден</span> @endif</td>
                    <td>{{$project_filter->priority}}</td>
                  </tr>
                @empty
                  <tr>
                    <td colspan="4">Нет данных</td>
                  </tr>
                @endforelse
              </tbody>
              <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Название РУ</th>
                  <th>Название EN</th>
                </tr>
              </tfoot>
            </table>
            </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Добавить параметр для проектов</h3>
            </div>
            <div class="box-body">
              <form method="post" action="{{route('project-filter.store')}}">
                {{csrf_field()}}
                <div class="form-group">
                  <h5>Название РУ<span class="text-danger">*</span></h5>
                  <div class="controls">
                    <input type="text" name="name_ru" class="form-control" required=""  value="">
                  </div>
                </div>
                <div class="form-group">
                  <h5>Название EN<span class="text-danger">*</span></h5>
                  <div class="controls">
                    <input type="text" name="name_en" class="form-control" required=""  value="">
                  </div>
                </div>
                <div class="form-group">
                  <h5>Приоритет</h5>
                  <div class="controls">
                    <input type="text" name="priority" class="form-control"  value="">
                  </div>
                </div>
                <button type="submit" class="btn btn-success float-left">Добавить параметр</button>
              </form>
            </div>
          </div>
        </div>
    </div>
  </section>
</div>


@if(isset($project_filters[0]))
  @component('invi.components.table_excel')
    @slot('table_id') extra @endslot
  @endcomponent
@endif

@endsection
