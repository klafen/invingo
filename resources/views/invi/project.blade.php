@extends('invi.layouts.app')
@section('title','inviadmin - проект '.$project->name)
@section('description','inviadmin - проект'.$project->name)
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title') проект {{$project->name}} @endslot
    @slot('parent') Проекты @endslot
    @slot('parent_link') {{route('projects.index')}} @endslot
    @slot('active') проект {{$project->name}} @endslot
  @endcomponent
  <section class="content">
    <form method="post" action="{{route('projects.update', $project->id)}}" enctype="multipart/form-data">
      {{csrf_field()}}
      <input type="hidden" name="id" value="{{$project->id}}">
      <input type="hidden" name="_method" value="put">
      <div class="row">
        <div class="col-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Данные проекта</h3>
            </div>
            <ul class="nav nav-tabs customtab" role="tablist">
              <li class="nav-item"> <a class="nav-link active show" data-toggle="tab" href="#all-container" role="tab" aria-selected="true"><span class="hidden-sm-up"><i class="ion-home"></i></span> <span class="hidden-xs-down">Основные</span></a> </li>
              <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#money-container" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ion-person"></i></span> <span class="hidden-xs-down">Финансы</span></a> </li>
              <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#lifetime-container" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ion-person"></i></span> <span class="hidden-xs-down">Жизненный цикл</span></a> </li>
              <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#info-container" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ion-person"></i></span> <span class="hidden-xs-down">Описание</span></a> </li>
              <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#team-container" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ion-email"></i></span> <span class="hidden-xs-down">Команда</span></a> </li>
              <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#road_map-container" role="tab" aria-selected="false"><span class="hidden-xs-down">Дорожная карта</span></a> </li>
              <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#agency-rating-container" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ion-email"></i></span> <span class="hidden-xs-down">Рейтинги агенств</span></a> </li>
              <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#rating-container" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ion-email"></i></span> <span class="hidden-xs-down">Доп рейтинги</span></a> </li>
              <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#social-container" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ion-email"></i></span> <span class="hidden-xs-down">Социальные сети</span></a> </li>
              <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#links-container" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ion-email"></i></span> <span class="hidden-xs-down">Другие ссылки</span></a> </li>
              <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#bonuses-container" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ion-email"></i></span> <span class="hidden-xs-down">Бонусы</span></a> </li>
              <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#jurisdiction-container" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ion-email"></i></span> <span class="hidden-xs-down">Блоки</span></a> </li>
              <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#filter-container" role="tab" aria-selected="false"><span class="hidden-sm-up"><i class="ion-email"></i></span> <span class="hidden-xs-down">Доп. параметры</span></a> </li>
            </ul>
            <div class="box-body">
              <div class="tab-content">
                <div class="tab-pane active show" id="all-container" role="tabpanel">
                  <div class="row">
                    <div class="col-lg-6 col-12">
                      <div class="form-group">
                        <h5>Название </h5>
                        <div class="controls">
                          <input type="text" name="name" class="form-control" required=""  value="{{ $project->name }}">
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Логотип </h5>
                        <div class="controls">
                          <div class="input-group">
                            <span class="input-group-btn">
                              <a data-uploader="true" data-input="thumbnail" data-preview="holder" class="btn btn-info text-white">
                                <i class="fa fa-picture-o"></i> Выбрать
                              </a>
                            </span>
                            <input id="thumbnail" class="form-control" type="text" name="logo" value="{{ $project->logo }}">
                          </div>
                        </div>
                        <div id="holder" class="holder">
                          <img src="{{ $project->logo }}" alt="" style="height: 5rem;">
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Токен </h5>
                        <div class="controls">
                          <input type="text" name="token" class="form-control" required=""  value="{{ $project->token }}">
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Рейтинг </h5>
                        <div class="controls">
                          <input type="number" name="rating" class="form-control" required=""   value="{{ $project->rating }}">
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Приоритет </h5>
                        <div class="controls">
                          <input type="number" name="priority" class="form-control"  value="{{ $project->priority }}">
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Главная валюта </h5>
                        <div class="controls">
                          <input type="text" name="all_currency" class="form-control"   value="{{ $project->all_currency }}">
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>ROI</h5>
                        <div class="controls">
                          <input type="text" name="roi" class="form-control"   value="{{$project->roi}}" required="">
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Категория </h5>
                        <div class="controls">
                          <select name="category_id" required="" class="form-control">
                            @foreach($categories as $category)
                              <option value="{{$category->id}}" @if($project->category_id == $category->id) selected @endif>{{$category->name_ru}} ({{$category->name_en}})</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Страна</h5>
                        <div class="controls">
                          <select name="country_id" class="form-control">
                            <option value=""></option>
                            @foreach($countries as $country)
                              <option value="{{$country->id}}" @if($project->country_id == $country->id) selected @endif>{{$country->name_ru}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Платформа </h5>
                        <div class="controls">
                          <select name="platform_id" required="" class="form-control">
                            @foreach($platforms as $platform)
                              <option value="{{$platform->id}}" @if($project->platform_id == $platform->id) selected @endif>{{$platform->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Тип токена </h5>
                        <div class="controls">
                          <select name="type_token_id" required="" class="form-control">
                            @foreach($type_tokens as $type_token)
                              <option value="{{$type_token->id}}" @if($project->type_token_id == $type_token->id) selected @endif>{{$type_token->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Валюты: <small>Зажмите ctrl</small> </h5>
                        <div class="controls">
                          <select name="accept[]" class="form-control" multiple>
                            @foreach($cryptocoins as $cryptocoin)
                              <option value="{{$cryptocoin->id}}" @if(in_array($cryptocoin->id,explode(',',$project_accept->cryptocoin_id))) selected @endif>{{$cryptocoin->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Торговля на биржах: <small>Зажмите ctrl</small> </h5>
                        <div class="controls">
                          <select name="project_tradings[]" class="form-control" multiple>
                            @foreach($tradings as $trading)
                              <option value="{{$trading->id}}" @if(in_array($trading->id,explode(',',$project_tradings->trading_id))) selected @endif>{{$trading->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-6 col-12">
                      <div class="form-group">
                        <h5>Видимость</h5>
                        <div class="controls">
                          <input type="radio" id="is_visible1" name="is_visible" value="1" @if($project->is_visible == 1) checked @endif>
                          <label for="is_visible1">Да</label>
                          <input type="radio" id="is_visible0" name="is_visible" value="0" @if($project->is_visible == 0) checked @endif>
                          <label for="is_visible0">Нет</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Отображать на слайдере?</h5>
                        <div class="controls">
                          <input type="radio" id="is_slider1" name="is_slider" value="1" @if($project->is_slider == 1) checked @endif>
                          <label for="is_slider1">Да</label>
                          <input type="radio" id="is_slider0" name="is_slider" value="0" @if($project->is_slider == 0) checked @endif>
                          <label for="is_slider0">Нет</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Рекламный проект?</h5>
                        <div class="controls">
                          <input type="radio" id="is_adv1" name="is_adv" value="1" @if($project->is_adv == 1) checked @endif>
                          <label for="is_adv1">Да</label>
                          <input type="radio" id="is_adv0" name="is_adv" value="0" @if($project->is_adv == 0) checked @endif>
                          <label for="is_adv0">Нет</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Белый лист (whitelist)?</h5>
                        <div class="controls">
                          <input type="radio" id="is_whitelist1" name="is_whitelist" value="1" @if($project->is_whitelist == 1) checked @endif>
                          <label for="is_whitelist1">Да</label>
                          <input type="radio" id="is_whitelist0" name="is_whitelist" value="0" @if($project->is_whitelist == 0) checked @endif>
                          <label for="is_whitelist0">Нет</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>KYC?</h5>
                        <div class="controls">
                          <input type="radio" id="is_kyc1" name="is_kyc" value="1" @if($project->is_kyc == 1) checked @endif>
                          <label for="is_kyc1">Да</label>
                          <input type="radio" id="is_kyc0" name="is_kyc" value="0" @if($project->is_kyc == 0) checked @endif>
                          <label for="is_kyc0">Нет</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>KYT?</h5>
                        <div class="controls">
                          <input type="radio" id="is_kyt1" name="is_kyt" value="1" @if($project->is_kyt == 1) checked @endif>
                          <label for="is_kyt1">Да</label>
                          <input type="radio" id="is_kyt0" name="is_kyt" value="0" @if($project->is_kyt == 0) checked @endif>
                          <label for="is_kyt0">Нет</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Юрисдикция </h5>
                        <div class="controls">
                          <input type="radio" id="jurisdiction1" name="jurisdiction" value="1" @if($project->jurisdiction == 1) checked @endif>
                          <label for="jurisdiction1">Verify</label>
                          <input type="radio" id="jurisdiction0" name="jurisdiction" value="0" @if($project->jurisdiction == 0) checked @endif>
                          <label for="jurisdiction0">Unverify</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Смарт контракт?</h5>
                        <div class="controls">
                          <input type="radio" id="is_smart1" name="is_smartcontract" value="1" @if($project->is_smartcontract == 1) checked @endif>
                          <label for="is_smart1">Да</label>
                          <input type="radio" id="is_smart0" name="is_smartcontract" value="0" @if($project->is_smartcontract == 0) checked @endif>
                          <label for="is_smart0">Нет</label>
                        </div>
                      </div>
                      <h5>Air drop?</h5>
                      <div class="controls">
                        <input type="radio" id="is_airdrop1" name="is_airdrop" value="1" @if($project->is_airdrop == 1) checked @endif>
                        <label for="is_airdrop1">Да</label>
                        <input type="radio" id="is_airdrop0" name="is_airdrop" value="0" @if($project->is_airdrop == 0) checked @endif>
                        <label for="is_airdrop0">Нет</label>
                      </div>
                      <h5>Бонусы?</h5>
                      <div class="controls">
                        <input type="radio" id="is_bonus1" name="is_bonus" value="1" @if($project->is_bonus == 1) checked @endif>
                        <label for="is_bonus1">Да</label>
                        <input type="radio" id="is_bonus0" name="is_bonus" value="0" @if($project->is_bonus == 0) checked @endif>
                        <label for="is_bonus0">Нет</label>
                      </div>
                      <div class="form-group">
                        <h5>Bounty?</h5>
                        <div class="controls">
                          <input type="radio" id="is_bounty1" name="is_bounty" value="1" @if($project->is_bounty == 1) checked @endif>
                          <label for="is_bounty1">Да</label>
                          <input type="radio" id="is_bounty0" name="is_bounty" value="0" @if($project->is_bounty == 0) checked @endif>
                          <label for="is_bounty0">Нет</label>
                        </div>
                      </div>
                      <h5>MVP?</h5>
                      <div class="controls">
                        <input type="radio" id="is_mvp1" name="is_mvp" value="1" @if($project->is_mvp == 1) checked @endif>
                        <label for="is_mvp1">Да</label>
                        <input type="radio" id="is_mvp0" name="is_mvp" value="0" @if($project->is_mvp == 0) checked @endif>
                        <label for="is_mvp0">Нет</label>
                      </div>
                      <h5>Etherscancheck?</h5>
                      <div class="controls">
                        <input type="radio" id="is_etherscancheck1" name="is_etherscancheck" value="1" @if($project->is_etherscancheck == 1) checked @endif>
                        <label for="is_etherscancheck1">Да</label>
                        <input type="radio" id="is_etherscancheck0" name="is_etherscancheck" value="0"  @if($project->is_etherscancheck == 0) checked @endif >
                        <label for="is_etherscancheck0">Нет</label>
                      </div>

                      <h5>Howey Test?</h5>
                      <div class="controls">
                        <input type="radio" id="is_howey_test1" name="is_howey_test" value="1" @if($project->is_howey_test == 1) checked @endif>
                        <label for="is_howey_test1">Да</label>
                        <input type="radio" id="is_howey_test0" name="is_howey_test" value="0"  @if($project->is_howey_test == 0) checked @endif >
                        <label for="is_howey_test0">Нет</label>
                      </div>
                      <h5>Escrow?</h5>
                      <div class="controls">
                        <input type="radio" id="is_escrow1" name="is_escrow" value="1" @if($project->is_escrow == 1) checked @endif>
                        <label for="is_escrow1">Да</label>
                        <input type="radio" id="is_escrow0" name="is_escrow" value="0" @if($project->is_escrow == 0) checked @endif>
                        <label for="is_escrow0">Нет</label>
                      </div>
                      <div class="form-group">
                        <h5>Запрет для стран: <small>Зажмите ctrl</small> </h5>
                        <div class="controls">
                          <select name="restricted_countries[]" class="form-control" multiple>
                            @foreach($countries as $country)
                              <option value="{{$country->id}}" @if(in_array($country->id,explode(',',$project_restricted_countries->country_id))) selected @endif>{{$country->name_ru}} ({{$country->name_en}})</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <h5>Extra: <small>Зажмите ctrl</small> </h5>
                        <div class="controls">
                          <select name="project_extra[]" class="form-control" multiple>
                            @foreach($extras as $extra)
                              <option value="{{$extra->id}}"  @if(in_array($extra->id,explode(',',$project_extra['extra_id']))) selected @endif>{{$extra->name_ru}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
      					</div>
                <div class="tab-pane pad" id="money-container" role="tabpanel">
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group">
                        <h5>Финансы</h5>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <h5>Soft cap</h5>
                        <div class="controls">
                          <input type="number" name="soft_cap" class="form-control"   value="{{$project_prices->soft_cap??''}}">
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <h5>Валюта</h5>
                        <div class="controls">
                          <select name="soft_cap_currency_id" class="form-control">
                            <option value=""></option>
                            @foreach($cryptocoins as $cryptocoin)
                              <option value="{{$cryptocoin->id}}" @if($project_prices->soft_cap_currency_id==$cryptocoin->id) selected @endif>{{$cryptocoin->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <h5>Hard cap</h5>
                        <div class="controls">
                          <input type="number" name="hard_cap" class="form-control"   value="{{$project_prices->hard_cap??''}}">
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <h5>Валюта</h5>
                        <div class="controls">
                          <select name="hard_cap_currency_id" class="form-control">
                            <option value=""></option>
                            @foreach($cryptocoins as $cryptocoin)
                              <option value="{{$cryptocoin->id}}" @if($project_prices->hard_cap_currency_id==$cryptocoin->id) selected @endif>{{$cryptocoin->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <h5>Собрано</h5>
                        <div class="controls">
                          <input type="number" name="collected" class="form-control"   value="{{$project_prices->collected??''}}">
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <h5>Валюта</h5>
                        <div class="controls">
                          <select name="collected_currency_id" class="form-control">
                            <option value=""></option>
                            @foreach($cryptocoins as $cryptocoin)
                              <option value="{{$cryptocoin->id}}" @if($project_prices->collected_currency_id==$cryptocoin->id) selected @endif>{{$cryptocoin->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <h5>Токенов на продажу</h5>
                        <div class="controls">
                          <input type="number" name="for_sale" class="form-control"   value="{{$project_prices->for_sale??''}}">
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <h5>Валюта</h5>
                        <div class="controls">
                          <select name="for_sale_currency_id" class="form-control">
                            <option value=""></option>
                            @foreach($cryptocoins as $cryptocoin)
                              <option value="{{$cryptocoin->id}}" @if($project_prices->for_sale_currency_id==$cryptocoin->id) selected @endif>{{$cryptocoin->name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane pad" id="lifetime-container" role="tabpanel">
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group">
                        <h5>Жизненный цикл</h5>
                      </div>
                      <div id="timeline-wrap">
                        @forelse($project_lifetime as $i => $lifetime)
                          <div class="timeline-item row" data-index="{{$i}}">
                            <div class="col-lg-2">
                              <div class="form-group">
                                <h5>Этап </h5>
                                <div class="controls">
                                  <select name="timeline[{{$i}}][status_id]" class="form-control" id="status_list">
                                    <option value=""></option>
                                    @foreach($statuses as $status)
                                      <option value="{{$status->id}}" @if($status->id == $lifetime['status_id']) selected @endif>{{$status->name_ru}} ({{$status->name_en}})</option>
                                    @endforeach
                                  </select>
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-1">
                              <div class="form-group">
                                <h5>Подэтап</h5>
                                <div class="controls">
                                  <input type="text" name="timeline[{{$i}}][substatus]" class="form-control" value="{{$lifetime['substatus']}}">
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-2">
                              <div class="form-group">
                                <h5>Начало</h5>
                                <div class="controls">
                                  <input type="date" name="timeline[{{$i}}][start]" class="form-control" value="{{$lifetime['start']}}">
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-2">
                              <div class="form-group">
                                <h5>Конец</h5>
                                <div class="controls">
                                  <input type="date" name="timeline[{{$i}}][end]" class="form-control" value="{{$lifetime['end']}}">
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-1">
                              <div class="form-group">
                                <h5>Час пояс</h5>
                                <div class="controls">
                                  <select name="timeline[{{$i}}][gmt]" class="form-control" id="gmt_list">
                                    <option value=""></option>
                                    @for ($k=-11; $k<12; $k++)
                                      @php if($k>0){{$gmt='+'.$k;}} else {$gmt=$k;} @endphp
                                    <option value="{{$gmt}}" @if($lifetime->gmt && $lifetime->gmt==$gmt) selected @endif>{{$gmt}} GMT</option>
                                    @endfor
                                  </select>
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-1">
                              <div class="form-group">
                                <h5>Цена</h5>
                                <div class="controls">
                                  <input type="number" name="timeline[{{$i}}][price]" step="any" class="form-control" value="@if($lifetime['price']){{rtrim(rtrim(number_format($lifetime['price'],10),0), '.')}}@endif">
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-1">
                              <div class="form-group">
                                <h5>Валюта</h5>
                                <div class="controls">
                                  <select name="timeline[{{$i}}][price_currency_id]" class="form-control" id="currency_list">
                                    <option value=""></option>
                                    @foreach($cryptocoins as $cryptocoin)
                                      <option value="{{$cryptocoin->id}}" @if($lifetime->price_currency_id==$cryptocoin->id) selected @endif>{{$cryptocoin->code}}</option>
                                    @endforeach
                                  </select>
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-1">
                              <div class="form-group">
                                <h5>Мин Инвест</h5>
                                <div class="controls">
                                  <input type="number" name="timeline[{{$i}}][min_invest]" step="any" class="form-control" value="@if($lifetime['min_invest']){{rtrim(rtrim(number_format($lifetime['min_invest'],10),0), '.')}}@endif">
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-1">
                              <div class="form-group">
                                <h5>Валюта</h5>
                                <div class="controls">
                                  <select name="timeline[{{$i}}][min_invest_currency_id]" class="form-control">
                                    <option value=""></option>
                                    @foreach($cryptocoins as $cryptocoin)
                                      <option value="{{$cryptocoin->id}}" @if($lifetime->min_invest_currency_id==$cryptocoin->id) selected @endif>{{$cryptocoin->code}}</option>
                                    @endforeach
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>
                        @empty
                          <div class="timeline-item row" data-index="0">
                            <div class="col-lg-2">
                              <div class="form-group">
                                <h5>Этап </h5>
                                <div class="controls">
                                  <select name="timeline[0][status_id]" class="form-control" id="status_list">
                                    <option value=""></option>
                                    @foreach($statuses as $status)
                                      <option value="{{$status->id}}">{{$status->name_ru}} ({{$status->name_en}})</option>
                                    @endforeach
                                  </select>
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-1">
                              <div class="form-group">
                                <h5>Подэтап</h5>
                                <div class="controls">
                                  <input type="text" name="timeline[0][substatus]" class="form-control" value="">
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-2">
                              <div class="form-group">
                                <h5>Начало</h5>
                                <div class="controls">
                                  <input type="date" name="timeline[0][start]" class="form-control" value="">
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-2">
                              <div class="form-group">
                                <h5>Конец</h5>
                                <div class="controls">
                                  <input type="date" name="timeline[0][end]" class="form-control" value="">
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-1">
                              <div class="form-group">
                                <h5>Час пояс</h5>
                                <div class="controls">
                                  <select name="timeline[0][gmt]" class="form-control" id="gmt_list">
                                    <option value=""></option>
                                    @for ($k=-11; $k<12; $k++)
                                      @php if($k>0){{$gmt='+'.$k;}} else {$gmt=$k;} @endphp
                                      <option value="{{$gmt}}" >{{$gmt}} GMT</option>
                                    @endfor
                                  </select>
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-1">
                              <div class="form-group">
                                <h5>Цена</h5>
                                <div class="controls">
                                  <input type="number" name="timeline[0][price]" step="any" class="form-control" value="">
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-1">
                              <div class="form-group">
                                <h5>Валюта</h5>
                                <div class="controls">
                                  <select name="timeline[0][price_currency_id]" class="form-control" id="currency_list">
                                    <option value=""></option>
                                    @foreach($cryptocoins as $cryptocoin)
                                      <option value="{{$cryptocoin->id}}">{{$cryptocoin->code}}</option>
                                    @endforeach
                                  </select>
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-1">
                              <div class="form-group">
                                <h5>Мин Инвест</h5>
                                <div class="controls">
                                  <input type="number" name="timeline[0][min_invest]" step="any" class="form-control" value="">
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-1">
                              <div class="form-group">
                                <h5>Валюта</h5>
                                <div class="controls">
                                  <select name="timeline[0][min_invest_currency_id]" class="form-control">
                                    <option value=""></option>
                                    @foreach($cryptocoins as $cryptocoin)
                                      <option value="{{$cryptocoin->id}}">{{$cryptocoin->code}}</option>
                                    @endforeach
                                  </select>
                                </div>
                              </div>
                            </div>
                          </div>
                        @endforelse
                      </div>
                      <button type="button" class="btn btn-info float-left" id="add_timeline_item"><i class="fa fa-plus"></i></button>
                      <button type="button" class="btn btn-info float-left remove_item" data-remove="timeline-item" style="margin-left: 5px"><i class="fa fa-minus"></i></button>
                    </div>
                  </div>
                </div>
                <div class="tab-pane pad" id="info-container" role="tabpanel">
          <div class="row">
            <div class="col-6">
              <div class="form-group">
                <h5>Основной текст РУ</h5>
                <div class="controls">
                  <textarea name="info_ru" class="form-control" rows="6" data-editor="true">{!! $project_description->info_ru??'' !!}</textarea>
                </div>
              </div>
              <div class="form-group">
                <h5>Про компанию РУ</h5>
                <div class="controls">
                  <textarea name="about_us_ru" class="form-control" rows="6" data-editor="true">{!! $project_description->about_us_ru??'' !!}</textarea>
                </div>
              </div>
              <div class="form-group">
              <h5>Короткий текст для слайдера RU</h5>
                <div class="controls">
                  <input type="text" name="slider_ru" class="form-control"  value="{{ $project_description->slider_ru }}">
                </div>
              </div>
            </div>
            <div class="col-6">
              <div class="form-group">
                <h5>Основной текст EN</h5>
                <div class="controls">
                  <textarea name="info_en" class="form-control" rows="6" data-editor="true">{!! $project_description->info_en??'' !!}</textarea>
                </div>
              </div>
              <div class="form-group">
                <h5>Про компанию EN</h5>
                <div class="controls">
                  <textarea name="about_us_en" class="form-control" rows="6" data-editor="true">{!! $project_description->about_us_en??'' !!}</textarea>
                </div>
              </div>
              <div class="form-group">
                <h5>Короткий текст для слайдера EN</h5>
                <div class="controls">
                  <input type="text" name="slider_en" class="form-control"   value="{{ $project_description->slider_en }}">
                </div>
              </div>
            </div>
          </div>
        </div>
                <div class="tab-pane pad" id="team-container" role="tabpanel">
          <div class="form-group">
            <h5>Команда</h5>
          </div>
          <div id="team-wrap">
            @forelse($project_team as $i => $team)
            <div class="team-item row" data-index="{{$i}}">
              <div class="col-lg-2">
                <div class="form-group">
                  <h5>Имя РУ</h5>
                  <div class="controls">
                    <input type="text" name="team[{{$i}}][name_ru]" class="form-control"  value="{{$team->name_ru}}">
                  </div>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <h5>Имя EN</h5>
                  <div class="controls">
                    <input type="text" name="team[{{$i}}][name_en]" class="form-control"  value="{{$team->name_en}}">
                  </div>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <h5>Картинка</h5>
                  <div class="controls">
                    <div class="input-group">
                      <span class="input-group-btn">
                        <a data-input="team_thumbnail{{$i}}" data-preview="team_holder{{$i}}" class="btn btn-info text-white" data-uploader="true">
                          <i class="fa fa-picture-o"></i> Выбрать
                        </a>
                      </span>
                      <input id="team_thumbnail{{$i}}" class="form-control" type="text" name="team[{{$i}}][avatar]" value="{{$team->avatar}}">
                    </div>
                  </div>
                  <div id="team_holder{{$i}}" class="holder">
                    @if($team->avatar != NULL)
                    <img src="{{$team->avatar}}" style="height: 5rem;">
                    @endif
                  </div>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <h5>Профессия РУ</h5>
                  <div class="controls">
                    <input type="text" name="team[{{$i}}][profession_ru]" class="form-control"  value="{{$team->profession_ru}}">
                  </div>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <h5>Профессия EN</h5>
                  <div class="controls">
                    <input type="text" name="team[{{$i}}][profession_en]" class="form-control"  value="{{$team->profession_en}}">
                  </div>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <h5>Вкладка</h5>
                  <div class="controls">
                    <select name="team[{{$i}}][group_id]" class="form-control" id="team_group">
                      @foreach($team_groups as $group)
                        <option value="{{$group->id}}" @if($group->id == $team['group_id']) selected @endif>{{$group->name_ru}} ({{$group->name_en}})</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
            </div>
            @empty
            <div class="team-item row" data-index="0">
              <div class="col-lg-2">
                <div class="form-group">
                  <h5>Имя РУ</h5>
                  <div class="controls">
                    <input type="text" name="team[0][name_ru]" class="form-control"  value="">
                  </div>
                </div>
              </div>
              <div class="col-lg-3">
                <div class="form-group">
                  <h5>Имя EN</h5>
                  <div class="controls">
                    <input type="text" name="team[0][name_en]" class="form-control"  value="">
                  </div>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <h5>Картинка</h5>
                  <div class="controls">
                    <div class="input-group">
                      <span class="input-group-btn">
                        <a data-input="team_thumbnail" data-preview="team_holder" class="btn btn-info text-white" data-uploader="true">
                          <i class="fa fa-picture-o"></i> Выбрать
                        </a>
                      </span>
                      <input id="team_thumbnail" class="form-control" type="text" name="team[0][avatar]">
                    </div>
                  </div>
                  <div id="team_holder" class="holder"></div>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <h5>Профессия РУ</h5>
                  <div class="controls">
                    <input type="text" name="team[0][profession_ru]" class="form-control"  value="">
                  </div>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <h5>Профессия EN</h5>
                  <div class="controls">
                    <input type="text" name="team[0][profession_en]" class="form-control"  value="">
                  </div>
                </div>
              </div>
              <div class="col-lg-2">
                <div class="form-group">
                  <h5>Вкладка</h5>
                  <div class="controls">
                    <select name="team[0][group_id]" class="form-control" id="team_group">
                      @foreach($team_groups as $group)
                        <option value="{{$group->id}}">{{$group->name_ru}} ({{$group->name_en}})</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
            </div>
            @endforelse
          </div>
          <button type="button" class="btn btn-info float-left" id="add_team_member"><i class="fa fa-plus"></i></button>
          <button type="button" class="btn btn-info float-left remove_item" data-remove="team-item" style="margin-left: 5px"><i class="fa fa-minus"></i></button>
        </div>
                <div class="tab-pane pad" id="road_map-container" role="tabpanel">
                  <div class="row">
                    <div class="col-6">
                      <div class="form-group">
                        <h5>Дорожная карта </h5>
                        <div class="controls">
                          <div class="input-group">
                            <span class="input-group-btn">
                              <a data-uploader="true" data-input="road_map_thumbnail" data-preview="road_map_holder" class="btn btn-info text-white">
                                <i class="fa fa-picture-o"></i> Выбрать
                              </a>
                            </span>
                            <input id="road_map_thumbnail" class="form-control" type="text" name="road_map" value="{{ $project->road_map }}">
                          </div>
                        </div>
                        <div id="road_map_holder" class="holder" style="max-width:none;">
                          <img src="{{ $project->road_map }}" alt="" >
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane pad" id="agency-rating-container" role="tabpanel">
                  <div class="form-group">
                    <h5>Рейтинги агенств</h5>
                  </div>
                  <div id="agency-rating-wrap">
                    @forelse($project_agencies_rating as $i => $agency)
                    <div class="agency-rating-item row" data-index="{{$i}}">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <h5>Имя</h5>
                          <div class="controls">
                            <select name="agency_rating[{{$i}}][agency_rating_id]" class="form-control" id="agency_ratings">
                              @foreach($agencies_ratings as $agencies_rating)
                                <option value="{{$agencies_rating->id}}" @if($agencies_rating->id == $agency->agency_rating_id) selected @endif>{{$agencies_rating->name}}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <h5>Рейтинг</h5>
                          <div class="controls">
                            <input type="text" name="agency_rating[{{$i}}][rating]" class="form-control" value="{{$agency->rating}}">
                          </div>
                        </div>
                      </div>
                    </div>
                    @empty
                    <div class="agency-rating-item row" data-index="0">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <h5>Имя</h5>
                          <div class="controls">
                            <select name="agency_rating[0][agency_rating_id]" class="form-control" id="agency_ratings">
                              @foreach($agencies_ratings as $agencies_rating)
                                <option value="{{$agencies_rating->id}}">{{$agencies_rating->name}}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <h5>Рейтинг</h5>
                          <div class="controls">
                            <input type="text" name="agency_rating[0][rating]" class="form-control" value="">
                          </div>
                        </div>
                      </div>
                    </div>
                    @endforelse
                  </div>
                  <button type="button" class="btn btn-info float-left" id="add_agency_rating_item"><i class="fa fa-plus"></i></button>
                  <button type="button" class="btn btn-info float-left remove_item" data-remove="agency-rating-item" style="margin-left: 5px"><i class="fa fa-minus"></i></button>
                </div>
                <div class="tab-pane pad" id="rating-container" role="tabpanel">
                  <div class="form-group">
                    <h5>Доп рейтинги</h5>
                  </div>
                  <div id="rating-wrap">
                    @forelse($project_custom_rating as $i => $custom)
                    <div class="rating-item row" data-index="{{$i}}">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <h5>Имя</h5>
                          <div class="controls">
                            <select name="custom_rating[{{$i}}][custom_rating_id]" class="form-control" id="cutsom_ratings">
                              @foreach($customratings as $customrating)
                                <option value="{{$customrating->id}}" @if($customrating->id == $custom->custom_rating_id) selected @endif>{{$customrating->name_ru}} ({{$customrating->name_en}})</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <h5>Рейтинг</h5>
                          <div class="controls">
                            <input type="number" name="custom_rating[{{$i}}][rating]" class="form-control" value="{{$custom->rating}}"  >
                          </div>
                        </div>
                      </div>
                    </div>
                    @empty
                    <div class="rating-item row" data-index="0">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <h5>Имя</h5>
                          <div class="controls">
                            <select name="custom_rating[0][custom_rating_id]" class="form-control" id="cutsom_ratings">
                              @foreach($customratings as $customrating)
                                <option value="{{$customrating->id}}">{{$customrating->name_ru}} ({{$customrating->name_en}})</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <h5>Рейтинг</h5>
                          <div class="controls">
                            <input type="number" name="custom_rating[0][rating]" class="form-control" value=""  >
                          </div>
                        </div>
                      </div>
                    </div>
                    @endforelse
                  </div>
                  <button type="button" class="btn btn-info float-left" id="add_rating_item"><i class="fa fa-plus"></i></button>
                  <button type="button" class="btn btn-info float-left remove_item" data-remove="rating-item" style="margin-left: 5px"><i class="fa fa-minus"></i></button>
                </div>
                <div class="tab-pane pad" id="social-container" role="tabpanel">
                  <div class="form-group">
                    <h5>Социальные сети</h5>
                  </div>
                  <div id="social-wrap">
                    @forelse($project_social as $i => $pr_social)
                    <div class="social-item row" data-index="{{$i}}">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <h5>Имя</h5>
                          <div class="controls">
                            <select name="social[{{$i}}][social_id]" class="form-control" id="social_list">
                              @foreach($socials as $social)
                                <option value="{{$social->id}}" @if($social->id == $pr_social->social_id) selected @endif>{{$social->name}}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <h5>Ссылка</h5>
                          <div class="controls">
                            <input type="text" name="social[{{$i}}][link]" class="form-control" value="{{$pr_social->link}}">
                          </div>
                        </div>
                      </div>
                    </div>
                    @empty
                    <div class="social-item row" data-index="0">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <h5>Имя</h5>
                          <div class="controls">
                            <select name="social[0][social_id]" class="form-control" id="social_list">
                              @foreach($socials as $social)
                                <option value="{{$social->id}}">{{$social->name}}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <h5>Ссылка</h5>
                          <div class="controls">
                            <input type="text" name="social[0][link]" class="form-control" value="">
                          </div>
                        </div>
                      </div>
                    </div>
                    @endforelse

                  </div>
                  <button type="button" class="btn btn-info float-left" id="add_social_item"><i class="fa fa-plus"></i></button>
                  <button type="button" class="btn btn-info float-left remove_item" data-remove="social-item" style="margin-left: 5px"><i class="fa fa-minus"></i></button>
                </div>
                <div class="tab-pane pad" id="links-container" role="tabpanel">
                  <div class="form-group">
                    <h5>Другие ссылки</h5>
                  </div>
                  <div id="links-wrap">
                    @forelse($project_links as $i => $pr_link)
                    <div class="links-item row" data-index="{{$i}}">
                      <div class="col-lg-4">
                        <div class="form-group">
                          <h5>Название РУ</h5>
                          <div class="controls">
                            <input type="text" name="links[{{$i}}][name_ru]" class="form-control" value="{{$pr_link->name_ru}}">
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group">
                          <h5>Название EN</h5>
                          <div class="controls">
                            <input type="text" name="links[{{$i}}][name_en]" class="form-control" value="{{$pr_link->name_en}}">
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group">
                          <h5>Ссылка</h5>
                          <div class="controls">
                            <input type="text" name="links[{{$i}}][link]" class="form-control" value="{{$pr_link->link}}">
                          </div>
                        </div>
                      </div>
                    </div>
                    @empty
                    <div class="links-item row" data-index="0">
                      <div class="col-lg-4">
                        <div class="form-group">
                          <h5>Название РУ</h5>
                          <div class="controls">
                            <input type="text" name="links[0][name_ru]" class="form-control" value="">
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group">
                          <h5>Название EN</h5>
                          <div class="controls">
                            <input type="text" name="links[0][name_en]" class="form-control" value="">
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <div class="form-group">
                          <h5>Ссылка</h5>
                          <div class="controls">
                            <input type="text" name="links[0][link]" class="form-control" value="">
                          </div>
                        </div>
                      </div>
                    </div>
                    @endforelse

                  </div>
                  <button type="button" class="btn btn-info float-left" id="add_link_item"><i class="fa fa-plus"></i></button>
                  <button type="button" class="btn btn-info float-left remove_item" data-remove="links-item" style="margin-left: 5px"><i class="fa fa-minus"></i></button>
                </div>
                <div class="tab-pane pad" id="bonuses-container" role="tabpanel">
                  <div class="form-group">
                    <h5>Бонусы</h5>
                  </div>
                  <div id="bonuses-wrap">
                    @forelse($project_bonuses as $i => $pr_bonus)
                      <div class="bonuses-item row" data-index="{{$i}}">
                        <div class="col-lg-4">
                          <div class="form-group">
                            <h5>Этап</h5>
                            <div class="controls">
                              <select name="bonuses[{{$i}}][stage]" class="form-control" id="stage_list">
                                  <option value="Private sale" @if($pr_bonus->stage == "Private sale") selected @endif>Private sale</option>
                                  <option value="Pre-sale" @if($pr_bonus->stage == "Pre-sale") selected @endif>Pre-sale</option>
                                  <option value="Sale" @if($pr_bonus->stage == "Sale") selected @endif>Sale</option>
                                  <option value="Other" @if($pr_bonus->stage == "Other") selected @endif>Other</option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-8">
                          <div class="form-group">
                            <h5>Описание бонуса</h5>
                            <div class="controls">
                              <input type="text" name="bonuses[{{$i}}][bonus]" class="form-control" value="{{$pr_bonus->bonus}}">
                            </div>
                          </div>
                        </div>
                      </div>
                    @empty
                      <div class="bonuses-item row" data-index="0">
                        <div class="col-lg-4">
                          <div class="form-group">
                            <h5>Этап</h5>
                            <div class="controls">
                              <select name="bonuses[0][stage]" class="form-control" id="stage_list">
                                <option value="Private sale">Private sale</option>
                                <option value="Pre-sale" >Pre-sale</option>
                                <option value="Sale">Sale</option>
                                <option value="Other">Other</option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-8">
                          <div class="form-group">
                            <h5>Описание бонуса</h5>
                            <div class="controls">
                              <input type="text" name="bonuses[0][bonus]" class="form-control" value="">
                            </div>
                          </div>
                        </div>
                      </div>
                    @endforelse

                  </div>
                  <button type="button" class="btn btn-info float-left" id="add_bonus_item"><i class="fa fa-plus"></i></button>
                  <button type="button" class="btn btn-info float-left remove_item" data-remove="bonuses-item" style="margin-left: 5px"><i class="fa fa-minus"></i></button>
                </div>
                <div class="tab-pane pad" id="jurisdiction-container" role="tabpanel">
                    <div class="form-group">
                      <h5>Блоки данных</h5>
                    </div>
                    <div id="block-wrap">
                      @forelse($project_blocks as $i => $wallet_block)
                        <div class="block-item row" data-index="{{$i}}">
                          <div class="col-lg-2">
                            <div class="form-group">
                              <h5>Порядок</h5>
                              <div class="controls">
                                <input type="number" name="blocks[{{$i}}][priority]" class="form-control" value="{{$wallet_block->priority}}">
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-5">
                            <div class="form-group">
                              <h5>Название РУ</h5>
                              <div class="controls">
                                <input type="text" name="blocks[{{$i}}][caption_ru]" class="form-control" value="{{$wallet_block->caption_ru}}">
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-5">
                            <div class="form-group">
                              <h5>Название EN</h5>
                              <div class="controls">
                                <input type="text" name="blocks[{{$i}}][caption_en]" class="form-control" value="{{$wallet_block->caption_en}}">
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-2">
                          </div>
                          <div class="col-lg-5">
                            <div class="form-group">
                              <h5>Текст РУ</h5>
                              <div class="controls">
                                <input type="text" name="blocks[{{$i}}][content_ru]" class="form-control" value="{{$wallet_block->content_ru}}">
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-5">
                            <div class="form-group">
                              <h5>Название EN</h5>
                              <div class="controls">
                                <input type="text" name="blocks[{{$i}}][content_en]" class="form-control" value="{{$wallet_block->content_en}}">
                              </div>
                            </div>
                          </div>
                        </div>
                      @empty
                      <div class="block-item row" data-index="0">
                        <div class="col-lg-2">
                          <div class="form-group">
                            <h5>Порядок</h5>
                            <div class="controls">
                              <input type="number" name="blocks[0][priority]" class="form-control" value="">
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-5">
                          <div class="form-group">
                            <h5>Название РУ</h5>
                            <div class="controls">
                              <input type="text" name="blocks[0][caption_ru]" class="form-control" value="">
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-5">
                          <div class="form-group">
                            <h5>Текст РУ</h5>
                            <div class="controls">
                              <input type="text" name="blocks[0][content_ru]" class="form-control" value="">
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-2">
                        </div>
                        <div class="col-lg-5">
                          <div class="form-group">
                            <h5>Название EN</h5>
                            <div class="controls">
                              <input type="text" name="blocks[0][caption_en]" class="form-control" value="">
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-5">
                          <div class="form-group">
                            <h5>Название EN</h5>
                            <div class="controls">
                              <input type="text" name="blocks[0][content_en]" class="form-control" value="">
                            </div>
                          </div>
                        </div>
                      </div>
                      @endforelse
                    </div>
                    <button type="button" class="btn btn-info float-left" id="add_block_item"><i class="fa fa-plus"></i></button>
                    <button type="button" class="btn btn-info float-left remove_item" data-remove="social-item" style="margin-left: 5px"><i class="fa fa-minus"></i></button>
                </div>
                <div class="tab-pane pad" id="filter-container" role="tabpanel">
                  <div class="form-group">
                    <h5>Пользовательские параметры</h5>
                  </div>
                  <div id="project-filter-values-wrap">
                    @forelse($project->project_filter_value_sets as $i => $project_filter_value_set)
                      <div class="project-filter-value-item row" data-index="{{$i}}">
                        <div class="col-lg-5">
                          <div class="form-group">
                            <h5>Параметр</h5>
                            <div class="controls">
                              <select name="project_filter_value_sets[{{$i}}][project_filter_id]" class="form-control project_filters_select" id="project_filters" data-type="project_filter" onchange="onChangeProjectFilter(this);"  >
                                <option value=""></option>
                                @foreach($project_filters as $project_filter)
                                  <option value="{{$project_filter->id}}" @if($project_filter->id == $project_filter_value_set->project_filter_value->project_filter->id) selected @endif>{{$project_filter->name_ru}} ({{$project_filter->name_en}})</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-5">
                          <div class="form-group">
                            <h5>Значение</h5>
                            <div class="controls">
                              <select name="project_filter_value_sets[{{$i}}][project_filter_value_id]" class="form-control" id="project_filter_values{{$i}}">
                                <option value=""></option>
                                @foreach($project_filter_value_set->project_filter_value->project_filter->values as $value)
                                  <option value="{{$value->id}}" @if($value->id == $project_filter_value_set->project_filter_value->id) selected @endif>{{$value->name_ru}} ({{$value->name_en}})</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-1">
                          <div class="form-group">
                            <h5>Удаление</h5>
                              <button type="button" class="btn btn-danger delete-project-filter-value-set">Удалить параметр</button>
                          </div>
                        </div>
                      </div>
                    @empty
                      <div class="project-filter-value-item row" data-index="0">
                        <div class="col-lg-5">
                          <div class="form-group">
                            <h5>Параметр</h5>
                            <div class="controls">
                              <select name="project_filter_value_sets[0][project_filter_id]" class="form-control" id="project_filters" data-type="project_filter" onchange="onChangeProjectFilter(this);">
                                <option value=""></option>
                                @foreach($project_filters as $project_filter)
                                  <option value="{{$project_filter->id}}" >{{$project_filter->name_ru}} ({{$project_filter->name_en}})</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-5">
                          <div class="form-group">
                            <h5>Значение</h5>
                            <div class="controls">
                              <select name="project_filter_value_sets[0][project_filter_value_id]" class="form-control" id="project_filter_values0">
                                <option value=""></option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-lg-1">
                          <div class="form-group">
                            <h5>Удаление</h5>
                            <div class="controls">
                              <button type="button" class="btn btn-danger delete-project-filter-value-set">Удалить параметр</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    @endforelse
                  </div>
                  <button type="button" class="btn btn-info float-left" id="add_project_filter_value_item" data-type="project_filter"><i class="fa fa-plus"></i></button>
                </div>
              <hr>
              <button type="submit" class="btn btn-success">Обновить проект</button>
              <button type="button" class="btn btn-danger" onclick="dropElement()">Удалить</button>
            </div>
          </div>
        </div>
      </div>
    </form>
  </section>
</div>

<form id="delete" action="{{ route('projects.destroy',$project->id) }}" method="POST" style="display: none;">
  <input type="hidden" name="_method" value="delete">
  @csrf
</form>

<script src="/vendor/laravel-filemanager/js/stand-alone-button.js"></script>
<script src="/vendor/ckeditor/ckeditor.js"></script>
<script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
<script src="{{ asset('invi/js/pages/project_create.js') }}?{{ time() }}"></script>


@endsection
