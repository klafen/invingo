@extends('invi.layouts.app')
@section('title','inviadmin - Служебные страницы')
@section('description','inviadmin - Служебные страницы')
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title') Служебные страницы @endslot
    @slot('active') Служебные страницы @endslot
  @endcomponent
  <section class="content">
      <div class="row">
        <div class="col-12 col-lg-6">
          <div class="box box-solid bg-dark">
            <div class="box-header with-border">
              <div class="row">
                <div class="col-lg-12">
                  <h3 class="box-title">Полный список служебных страниц</h3>
                  <h6 class="box-subtitle">Данные можно экспортировать в CSV, Excel, PDF.</h6>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

            <div class="table-responsive">
              <table id="invilinks" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Название</th>
                  <th>URL</th>
                  <th>Приоритет</th>
                  <th>Состояние</th>
                </tr>
              </thead>

              <tbody>
                @forelse($invilinks as $invilink)
                  <tr>
                    <td>{{$invilink->id}}</td>
                    <td><a href="{{route('invilinks.show',$invilink->id)}}">{{$invilink->name_ru}} ({{$invilink->name_en}})</a></td>
                    <td>{{$invilink->url}}</td>
                    <td>{{$invilink->priority}}</td>
                    <td>@if($invilink->is_visible == 1) <span class="label label-success">Видна</span> @else <span class="label label-danger">Скрыта</span> @endif</td>
                  </tr>
                @empty
                  <tr>
                    <td colspan="5">Нет данных</td>
                  </tr>
                @endforelse
              </tbody>
              <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Название</th>
                  <th>URL</th>
                  <th>Приоритет</th>
                  <th>Состояние</th>
                </tr>
              </tfoot>
            </table>
            </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Добавить Служебную страницу</h3>
            </div>
            <div class="box-body">
              <form method="post" action="{{route('invilinks.store')}}">
                {{csrf_field()}}
                  <div class="form-group">
                    <h5>Название РУ<span class="text-danger">*</span></h5>
                    <div class="controls">
                      <input type="text" name="name_ru" class="form-control" required=""  value="">
                    </div>
                  </div>
                  <div class="form-group">
                    <h5>Название EN<span class="text-danger">*</span></h5>
                    <div class="controls">
                      <input type="text" name="name_en" class="form-control" required=""  value="">
                    </div>
                  </div>
                <input type="hidden" name="is_visible" value="0">
                <button type="submit" class="btn btn-success float-left">Добавить служебную страницу</button>
              </form>
            </div>
          </div>
        </div>
    </div>
  </section>
</div>


@if(isset($socials[0]))
  @component('invi.components.table_excel')
    @slot('table_id') socials @endslot
  @endcomponent
@endif


@endsection
