@extends('invi.layouts.app')
@section('title','inviadmin - '.$custom->name_ru)
@section('description','inviadmin - Рейтинг '.$custom->name_ru)
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title') {{$custom->name_ru}} @endslot
    @slot('parent') Рейтинг @endslot
    @slot('parent_link') {{route('custom-rating.index')}} @endslot
    @slot('active') {{$custom->name_ru}} @endslot
  @endcomponent
  <section class="content">
      <div class="row">
        <div class="col-lg-6 col-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Рейтинг</h3>
            </div>
            <div class="box-body">
              <form method="post" action="{{route('custom-rating.update', $custom)}}">
                <input type="hidden" name="_method" value="put">
                <input type="hidden" name="id" value="{{$custom->id}}">
                {{csrf_field()}}
                <div class="form-group">
                  <h5>Название РУ<span class="text-danger">*</span></h5>
                  <div class="controls">
                    <input type="text" name="name_ru" class="form-control" required=""  value="{{$custom->name_ru}}">
                  </div>
                </div>
                <div class="form-group">
                  <h5>Название EN<span class="text-danger">*</span></h5>
                  <div class="controls">
                    <input type="text" name="name_en" class="form-control" required=""  value="{{$custom->name_en}}">
                  </div>
                </div>
                <button type="submit" class="btn btn-success">Сохранить</button>
                <button type="button" class="btn btn-danger" onclick="event.preventDefault();document.getElementById('delete').submit();">Удалить</button>
              </form>
            </div>
          </div>
        </div>
    </div>
  </section>
</div>



<form id="delete" action="{{ route('custom-rating.destroy',$custom->id) }}" method="POST" style="display: none;">
  <input type="hidden" name="_method" value="delete">
  @csrf
</form>

@endsection
