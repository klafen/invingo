@extends('invi.layouts.app')
@section('title','inviadmin - '.$substatus->name_ru)
@section('description','inviadmin - Подстатус '.$substatus->name_ru)
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title') {{$substatus->name_ru}} @endslot
    @slot('parent') Подстатусы @endslot
    @slot('parent_link') {{route('substatus.index')}} @endslot
    @slot('active') {{$substatus->name_ru}} @endslot
  @endcomponent
  <section class="content">
      <div class="row">
        <div class="col-lg-6 col-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Подстатус</h3>
            </div>
            <div class="box-body">
              <form method="post" action="{{route('substatus.update', $substatus)}}">
                <input type="hidden" name="_method" value="put">
                <input type="hidden" name="id" value="{{$substatus->id}}">
                {{csrf_field()}}
                <div class="form-group">
                  <h5>Название РУ<span class="text-danger">*</span></h5>
                  <div class="controls">
                    <input type="text" name="name_ru" class="form-control" required=""  value="{{$substatus->name_ru}}">
                  </div>
                </div>
                <div class="form-group">
                  <h5>Название EN<span class="text-danger">*</span></h5>
                  <div class="controls">
                    <input type="text" name="name_en" class="form-control" required=""  value="{{$substatus->name_en}}">
                  </div>
                </div>
                <button type="submit" class="btn btn-success">Сохранить</button>
                <button type="button" class="btn btn-danger" onclick="event.preventDefault();document.getElementById('delete').submit();">Удалить</button>
              </form>
            </div>
          </div>
        </div>
    </div>
  </section>
</div>



<form id="delete" action="{{ route('substatus.destroy',$substatus->id) }}" method="POST" style="display: none;">
  <input type="hidden" name="_method" value="delete">
  @csrf
</form>

@endsection
