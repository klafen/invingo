@extends('invi.layouts.app')
@section('title','inviadmin - Рейтинги')
@section('description','inviadmin - Рейтинги')
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title') Рейтинги @endslot
    @slot('active') Рейтинги @endslot
  @endcomponent
  <section class="content">
      <div class="row">
        <div class="col-12 col-lg-6">
          <div class="box box-solid bg-dark">
            <div class="box-header with-border">
              <div class="row">
                <div class="col-lg-12">
                  <h3 class="box-title">Полный список рейтингов</h3>
                  <h6 class="box-subtitle">Данные можно экспортировать в CSV, Excel, PDF.</h6>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

            <div class="table-responsive">
              <table id="custom-rating" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Название РУ</th>
                  <th>Название EN</th>
                </tr>
              </thead>
              <tbody>
                @forelse($customs as $custom)
                  <tr>
                    <td>{{$custom->id}}</td>
                    <td><a href="{{route('custom-rating.show',$custom->id)}}">{{$custom->name_ru}}</a></td>
                    <td><a href="{{route('custom-rating.show',$custom->id)}}">{{$custom->name_en}}</a></td>
                  </tr>
                @empty
                  <tr>
                    <td colspan="3">Нет данных</td>
                  </tr>
                @endforelse
              </tbody>
              <tfoot>
                <tr>
                  <th>ID</th>
                  <th>Название РУ</th>
                  <th>Название EN</th>
                </tr>
              </tfoot>
            </table>
            </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Добавить рейтинг</h3>
            </div>
            <div class="box-body">
              <form method="post" action="{{route('custom-rating.store')}}">
                {{csrf_field()}}
                <div class="form-group">
                  <h5>Название РУ<span class="text-danger">*</span></h5>
                  <div class="controls">
                    <input type="text" name="name_ru" class="form-control" required=""  value="">
                  </div>
                </div>
                <div class="form-group">
                  <h5>Название EN<span class="text-danger">*</span></h5>
                  <div class="controls">
                    <input type="text" name="name_en" class="form-control" required=""  value="">
                  </div>
                </div>
                <button type="submit" class="btn btn-success float-left">Добавить рейтинг</button>
              </form>
            </div>
          </div>
        </div>
    </div>
  </section>
</div>


@if(isset($customs[0]))
  @component('invi.components.table_excel')
    @slot('table_id') custom-rating @endslot
  @endcomponent
@endif

@endsection
