@extends('invi.layouts.app')
@section('title','inviadmin - Настройки сайта ')
@section('description','inviadmin - Настройки сайта ')
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title')Настройки сайта  @endslot
    @slot('parent')Настройки сайта @endslot
    @slot('parent_link') {{route('invi_dashboard')}} @endslot
    @slot('active') Настройки сайта @endslot
  @endcomponent
  <section class="content">
    <form method="post" action="{{route('settings.update',$inviset)}}">
      <input type="hidden" name="_method" value="put">
      <input type="hidden" name="id" value="{{$inviset->id}}">
      {{csrf_field()}}
      <div class="row">
        <div class="col-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Настройки сайта</h3>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-6">
                  <div class="form-group">
                    <h5>Email для оповещении о заполнени контактной формы (можно несколько, через запятую)</h5>
                    <div class="controls">
                      <input type="text" name="email_form" class="form-control" required=""  value="{{ $inviset->email_form }}">
                    </div>
                  </div>
                  <div class="form-group">
                    <h5>Email для отображения в футере (только один)</h5>
                    <div class="controls">
                      <input type="email" name="email_contact" class="form-control" required=""  value="{{ $inviset->email_contact }}">
                    </div>
                  </div>
                  <div class="form-group">
                    <h5>Количество проектов на странице</h5>
                    <div class="controls">
                      <input type="number" name="projects_by_page" class="form-control" required=""  value="{{ $inviset->projects_by_page }}">
                    </div>
                  </div>
                  <div class="form-group">
                    <h5>Количество кошельков на странице</h5>
                    <div class="controls">
                      <input type="number" name="wallets_by_page" class="form-control" required=""  value="{{ $inviset->wallets_by_page }}">
                    </div>
                  </div>
                  <div class="form-group">
                    <h5>Назание сайта (отображается в title страницы)</h5>
                    <div class="controls">
                      <input type="text" name="namecaption_site" class="form-control" required=""  value="{{ $inviset->namecaption_site }}">
                    </div>
                  </div>
                  <div class="form-group">
                    <h5>Максимальное количество проектов в слайдере (если не указано, то 5)</h5>
                    <div class="controls">
                      <input type="text" name="slider_max_projects" class="form-control"  value="{{ $inviset->slider_max_projects }}">
                    </div>
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-success">Сохранить настройки</button>
                  </div>
                </div>

                </div>
              </div>
            </div>
          </div>
        </div>
    </form>
  </section>
</div>



@endsection
