<script src="{{ asset('invi/js/dataTables.buttons.min.js') }}?{{ time() }}"></script>
<script src="{{ asset('invi/js/buttons.flash.min.js') }}?{{ time() }}"></script>
<script src="{{ asset('invi/js/jszip.min.js') }}?{{ time() }}"></script>
<script src="{{ asset('invi/js/pdfmake.min.js') }}?{{ time() }}"></script>
<script src="{{ asset('invi/js/vfs_fonts.js') }}?{{ time() }}"></script>
<script src="{{ asset('invi/js/buttons.html5.min.js') }}?{{ time() }}"></script>
<script src="{{ asset('invi/js/buttons.print.min.js') }}?{{ time() }}"></script>

<script>
$('#{{$table_id}}').DataTable( {
  dom: 'Bfrtip',
  buttons: [
   'csv', 'excel', 'pdf'
 ],
 @if(isset($sort)) {{$sort}} @endif
 pageLength: 100
} );
@if(isset($alter_table_id))
$('#{{$alter_table_id}}').DataTable( {
  dom: 'Bfrtip',
  buttons: [
   'csv', 'excel', 'pdf'
  ],
  pageLength: 100
} );
@endif
</script>
