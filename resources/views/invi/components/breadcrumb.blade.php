<section class="content-header">
  <h1>
    {{$title}}
  </h1>
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('invi_dashboard')}}"><i class="fa fa-dashboard"></i>inviadmin</a></li>
    @isset($parent)
    <li class="breadcrumb-item"><a href="{{$parent_link}}">{{$parent}}</a></li>
    @endisset
    <li class="breadcrumb-item active">{{$active}}</li>
  </ol>
</section>
