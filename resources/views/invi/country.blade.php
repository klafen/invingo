@extends('invi.layouts.app')
@section('title','inviadmin - '.$country->name_ru)
@section('description','inviadmin - страна '.$country->name_ru)
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title') {{$country->name_ru}} @endslot
    @slot('parent') Страны @endslot
    @slot('parent_link') {{route('countries.index')}} @endslot
    @slot('active') {{$country->name_ru}} @endslot
  @endcomponent
  <section class="content">
      <div class="row">
        <div class="col-lg-6 col-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Страна</h3>
            </div>
            <div class="box-body">
              <form method="post" action="{{route('countries.update', $country)}}">
                <input type="hidden" name="_method" value="put">
                <input type="hidden" name="id" value="{{$country->id}}">
                {{csrf_field()}}
                <div class="form-group">
                  <h5>Название РУ<span class="text-danger">*</span></h5>
                  <div class="controls">
                    <input type="text" name="name_ru" class="form-control" required=""  value="{{$country->name_ru}}">
                  </div>
                </div>
                <div class="form-group">
                  <h5>Название EN<span class="text-danger">*</span></h5>
                  <div class="controls">
                    <input type="text" name="name_en" class="form-control" required=""  value="{{$country->name_en}}">
                  </div>
                </div>
                <button type="submit" class="btn btn-success">Сохранить</button>
                <button type="button" class="btn btn-danger" onclick="event.preventDefault();document.getElementById('delete').submit();">Удалить</button>
              </form>
            </div>
          </div>
        </div>
    </div>
  </section>
</div>



<form id="delete" action="{{ route('countries.destroy',$country->id) }}" method="POST" style="display: none;">
  <input type="hidden" name="_method" value="delete">
  @csrf
</form>

@endsection
