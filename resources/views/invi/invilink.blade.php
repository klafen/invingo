@extends('invi.layouts.app')
@section('title','inviadmin - Служебная страница - '.$invilink->name_ru)
@section('description','inviadmin - Служебная страница - '.$invilink->name_ru)
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title') Служебная страница - {{$invilink->name_ru}} @endslot
    @slot('parent') Служебная страница @endslot
    @slot('parent_link') {{route('advert.index')}} @endslot
    @slot('active') Служебная страница - {{$invilink->name_ru}} @endslot
  @endcomponent
  <section class="content">
    <form method="post" action="{{route('invilinks.update',$invilink)}}">
      <input type="hidden" name="_method" value="put">
      <input type="hidden" name="id" value="{{$invilink->id}}">
      {{csrf_field()}}
      <div class="row">
        <div class="col-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Данные Служебной страницы</h3>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-6">
                  <div class="form-group">
                    <h5>Название РУ</h5>
                    <div class="controls">
                      <input type="text" name="name_ru" class="form-control" required=""  value="{{ $invilink->name_ru }}">
                    </div>
                  </div>
                  <div class="form-group">
                    <h5>Основной текст РУ</h5>
                    <div class="controls">
                      <textarea name="text_ru" class="form-control" rows="6" data-editor="true">{!! $invilink->text_ru??'' !!}</textarea>
                    </div>
                  </div>

                </div>
                <div class="col-6">
                  <div class="form-group">
                    <h5>Название EN</h5>
                    <div class="controls">
                      <input type="text" name="name_en" class="form-control" required=""  value="{{ $invilink->name_en }}">
                    </div>
                  </div>
                  <div class="form-group">
                    <h5>Основной текст EN</h5>
                    <div class="controls">
                      <textarea name="text_en" class="form-control" rows="6" data-editor="true">{!! $invilink->text_en??'' !!}</textarea>
                    </div>
                  </div>

                </div>
                </div>
              <div class="row">
                <div class="col-6">
                    <div class="form-group">
                      <h5>URL</h5>
                      <div class="controls">
                        <input type="text" name="url" class="form-control" required="" value="{{$invilink->url}}">
                      </div>
                    </div>
                  <div class="form-group">
                    <h5>Приоритет</h5>
                    <div class="controls">
                      <input type="number" name="priority" class="form-control" required="" value="{{$invilink->priority}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <h5>Видимость</h5>
                    <div class="controls">
                      <input type="radio" id="is_visible1" name="is_visible" value="1" @if($invilink->is_visible == 1) checked @endif>
                      <label for="is_visible1">Да</label>
                      <input type="radio" id="is_visible0" name="is_visible" value="0" @if($invilink->is_visible == 0) checked @endif>
                      <label for="is_visible0">Нет</label>
                    </div>
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-success">Обновить контетный блок</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </section>
</div>


<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
<link rel="stylesheet" href="{{ asset('invi/css/bootstrap3-wysihtml5.min.css') }}?{{ time() }}">
<script src="{{ asset('invi/js/bootstrap3-wysihtml5.all.js') }}?{{ time() }}"></script>
<script src="{{ asset('invi/js/pages/project_create.js') }}?{{ time() }}"></script>


@endsection
