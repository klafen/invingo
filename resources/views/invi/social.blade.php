@extends('invi.layouts.app')
@section('title','inviadmin - '.$social->name)
@section('description','inviadmin - Соц сеть '.$social->name)
@section('content')
<div class="content-wrapper">
  @component('invi.components.breadcrumb')
    @slot('title') {{$social->name}} @endslot
    @slot('parent') Соц сети @endslot
    @slot('parent_link') {{route('socials.index')}} @endslot
    @slot('active') {{$social->name}} @endslot
  @endcomponent
  <section class="content">
      <div class="row">
        <div class="col-lg-6 col-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Соц сеть</h3>
            </div>
            <div class="box-body">
              <form method="post" action="{{route('socials.update', $social)}}">
                <input type="hidden" name="_method" value="put">
                <input type="hidden" name="id" value="{{$social->id}}">
                {{csrf_field()}}
                <div class="form-group">
                  <h5>Название <span class="text-danger">*</span></h5>
                  <div class="controls">
                    <input type="text" name="name" class="form-control" required=""  value="{{$social->name}}">
                  </div>
                </div>
                <div class="form-group">
                  <h5>Иконка <span class="text-danger">*</span></h5>
                  <div class="controls">
                    <button type="button" class="form-control-button" id="select-icon">Выбрать</button>
                    <input type="text" name="icon" class="form-control" required=""  value="{{$social->icon}}">
                  </div>
                </div>
                <button type="submit" class="btn btn-success">Сохранить</button>
                <button type="button" class="btn btn-danger" onclick="event.preventDefault();document.getElementById('delete').submit();">Удалить</button>
              </form>
            </div>
          </div>
        </div>
    </div>
  </section>
</div>



<form id="delete" action="{{ route('socials.destroy',$social->id) }}" method="POST" style="display: none;">
  <input type="hidden" name="_method" value="delete">
  @csrf
</form>


<div class="popup select-icon">
  <div class="popup-wrap">
    <div class="box">
  			<div class="social-icon-list row">
          <div class="social-icon-item"><i class="fa fa-facebook-square" aria-hidden="true"></i></div>
          <div class="social-icon-item"><i class="fa fa-facebook" aria-hidden="true"></i></div>
          <div class="social-icon-item"><i class="fa fa-facebook-official" aria-hidden="true"></i></div>
          <div class="social-icon-item"><i class="fa fa-github" aria-hidden="true"></i></div>
          <div class="social-icon-item"><i class="fa fa-google" aria-hidden="true"></i></div>
          <div class="social-icon-item"><i class="fa fa-google-plus-official" aria-hidden="true"></i></div>
          <div class="social-icon-item"><i class="fa fa-google-plus" aria-hidden="true"></i></div>
          <div class="social-icon-item"><i class="fa fa-instagram" aria-hidden="true"></i></div>
          <div class="social-icon-item"><i class="fa fa-linkedin" aria-hidden="true"></i></div>
          <div class="social-icon-item"><i class="fa fa-linkedin-square" aria-hidden="true"></i></div>
          <div class="social-icon-item"><i class="fa fa-odnoklassniki-square" aria-hidden="true"></i></div>
          <div class="social-icon-item"><i class="fa fa-odnoklassniki" aria-hidden="true"></i></div>
          <div class="social-icon-item"><i class="fa fa-reddit-square" aria-hidden="true"></i></div>
          <div class="social-icon-item"><i class="fa fa-reddit" aria-hidden="true"></i></div>
          <div class="social-icon-item"><i class="fa fa-reddit-alien" aria-hidden="true"></i></div>
          <div class="social-icon-item"><i class="fa fa-skype" aria-hidden="true"></i></div>
          <div class="social-icon-item"><i class="fa fa-steam-square" aria-hidden="true"></i></div>
          <div class="social-icon-item"><i class="fa fa-steam" aria-hidden="true"></i></div>
          <div class="social-icon-item"><i class="fa fa-telegram" aria-hidden="true"></i></div>
          <div class="social-icon-item"><i class="fa fa-twitch" aria-hidden="true"></i></div>
          <div class="social-icon-item"><i class="fa fa-twitter" aria-hidden="true"></i></div>
          <div class="social-icon-item"><i class="fa fa-twitter-square" aria-hidden="true"></i></div>
          <div class="social-icon-item"><i class="fa fa-vimeo" aria-hidden="true"></i></div>
          <div class="social-icon-item"><i class="fa fa-weixin" aria-hidden="true"></i></div>
          <div class="social-icon-item"><i class="fa fa-vk" aria-hidden="true"></i></div>
          <div class="social-icon-item"><i class="fa fa-weibo" aria-hidden="true"></i></div>
          <div class="social-icon-item"><i class="fa fa-whatsapp" aria-hidden="true"></i></div>
          <div class="social-icon-item"><i class="fa fa-youtube-square" aria-hidden="true"></i></div>
          <div class="social-icon-item"><i class="fa fa-youtube-play" aria-hidden="true"></i></div>
          <div class="social-icon-item"><i class="fa fa-youtube" aria-hidden="true"></i></div>
        </div>
  	</div>
  </div>
</div>
<script src="{{ asset('invi/js/pages/socials.js') }}"></script>
@endsection
