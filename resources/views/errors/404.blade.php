@extends('layouts.app')
@section('title',__('404.page_not_found')." - ")
@section('content')


    <div class="section page">
        <div class="container">
            <h1 class="page__title">{{__('404.h1')}}</h1>
            <div class="row">
                <div class="col-12 col-md-12">
                    {!! __('404.content') !!}
                </div>
                <div class="col">

                </div>
            </div>
        </div>
    </div>



@endsection
