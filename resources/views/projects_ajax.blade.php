
<div class="row">
  <!-- Items -->
  @forelse($projects as $project)
    <div class="@if(isset($class_item)){{$class_item}}@else col-12 col-sm-6 col-lg-4 p-2 @endif">
      <a href="{{route('ico-project',$project['project']->id)}}">
        <div class="project__content">
          <div class="content__top">
            <div class="project__logo">
              <img src="{{$project['project']->logo}}" alt="img">
            </div>
            <div>
              <span class="project__name">{{$project['project']->name}}</span>
              <span class="project__category">{{$project['project']->category}}</span>
            </div>
            @if($project['project']->is_adv == 1)
              <div>
                      <span class="sponsor" data-toggle="tooltip" data-placement="bottom" title="{{__('projects.sponsored')}}" >
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                          <use xlink:href="{{asset('assets/img/sprite.svg#hand-shake')}}"></use>
                        </svg>
                      </span>
              </div>
            @endif
          </div>
          <div class="content__middle">
            <div>
              <span class="project__price">@if ($project['project']->collected){{$project['project']->prices->collected_currency_string()}} {{number_format($project['project']->collected,0,',',' ')}}@else {{__('project.no_info')}} @endif</span>
              <span class="project__goal">@if ($project['project']->hard_cap){{$project['project']->prices->hard_cap_currency_string()}} {{number_format($project['project']->hard_cap, 0,',',' ')}} @if($project['project']->hard_cap!=0 && $project['project']->collected && $project['project']->prices->hard_cap_currency_string()===$project['project']->prices->collected_currency_string()) / {{ number_format($project['project']->collected*100 / $project['project']->hard_cap,2,',',' ') }}% @endif @else {{__('project.no_info')}} @endif</span>
            </div>
            <div>
              <span class="project__count">{{$project['project']->rating}}</span>
            </div>
          </div>
          <div class="content__bottom">
            <div>
              @if ($project['current_lifetime']['status'] !='ended')
                <span class="project__date">{{$project['current_lifetime']['name']}}:
                @if ($project['current_lifetime']['date_string'] !='')
                   {{date('d M', strtotime($project['current_lifetime']['project_lifetime']->start))}} – {{date('d M', strtotime($project['current_lifetime']['project_lifetime']->end))}}
                @else
                   {{__('projects.TBA')}}
                @endif
                </span>
              @endif
              <span class="project__state">{{__('projects.'.$project['current_lifetime']['status'])}}</span>
            </div>
          </div>
        </div>
      </a>
    </div>
  @empty
    NO DATA
  @endforelse
</div>