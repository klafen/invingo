@extends('layouts.app')
@section('title',__('search.search')." - ")
@section('content')


<div class="section search__page">
  <div class="container">
    <h1 class="page__title">@if($s==''){{__('search.empty_search')}} @else{{__('search.search_result_for')}} <span> «{{$s}}»</span>@endif</h1>
    <div class="row">
      <div class="col-12">
        <div class="search__category">
          <ul class="d-flex nav" role="tablist">
            <li>
              <a class="active show" href="#projects" data-toggle="tab" role="tab" aria-controls="projects" id="projects-tab" aria-selected="true">{{__('search.ico_projects')}}</a>
            </li>
            <li>
              <a href="#wallets" data-toggle="tab" role="tab" aria-controls="wallets" id="wallets-tab" aria-selected="false" class="">{{__('search.wallets')}}</a>
            </li>
          </ul>
        </div>
        <div class="tab-content">
          <div class="detailed_about-content tab-pane fade active show" id="projects" role="tabpanel" aria-labelledby="projects-tab">
            <div class="projects__table">
              @php $class_item='col-12 col-sm-6 col-md-4 col-lg-3 p-2' @endphp
              @include('projects_ajax')
            </div>
          </div>
          <div class="detailed_about-content about_content-team tab-pane fade" id="wallets" role="tabpanel" aria-labelledby="wallets-tab">
            <div class="projects__table">
              @php $class_item='col-12 col-sm-6 col-md-4 col-lg-3 p-2' @endphp
              @include('wallets_ajax')
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



@endsection
