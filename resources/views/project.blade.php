@extends('layouts.app')
@section('title',__('project.project').' '.$project->name." - ")
@section('content')
  <style> p iframe {max-width: 100%;height: auto;min-height:440px;} </style>
<div class="section detailed">
  <div class="container--bg">
    <div class="container">
      <div class="row pt-5">
        <div class="col-12 col-md-8">
          <div class="detailed__head-left">
            <div class="detailed__logo-wrap">
              <img src="{{$project->logo}}" alt="logo" class="figure-img" style="max-width: 80px">
            </div>
            <div class="detailed__title">
              <h1>{{$project->name}}</h1>
              <span>{{$project->category}}</span>
            </div>
            @if($project->is_adv == 1)
                <div class="detailed__title">
                  <span class="sponsor" style="width:30px;height:30px;" data-toggle="tooltip" data-placement="bottom" title="{{__('project.sponsored')}}">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                          <use xlink:href="{{asset('assets/img/sprite.svg#hand-shake')}}"></use>
                        </svg>
                      </span>
                </div>
            @endif
          </div>

        </div>
        <div class="col">
          <div class="detailed__head-right">
            <div class="detailed__price">@if(isset($project->collected)){{$project->prices->collected_currency_string()}} {{number_format($project->collected, 0, ',',' ')}}  @else {{__('project.no_info')}} @endif</div>
            <div class="detailed__goal">@if(isset($project->hard_cap)){{$project->prices->hard_cap_currency_string()}} {{number_format($project->hard_cap, 0, ',',' ')}} @if($project->hard_cap!=0 && $project->collected && $project->prices->hard_cap_currency_string()===$project->prices->collected_currency_string()) / {{ number_format($project->collected*100 / $project->hard_cap, 2, ',',' ' ) }}% @endif @else {{__('project.no_info')}} @endif</div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12 col-md-8">
          <div class="detailed_content">
            <div class="row row_rating">
              <div class="col">
                <div class="row_rating-title">
                  {{__('project.rating')}}
                  <a href="{{route('invilink','ICO%20Metodology')}}">
                    <span class="help_info" tabindex="0" role="button" data-toggle="popover" data-content="Lorem ipsum dolor sit amet. Suscipit laboriosam, nisi ut enim.">
                      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href={{asset('assets/img/sprite.svg#help-info')}}></use></svg>
                    </span>
                  </a>
                </div>
                <div class="rating_content-number">
                  <ul class="d-flex justify-content-between align-items-center">
                    @forelse($project_custom_rating as $cutsom_rating)
                    <li><span>{{$cutsom_rating->rating}}</span><i>{{$cutsom_rating->name}}</i></li>
                    @empty
                    @endforelse
                    <li class="number_total">{{$project->rating}}<span>/100</span>
                      <span class="total">{{__('project.total')}}</span>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="col-6 col-sm-3">
                <div class="row_rating-title">
                  {{__('project.roi_level')}}
                </div>
                <div class="rating_content-score">
                  {{$project->roi}}
                </div>
              </div>
            </div>

            {!! $project->info !!}

            <div class="detailed_link">
              @if (count($project_links)>0)
              <div class="detailed_link-links">
                <div class="link_title">{{__('project.links')}}</div>
                <div class="link_links">
                  @forelse($project_links as $project_link)
                  <a href="{{$project_link->link}}" target="_blank">{{$project_link->name}}</a>
                  @empty
                  @endforelse
                </div>
              </div>
              @endif
              @if(count($project_socials)>0)
              <div class="detailed_link-social">
                <div class="link_title">{{__('project.social_media')}}</div>
                <div class="link_social">
                  <ul>
                    @forelse($project_socials as $project_social)
                    <li><a href="{{$project_social->link}}" target="_blank">{!! $project_social->icon !!}</a></li>
                    @empty
                    @endforelse
                  </ul>
                </div>
              </div>
              @endif
            </div>
          </div>
        </div>
        <div class="col">
          <div class="detailed_sidebar">
            <div class="sidebar_time-header">
              <div>{{$current_lifetime['name']}} {{$current_lifetime['project_lifetime']->substatus}}</div>
              <div>
                <span>{{__('project.'.$current_lifetime['status'])}}</span>
              </div>
            </div>
            <div class="sidebar_timer">
              <div id="countdown">
                @if($current_lifetime['project_lifetime']->is_tba and $current_lifetime['date_string']=='' )
                 <p class="text-center">{{__('project.TBA')}}</p>
                @endif
              </div>
            </div>
            @if($next_lifetime)
            <div class="d-table">
              <div>{{__('project.next_stage')}}</div>
              <div>{{$next_lifetime->name}} {{$next_lifetime->substatus}}@if ($next_lifetime->start): {{$next_lifetime->start}} @else: {{__('project.TBA')}}@endif</div>
            </div>
            @endif
            <hr>
            <div class="d-table">
              <div>{{__('project.token')}}</div>
              <div>{{$project->token}}</div>
            </div>
            <div class="d-table">
              <div>{{__('project.platform')}}</div>
              <div>{{$project->platform}}</div>
            </div>
            <div class="d-table">
              <div>{{__('project.accepting')}}</div>
              <div>
                {{$project->currency}}
              </div>
            </div>
            <hr>
            <div class="d-table">
              <div>{{__('project.price')}}</div>
              <div></div>
            </div>
            <div class="d-table">
              <div class="pr-3">
                <select class="custom-select" id="price-select">
                  @foreach($project_lifetimes as $project_lifetime)
                    <option value="@if($project_lifetime->price){{rtrim(rtrim(number_format($project_lifetime->price,10,',',' '),0),',')}} {{$project_lifetime->price_currency_string()}}@else {{__('project.no_info')}} @endif" @if($project_lifetime->id==$current_lifetime['project_lifetime']->id) selected="selected" @endif>{{$project_lifetime['name']}} {{$project_lifetime['substatus']}}</option>
                  @endforeach
                </select>
              </div>
              <div id="price-value"></div>
            </div>
            <hr>
            <div class="d-table triple">
              <div>{{__('project.min_invest')}}</div>
              <div>{{__('project.soft_cap')}}</div>
             <div>{{__('project.hard_cap')}}</div>
            </div>
            <div class="d-table triple-white">
              <div>@if(isset($current_lifetime['project_lifetime']->min_invest)){{$current_lifetime['project_lifetime']->min_invest_string()}} {{$current_lifetime['project_lifetime']->min_invest_currency_string()}} @else {{__('project.no_info')}} @endif</div>
              <div>@if(isset($project->soft_cap)){{number_format($project->soft_cap, 0,',',' ')}} {{$project->prices->soft_cap_currency_string()}} @else {{__('project.no_info')}} @endif</div>
              <div>@if(isset($project->hard_cap)){{number_format($project->hard_cap, 0,',',' ')}} {{$project->prices->hard_cap_currency_string()}} @else {{__('project.no_info')}} @endif</div>
            </div>
            <hr>
            <div class="d-table">
              <div>{{__('project.mvp')}}</div>
              <div>@if ($project->is_mvp == 1){{__('project.yes')}}  @else  {{__('project.no')}}@endif</div>
            </div>
            <div class="d-table">
              <div>{{__('project.escrow')}}</div>
              <div>@if ($project->is_escrow == 1){{__('project.yes')}}  @else  {{__('project.no')}}@endif</div>
            </div>
            <div class="d-table">
              <div>{{__('project.smart_contract')}}</div>
              <div>@if ($project->is_smartcontract == 1){{__('project.verify')}}  @else  {{__('project.unverify')}}@endif</div>
            </div>
            <div class="d-table">
              <div>{{__('project.kyc')}}</div>
              <div>@if ($project->is_kyc == 1) {{__('project.yes')}}  @else  {{__('project.no')}}@endif</div>
            </div>
            <div class="d-table">
              <div>{{__('project.kyt')}}</div>
              <div>@if ($project->is_kyt == 1) {{__('project.yes')}}  @else  {{__('project.no')}}@endif</div>
            </div>
            <div class="d-table">
              <div>{{__('project.whitelist')}}</div>
              <div>@if ($project->is_whitelist == 1) {{__('project.yes')}} @else  {{__('project.no')}}@endif</div>
            </div>
            @if($project->restricted)
            <div class="d-table">
              <div>{{__('project.restricted_area')}}</div>
              <div>{{$project->restricted}}</div>
            </div>
            @endif
            <div class="d-table">
              <div>{{__('project.bonus')}}</div>
              <div>@if ($project->is_bonus == 1){{__('project.yes')}}  @else  {{__('project.no')}}@endif</div>
            </div>
            <div class="d-table">
              <div>{{__('project.bounty')}}</div>
              <div>@if ($project->is_bounty == 1){{__('project.yes')}}  @else  {{__('project.no')}}@endif</div>
            </div>
            <div class="d-table">
              <div>{{__('project.air_drop')}}</div>
              <div>@if ($project->is_airdrop == 1){{__('project.yes')}}  @else  {{__('project.no')}}@endif</div>
            </div>
            <div class="d-table">
              <div>{{__('project.howey_test')}}</div>
              <div>@if ($project->is_howey_test == 1){{__('project.yes')}}  @else  {{__('project.no')}}@endif</div>
            </div>
            <div class="d-table">
              <div>{{__('project.etherscancheck')}}</div>
              <div>@if ($project->is_etherscancheck == 1){{__('project.yes')}}  @else  {{__('project.no')}}@endif</div>
            </div>
            @if($project->trading)
            <div class="d-table">
              <div>{{__('project.trading_on')}}</div>
              <div>{{$project->trading}}</div>
            </div>
            @endif
            <div class="d-table">
              <div>{{__('project.jurisdiction')}}</div>
              <div>@if ($project->jurisdiction == 1){{__('project.verify')}}  @else  {{__('project.unverify')}}@endif</div>
            </div>
            @if($project->extra)
            <div class="d-table">
              <div>{{__('project.extra')}}</div>
              <div>{{$project->extra}}</div>
            </div>
            @endif
            <div class="d-table">
              <div>{{__('project.country')}}</div>
              <div>{{$project->country}}</div>
            </div>
            @foreach ($project->project_filter_value_sets as $value_set)
              @if ($value_set->project_filter_value->project_filter->is_visible)
                <div class="d-table">
                  <div>{{$value_set->project_filter_value->project_filter->name()}}</div>
                  <div>{{$value_set->project_filter_value->name()}}</div>
                </div>
              @endif
            @endforeach
          </div>
        </div>
      </div>
      <div class="row pt-5">
        <div class="col-12 col-lg-8">
          <div class="detailed_about">
            <div class="detailed_about-title">
              <ul class="d-flex justify-content-between nav" role="tablist">
                <li>
                  <a class="active" href="#about" data-toggle="tab" role="tab" aria-controls="about" id="about-tab" aria-selected="true">{{__('project.about_us')}}</a>
                </li>
                <li>
                  <a href="#team" data-toggle="tab" role="tab" aria-controls="team" id="team-tab" aria-selected="false">{{__('project.team')}}</a>
                </li>
                <li>
                  <a href="#map" data-toggle="tab" role="tab" aria-controls="map" id="map-tab" aria-selected="false">{{__('project.roadmap')}}</a>
                </li>
                <li>
                  <a href="#token" data-toggle="tab" role="tab" aria-controls="token" id="token-tab" aria-selected="false">{{__('project.token_economic')}}</a>
                </li>
                <li>
                  <a href="#other" data-toggle="tab" role="tab" aria-controls="other" id="other-tab" aria-selected="false">{{__('project.other')}}</a>
                </li>
              </ul>
            </div>
            <div class="tab-content">
              <div class="detailed_about-content tab-pane fade show active" id="about" role="tabpanel" aria-labelledby="about-tab">
                {!! $project->about_us !!}
              </div>
              <div class="detailed_about-content about_content-team tab-pane fade " id="team" role="tabpanel" aria-labelledby="team-tab">
                @include('teamlist',['teams'=>$project->teams])
              </div>
              <div class="detailed_about-content about_content-maps tab-pane fade " id="map" role="tabpanel" aria-labelledby="map-tab">
                <div id="maps">@if($project->road_map)<img src="{{$project->road_map}}" alt="{{__('project.road_map')}} {{$project->name}}">@endif</div>
              </div>
              <div class="detailed_about-content about_content-token tab-pane fade " id="token" role="tabpanel" aria-labelledby="token-tab">
                <div class="row">
                  <div class="col-12 col-sm-6">
                    <div class="token-title">{{__('project.token_info')}}</div>
                    <div class="token_content">
                      <div>{{__('project.token')}}</div>
                      <div>{{$project->token}}</div>
                    </div>
                    <div class="token_content">
                      <div>{{__('project.platform')}}</div>
                      <div>{{$project->platform}}</div>
                    </div>
                    <div class="token_content">
                      <div>{{__('project.type')}}</div>
                      <div>{{$project->type_token}}</div>
                    </div>
                    <div class="token_content mt-3">
                    </div>
                    @foreach($project_lifetimes as $project_lifetime)
                      <div class="token_content">
                        <div>{{$project_lifetime['name']}} {{$project_lifetime['substatus']}}</div>
                        <div>1 {{$project->token}} = @if($project_lifetime->price){{rtrim(rtrim(number_format($project_lifetime->price,10, ',',' '),0),',')}} {{$project_lifetime->price_currency_string()}}@else{{__('project.no_info')}}@endif</div>
                      </div>
                    @endforeach
                    <div class="token_content">
                      <div>{{__('project.for_sale')}}</div>
                      <div>@if (isset($project->for_sale)){{ number_format($project->for_sale, 0, ',',' ') }} {{$project->prices->for_sale_currency_string()}}@else {{__('project.no_info')}} @endif</div>
                    </div>
                    @if (count($project_bonuses)>0)
                      <div class="token-title">{{__('project.bonuses')}}</div>
                      @foreach($project_bonuses as $project_bonus)
                        <div class="token_content">
                          <div>{{__('project.'.$project_bonus['stage'])}}</div>
                          <div>{{$project_bonus['bonus']}}</div>
                        </div>
                      @endforeach
                    @endif
                  </div>
                  <div class="col-12 col-sm-6">
                    <div class="token-title">{{__('project.investment_info')}}</div>
                    <div class="token_content">
                      <div>{{__('project.min_investment')}}</div>
                      <div>@foreach($project_lifetimes as $project_lifetime)
                          {{$project_lifetime['name']}} {{$project_lifetime['substatus']}}: @if($project_lifetime->min_invest){{rtrim(rtrim(number_format($project_lifetime->min_invest,10, ',',' '),0),',')}} {{$project_lifetime->min_invest_currency_string()}} @else {{__('project.no_info')}} @endif<br/>
                        @endforeach
                      </div>
                    </div>
                    <div class="token_content">
                      <div>{{__('project.accepting')}}</div>
                      <div>{{$project->currency}}</div>
                    </div>
                    <div class="token_content">
                      <div>{{__('project.soft_cap')}}</div>
                      <div>@if (isset($project->soft_cap)){{number_format($project->soft_cap,0, ',',' ')}} {{$project->prices->soft_cap_currency_string()}}@else {{__('project.no_info')}} @endif</div>
                    </div>
                    <div class="token_content">
                      <div>{{__('project.hard_cap')}}</div>
                      <div>@if (isset($project->hard_cap)){{number_format($project->hard_cap, 0, ',',' ')}} {{$project->prices->hard_cap_currency_string()}}@else {{__('project.no_info')}} @endif</div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="detailed_about-content tab-pane fade " id="other" role="tabpanel" aria-labelledby="other-tab">
                <div class="about_content-token">
                  <div class="col">
                    @foreach($project->blocks as $block)
                      <div class="token_content">
                        <div>{{$block->name()}}</div>
                        <div>{{$block->content()}}</div>
                      </div>
                    @endforeach
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@if($project->is_adv != 1 && count($more_projects)>0)
<div class="section more_project">
  <div class="container">
    <div class="row">
      <div class="col-12 col-lg-3">
        <h2 class="section__title">{{__('project.more_projects')}}</h2>
      </div>
      <div class="col-12 col-lg-9">
        <div class="projects__table">
          @php $projects=$more_projects @endphp
          @include('projects_ajax')
        </div>
      </div>
    </div>
  </div>
</div>

@endif



  <script>
    var price_select=$('#price-select');
    price_select.change(function(){
        document.getElementById('price-value').innerHTML =$('#price-select :selected').val();
      });

    price_select.change();
  @if($current_lifetime['date_string']!='')
    $(function () {
    var date=new Date(Date.UTC({{$current_lifetime['date_string']}}));
    var gmt=0;
    @if ($current_lifetime['gmt'])
    var gmt=-1*({{$current_lifetime['gmt']}});
    @endif
    date.setHours(date.getHours()+gmt);
      $('#countdown').mbComingsoon({ expiryDate: date, speed:100, localization: { days: "{{__('project.days')}}", hours: "{{__('project.hours')}}", minutes: "{{__('project.minutes')}}", seconds: "{{__('project.seconds')}}" } });
    });
  @endif
  </script>
@endsection
