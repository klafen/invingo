<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="/public/img/favicon.ico">
  <title>Wattson</title>
  <link rel="stylesheet" href="{{ asset('invi/css/bootstrap.css') }}?{{ time() }}">
  <link rel="stylesheet" href="{{ asset('invi/css/export.css') }}?{{ time() }}" type="text/css" />
  <link rel="stylesheet" href="{{ asset('invi/css/bootstrap-extend.css') }}?{{ time() }}">
  <link rel="stylesheet" href="{{ asset('invi/css/admin_style.css') }}?{{ time() }}">


	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body class="hold-transition login-page">

<div class="login-box">
  <div class="login-logo">
    <a href="/" >
      <img src="{{ asset('invi/img/logo.svg') }}" alt="">
    </a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">

    <form method="POST" action="{{ route('register') }}" class="form-element">
      @csrf
      <div class="form-group has-feedback">
        <input id="email" type="email" class="form-control{{ isset($errors) ? ' is-invalid' : '' }}" value="{{ old('email') }}" name="email" placeholder="E-mail">
        <span class="fa fa-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Пароль">
        <span class="fa fa-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input id="password-confirm" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password_confirmation" placeholder="Подтвердите пароль">
        <span class="fa fa-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input id="secret" type="text" class="form-control{{ $errors->has('secret') ? ' is-invalid' : '' }}" name="secret" placeholder="Ключ">
        <span class="fa fa-key form-control-feedback"></span>
      </div>
      <div class="row">
        <!-- /.col -->
        <div class="col-12 text-center">
          <button type="submit" class="btn btn-info btn-block margin-top-10">Зарегистрировать</button>
        </div>
        <!-- /.col -->
      </div>
    </form>


  </div>
</div>


</body>
</html>
