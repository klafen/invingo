<div class="card_item">
  @if($team != NULL)
    <div class="card_item-image">
      <img src="{{$team->avatar}}" class="img-fluid" alt="">
    </div>
  @endif
  <div class="card_item-title"><a href="{{route('search', ['s'=>$team->name(),'mode'=>'team'])}}">{{$team->name()}} ({{$team->count_works()}})</a></div>
  <div class="card_item-post">{{$team->profession_name()}}</div>
</div>