@extends('layouts.app')
@section('title',__('wallets.crypto_wallets')." - ")
@section('content')



<div class="section crypto__projects">
  <div class="container">
    <h1 class="page__title">{{__('wallets.crypto_wallets')}}</h1>
    <div class="row">
      <div class="col-12 col-md-4 col-lg-3 p-2">
        <div class="filter__col">
          <div class="filter__header">
            <div>
              <h3>{{__('wallets.filter')}}</h3>
            </div>
            <div>
              <span class="filter__icon">
                <span class="svg-icon">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <use xlink:href="assets/img/sprite.svg#filter-icon"></use>
                  </svg>
                </span>
              </span>
              <a href="javascript:void(0)" class="filter__clear" id="clearfilter">
                <span>{{__('wallets.clear')}}</span>
                <span class="svg-icon">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                    <use xlink:href="assets/img/sprite.svg#refresh"></use>
                  </svg>
                </span>
              </a>
            </div>
          </div>
          <div class="filter__body d-none d-md-block">
            <form action="" method="get" name="filter_form">

              @include('project_filters.index',['filters'=>$filters])

              <div class="d-block" id="btn_filter">
                <button type="submit" class="btn btn-submit" id="apply_filter">{{__('wallets.apply_filter')}}</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="col">
        <div class="crypto__table" id="wallets__table">
          @include('wallets_ajax')
        </div>
        @if ($page<(floor($count / $pagination) + ($count % $pagination > 0 ? 1 : 0)))
          <div class="crypto__pagination text-center">
            <a href="javascript:void(0)" class="btn" id="more">
              {{__('wallets.load_more')}}
              <span class="eye-icon">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                  <use xlink:href="{{asset('assets/img/sprite.svg#arrow-down')}}"></use>
                </svg>
              </span>
            </a>
          </div>
        @endif
      </div>
    </div>
  </div>
</div>

<script>
  /* slider range init */
  /* jQueryUI */
  if ( $('.custom-range').length || $('#range-slider').length ) {
    $("#range-slider").slider({
      range: true,
      min: 0,
      max: 100,
      values: [@if(isset($_GET['range-start']) ){{$_GET['range-start']}}@else 0 @endif, @if(isset($_GET['range-end']) ){{$_GET['range-end']}}@else 100 @endif],
      slide: function (event, ui) {
        $("#range-start").val(ui.values[0]);
        $("#range-end").val(ui.values[1]);
      }
    });
    $("#range-start").val($("#range-slider").slider("values", 0));
    $("#range-end").val($("#range-slider").slider("values", 1));
  }

  $('#clearos').click(function() {
    $('#os').prop('selectedIndex', 0);
  });

  $('#clearlanguage').click(function() {
    $('#language').prop('selectedIndex', 0);
  });

  $('#clearprivate').click(function() {
    $('#private_key_control').prop('selectedIndex', 0);
  });

  $('#clearvalidation').click(function() {
    $('#validation').prop('selectedIndex', 0);
  });

  $('#clearanonymity').click(function() {
    $('#anonymity').prop('selectedIndex', 0);
  });

  $('#cleartype').click(function() {
    $('#wallettype').prop('selectedIndex', 0);
  });

  $('#clearextra').click(function() {
    $('#extra').prop('selectedIndex', 0);
  });

  $('#clearrating').click(function() {
    $("#range-slider").slider({
      range: true,
      min: 0,
      max: 100,
      values: [@if(isset($_GET['range-start']) ){{$_GET['range-start']}}@else 0 @endif, @if(isset($_GET['range-end']) ){{$_GET['range-end']}}@else 100 @endif],
      slide: function (event, ui) {
        $("#range-start").val(ui.values[0]);
        $("#range-end").val(ui.values[1]);
      }
    });
    $("#range-start").val($("#range-slider").slider("values", 0));
    $("#range-end").val($("#range-slider").slider("values", 1));
  });

  $('#clearfilter').click(function() {
    $('#clearos').click();
    $('#clearlanguage').click();
    $('#clearprivate').click();
    $('#clearvalidation').click();
    $('#clearrating').click();
    $('#cleartype').click();
    $('#clearanonymity').click();
    $('#hardware').prop('checked', false);
    $('#multysignature').prop('checked', false);
    $('#2FA').prop('checked', false);
    $('#reserve_copy').prop('checked', false);
    $('#seed_phrase').prop('checked', false);
    $('#smart_contract').prop('checked', false);
    $('#open_source').prop('checked', false);
    $('#exchange').prop('checked', false);
    $('#qrcode').prop('checked', false);
    $('#touch').prop('checked', false);
    $('#fiat').prop('checked', false);
    $('#clearextra').click();
    $('#wallet_country').prop('selectedIndex', 0);
  });

  var page = 1;
  var pageCount = {{ floor($count / $pagination) + ($count % $pagination > 0 ? 1 : 0) }};
  var more = $('#more');
  more.click(function() {
    more.prop('disabled', true);
    $('#wallets-notice').remove();
    ++page;
    if (page <= pageCount) {
      var separator = $('<h5><span>{{__('pagination.page')}} ' + page + ' {{__('pagination.of')}} ' + pageCount + '</span></h5>');
      var list = $('#wallets__table');
      list.append(separator).append('<p id="wallets-notice" style="text-align:center;">{{__('pagination.loading')}}...</p>');
      $.ajax({
        cache: false,
        data: $('form[name="filter_form"]').serialize() + '&page=' + page,
        dataType: 'html',
        error: function () {
          $('#wallets-notice').remove();
          list.find('h5').last().remove();
          list.append('<p id="wallets-notice" style="margin-top:20px;text-align:center;">{{__('pagination.loading_failed')}}</p>');
          --page;
          more.prop('disabled', false);
        },
        success: function (data) {
          $('#wallets-notice').remove();
          list.append(data);
          if (page == pageCount) more.parent().hide();
          more.prop('disabled', false);
        },
        type: 'GET',
        url: $(location).attr('href')
      });
    }
    else more.parent().hide();
  });


</script>
<script type='text/javascript' src="{{asset('/assets/js/btn_filter.js')}}"></script>
@endsection
