@extends('layouts.app')
@section('content')

<div class="section homeslider">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="home__slider" id="home-slider">
          @forelse($sliders as $slide)
          <div class="slider-item">
            <div class="row no-gutters">
              <div class="col-12 col-md-7 col-left">
                <h1>
                  {{$slide['project']->name}}
                  <span class="slider__title-logo">
                  <img src="{{$slide['project']->logo}}" alt="img">
                  </span>
                </h1>
                <div class="slider__subtitle">
                    {{$slide['project']->category}}
                </div>
                <div class="slider__decription">
                    {{$slide['project']->slider}}
                </div>
                <div class="slider__more-btn">
                  <a href="{{route('ico-project',$slide['project']->id)}}" class="btn">
                    {{__('home.view_project')}}
                    <span class="eye-icon">
                      <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <use xlink:href="assets/img/sprite.svg#eye"></use>
                      </svg>
                    </span>
                  </a>
                </div>
              </div>
              <div class="d-none d-md-block col-5 col-right text-right">
                <div class="slider__price">
                  {{$slide['project']->prices->collected_currency_string()}} {{number_format($slide['project']->collected, 0,',' ,' ')}}
                </div>
                <div class="slider__token">
                  {!!$slide['sale_ended']!!}
                </div>
                <div class="slider__total">
                  {{$slide['project']->prices->hard_cap_currency_string()}} {{number_format($slide['project']->hard_cap, 0,',' ,' ')}}
                </div>
              </div>
            </div>
          </div>
          @empty
          @endforelse
        </div>
      </div>
    </div>
  </div>
</div>

<div class="section projects">
  <div class="container">
    <div class="row">
      <div class="col-12 col-lg-3 px-1 px-lg-3">
        <h2 class="section__title">
          {{__('home.ico_projects')}}
        </h2>
      </div>

      <div class="col-12 col-md-4 col-lg-3 px-1 px-xl-3">
        <a style="text-decoration: none" href="{{route('ico-projects','substatuss%5B3%5D=on')}}">
        <div class="projects__card bg-active">
          <div class="card__title">{{__('home.active_ico')}}</div>
          <div class="card__count">{{$count_active}}</div>
          <div class="card__view">{{__('home.view_active_ICO')}}</div>
        </div>
        </a>
      </div>

      <div class="col-12 col-md-4 col-lg-3 px-1 px-xl-3">
        <a style="text-decoration: none" href="{{route('ico-projects','substatuss%5B4%5D=on')}}">
        <div class="projects__card bg-upcomig">
          <div class="card__title">{{__('home.upcoming_ico')}}</div>
          <div class="card__count">{{$count_upcoming}}</div>
          <div class="card__view">{{__('home.view_upcoming_ICO')}}</div>
        </div>
        </a>
      </div>
      <div class="col-12 col-md-4 col-lg-3 px-1 px-xl-3">
        <a style="text-decoration: none" href="{{route('ico-projects','substatuss%5B6%5D=on')}}">
        <div class="projects__card bg-ended">
          <div class="card__title">{{__('home.ended_ICO')}}</div>
          <div class="card__count">{{$count_ended}}</div>
          <div class="card__view">{{__('home.view_ended_ICO')}}</div>
        </div>
          </a>
      </div>
    </div>
  </div>
</div>

<div class="section rating">
  <div class="container">
    <div class="row">
      <div class="col-12 col-lg-3">
        <h2 class="section__title">
          {{__('home.ICO_rating_map')}}
        </h2>
        <a href="{{route('ico-ratings')}}" class="btn rating__button">
          {{__('home.view_full_list')}}
          <span class="eye-icon">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
              <use xlink:href="{{asset('assets/img/sprite.svg#eye')}}"></use>
            </svg>
          </span>
        </a>
      </div>
      <div class="col-12 col-lg-9">
        <div class="table-responsive rating-home" data-simplebar="init">
          <table class="rating-table">
            <thead>
              <tr>
                <th class="d-flex justify-content-between">
                  <span>{{__('home.ico_projects')}}</span>
                </th>
                <th>
                  <span>ICO Gravity</span>
                </th>
                @foreach($agencies as $agency)
                <th><span>{{$agency->name}}</span></th>
                @endforeach
              </tr>
            </thead>
            <tbody>
              @foreach($projects as $project)
              <tr>
                <td class=""> <!--sticky-col-->
                  <a href="{{route('ico-project',$project['id'])}}">
                    <span class="project__icon">
                      <img src="{{$project['logo']}}" alt="logo">
                    </span>
                    {{$project['name']}}
                  </a>
                </td>
                <td>{{$project['rating']}}</td>
                @foreach($agencies as $agency)
                <td>@if(isset($project['agency'][$agency->id])) {{$project['agency'][$agency->id]}} @else  @endif</td>
                @endforeach
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>



@endsection
