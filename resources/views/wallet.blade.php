@extends('layouts.app')
@section('title',__('wallet.wallet').' '.$wallet->name." - ")
@section('content')

  <style> p iframe {max-width: 100%;height: auto;min-height:440px;} </style>

<div class="section detailed crypto">
  <div class="container--bg">
    <div class="container">
      <div class="row pt-5">
        <div class="col-12 col-md-8">
          <div class="detailed__head-left">
            <div class="detailed__logo-wrap">
              <img src="{{$wallet->logo}}" alt="logo" class="figure-img" style="max-width: 80px">
            </div>
            <div class="detailed__title">
              <h1>{{$wallet->name}}</h1>
              <span>{{$wallet->wallet_type}}</span>
            </div>
            @if($wallet->is_sponsored == 1)
              <div class="detailed__title">
                  <span class="sponsor" style="width:30px;height:30px;" data-toggle="tooltip" data-placement="bottom" title="{{__('wallet.sponsored')}}">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                          <use xlink:href="{{asset('assets/img/sprite.svg#hand-shake')}}"></use>
                        </svg>
                      </span>
              </div>
            @endif
          </div>
        </div>
        <div class="col">
          <div class="detailed__head-right">
            <div class="row_rating-title">
              {{__('wallet.rating')}}
              <a href="{{route('invilink','Cryptowallet%20Metodology')}}">
                <span class="help_info" tabindex="0" role="button" data-toggle="popover" data-content="Lorem ipsum dolor sit amet. Suscipit laboriosam, nisi ut enim.">
                  <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><use xlink:href="{{asset('assets/img/sprite.svg#help-info')}}"></use></svg>
                </span>
              </a>
            </div>
            <div class="number_total">
              {{$wallet->rating}}<span>/100</span>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12 col-md-8">
          <div class="detailed_content">
            {!! $wallet->info !!}
          </div>
        </div>
        <div class="col">
          <div class="detailed_sidebar">
            @if($wallet->is_adapt)
            <div class="sidebar_time-header">
              <span class="sponsor">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                  <use xlink:href="{{asset('assets/img/sprite.svg#hand-like')}}"></use>
                </svg>
              </span>
              <span>{{__('wallet.good_for_ico')}}</span>
            </div>
            <hr>
            @endif
            <div class="d-table">
              <div>{{__('wallet.supported_OS')}}</div>
            </div>
            <div class="suported_os">
              @forelse(explode(',',$wallet->os) as $os)
              <span>{{$os}}</span>
              @empty
              @endforelse
            </div>
            <hr>
              <div class="d-table last-white">
                <div>{{__('wallet.private_key_control')}}</div>
                <div>@if($wallet->private_key_control_id == 1) {{__('wallet.available')}} @elseif($wallet->private_key_control_id == 2) {{__('wallet.unavailable')}} @else {{$wallet->private_key_control_name}} @endif</div>
              </div>
              <div class="d-table last-white">
                <div>{{__('wallet.anonymity')}}</div>
                <div>{{$wallet->anonymity_name}}</div>
              </div>
              @if($wallet->validation_name)
              <div class="d-table last-white">
                <div>{{__('wallet.validation')}}</div>
                <div>{{$wallet->validation_name}}</div>
              </div>
              @endif

            <div class="d-table last-white">
              <div>{{__('wallets.smart_contract')}}</div>
              <div>@if ($wallet->is_smart_contract){{__('wallet.yes')}} @else {{__('wallet.no')}} @endif</div>
            </div>

            <div class="d-table last-white">
              <div>{{__('wallet.multysignature')}}</div>
              <div> @if ($wallet->is_multysignature){{__('wallet.available')}} @else {{__('wallet.unavailable')}} @endif</div>
            </div>

            <div class="d-table last-white">
              <div>{{__('wallet.seed_phrase')}}</div>
              <div>@if ($wallet->is_seed_phrase){{__('wallet.yes')}} @else {{__('wallet.no')}} @endif</div>
            </div>
            <div class="d-table last-white">
              <div>{{__('wallet.QR_Code')}}</div>
              <div>@if ($wallet->is_qrcode){{__('wallet.yes')}} @else {{__('wallet.no')}} @endif</div>
            </div>
            <div class="d-table last-white">
              <div>{{__('wallet.2FA')}}</div>
              <div>@if ($wallet->is_2FA){{__('wallet.yes')}} @else {{__('wallet.no')}} @endif</div>
            </div>
            <div class="d-table last-white">
              <div>{{__('wallet.reserve_copy')}}</div>
              <div>@if ($wallet->is_reserve_copy){{__('wallet.available')}} @else {{__('wallet.unavailable')}} @endif</div>
            </div>
            <div class="d-table last-white">
              <div>{{__('wallet.fiat_currency')}}</div>
              <div>@if ($wallet->is_fiat){{__('wallet.available')}} @else {{__('wallet.unavailable')}} @endif</div>
            </div>
            <div class="d-table last-white">
              <div>{{__('wallet.exchange')}}</div>
              <div>@if ($wallet->is_exchange){{__('wallet.yes')}} @else {{__('wallet.no')}} @endif</div>
            </div>
            <div class="d-table last-white">
              <div>{{__('wallet.open_source')}}</div>
              <div>@if ($wallet->is_open_source){{__('wallet.yes')}} @else {{__('wallet.no')}} @endif</div>
            </div>
            @if (strlen($wallet->extra)>0)
            <div class="d-table last-white">
              <div>{{__('wallet.extra')}}</div>
              <div>@forelse(explode(',',$wallet->extra) as $extra)
                  <span>{{$extra}}</span>
                @empty
                @endforelse
              </div>
            </div>
              @endif
            <div class="d-table last-white">
              <div>{{__('wallet.touch_ID')}}</div>
              <div>@if ($wallet->is_touch_id){{__('wallet.yes')}} @else {{__('wallet.no')}} @endif</div>
            </div>
            <div class="d-table last-white">
              <div>{{__('wallet.languages')}}</div>
              <div>
                @forelse(explode(',',$wallet->language) as $language)
                  {{$language}}
                @empty
                @endforelse
              </div>
            </div>
            <div class="d-table last-white">
              <div>{{__('wallet.hardware')}}</div>
              <div>@if ($wallet->is_hardware){{__('wallet.yes')}} @else {{__('wallet.no')}} @endif</div>
            </div>
            <div class="d-table last-white">
              <div>{{__('wallet.country')}}</div>
              <div>{{$wallet->country}}</div>
            </div>
            @foreach ($wallet->filter_value_sets as $value_set)
              @if ($value_set->filter_value->filter->is_visible)
                <div class="d-table last-white">
                  <div>{{$value_set->filter_value->filter->name()}}</div>
                  <div>{{$value_set->filter_value->name()}}</div>
                </div>
              @endif
            @endforeach
            <hr>
            <div class="d-table">
              <div>{{__('wallet.supported_cryptomoney')}}</div>
            </div>
            <div class="support_money">
              {{str_replace(",",", ",$wallet->currency)}}
            </div>
            <hr>
            @if(count($wallet_socials)>0)
              <div class="d-table">
                <div>{{__('wallet.social_media')}}</div>
              </div>
              <div class="link_social">
                <ul>
                  @forelse($wallet_socials as $wallet_social)
                    <li><a href="{{$wallet_social->link}}" target="_blank">{!! $wallet_social->icon !!}</a></li>
                  @empty
                  @endforelse
                </ul>
              </div>
            <hr>
            @endif
            @if(count($wallet_links)>0)
              <div class="d-table">
                <div>{{__('wallet.links')}}</div>
              </div>
              <div class="link_links">
                @forelse($wallet_links as $wallet_link)
                  <a href="{{$wallet_link->link}}" target="_blank">{!! $wallet_link->name !!}</a>
                @empty
                @endforelse
              </div>
            @endif
          </div>
        </div>
      </div>
      <div class="row pt-5">
        <div class="col-12 col-lg-8">
          <div class="detailed_about">
            <div class="detailed_about-title">
              <ul class="d-flex justify-content-around nav" role="tablist">
                <li>
                  <a class="active" id="team-tab" data-toggle="tab" href="#team" role="tab" aria-controls="team" aria-selected="true">{{__('wallet.team')}}</a>
                </li>
                <li>
                  <a id="map-tab" data-toggle="tab" href="#map" role="tab" aria-controls="map" aria-selected="false">{{__('wallet.road_map')}}</a>
                </li>
                <li>
                  <a id="juris-tab" data-toggle="tab" href="#juris" role="tab" aria-controls="juris" aria-selected="false">{{__('wallet.jurisdiction')}}</a>
                </li>
                <li>
                  <a id="other-tab" data-toggle="tab"  href="#other" role="tab" aria-controls="other"  aria-selected="false">{{__('wallet.other')}}</a>
                </li>
              </ul>
            </div>
            <div class="tab-content">
              <div class="detailed_about-content about_content-team tab-pane fade show active" id="team" role="tabpanel" aria-labelledby="team-tab">
                @include('teamlist',['teams'=>$wallet->teams])
              </div>
              <div class="detailed_about-content about_content-maps tab-pane fade" id="map" role="tabpanel" aria-labelledby="map-tab">
                <div id="maps">@if($wallet->road_map)<img src="{{$wallet->road_map}}" alt="{{__('wallet.road_map')}} {{$wallet->name}}">@endif</div>
              </div>
              <div class="detailed_about-content about_content-token tab-pane fade" id="juris" role="tabpanel" aria-labelledby="juris-tab">
                <div class="row">
                  <div class="col">
                    @foreach($wallet->blocks as $block)
                      <div class="token_content">
                        <div>{{$block->name()}}</div>
                        <div>{{$block->content()}}</div>
                      </div>
                    @endforeach
                  </div>
                </div>
              </div>
              <div class="detailed_about-content tab-pane fade " id="other" role="tabpanel" aria-labelledby="other-tab">
                {!! $wallet->other !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


  @if(!($wallet->is_sponsored) and count($wallets)>0 )
<div class="section more_project">
  <div class="container">
    <div class="row">
      <div class="col-12 col-lg-3">
        <h2 class="section__title">{{__('wallet.more_wallets')}}</h2>
      </div>
      <div class="col-12 col-lg-9">
        <div class="crypto__table">
          @include('wallets_ajax')

        </div>
      </div>
    </div>
  </div>
</div>
  @endif

@endsection
