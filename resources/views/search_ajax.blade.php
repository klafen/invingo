@if(count($projects)>0)
  <div class="search_ajax_title">{{__('search.ico_projects')}}</div>
  @foreach($projects as $project)
    <div class="search_ajax_item"><a href="{{route('ico-project',$project['project']->id)}}"><img src="{{$project['project']->logo}}" alt="{{$project['project']->name}}"> {{$project['project']->name}} ({{$project['project']->token}})</a></div>
  @endforeach
@endif
@if(count($wallets)>0)
  <div class="search_ajax_title">{{__('search.wallets')}}</div>
  @foreach($wallets as $wallet)
    <div class="search_ajax_item"><a href="{{route('crypto-wallet',$wallet->id)}}"><img src="{{$wallet->logo}}" alt="{{$wallet->name}}"> {{$wallet->name}}</a></div>
  @endforeach
@endif

