<?php

return [
    'links' => 'Ссылки',
    'disclamer' => 'Дисклеймер',
    'read_more' => 'Далее',
    'contact_email' => 'Для получения информации пишите на email',
    'search' => 'Поиск',
    'tooltip_search' => 'Нажмите Enter для поиска или ESC для отмены',
    'follow_us'=>'Наши соцсети'
];

?>