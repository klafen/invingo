<?php

return [
    'ico_projects' => 'ICO проекты',
    'view_project' => 'Смотреть',
    'active_ico' => 'Активные ICO',
    'view_active_ICO' => 'Смотреть активные ICO',
    'upcoming_ico' => 'Предстоящие ICO',
    'view_upcoming_ICO' => 'Смотреть предстоящие ICO',
    'ended_ICO' => 'Завершенные ICO',
    'view_ended_ICO' => 'Смотреть завершенные ICO',
    'view_full_list' => 'Полный список',
    'ICO_rating_map' => 'Регистр рейтингов ICO',
    'read_more' => 'Подробнее',
    'metodology' => 'Методология',
];


?>
