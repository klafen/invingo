<?php

return [
    'crypto_wallets' => 'Криптокошельки',
    'filter' => 'Фильтр',
    'clear' => 'Сброс',
    'all' => 'Все',
    'platform' => 'Платформа',
    'type' => 'Тип',
    'rating' => 'Рейтинг',
    'hardware' => 'Аппаратное хранение',
    'universality' => 'Universality',
    'anonimity' => 'Анонимность',
    'language' => 'Язык',
    'security' => 'Безопасность',
    'multysignature' => 'Мультиподпись',
    'any' => 'Любой',
    'reserve_copy' => 'Резервное копирование',
    'extra' => 'Экстра',
    'apply_filter' => 'Найти',
    'load_more' => 'Показать еще',
    'sponsored' => 'Партнер',
    'private_key_control' => 'Контроль приватного ключа',
    'supported_coins' => 'Поддерживаемые монеты',
    'available' => 'Доступно',
    'unavailable' => 'Недоступно',
    '2FA' => 'Двухфакторная аутентификация',
    'seed_phrase' => 'Seed-фраза',
    'smart_contract' => 'Smart сontract поддрежка',
    'open_source' => 'Открытый исходный код',
    'exchange' => 'Криптообменник',
    'not_select' => 'Нет',
    'validation' => 'Валидация',
    'anonymity' => 'Анонимность',
    'QR_Code' => 'QR-коды',
    'touch_ID' => 'Поддержка Touch ID',
    'fiat_currency' => 'Поддержка фиата',
    'country' => 'Страна',
];



?>
