<?php

return [
    'ico_projects' => 'ICO Projects',
    'view_project' => 'View project',
    'active_ico' => 'Active ICO',
    'view_active_ICO' => 'View Active ICO',
    'upcoming_ico' => 'Upcoming ICO',
    'view_upcoming_ICO' => 'View Upcoming ICO',
    'ended_ICO' => 'Ended ICO',
    'view_ended_ICO' => 'View Ended ICO',
    'view_full_list' => 'View full list',
    'ICO_rating_map' => 'Rating register of ICO',
    'metodology' => 'Metodology',
];



?>