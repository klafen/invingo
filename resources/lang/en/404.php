<?php

  return [
    'page_not_found' => 'Page Not Found',
    'h1' => 'Error 404: Page Not Found',
    'content' => 'The page you requested is not found, go back <a href="/">to the main page</a> and try again.',
  ];



?>