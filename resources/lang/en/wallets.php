<?php

return [
    'crypto_wallets' => 'Cryptocurrency wallets',
    'filter' => 'Filter',
    'clear' => 'Clear',
    'all' => 'All',
    'platform' => 'Platform',
    'type' => 'Type',
    'rating' => 'Rating',
    'hardware' => 'Hardware',
    'universality' => 'Universality',
    'anonimity' => 'Anonimity',
    'language' => 'Language',
    'security' => 'Security',
    'multysignature' => 'Multisignature',
    'any' => 'Any',
    'reserve_copy' => 'Reserve Copy',
    'extra' => 'Extra',
    'apply_filter' => 'Apply',
    'load_more' => 'Load more',
    'sponsored' => 'Sponsored',
    'private_key_control' => 'Private key control',
    'supported_coins' => 'Supported Coins',
    'available' => 'Available',
    'unavailable' => 'Unavailable',
    '2FA' => '2FA',
    'seed_phrase' => 'Seed phrase',
    'smart_contract' => 'Smart contracts support',
    'open_source' => 'Open Source',
    'exchange' => 'Exchange',
    'not_select' => 'No',
    'validation' => 'Validation',
    'anonymity' => 'Anonymity',
    'QR_Code' => 'QR-Code',
    'touch_ID' => 'Touch ID',
    'fiat_currency' => 'Fiat',
    'country' => 'Country',
];


?>
