<?php

return [
    'links' => 'Links',
    'disclamer' => 'Disclaimer',
    'read_more' => 'Read more',
    'contact_email' => 'For more information e-mail us on',
    'search' => 'Search',
    'tooltip_search' => 'Hit enter to search or ESC to close',
    'follow_us'=>'Follow us'
];



?>