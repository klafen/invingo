<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['locale']], function(){
  Route::get('/', 'HomeController@index')->name('home');
  Route::get('/advertise', 'AdvertController@index')->name('advertise');
  Route::get('/metodology', 'MetodologyController@show')->name('metodology');
  Route::get('/ico-ratings', 'RatingsController@index')->name('ico-ratings');
  Route::get('/search', 'SearchController@show')->name('search');
  Route::get('/search_ajax', 'SearchController@search_ajax')->name('search_ajax');
  Route::get('/crypto-wallets', 'WalletsController@index')->name('crypto-wallets');
  Route::get('/crypto-wallet/{id}', 'WalletsController@show')->name('crypto-wallet');
  Route::get('/ico-projects', 'ProjectsController@index')->name('ico-projects');
  Route::get('/ico-project/{id}', 'ProjectsController@show')->name('ico-project');
});

Route::group(['prefix' => 'invi', 'middleware' => ['auth']], function(){
  Route::get('/dashboard', 'invi\DashboardController@index')->name('invi_dashboard');
  Route::get('/filewizard', 'invi\FilewizardController@index')->name('filewizard');
  Route::resource('/locales', 'invi\LocalesController');
  Route::resource('/cryptocoins', 'invi\CryptocoinsController');
  Route::resource('/countries', 'invi\CountriesController');
  Route::resource('/substatus', 'invi\SubstatusController');
  Route::resource('/language', 'invi\LanguageController');
  Route::resource('/extra', 'invi\ExtraController');
  Route::resource('/status', 'invi\StatusController');
  Route::resource('/projects', 'invi\ProjectsController');
  Route::resource('/advert', 'invi\AdvContractsController');
  Route::resource('/categories', 'invi\CategoriesController');
  Route::resource('/platforms', 'invi\PlatformsController');
  Route::resource('/socials', 'invi\SocialsController');
  Route::resource('/invisocials', 'invi\InviSocialsController');
  Route::resource('/contentsettings', 'invi\ContentSettingsController');
  Route::resource('/invilinks', 'invi\InviLinksController');
  Route::resource('/os', 'invi\OSController');
  Route::resource('/agencies-rating', 'invi\AgenciesRatingController');
  Route::resource('/custom-rating', 'invi\CustomRatingController');
  Route::resource('/professions', 'invi\ProfessionsController');
  Route::resource('/advrequest', 'invi\AdvRequestController');
  Route::resource('/wallettype', 'invi\WalletTypeController');
  Route::resource('/wallets', 'invi\WalletsController');
  Route::resource('/type_tokens', 'invi\TypeTokenController');
  Route::resource('/tradings', 'invi\TradingController');
  Route::resource('/settings', 'invi\InviSetController');
  Route::resource('/ajax', 'invi\InviAjaxController');
  Route::resource('/project-filter', 'invi\ProjectFilterController');
  Route::resource('/wallet-filter', 'invi\WalletFilterController');
  Route::get('/advrequest-archive', 'invi\AdvRequestController@archive')->name('advrequest-archive');

});

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});


Route::get('setlocale/{locale}','LocaleController@locale')->name('setlocale');
Route::post('/ajax/savecontracts','AjaxController@saveContracts');

Route::get('invilogin', 'Auth\LoginController@showLoginForm')->name('login');
Route::get('invireg', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('invilogin', 'Auth\LoginController@login');
Route::post('invireg', 'Auth\RegisterController@register');
Route::post('invilogout', 'Auth\LoginController@logout')->name('logout');

Route::group(['middleware' => ['locale']], function(){
  Route::get('/{url}', 'InviLinksController@show')->name('invilink');
});


